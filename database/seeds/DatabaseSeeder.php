<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->users();
        $this->categorias();
        $this->subcategorias();
        $this->marcas();
        $this->color();
        $this->talles();
        $this->secciones();

        $categorias = DB::table('categorias')->get(['id_categorias'])->toArray();
        $categorias = array_column($categorias, 'id_categorias');
        $subcategorias = DB::table('subcategorias')->get(['id_subcategorias'])->toArray();
        $subcategorias = array_column($subcategorias, 'id_subcategorias');
        $tipos = [
            'Dama',
            'Hombre'
        ];

        for ($i = 1; $i <= 500; $i++) {
            $code = $i < 10 ? '0' . $i : $i;
            DB::table('productos')->insert([
                'codigo_productos' => '#Cod-' . $code,
                'nombre_productos' => "producto " . $code,
                'descripcion_productos' => "descripcion de producto " . $code,
                'tipo_producto' => $tipos[random_int(0, (count($tipos) - 1))],
                'pk_categorias_productos' => $categorias[random_int(0, (count($categorias) - 1))],
                'pk_subcategorias_productos' => $subcategorias[random_int(0, (count($subcategorias) - 1))],
                'pk_marcas_productos' => 1,
                'lanzamientos_productos' => false,
                'activo_productos' => true,
                'descuentos_productos' => false,
                'premium' => false,
                'precio_descuento_productos' => random_int(20000, 30000),
                'mayorista' => random_int(30000, 50000),
                'minorista' => random_int(50000, 55000),
                'created_productos' => 1
            ]);
        }
        $color_productos = DB::table('color_productos')->get();
        foreach ($color_productos as $color_producto) {
            $talles = DB::table('talles_productos')->where('pk_productos_talles_productos', $color_producto->pk_productos_color_productos)->get(['pk_talles_productos'])->toArray();
            $array_tallas = array_column($talles, 'pk_talles_productos');
            $talles_string = json_encode(array_map('strval', $array_tallas));
            DB::table('color_productos')->where('pk_productos_color_productos', $color_producto->pk_productos_color_productos)->update([
                'tamanos' => $talles_string
            ]);
        }

        $files = scandir(public_path('img/productos'));
        $files = array_slice($files, 2);
        $productosCount = DB::table('productos')->get()->count();
        $colores = DB::table('color')->get('id_color')->toArray();
        $colores = array_column($colores, 'id_color');
        for ($i = 0; $i < $productosCount; $i++) {
            DB::table('imagen')->insert([
                'path_imagen' => $files[random_int(0, (count($files) - 1))],
                'created_imagen' => 1
            ]);
            DB::table('imagen_productos')->insert([
                'principal_imagen_productos' => true,
                'pk_productos_imagen_productos' => $i + 1,
                'pk_imagen_productos' => $i + 1,
                'created_imagen_productos' => 1
            ]);
            DB::table('color_imagen')->insert([
                'pk_imagen_color_imagen' => $i + 1,
                'pk_productos_color_imagen' => $i + 1,
                'principal_color_imagen' => true,
                'seccion_color_imagen' => 1,
                'pk_color_color_imagen' => 1,
                'created_color_imagen' => 1
            ]);
            DB::table('color_imagen')->insert([
                'pk_imagen_color_imagen' => $i + 1,
                'pk_productos_color_imagen' => $i + 1,
                'principal_color_imagen' => true,
                'seccion_color_imagen' => 2,
                'pk_color_color_imagen' => 2,
                'created_color_imagen' => 1
            ]);
            DB::table('color_imagen')->insert([
                'pk_imagen_color_imagen' => $i + 1,
                'pk_productos_color_imagen' => $i + 1,
                'principal_color_imagen' => true,
                'seccion_color_imagen' => 3,
                'pk_color_color_imagen' => 3,
                'created_color_imagen' => 1
            ]);
        }
    }

    public function users()
    {
        DB::table('users')->insert([
            'name' => "Samuel Kim",
            'email' => "drummerkim11@gmail.com",
            'password' => bcrypt('12345678'),
            'razon_users' => "Soluciones Inteligentes",
            'ruc_users' => '3596186-4',
            'direccion_users' => "Pizarro 912 casi Zuñiga",
            'telefono_users' => '0984588111',
            'perfil_users' => 'cliente',
            'estado_users' => 'activo',
        ]);
    }

    public function categorias()
    {
        $categorias = [
            'Tela',
            'Jeans',
            'Lycra',
            'Olimpia',
            'Sarga',
            'Gabardina',
            'Sarga rígida',
        ];
        foreach ($categorias as $categoria) {
            DB::table('categorias')->insert([
                'nombre_categorias' => $categoria,
                'created_categorias' => 1,
            ]);
        }
    }

    public function subcategorias()
    {

        $subcategorias = [
            [
                'nombre' => 'Campera',
                'cat' => 2
            ],
            [
                'nombre' => 'Falda',
                'cat' => 2
            ],
            [
                'nombre' => 'Skinny',
                'cat' => 2
            ],
            [
                'nombre' => 'Slim',
                'cat' => 2
            ],
            [
                'nombre' => 'Jogger',
                'cat' => 2
            ],
            [
                'nombre' => 'Oxford',
                'cat' => 2
            ],
            [
                'nombre' => 'Bermuda',
                'cat' => 2
            ],
            [
                'nombre' => 'Jardinera',
                'cat' => 2
            ],
            [
                'nombre' => 'Carpintero',
                'cat' => 2
            ],
            [
                'nombre' => 'Mom',
                'cat' => 2
            ],
            [
                'nombre' => 'Straight',
                'cat' => 2
            ],
            [
                'nombre' => 'Remera',
                'cat' => 4
            ],
            [
                'nombre' => 'Mochila',
                'cat' => 4
            ],
            [
                'nombre' => 'Bolsón',
                'cat' => 4
            ],
            [
                'nombre' => 'Accesorios',
                'cat' => 4
            ],
            [
                'nombre' => 'Arts de Oficina',
                'cat' => 4
            ],
            [
                'nombre' => 'Termo',
                'cat' => 4
            ],
            [
                'nombre' => 'Hoppie',
                'cat' => 4
            ],
            [
                'nombre' => 'Taza',
                'cat' => 4
            ],
            [
                'nombre' => 'Short',
                'cat' => 2
            ],
            [
                'nombre' => 'Camisa',
                'cat' => 2
            ],
            [
                'nombre' => 'Short Pollera',
                'cat' => 2
            ],
            [
                'nombre' => 'Camisola',
                'cat' => 2
            ],
        ];
        foreach ($subcategorias as $subcategoria) {
            DB::table('subcategorias')->insert([
                'nombre_subcategorias' => $subcategoria['nombre'],
                'pk_categorias_subcategorias' => $subcategoria['cat'],
                'active_subcategorias' => 1,
                'created_subcategorias' => 1,
            ]);
        }
    }

    public function marcas()
    {
        DB::table('marcas')->insert([
            'nombre_marcas' => "Brabus",
            'created_marcas' => 1,
        ]);
        DB::table('marcas')->insert([
            'nombre_marcas' => "Equus",
            'created_marcas' => 1,
        ]);
        DB::table('marcas')->insert([
            'nombre_marcas' => "Global",
            'created_marcas' => 1,
        ]);
        DB::table('marcas')->insert([
            'nombre_marcas' => "Triton",
            'created_marcas' => 1,
        ]);
        DB::table('marcas')->insert([
            'nombre_marcas' => "Desvio",
            'created_marcas' => 1,
        ]);
        DB::table('marcas')->insert([
            'nombre_marcas' => "Bivik",
            'created_marcas' => 1,
        ]);
        DB::table('marcas')->insert([
            'nombre_marcas' => "Olimpia",
            'created_marcas' => 1,
        ]);
    }

    public function color()
    {
        DB::table('color')->insert([
            'nombre_color' => "Azul",
            'prefijo_color' => "#1755F7",
            'created_color' => 1,
        ]);

        DB::table('color')->insert([
            'nombre_color' => "Marino",
            'prefijo_color' => "#09209B",
            'created_color' => 1,
        ]);

        DB::table('color')->insert([
            'nombre_color' => "Celeste",
            'prefijo_color' => "#BCD8F5",
            'created_color' => 1,
        ]);

        DB::table('color')->insert([
            'nombre_color' => "Negro",
            'prefijo_color' => "#000",
            'created_color' => 1,
        ]);
    }

    public function talles()
    {
        DB::table('talles')->insert([
            'nombre_talles' => "2",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "4",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "6",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "8",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "10",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "12",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "14",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "16",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "36",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "38",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "40",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "42",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "44",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "46",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "48",
            'created_talles' => 1,
        ]);
        DB::table('talles')->insert([
            'nombre_talles' => "50",
            'created_talles' => 1,
        ]);
    }

    public function secciones()
    {
        DB::table('secciones')->insert([
            'nombre_secciones' => "Slider",
            'referencias_secciones' => "Pagina Principal",
            'activo_secciones' => 1,
            'created_secciones' => 1,
        ]);
        DB::table('secciones')->insert([
            'nombre_secciones' => "Presentacion",
            'referencias_secciones' => "Pagina Principal",
            'activo_secciones' => 1,
            'created_secciones' => 1,
        ]);
        DB::table('secciones')->insert([
            'nombre_secciones' => "Contenido1",
            'referencias_secciones' => "Pagina Principal",
            'activo_secciones' => 1,
            'created_secciones' => 1,
        ]);
        DB::table('secciones')->insert([
            'nombre_secciones' => "Slider2",
            'referencias_secciones' => "Pagina principal lanzamientos",
            'activo_secciones' => 1,
            'created_secciones' => 1,
        ]);
        DB::table('secciones')->insert([
            'nombre_secciones' => "Historia",
            'referencias_secciones' => "Pagina principal historias",
            'activo_secciones' => 1,
            'created_secciones' => 1,
        ]);
        DB::table('secciones')->insert([
            'nombre_secciones' => "Covid",
            'referencias_secciones' => "Pagina Principal seccion modo covid",
            'activo_secciones' => 1,
            'created_secciones' => 1,
        ]);
        DB::table('secciones')->insert([
            'nombre_secciones' => "Contactos",
            'referencias_secciones' => "Slider para contactos",
            'activo_secciones' => 1,
            'created_secciones' => 1,
        ]);
    }
}
