<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContenidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contenido', function (Blueprint $table) {
          $table->bigIncrements('id_contenido');
          $table->unsignedInteger('pk_secciones_contenido');
          $table->foreign('pk_secciones_contenido')->references('id_secciones')->on('secciones');
          $table->string('titulo_contenido');
          $table->text('texto_contenido');
          $table->unsignedInteger('created_contenido');
          $table->foreign('created_contenido')->references('id')->on('users');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contenido');
    }
}
