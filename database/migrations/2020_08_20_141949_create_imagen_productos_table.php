<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagenProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagen_productos', function (Blueprint $table) {
          $table->id('id_imagen_productos');
          $table->unsignedInteger('pk_imagen_productos');
          $table->foreign('pk_imagen_productos')->references('id_imagen')->on('imagen');
          $table->unsignedInteger('pk_productos_imagen_productos');
          $table->foreign('pk_productos_imagen_productos')->references('id_productos')->on('productos');
          $table->unsignedInteger('created_imagen_productos');
          $table->foreign('created_imagen_productos')->references('id')->on('users');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagen_productos');
    }
}
