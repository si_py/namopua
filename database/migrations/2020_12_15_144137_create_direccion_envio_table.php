 <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDireccionEnvioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direccion_envio', function (Blueprint $table) {
            $table->bigIncrements('id_direccion_envio');
            $table->string('direccion_envio');
            $table->string('ciudad_direccion_envio');
            $table->string('referencia_direccion_envio')->nullable();
            $table->string('coordenadas_direccion_envio');
            $table->unsignedInteger('user_direccion_envio');
            $table->foreign('user_direccion_envio')->references('id')->on('users');
            $table->boolean('active_direccion_envio')->default('true');
            $table->unsignedInteger('created_direccion_envio');
            $table->foreign('created_direccion_envio')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direccion_envio');
    }
}
