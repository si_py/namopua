<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternacional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internacional', function (Blueprint $table) {
            $table->id();
            $table->string('empresa');
            $table->string('nombre'); 
            $table->string('correo');
            $table->string('web')->nullable(); 
            $table->string('telefono'); 
            $table->string('pais');
            $table->text('mensaje');
            $table->string('pdf');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internacional');
    }
}
