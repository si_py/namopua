<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
          $table->bigIncrements('id_productos');
          $table->string('nombre_productos');
          $table->string('codigo_productos');
          $table->string('tipo_producto');
          $table->unsignedInteger('pk_categorias_productos');
          $table->foreign('pk_categorias_productos')->references('id_categorias')->on('categorias');
          $table->unsignedInteger('pk_subcategorias_productos');
          $table->foreign('pk_subcategorias_productos')->references('id_subcategorias')->on('subcategorias');
          $table->unsignedInteger('pk_marcas_productos');
          $table->foreign('pk_marcas_productos')->references('id_marcas')->on('marcas');
          $table->boolean('lanzamientos_productos');
          $table->boolean('descuentos_productos');
          $table->integer('precio_descuento_productos')->nullable();
          $table->integer('mayorista')->nullable();
          $table->integer('minorista')->nullable();
          $table->unsignedInteger('created_productos');
          $table->foreign('created_productos')->references('id')->on('users');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
