<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('users', function (Blueprint $table) {
        $table->string('razon_users')->nullable();
        $table->string('ruc_users')->nullable();
        $table->string('direccion_users')->nullable();
        $table->string('telefono_users')->nullable();
        $table->string('perfil_users')->nullable();
        $table->string('estado_users')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
