<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
          $table->bigIncrements('id_sliders');
          $table->string('path_sliders');
          $table->unsignedInteger('pk_secciones_sliders');
          $table->foreign('pk_secciones_sliders')->references('id_secciones')->on('secciones');
          $table->unsignedInteger('created_sliders');
          $table->foreign('created_sliders')->references('id')->on('users');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
