<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTallesProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('talles_productos', function (Blueprint $table) {
        $table->id('id_talles_productos');
        $table->unsignedInteger('pk_talles_productos');
        $table->foreign('pk_talles_productos')->references('id_talles')->on('talles');
        $table->unsignedInteger('pk_productos_talles_productos');
        $table->foreign('pk_productos_talles_productos')->references('id_productos')->on('productos');
        $table->unsignedInteger('created_talles_productos');
        $table->foreign('created_talles_productos')->references('id')->on('users');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('talles_productos');
    }
}
