<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosFacturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_factura', function (Blueprint $table) {
            $table->bigIncrements('id_datos_factura');
            $table->unsignedInteger('user_datos_factura');
            $table->foreign('user_datos_factura')->references('id')->on('users');
            $table->string('razon_datos_factura');
            $table->string('ruc_datos_factura');
            $table->string('email_datos_factura')->nullable();
            $table->string('telefono_datos_factura')->nullable();
            $table->string('direccion_datos_factura')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_factura');
    }
}
