<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido', function (Blueprint $table) {
            $table->bigIncrements('id_pedido');
            $table->date('fecha_pedido');
            $table->time('hora_pedido');
            $table->unsignedInteger('id_direccion_envio_pedido');
            $table->foreign('id_direccion_envio_pedido')->references('id_direccion_envio')->on('direccion_envio');
            $table->unsignedInteger('id_datos_factura_envio_pedido');
            $table->foreign('id_datos_factura_envio_pedido')->references('id_datos_factura')->on('datos_factura');
            $table->integer('total_pedido');
            $table->integer('shipping_pedido');
            $table->unsignedInteger('user_pedido');
            $table->foreign('user_pedido')->references('id')->on('users');
            $table->string('estado_pedido')->default('Pendiente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido');
    }
}
