<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurriculumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculum', function (Blueprint $table) {
            $table->id();
            $table->string('pdf_path')->nullable();
            $table->unsignedInteger('pk_empleos');
            $table->foreign('pk_empleos')->references('id')->on('empleos');
            $table->string('nombre');
            $table->string('documento');
            $table->string('imagen_documento')->nullable();
            $table->string('direccion');
            $table->string('ciudad');
            $table->string('barrio');
            $table->string('telefono');
            $table->integer('experiencia');
            $table->text('descripcion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curriculum');
    }
}
