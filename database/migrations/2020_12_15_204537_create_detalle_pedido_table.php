<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetallePedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_pedido', function (Blueprint $table) {
            $table->bigIncrements('id_detalle_pedido');
            $table->string('cod_producto_detalle_pedido');
            $table->string('color_detalle_pedido');
            $table->string('talla_detalle_pedido');
            $table->integer('cantidad_detalle_pedido');
            $table->integer('precio_detalle_pedido');
            $table->string('nombre_producto_detalle_pedido');
            $table->unsignedInteger('id_venta_detalle_pedido');
            $table->foreign('id_venta_detalle_pedido')->references('id_pedido')->on('pedido');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_pedido');
    }
}
