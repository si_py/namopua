<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColorImagenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('color_imagen', function (Blueprint $table) {
            $table->id('id_color_imagen');
            $table->unsignedInteger('pk_color_color_imagen');
            $table->foreign('pk_color_color_imagen')->references('id_color')->on('color');
            $table->unsignedInteger('pk_imagen_color_imagen');
            $table->foreign('pk_imagen_color_imagen')->references('id_imagen')->on('imagen');
            $table->unsignedInteger('pk_productos_color_imagen');
            $table->foreign('pk_productos_color_imagen')->references('id_productos')->on('productos');
            $table->unsignedInteger('created_color_imagen');
            $table->foreign('created_color_imagen')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('color_imagen');
    }
}
