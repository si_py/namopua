<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColorProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('color_productos', function (Blueprint $table) {
            $table->id('id_color_productos');
            $table->unsignedInteger('pk_color_productos');
            $table->foreign('pk_color_productos')->references('id_color')->on('color');
            $table->unsignedInteger('pk_productos_color_productos');
            $table->foreign('pk_productos_color_productos')->references('id_productos')->on('productos');
            $table->unsignedInteger('created_color_productos');
            $table->foreign('created_color_productos')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('color_productos');
    }
}
