<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategorias', function (Blueprint $table) {
          $table->bigIncrements('id_subcategorias');
          $table->string('nombre_subcategorias');
          $table->unsignedInteger('pk_categorias_subcategorias');
          $table->foreign('pk_categorias_subcategorias')->references('id_categorias')->on('categorias');
          $table->boolean('active_subcategorias');
          $table->unsignedInteger('created_subcategorias');
          $table->foreign('created_subcategorias')->references('id')->on('users');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategorias');
    }
}
