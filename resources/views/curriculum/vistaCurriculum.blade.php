@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('summernote/summernote-bs4.min.css') }}">

    <!-- Breadcrumb-->
<div class="breadcrumb-holder">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">Curriculum</li>
        </ul>
    </div>
</div>

<section>
    <div class="col-sm-12" style="padding: 10px;">
        <div class="row">
    
            <div class="col-lg-3" style="padding-right: 5px;">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h4>Empleos</h4>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" style="position: absolute; right: 20px;"> + Nuevo</a>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Activo</th>
                                    <th>Editar</th>
                                </tr>
                            </thead>
                            <tbody class="tabla_empleo">
                                @foreach($empleo as $e)
                                <tr>
                                    <td>{{ $e->nombre }}</td>
                                    <td>
                                        @if($e->activo == true)
                                            Activo
                                        @else
                                            Inactivo
                                        @endif
                                    </td>
                                    <td style="text-align: center;">
                                        <a  href="javascript:void(0)" 
                                            data-toggle="modal" 
                                            data-target="#exampleModal2" 
                                            onclick="enviarEmpleos('{{ $e->id }}', 
                                                                    '{{ $e->nombre }}', 
                                                                    '{{ $e->activo }}', 
                                                                    '{{ $e->descripcion }}')">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-9" style="padding-left: 5px;">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h4>Curriculum Recibidos</h4>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Fecha / Hora</th>
                                    <th>Nombre</th>
                                    <th>Empleo</th>
                                    <th>Teléfono</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($curri as $c)
                                <tr id="row-curriculim-{{$c->id}}">
                                    <td>{{ $c->created_at }}</td>
                                    <td>{{ $c->nombre }}</td>
                                    <td>{{ $c->nombre_empleo }}</td>
                                    <td>{{ $c->telefono }}</td>
                                    <td>
                                        <a class="btn btn-primary" data-toggle="modal" data-target="#exampleModal3" href="javascript:void(0)"
                                           onclick="mostrarCurriculum('{{ $c->nombre }}', 
                                                                      '{{ $c->nombre_empleo}}', 
                                                                      '{{ $c->telefono}}', 
                                                                      '{{ $c->documento}}', 
                                                                      '{{ $c->direccion}}', 
                                                                      '{{ $c->ciudad}}', 
                                                                      '{{ $c->barrio}}', 
                                                                      '{{ $c->experiencia}}', 
                                                                      '{{ $c->descripcion}}', 
                                                                      '{{ $c->pdf_path }}')">
                                            <i class="fa fa-search"></i>
                                        </a>
                                        <a class="btn btn-danger" onclick="eliminarCurriculum('{{ $c->id }}')" href="javascript:void(0)" style="margin-left: 20px;">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="paginate_curriculum">
                        {{ $curri->links() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>




<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Empleo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" id="nombre_empleo">
                </div>
                <div class="form-group">
                    <label for="">Descripción</label>
                    <textarea type="text" id="descripcion_empleo"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" onclick="agregarEmpleos()" class="btn btn-primary">Agregar</button>
            </div>
        </div>
    </div>
</div>


{{-- Modal Para editar empleo --}}
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Empleo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="hidden" class="form-control" id="mod_id_empleo">
                    <input type="text" class="form-control" id="mod_nombre_empleo">
                </div>
                <div class="form-group">
                    <label for="">Descripción</label>
                    <textarea type="text" id="mod_descripcion_empleo"></textarea>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="mod_activo_empleo">
                    <label class="form-check-label" for="mod_activo_empleo">Activo</label>
                  </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" onclick="editarEmpleos()" class="btn btn-primary">Editar</button>
            </div>
        </div>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 90%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Curriculum</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group col-sm-12" style="padding: 0;">
                            <p for="" style="color: #808080; font-weight: 300; margin:0;">Nombre y Apellido</p>
                            <input type="text" class="form-control" name="nombre_empleo" id="nombre_empleos" readonly>
                        </div>
                        <div class="form-group col-sm-12" style="padding: 0;">
                            <p for="" style="color: #808080; font-weight: 300; margin:0;">Nº Cedula de Identidad</p>
                            <input type="text" class="form-control" name="ci_empleo" id="ci_empleo" readonly>
                        </div>
                        <div class="form-group col-sm-12" style="padding: 0;">
                            <p for="" style="color: #808080; font-weight: 300; margin:0;">Teléfono (Celular)</p>
                            <input type="text" class="form-control" name="telefono_empleo" id="telefono_empleo" readonly>
                        </div>
                        <div class="form-group col-sm-12" style="padding: 0;">
                            <p for="" style="color: #808080; font-weight: 300; margin:0;">Dirección</p>
                            <input type="text" class="form-control" name="direccion_empleo" id="direccion_empleo" readonly>
                        </div>
                        <div class="form-group col-sm-12" style="padding: 0;">
                            <p for="" style="color: #808080; font-weight: 300; margin:0;">Ciudad</p>
                            <input type="text" class="form-control" name="ciudad_empleo" id="ciudad_empleo" readonly>
                        </div>
                        <div class="form-group col-sm-12" style="padding: 0;">
                            <p for="" style="color: #808080; font-weight: 300; margin:0;">Barrio</p>
                            <input type="text" class="form-control" name="barrio_empleo" id="barrio_empleo" readonly>
                        </div>
                        <div class="form-group col-sm-12" style="padding: 0;">
                            <p for="" style="color: #808080; font-weight: 300; margin:0;">Año de Experiencia</p>
                            <input type="number" class="form-control" name="experiencia_empleo" id="experiencia_empleo" readonly>
                        </div>
                        <div class="form-group col-sm-12" id="textarea_container" style="padding: 0;">
                            
                        </div>
                    </div>
                    <div class="col-sm-8" id="vista_pdf">
                        <embed type="text/html" src="snippet.html"  width="500" height="200">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('summernote/summernote-bs4.min.js') }}"></script>

<script>
$(document).ready(function() {
    $('#descripcion_empleo').summernote({
        height: 300,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    });

    $('#mod_descripcion_empleo').summernote({
        height: 300,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    });
});

function agregarEmpleos() {
    var nombre = $('#nombre_empleo').val();
    var descripcion = $('#descripcion_empleo').val();

    var tabla= '';
    $.ajax({
        url: "{{ route('curriculum.agregarEmpleo') }}",
        method: "POST",
        data: {nombre: nombre, descripcion: descripcion, _token: '{{ csrf_token() }}',
        },
        success: function(data) {
            
            for (var i=0; i<data.length;i++) {
                tabla += '<tr>'+
                            '<td>'+data[i].nombre+'</td>'+
                            '<td>'+data[i].activo+'</td>'+
                            '<td style="text-align: center;">'+
                            '<a  href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal2" onclick="enviarEmpleos(\''+data[i].id+'\', \''+data[i].nombre+'\', \''+data[i].activo+'\', \''+data[i].descripcion+'\')">'+
                            '<i class="fa fa-edit"></i>'+
                            '</a>'+
                            '</td>'+
                         '</tr>'

                $('.tabla_empleo').empty();
                $('.tabla_empleo').html(tabla);
            }

            $('#exampleModal').modal('hide');

        }
    });
}

function enviarEmpleos(id, nombre, activo, descripcion) {
    $('#mod_id_empleo').val(id);
    $('#mod_nombre_empleo').val(nombre);
    $('#mod_descripcion_empleo').summernote('code', descripcion);
    if(activo == true) {
        $('#mod_activo_empleo').prop('checked',true);
    } else {
        $('#mod_activo_empleo').prop('checked',false);
    }
}

function editarEmpleos() {
    var id = $('#mod_id_empleo').val();
    var nombre = $('#mod_nombre_empleo').val();
    var descripcion = $('#mod_descripcion_empleo').val();
    var activo;
    if($('#mod_activo_empleo').is(':checked')) {
        activo = true;
    } else {
        activo = false;
    }

    var tabla= '';
    $.ajax({
        url: "{{ route('curriculum.editarEmpleo') }}",
        method: "POST",
        data: {id: id, nombre: nombre, activo: activo, descripcion: descripcion, _token: '{{ csrf_token() }}',
        },
        success: function(data) {
            
            for (var i=0; i<data.length;i++) {
                tabla += '<tr>'+
                            '<td>'+data[i].nombre+'</td>'+
                            '<td>'+data[i].activo+'</td>'+
                            '<td style="text-align: center;">'+
                            '<a  href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal2" onclick="enviarEmpleos(\''+data[i].id+'\', \''+data[i].nombre+'\', \''+data[i].activo+'\', \''+data[i].descripcion+'\')">'+
                            '<i class="fa fa-edit"></i>'+
                            '</a>'+
                            '</td>'+
                         '</tr>'

                $('.tabla_empleo').empty();
                $('.tabla_empleo').html(tabla);
            }

            $('#exampleModal2').modal('hide');

        }
    });

    
}


function mostrarCurriculum(nombre, nombre_empleo, telefono, documento, direccion, ciudad, barrio, experiencia, descripcion, pdf) {

    $('#nombre_empleos').val(nombre);
    $('#ci_empleo').val(documento);
    $('#telefono_empleo').val(telefono);
    $('#direccion_empleo').val(direccion);
    $('#ciudad_empleo').val(ciudad);
    $('#barrio_empleo').val(barrio);
    $('#experiencia_empleo').val(experiencia);
    $('#descripcion_empleo').val(descripcion);

    var textarea = '<p for="" style="color: #808080; font-weight: 300; margin:0;">Descripción</p>'+
                    '<textarea class="form-control" name="descripcion_empleo" id="descripcion_empleo" readonly>'+descripcion+'</textarea>';

    var pdf = '<embed type="text/html" src="uploads/curriculum/'+pdf+'"  width="100%" height="600">';

    $('#vista_pdf').empty();
    $('#vista_pdf').html(pdf);

    $('#textarea_container').empty();
    $('#textarea_container').html(textarea);

}

function eliminarCurriculum(id) {
    $.ajax({
        url: "{{route('curriculum.eliminar')}}",
        data: {
            id: id,
            _token: "{{csrf_token()}}",
        },
        success: function(resp){
            console.log(resp)
            $('#row-curriculim-' + id).remove()
        }
    })
}

</script>


@endsection