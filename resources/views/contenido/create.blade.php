@extends('layouts.app')

@section('content')

@extends('layouts.app')

<!-- Breadcrumb-->
<div class="breadcrumb-holder">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Home</a></li>
      <li class="breadcrumb-item"><a href="/contenido/">Contenido</a></li>
      <li class="breadcrumb-item active">Edit</li>
    </ul>
  </div>
</div>

<section>
  <div class="col-sm-12" style="padding: 10px;">
    <div class="row">
      <div class="col-lg-6">
        <div class="card">
          <form class="form-horizontal" action="{{route('contenido.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label>Sección</label>
                <select name="pk_secciones_contenido" class="mr-3 form-control">
                  <option>Seleccione...</option>
                  @foreach($secciones as $se)
                  <option value="{{$se->id_secciones}}">{{$se->nombre_secciones}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Titulo</label>
                <input name="titulo_contenido" id="titulo_contenido" type="text" placeholder="Titulo del contenido" class="mr-3 form-control">
              </div>
              <div class="form-group row">
                  <label>Imagen</label>
                  <input id="file" name="file" type="file" class="form-control form-control-warning">
              
              </div>
              <div class="form-group">
                <label>Texto del contenido</label>
                <textarea name="texto_contenido" id="texto_contenido" class="summernote" required></textarea>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Agregar</button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
</section>

@endsection
