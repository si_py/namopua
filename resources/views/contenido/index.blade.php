@extends('layouts.app')

@section('content')

<!-- Breadcrumb-->
<div class="breadcrumb-holder">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Home</a></li>
      <li class="breadcrumb-item active">Contenido</li>
    </ul>
  </div>
</div>

<section>
  <div class="col-sm-12" style="padding: 10px;">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <div class="card-body" style="padding: 0;">
              <div class="form-inline">
                <div class="form-group">
                  <a href="/contenido/create" class="mr-3 btn btn-primary">
                    <i class="fa fa-plus"></i> Agregar Contenido
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Seccion</th>
                    <th>Titulo</th>
                    <th>Texto</th>
                    <th>Icono</th>
                    <th>Acción</th>
                  </tr>
                </thead>
                <tbody class="table_contenido">
                  @foreach($contenido as $co)
                  <tr>
                    <th scope="row">{{$co->id_contenido}}</th>
                    <td>
                      @foreach($secciones as $se)
                      @if($se->id_secciones == $co->pk_secciones_contenido)
                        {{$se->nombre_secciones}}
                      @endif
                      @endforeach
                    </td>
                    <td>{{$co->titulo_contenido}}</td>
                    <td><?php echo( $co->texto_contenido )?></td>
                    <td>
                      <img src="../img/{{$co->path_contenido}}" style="width: 50px;"/>
                    </td>
                    <td>
                        <a href="#" 
                           data-toggle="modal" 
                           data-target="#modalEditar" 
                           onclick="modificarContenido('{{$co->id_contenido}}')">
                          <i class="fa fa-pencil"></i>
                        </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<!-- Modal -->
<div class="modal fade" id="modalEditar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Contenido</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" id="modificar">
        <div class="modal-body">
          <div class="form-group">
            <label for="">Sección</label>
            <input type="hidden" id="idContenido" class="form-control">
            <select class="form-control" id="seccionContenido">
              @foreach($secciones as $s)
                <option value="{{ $s->id_secciones }}">{{ $s->nombre_secciones }}</option>
              @endforeach
            </select>
          </div>
  
          <div class="form-group">
            <label for="">Titulo</label>
            <input type="text" id="tituloContenido" class="form-control">
          </div>
  
          <div class="form-group">
            <label for="">Texto Contenido</label>
            <textarea name="texto_contenido" id="texto_contenido" class="summernote" required></textarea>
          </div>
  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Modificar</button>
        </div>
      </form>
      
    </div>
  </div>
</div>

<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>

<script>
  function modificarContenido(id) {
    $.ajax({
      type: "ajax",
      url: "{{ route('contenido.modificarContenido') }}",
      type: 'GET',
      data: { id: id },
      success: function(data) {
          $('#idContenido').val(data.contenido.id_contenido);
          $('#seccionContenido').val(data.contenido.pk_secciones_contenido);
          $('#tituloContenido').val(data.contenido.titulo_contenido);
          $('#texto_contenido').summernote('code', data.contenido.texto_contenido);
      }
    });
  }

  $('#modificar').submit(function(e) {
    e.preventDefault();

    var id = $('#idContenido').val();
    var titulo = $('#tituloContenido').val();
    var seccion = $('#seccionContenido').val();
    var texto = $('#texto_contenido').summernote('code');

    $.ajax({
      url: "{{ route('contenido.modificarContenidoModal') }}",
      type: "POST",
      data: { id: id, titulo: titulo, seccion: seccion, texto: texto, _token: "{{ csrf_token() }}" },
      success: function (data){
        location.reload();
      }
    });

  })
</script>

@endsection
