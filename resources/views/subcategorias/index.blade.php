@extends('layouts.app')

@section('content')
    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Sub Categorias</li>
            </ul>
        </div>
    </div>

    <section>
        <div class="col-sm-12" style="padding: 10px;">
            <div class="row">
                <div class="col-lg-5">
                    {{-- formulario de agregar --}}
                    <div class="card">
                        <div class="card-header">
                            <div class="card-body" style="padding: 0;">
                                <div class="form-inline">
                                    @csrf
                                    <div class="form-group">
                                        <label for="inlineFormInput" class="sr-only">Agregar sub categorias</label>
                                        <input id="nombre" type="text" placeholder="Nombre de subcategoria"
                                            class="mr-3 form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="inlineFormInput" class="sr-only">Categorias</label>
                                        <select id="categoria" class="mr-3 form-control" style="width: 200px;">
                                            <option selected disabled>Categoria...</option>
                                            @foreach ($categoria as $cat)
                                                <option value="{{ $cat->id_categorias }}">{{ $cat->nombre_categorias }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button onclick="agregar_subcategorias()"
                                            class="mr-3 btn btn-primary">Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Categoria</th>
                                            <th>Accion</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-subcategorias">
                                        @foreach ($sub as $su)
                                            <tr>
                                                <th scope="row">{{ $su->id_subcategorias }}</th>
                                                <td>{{ $su->nombre_subcategorias }}</td>
                                                <td>{{ $su->nombre_categorias }}</td>
                                                <td>
                                                    <a href="{{ route('subcategorias.edit', $su->id_subcategorias) }}">
                                                        <i class="icon-form"></i>
                                                    </a>
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <i class="fa fa-arrow-right cambiar_estado"
                                                        data-nuevo-estado="{{ $su->active_subcategorias }}"
                                                        data-colum-name="active_subcategorias"></i>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-5 col-sm-12">
                    {{-- tabla de subcategorias activas --}}
                    <div class="card">
                        <div class="card-header">
                            Activos
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Categoria</th>
                                            <th>Accion</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-subcategorias" id="activas">
                                        @foreach ($subActive as $su)
                                            <tr data-id="{{ $su->id_subcategorias }}">
                                                <th scope="row">{{ $su->id_subcategorias }}</th>
                                                <td>{{ $su->nombre_subcategorias }}</td>
                                                <td>{{ $su->nombre_categorias }}</td>
                                                <td>
                                                    <a href="{{ route('subcategorias.edit', $su->id_subcategorias) }}">
                                                        <i class="icon-form"></i>
                                                    </a>
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <i class="fa fa-arrow-right cambiar_estado p-1"
                                                        data-id="{{ $su->id_subcategorias }}"
                                                        data-nuevo-estado="false"
                                                        data-column-name="active_subcategorias"></i>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-sm-12">
                    {{-- tabla de subcategorias inactivas --}}
                    <div class="card">
                        <div class="card-header">
                            Inactivos
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Categoria</th>
                                            <th>Accion</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-subcategorias" id="inactivas">
                                        @foreach ($subInactive as $su)
                                            <tr data-id="{{ $su->id_subcategorias }}">
                                                <th scope="row">{{ $su->id_subcategorias }}</th>
                                                <td>{{ $su->nombre_subcategorias }}</td>
                                                <td>{{ $su->nombre_categorias }}</td>
                                                <td>
                                                    <a href="{{ route('subcategorias.edit', $su->id_subcategorias) }}">
                                                        <i class="icon-form"></i>
                                                    </a>
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <i class="fa fa-arrow-left cambiar_estado p-1"
                                                        data-id="{{ $su->id_subcategorias }}"
                                                        data-nuevo-estado="true"
                                                        data-column-name="active_subcategorias"></i>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        const url_tabla = "/subcategorias/"

        function agregar_subcategorias() {
            var nombre = document.getElementById("nombre").value;
            var categoria = document.getElementById("categoria").value;
            var id_stock = "";
            console.log(nombre);
            console.log(categoria);
            $.ajax({
                type: "ajax",
                url: "{{ route('subcategorias.store') }}",
                type: 'POST',
                data: {
                    nombre: nombre,
                    categoria: categoria,
                    _token: '{{ csrf_token() }}'
                },

                success: function() {

                    location.reload();

                }
            })
        };

        function nuevaFilaEstado(data) {
            let arrow = ""
            let nuevoEstado
            let fila = ""
            if (data.active_subcategorias) {
                arrow = "right"
                nuevoEstado = "false"
            } else {
                arrow = "left"
                nuevoEstado = "true"
            }
            fila = `
              <tr data-id="${data.id_subcategorias}">
                  <th scope="row">${data.id_subcategorias}</th>
                  <td>${data.nombre_subcategorias}</td>
                  <td>${data.nombre_categorias}</td>
                  <td>
                      <a href="/subcategorias/${data.id_subcategorias}/edit">
                          <i class="icon-form"></i>
                      </a>
                      &nbsp;
                      &nbsp;
                      &nbsp;
                      <i class="fa fa-arrow-${arrow} cambiar_estado p-1"
                          data-id="${data.id_subcategorias}"
                          data-nuevo-estado="${nuevoEstado}"
                          data-column-name="active_subcategorias"></i>
                  </td>
              </tr>
            `;
            //se agrega la nueva fila a la tabla activos o inactivos
            if (data.active_subcategorias) {
              $("#activas").append(fila)
            } else {
              $("#inactivas").append(fila)
            }
        }
    </script>

@endsection
