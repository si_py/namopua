
@extends('layouts.app')

@section('content')

<!-- Breadcrumb-->
<div class="breadcrumb-holder">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Home</a></li>
      <li class="breadcrumb-item"><a href="/subcategorias">Sub categorias</a></li>
      <li class="breadcrumb-item active">Editar</li>
    </ul>
  </div>
</div>

<section>
  <div class="col-sm-12" style="padding: 10px;">
    <div class="row">
      <div class="col-lg-5">
        <div class="card">
          <div class="card-body">
            <form action="{{url('/subcategorias/'. $category->id_subcategorias)}}" method="POST" role="form">
              @csrf
              @method('PATCH')
              <div class="form-group">
                <label for="inlineFormInput" class="sr-only">Agregar sub categorias</label>
                <input id="nombre" name="nombre_subcategorias" type="text" placeholder="Nombre de subcategoria" class="mr-3 form-control" value="{{$category->nombre_subcategorias}}">
              </div>
              <div class="form-group">
                <select id="categoria" name="pk_categorias_subcategorias" class="mr-3 form-control" style="width: 100%">
                  @foreach($cat as $c)
                  @if($c->id_categorias == $category->pk_categorias_subcategorias)
                  <option value="{{$c->id_categorias}}">{{$c->nombre_categorias}}</option>
                  @endif
                  @endforeach

                  @foreach($cat as $ca)
                  <option value="{{$ca->id_categorias}}">{{$ca->nombre_categorias}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <button type="submit" class="mr-3 btn btn-primary">Modificar</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
