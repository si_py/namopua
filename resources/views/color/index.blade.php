@extends('layouts.app')

@section('content')

    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Color</li>
            </ul>
        </div>
    </div>

    <section>
        <div class="col-sm-12" style="padding: 10px;">
            <div class="row">
                <div class="col-sm-4">

                    <div class="card-body" style="padding: 0;">
                        <div class="form-inline">
                            @csrf
                            <div class="form-row align-items-center">
                                <div class="col-auto">
                                    <label for="inlineFormInput" class="sr-only">Agregar color</label>
                                    <input id="nombre" type="text" placeholder="Ej. Azul" class="mb-2 form-control">
                                </div>
                                <div class="col-auto">
                                    <label for="inlineFormInput" class="sr-only">Agregar color</label>
                                    <input id="prefijo" type="text" placeholder="Ej. #353535" class="mb-2 form-control">
                                </div>
                                <div class="col-auto">
                                    <button onclick="agregar_color()" class="mb-2 btn btn-primary">Agregar</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header">
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Prefijo</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_color">
                                        @foreach ($color as $c)
                                            <tr>
                                                <th scope="row">{{ $c->id_color }}</th>
                                                <td contenteditable="true" class="column_name"
                                                    data-column_name="nombre_color" data-id="{{ $c->id_color }}">
                                                    {{ $c->nombre_color }}</td>
                                                <td contenteditable="true" class="column_name"
                                                    data-column_name="prefijo_color" data-id="{{ $c->id_color }}"
                                                    style="color: {{ $c->prefijo_color }}; text-shadow: 0 0 1px black">
                                                    {{ $c->prefijo_color }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                Activos
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_color" id="activos">

                                        @foreach ($coloresActivos as $c)
                                            <tr data-id="{{ $c->id_color }}">
                                                <th scope="row">{{ $c->id_color }}</th>
                                                <td contenteditable="true" class="column_name"
                                                    data-column_name="nombre_color" data-id="{{ $c->id_color }}">
                                                    {{ $c->nombre_color }}</td>
                                                <td contenteditable="true" class="column_name"
                                                    data-column_name="prefijo_color" data-id="{{ $c->id_color }}"
                                                    style="color: {{ $c->prefijo_color }}; text-shadow: 0 0 1px black">
                                                    {{ $c->prefijo_color }}</td>
                                                <td><i class="fa fa-arrow-right cambiar_estado"
                                                        data-id="{{ $c->id_color }}" data-nuevo-estado="false"
                                                        data-column-name="estado_color"></i></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                Inactivos
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_color" id="inactivos">

                                        @foreach ($coloresInactivos as $c)
                                            <tr data-id="{{ $c->id_color }}">
                                                <th scope="row">{{ $c->id_color }}</th>
                                                <td contenteditable="true" class="column_name"
                                                    data-column_name="nombre_color" data-id="{{ $c->id_color }}">
                                                    {{ $c->nombre_color }}</td>
                                                <td contenteditable="true" class="column_name"
                                                    data-column_name="prefijo_color" data-id="{{ $c->id_color }}"
                                                    style="color: {{ $c->prefijo_color }}; text-shadow: 0 0 1px black">
                                                    {{ $c->prefijo_color }}</td>
                                                <td><i class="fa fa-arrow-left cambiar_estado"
                                                        data-id="{{ $c->id_color }}" data-nuevo-estado="true"
                                                        data-column-name="estado_color"></i></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        const url_tabla = '/color/'

        function agregar_color() {
            var nombre = document.getElementById("nombre").value;
            var prefijo = document.getElementById("prefijo").value;
            var activos = "";
            var inactivos = "";
            console.log(nombre);
            $.ajax({
                type: "ajax",
                url: "{{ route('color.store') }}",
                type: 'POST',
                data: {
                    nombre_color: nombre,
                    prefijo_color: prefijo,
                    _token: '{{ csrf_token() }}'
                },

                success: function(resp) {

                    if (resp.errors != null) {
                        alert('Ya existe la el color: ' + nombre)
                    } else {
                        // var categoria = $(this).val();
                        $.get('/tabla_color', function(data) {
                            //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
                            console.log(data);
                            let activos = data.activos
                            for (var i = 0; i < activos.length; i++) {
                                activos += '<tr>' +
                                    '<td>' + activos[i].id_color + '</td>' +
                                    '<td contenteditable="true" class="column_name" data-column_name="nombre_color" data-id="' +
                                    activos[i].id_color + '">' + activos[i].nombre_color + '</td>' +
                                    '<td contenteditable="true" class="column_name" data-column_name="prefijo_color" data-id="' +
                                    activos[i].id_color + '" style="color:' + activos[i].prefijo_color +
                                    '">' +
                                    activos[i].prefijo_color + '</td>' +
                                    '</tr>'
                            }
                            let inactivos = data.inactivos
                            for (var i = 0; i < inactivos.length; i++) {
                                activos += '<tr>' +
                                    '<td>' + inactivos[i].id_color + '</td>' +
                                    '<td contenteditable="true" class="column_name" data-column_name="nombre_color" data-id="' +
                                    inactivos[i].id_color + '">' + inactivos[i].nombre_color + '</td>' +
                                    '<td contenteditable="true" class="column_name" data-column_name="prefijo_color" data-id="' +
                                    inactivos[i].id_color + '" style="color:' + inactivos[i].prefijo_color +
                                    '">' +
                                    inactivos[i].prefijo_color + '</td>' +
                                    '</tr>'
                            }
                            $(".table_color#activos").empty();
                            $('.table_color#activos').append(activos);
                            $(".table_color#inactivos").empty();
                            $('.table_color#inactivos').append(inactivos);
                        })
                    }
                }
            })
        };

        function nuevaFilaEstado(data) {
            $(".table_color tr[data-id=" + data.id_color + "]").remove()
            let estado = data.estado_color
            let fila = `
                <tr data-id="${data.id_color}">
                    <th scope="row">${data.id_color}</th>
                    <td contenteditable="true" class="column_name"
                        data-column_name="nombre_color" data-id="${data.id_color}">
                        ${data.nombre_color}</td>
                    <td contenteditable="true" class="column_name"
                        data-column_name="prefijo_color" data-id="${data.id_color}"
                        style="color: ${data.prefijo_color}; text-shadow: 0 0 1px black">
                        ${data.prefijo_color}</td>
                    <td><i class="fa fa-arrow-${estado ? 'right' : 'left'} cambiar_estado"
                            data-id="${data.id_color}" data-nuevo-estado="${estado ? 'false' : 'true'}"
                            data-column-name="estado_color"></i></td>
                </tr>
            `
            if (estado) {
                $(".table_color#activos").append(fila)
            } else {
                $(".table_color#inactivos").append(fila)
            }
        }
    </script>

@endsection
