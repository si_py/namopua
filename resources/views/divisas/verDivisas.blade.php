@extends('layouts.app')

@section('content')

  <div class="col-12 p-3">
    <div class="col-12 bg-white p-3" style="border-radius: 15px;">
      <div class="col-12 mt-3">
        <h1>Divisas</h1>
      </div>
      @if(count($divisa) > 0)
        @foreach($divisa as $d)
          <div class="col-6 mt-4">
            <form action="" id="modificarDivisas">
              <div class="form-group">
                <label for="">Nombre</label>
                <input type="hidden" id="id" value="{{ $d->id }}" class="form-control">
                <input type="text" id="nombre" value="{{ $d->nombre }}" class="form-control" required>
              </div>
              <div class="form-group">
                <label for="">Prefijo</label>
                <input type="text" id="prefijo" value="{{ $d->prefijo }}" class="form-control" required>
              </div>
              <div class="form-group">
                <label for="">Monto</label>
                <input type="text" id="divisa" value="{{ $d->divisa }}" class="form-control" required>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Modificar</button>
              </div>
            </form>
          </div>
        @endforeach
      @else
        <div class="col-6 mt-4">
          <form action="" id="agregarDivisas">
            <div class="form-group">
              <label for="">Nombre</label>
              <input type="text" id="nombre" class="form-control" required>
            </div>
            <div class="form-group">
              <label for="">Prefijo</label>
              <input type="text" id="prefijo" class="form-control" required>
            </div>
            <div class="form-group">
              <label for="">Monto</label>
              <input type="text" id="divisa" class="form-control" required>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Agregar</button>
            </div>
          </form>
        </div>
      @endif
    </div>
  </div>

  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
  <script>
    $('#agregarDivisas').submit(function(e){
      e.preventDefault();

      var nombre = $('#nombre').val();
      var prefijo = $('#prefijo').val();
      var divisa = $('#divisa').val();

      var formData = new FormData();
      formData.append('_token', "{{ csrf_token() }}");
      formData.append('nombre', nombre);
      formData.append('prefijo', prefijo);
      formData.append('divisa', divisa);

      $.ajax({
          url: "{{ route('divisa.agregarDivisa') }}",
          type: "POST",
          data: formData,
          processData: false,
          contentType: false,
          success: function (data){
              location.reload();
          } 
      });

    });

    $('#modificarDivisas').submit(function(e){
      e.preventDefault();
      var id = $('#id').val();
      var nombre = $('#nombre').val();
      var prefijo = $('#prefijo').val();
      var divisa = $('#divisa').val();

      var formData = new FormData();
      formData.append('_token', "{{ csrf_token() }}");
      formData.append('id', id);
      formData.append('nombre', nombre);
      formData.append('prefijo', prefijo);
      formData.append('divisa', divisa);

      $.ajax({
          url: "{{ route('divisa.modificarrDivisa') }}",
          type: "POST",
          data: formData,
          processData: false,
          contentType: false,
          success: function (data){
              location.reload();
          } 
      });

    });
  </script>

@endsection