@extends('layouts.front')

@section('content')
<div class="col-sm-12 title-shop">
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div style="width: 100%; text-align: center; float: left; font-weight: 400;">
                    <img src="../img/icon/icon/credit-card.svg" style="margin-top: -4px;"/>
                    Descuentos | <a href="#">Click aquí! Como obtener tu carnet</a>
                </div>
            </div>
            <div class="carousel-item">
                <div style="width: 100%; text-align: center; float: left; font-weight: 400;">
                    <img src="../img/icon/icon/danger.svg" style="margin-top: -4px;"/>
                    Modo COVID-19 | <a href="#">Conocė nuestros productos</a>
                </div>
            </div>
            <div class="carousel-item">
                <div style="width: 100%; text-align: center; float: left; font-weight: 400;">
                    <img src="../img/icon/icon/transport.svg" style="margin-top: -4px;"/>
                    Delivery | Asunción y Gran Asunción
                </div>
            </div>
            <div class="carousel-item">
                <div style="width: 100%; text-align: center; float: left; font-weight: 400;">
                    <img src="../img/icon/icon/box.svg" style="margin-top: -4px; width: 23px;"/>
                    Encomienda | para todo territorio nacional
                </div>
            </div>
        </div>
    </div>
<style>
    .title-shop {
    text-align: center;
    padding: 15px 0;
    height: 55px;
    overflow: hidden;
    border-bottom: 1px solid #e5e5e5;
    font-family: 'Montserrat', sans-serif;
    font-weight: 100;
    font-size: 15px;
    }
    h1 {
        font-family: 'Montserrat', sans-serif;
        color: #808080;
        font-weight: 600;
        font-size: 40px;
    }
    p {
        font-family: 'Montserrat', sans-serif;
        color: #808080;
        font-weight: 600;
        margin-bottom: 0;
    }
    .btn-primary {
        font-family: 'Montserrat', sans-serif;
    }
    .check img {width: 300px;}
    @media screen and (max-width: 1199px) {
        .img-box img {width: 100%;}
    }
    @media screen and (max-width: 991px) {
        .img-box img {width: 526px;}
    }
    @media screen and (max-width: 676px) {
        .check img {width: 60%;}
    }
    @media screen and (max-width: 600px) {
        .img-box img {width: 100%;}
    }
</style>
</div>
<section>
    <div class="container-lg" style="padding-bottom: 100px; overflow: hidden;">
        <div class="col-12 check" style="text-align: center; padding: 50px;">
            <img src="img/icon/check-green.svg" style="margin-bottom: 30px;">
            <h1>Gracias</h1>
            <h1>por su compra!</h1>
            <a class="btn btn-primary" style="border-radius: 30px; margin-top: 20px; padding-left: 20px; padding-right: 20px;" href="/shop">Volver a la tienda</a>
        </div>

        <div class="col-12" style="text-align: center; padding: 50px;">
            <p>MÉTODO DE PAGO ONLINE</p>
            <p style="font-weight: 400;">Realice su pago en tan sólo 4 pasos</p>
            
        </div>

        <div class="col-12 img-box" style="padding: 0; text-align: center;">
            <div class="col-lg-6" style="float: left; padding-bottom: 50px;">
                
                <img src="img/fabrica_de_jeans_en_paraguay_compras_1.png">
                <p style="margin-top: 20px; font-weight: 400;"><span style=" color: #175b89; font-weight: 600;">Paso 1</span> Seleccione la opción de Adjuntar comprobante</p>
            </div>
            <div class="col-lg-6" style="float: left; padding-bottom: 50px;">
                
                <img src="img/fabrica_de_jeans_en_paraguay_compras_2.png">
                <p style="margin-top: 20px; font-weight: 400;"><span style=" color: #175b89; font-weight: 600;">Paso 2</span> Click en ELEGIR para subir comprobante de pago</p>
            </div>
        </div>
        <div class="col-12 img-box" style="padding: 0; text-align: center;">
            <div class="col-lg-6" style="float: left; padding-bottom: 50px;">
                <img src="img/fabrica_de_jeans_en_paraguay_compras_3.png">
                <p style="margin-top: 20px; font-weight: 400;"><span style=" color: #175b89; font-weight: 600;">Paso 3</span> Una vez adjunto el comprobante, click en subir imagen </p>
            </div>
            <div class="col-lg-6" style="float: left; padding-bottom: 50px;">
                <img src="img/fabrica_de_jeans_en_paraguay_compras_4.png">
                <p style="margin-top: 20px; font-weight: 400;"><span style=" color: #175b89; font-weight: 600;">Paso 4</span> Una vez finalizado este proceso, podrá visualizar el estado de su compra y en las próximas 24hrs podrá recibir su pedido dentro del horario establecido (10:00hs a 16:00hs de Lunes a viernes.</p>
            </div>

        </div>

        
    </div>
</section>


@endsection