@extends('layouts.front')

<?php
$cant = 0;
$sum = 0;
?>
<!-- Google Maps -->
<script defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHkLAEp8g6OpmRVeXob0vKa6Gyag6J7SY&callback=initMap"></script>
<script defer type="text/javascript">
    var marker; //variable del marcador
    var coords = {}; //coordenadas obtenidas con la geolocalización

    //Funcion principal
    initMap = function() {
        //usamos la API para geolocalizar el usuario
        navigator.geolocation.getCurrentPosition(
            function(position) {
                coords = {
                    lng: position.coords.longitude,
                    lat: position.coords.latitude
                };

                setMapa(coords); //pasamos las coordenadas al metodo para crear el mapa
                //console.log(position.coords.longitude);
                document.getElementById("coords").value = position.coords.longitude + ',' + position.coords
                    .latitude;
            },
            function(error) {
                console.log(error);
            });
    }

    function setMapa(coords) {

        //Se crea una nueva instancia del objeto mapa
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: new google.maps.LatLng(coords.lat, coords.lng),

        });


        //evento para buscar por input buscador por direcciones
        const geocoder = new google.maps.Geocoder();

        document.getElementById("submit").addEventListener("click", () => {
            geocodeAddress(geocoder, map);

        });

        //Creamos el marcador en el mapa con sus propiedades
        //para nuestro obetivo tenemos que poner el atributo draggable en true
        //position pondremos las mismas coordenas que obtuvimos en la geolocalización
        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(coords.lat, coords.lng),

        });

        //agregamos un evento al marcador junto con la funcion callback al igual que el evento dragend que indica
        //cuando el usuario a soltado el marcador
        marker.addListener('click', toggleBounce);

        marker.addListener('dragend', function(event) {
            //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords
            document.getElementById("coords").value = this.getPosition().lat() + "," + this.getPosition().lng();
        });
    }

    function geocodeAddress(geocoder, resultsMap) {

        const address = document.getElementById("address").value + ', ' + document.getElementById("city").value;
        geocoder.geocode({
            address: address
        }, (results, status) => {
            if (status === "OK") {
                resultsMap.setCenter(results[0].geometry.location);
                new google.maps.Marker({
                    map: resultsMap,
                    //position: new google.maps.LatLng(coords.lat,coords.lng),
                    position: results[0].geometry.location,
                    draggable: true,
                    animation: google.maps.Animation.DROP,

                });

                document.getElementById("coords").value = results[0].geometry.location;

                //agregamos un evento al marcador junto con la funcion callback al igual que el evento dragend que indica
                //cuando el usuario a soltado el marcador
                marker.addListener('click', toggleBounce);

                marker.addListener('dragend', function(event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords
                    document.getElementById("coords").value = this.getPosition().lat() + "," + this
                        .getPosition().lng();
                });

            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    }

    //callback al hacer clic en el marcador lo que hace es quitar y poner la animacion BOUNCE
    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

</script>

@section('content')

    <div class="contenedor">
        <div class="container-lg">
            <div class="col-sm-12 title-shop">
                <h1 id="titulo_caja" style="font-weight: 900; font-family: 'Montserrat', sans-serif;">Caja</h1>
                <p id="cantidad_title" style="margin-bottom: 0; font-weight: normal;">
                    @if (session('cart'))
                        @foreach (session('cart') as $position => $y)
                            <?php
                            $cant += $y['cantidad'];
                            $sum += $y['total'];
                            ?>
                        @endforeach

                        @if ($cant > 0)
                            Tenés
                            <span id="item-letter" style="color: #1677BB; font-weight: normal">
                                {{ $cant }} prenda(s)
                            </span> en tu carrito
                        @else
                            <span id="item-letter" style="color: #1677BB; font-weight: normal">
                                Tenés 0 prenda(s)
                            </span> en tu carrito
                        @endif
                    @endif
                </p>

            </div>
            <style>
                body {}

                .contenedor {
                    position: relative;
                    height: 1440px;
                }

                .title-shop {
                    text-align: center;
                    padding: 80px 0 80px 0;
                }

                .title-shop h1 {
                    font-size: 100px;
                    font-weight: 900;
                }

                .title-shop p {
                    font-size: 17px;
                    font-weight: 100;
                    color: #909090;
                    margin-top: 0px;
                    margin-bottom: 0;
                }

                .title-compra {
                    font-size: 17px;
                    font-weight: 100;
                    color: #909090;
                    margin-top: 0px;
                    margin-bottom: 0;
                }

                .contenedor p {
                    font-family: 'Montserrat', sans-serif;
                    font-weight: 600;
                }

                .contenedor input:focus {
                    border: none;
                    border-bottom: 1px solid #1677BB;
                    box-shadow: none;
                }

                .contenedor input[readonly] {
                    background-color: #fff;
                }

                .contenedor button:focus {
                    box-shadow: none;
                }

                @media screen and (max-width: 991px) {
                    .title-shop {
                        text-align: center;
                        padding: 30px 0 30px 0;
                    }

                    .title-shop h1 {
                        font-size: 40px;
                    }

                    .title-shop p {
                        font-size: 14px;
                        font-weight: 100;
                        color: #909090;
                        margin-top: 0px;
                        margin-bottom: 0;
                    }

                    #item-letter,
                    #cantidad_title {
                        font-size: 14px;
                    }
                }

            </style>

            <div class="col-sm-12" style="color: #a1a1a1;">


                <div class="col-sm-12 step" id="step-1">

                    <div class="col-sm-6" style="float: left; margin-bottom: 20px;">
                        <p style="color: #1677BB; margin-bottom: 0px;">Mis datos de factura </p>
                        <span style="color: #909090; font-weight: 300; font-size: 15px; margin-bottom: 20px;"> * Seleccione
                            una factura que desea</span>
                        <input type="hidden" id="id_factura_asignada" class="form-control form-control-sm">
                        <div id="scroll"></div>
                        <ul class="nav nav-tabs" id="myTabFactura" role="tablist">
                            @foreach ($factura as $fa)
                                <li class="nav-item direccion_asignado" style="margin-bottom: 10px;">
                                    <a class="nav-link" onclick="asignarFactura('{{ $fa->id_datos_factura }}')"
                                        id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                                        aria-selected="true">
                                        <p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Razón Social: <span
                                                style="font-weight: 600;">{{ $fa->razon_datos_factura }}</span></p>
                                        <p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Ruc: <span
                                                style="font-weight: 600;">{{ $fa->ruc_datos_factura }}</span></p>
                                        <p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Dirección:
                                            <span style="font-weight: 600;">
                                                @if ($fa->direccion_datos_factura == null)

                                                @else
                                                    {{ $fa->direccion_datos_factura }}
                                                @endif
                                            </span>
                                        </p>
                                        <p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Telefono:
                                            <span style="font-weight: 600;">
                                                @if ($fa->telefono_datos_factura == null)

                                                @else
                                                    {{ $fa->telefono_datos_factura }}
                                                @endif
                                            </span>
                                        </p>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="col-sm-6" style="float: left;">
                        <p style="color: #1677BB;">Información Personal</p>
                        <input type="hidden" id="fecha_pedido" class="form-control form-control-sm"
                            value="<?php echo date('Y-m-d'); ?>">
                        <input type="hidden" id="hora_pedido" class="form-control form-control-sm"
                            value="<?php echo date('H:i'); ?>">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- <label for="name" style="font-weight: 300; margin-bottom: 0;">Nombre y Apellido</label> -->
                                <input value="{{ Auth::user()->id }}" id="id" name="id" type="hidden"
                                    class="form-control form-control-sm">
                                <input value="{{ Auth::user()->name }}" id="name" name="name" type="hidden"
                                    class="form-control form-control-sm">
                                <input value="{{ Auth::user()->razon_users }}" id="razon" name="razon" type="text"
                                    class="form-control form-control-sm" readonly>
                                <label for="razon" style="font-weight: 300; margin-bottom: 0;">Razón Social</label>
                            </div>
                            <div class="col-sm-12">
                                <input readonly value="{{ Auth::user()->ruc_users }}" id="ruc" name="ruc" type="text"
                                    class="form-control form-control-sm">
                                <label for="ruc" style="font-weight: 300; margin-bottom: 0;">RUC / C.I.No.</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">

                                <input readonly value="{{ Auth::user()->email }}" name="email" type="email"
                                    class="form-control form-control-sm" id="correo" aria-describedby="emailHelp">
                                <label for="exampleInputEmail1" style="font-weight: 300; margin-bottom: 0;">Correo
                                    Electrónico</label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-12">

                                <input readonly value="{{ Auth::user()->direccion_users }}" id="direccion"
                                    name="direccion" type="text" class="form-control form-control-sm" id="correo"
                                    aria-describedby="emailHelp">
                                <label for="direccion" style="font-weight: 300; margin-bottom: 0;">Dirección Inicial</label>
                            </div>
                            <div class="col-sm-12">

                                <input readonly value="{{ Auth::user()->telefono_users }}" id="telefono" name="telefono"
                                    type="text" class="form-control form-control-sm" id="correo"
                                    aria-describedby="emailHelp">
                                <label for="telefono" style="font-weight: 300; margin-bottom: 0;">Teléfono</label>
                            </div>
                        </div>


                        <p style="color: #1677BB; margin-top: 30px; margin-bottom: 0;">
                            Datos de Facturación
                        </p>
                        <!-- <span style="color: #909090; font-weight: 300">* En caso que quiera tener varias facturas a elegir, si solo realizará el pedido con la factura que ingreso cuando inica sesión puede omitir</span> -->
                        <div class="col-sm-12" id="agregar_factura" style="padding: 0; overflow: hidden;">
                            <div class="row">
                                <div class="col-sm-12">

                                    <input name="razon_factura" id="razon_factura" type="text"
                                        class="form-control form-control-sm">
                                    <label for="razon_factura" style="font-weight: 300; margin-bottom: 0;">Razón
                                        Social</label>
                                </div>
                                <div class="col-sm-12">
                                    <input name="ruc_factura" id="ruc_factura" type="text"
                                        class="form-control form-control-sm">
                                    <label for="ruc_factura" style="font-weight: 300; margin-bottom: 0;">RUC /
                                        C.I.No.</label>
                                </div>
                                <div class="col-sm-12">
                                    <input name="telefono_factura" id="telefono_factura" type="text"
                                        class="form-control form-control-sm">
                                    <label for="telefono_factura"
                                        style="font-weight: 300; margin-bottom: 0;">Teléfono</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input name="direccion_factura" id="direccion_factura" type="text"
                                        class="form-control form-control-sm">
                                    <label for="direccion_factura"
                                        style="font-weight: 300; margin-bottom: 0;">Dirección</label>
                                </div>
                                <div class="col-sm-12" style="text-align: right;">
                                    <button onclick="agregar_factura()" class="btn btn-sm btn-success"
                                        style="margin-top: 24px; border-radius: 30px; padding-left: 20px; padding-right: 20px;">+
                                        Agregar Datos de factura</button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="text-align: right; margin-top: 40px;">
                            <span class="form-text" style="color: #909090; font-weight: 300;  margin-bottom: 20px;">*Si
                                desea usar sólo sus datos de información personal, <br>omita los datos de
                                facturación</span><br>
                            <a href="#" class="btn btn-outline-primary btn-sm" onclick="desplazar_izquierda()"
                                style="border-radius: 30px; padding: 5px 30px;">Omitir</a>
                            <button href="#" id="btn_siguiente_datos" disabled class="btn btn-primary btn-sm"
                                onclick="desplazar_izquierda()"
                                style="border-radius: 30px; padding: 5px 20px;">Siguente</button>
                            <span id="mensaje_siguiente" style="color: #909090; font-weight: 300;">*Si desea usar sólo sus
                                datos de información personal</span>
                        </div>
                    </div>
                </div>

                <style>
                    #mensaje_siguiente {
                        display: none;
                    }

                    .btn-outline-primary {
                        border: 1px solid #1677BB;
                        color: #1677BB !important;
                    }

                    .btn-outline-primary:hover {
                        background: #1677BB;
                        color: #fff !important;
                        border: 1px solid #1677BB;
                    }

                    .step {
                        padding: 40px 50px;
                        border-radius: 15px;
                    }

                    #step-1 {
                        position: absolute;
                        top: 0;
                        left: 0;
                        background: #fff;
                        z-index: 20000;
                        border: 1px solid #cecece;
                    }

                    #step-2 {
                        position: absolute;
                        top: 0;
                        left: 0;
                        background: #fff;
                        z-index: 19999;
                        display: none;
                        border: 1px solid #cecece;
                    }

                    #step-3 {
                        position: absolute;
                        top: 0;
                        left: 0;
                        background: #fff;
                        z-index: 19998;
                        display: none;
                        border: 1px solid #cecece;
                    }

                    #step-4 {
                        position: absolute;
                        top: 0;
                        left: 0;
                        background: #fff;
                        z-index: 19997;
                        display: none;
                        border: 1px solid #cecece;
                    }

                    .contenedor input {
                        border: none;
                        border-bottom: 1px solid #c5c5c5;
                    }

                    #myTabFactura {
                        border-bottom: none;
                    }

                    .direccion_asignado {
                        width: 100%;
                        border: 1px solid #d5d5d5;
                        border-radius: 5px;
                    }

                    .direccion_asignado a {
                        color: #909090;
                    }

                    .nav-tabs .nav-link.active,
                    .nav-tabs .nav-item.show .nav-link {
                        border: 2px solid #1677BB;
                        border-radius: 5px;
                    }

                    @media screen and (max-width: 991px) {
                        .step {
                            padding: 10px 10px;
                        }

                        .step p {
                            font-size: 16px;
                        }

                        .form-text {
                            font-size: 12px;
                        }
                    }

                </style>

                <div class="col-sm-12 step" id="step-3">


                    <div class="col-sm-8" style="float: left;">
                        <p style="color: #1677BB;">Datos de Envío <span id="elegir_direccion" style="color: #F25E52;">*
                                Elija una dirección, si no lo tiene debe agregar una dirección</span></p>
                        <div class="col-sm-12" id="agregar_direccion_envio" style="padding: 0; overflow: hidden;">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input id="address" class="form-control form-control-sm" type="text"
                                        placeholder="escriba su dirección" />
                                    <label style="font-weight: 300; margin-bottom: 0;">Dirección *</label>
                                </div>
                                <div class="col-sm-4" style="margin-top: 5px;">
                                    <input type="hidden" id="coords" />
                                    <input id="city" class="form-control form-control-sm" type="text"
                                        placeholder="escriba su ciudad" />
                                    <label style="font-weight: 300; margin-bottom: 0;">Ciudad *</label>
                                </div>
                                <div class="col-sm-8" style="margin-top: 5px;">
                                    <input id="referencia" class="form-control form-control-sm" type="text"
                                        placeholder="Ej. porton negro" />
                                    <label style="font-weight: 300; margin-bottom: 0;">Referencia</label>
                                </div>
                                <div class="col-sm-6">
                                    <button id="submit" class="btn btn-secondary btn-sm"
                                        style="margin-top: 24px; width: 150px; border-radius: 30px;">
                                        <img src="../img/icon/search-w.svg" style="width: 20px;"> Buscar
                                    </button>

                                    <button onclick="agregar_direccion_envio()" class="btn btn-success btn-sm" type="button"
                                        style="margin-top: 24px; width: 150px; border-radius: 30px;"> + Agregar
                                        dirección</button>
                                </div>

                            </div>
                        </div>

                        <style>
                            #elegir_direccion {
                                display: none;
                            }

                        </style>

                        <div class="col-sm-12" style="padding: 0; margin-top: 15px; margin-bottom: 15px;">
                            <div class="row mb-3">
                                <div class="col">
                                    <small class="form-text">
                                        Agregue su ciudad y la dirección. Nuestra pagina trabaja con geolocalización para su
                                        mayor presición para un mejor servicio.
                                    </small>

                                </div>
                            </div>
                            <div id="map"></div>

                        </div>
                    </div>

                    <div class="col-sm-4" style="float: left; padding: 0; margin-bottom: 10px;">
                        <div class="col-12">
                            <p style="color: #1677BB; margin-bottom: 0;">
                                Mis Direcciones
                            </p>
                            <span class="form-text" style="color: #909090; font-weight: 300; margin-bottom: 20px;"> *
                                Seleccione una de sus direcciones</span>

                            <input type="hidden" id="id_direccion_asignada" class="form-control form-control-sm">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                @foreach ($direccion as $dir)
                                    <li class="nav-item direccion_asignado" style="margin-bottom: 10px;">
                                        <a onclick="asignar_direccion('{{ $dir->id_direccion_envio }}')" class="nav-link"
                                            id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                                            aria-selected="true">
                                            <p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Dirección: <span
                                                    style="font-weight: 600;">{{ $dir->direccion_envio }}</span></p>
                                            <p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Referencias:
                                                <span
                                                    style="font-weight: 600;">{{ $dir->referencia_direccion_envio }}</span>
                                            </p>
                                            <p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Teléfono: <span
                                                    style="font-weight: 600;">{{ Auth::user()->telefono_users }}</span>
                                            </p>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-12" style="margin-top: 40px; padding: 15px; overflow: hidden;">
                        <div style="float: left; width: 50%; text-align: left;">
                            <a href="#" onclick="volver_ventana2()" class="btn btn-secondary btn-sm"
                                style="padding: 5px 20px; border-radius: 30px;">Volver</a>
                        </div>
                        <div style="float: left; width: 50%; text-align: right;">
                            <button id="btn_siguiente_datos_3" disabled href="#" onclick="desplazar_izquierda3()"
                                class="btn btn-primary btn-sm"
                                style="padding: 5px 20px; border-radius: 30px;">Siguente</button>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 step" id="step-4">
                    <div class="col-sm-12" style="padding: 0 15px 15px 15px;">
                        <p style="color: #1677BB; margin-bottom: 20px;">Tu Pedido</p>
                        <div class="col-sm-12" style="background: #f9f9f9; padding: 30px 15px; border-radius: 20px;">

                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th style="border: none;">SubTotal</th>
                                        <td style="border: none;">Gs. <span id="total_factura" style="display: none;"><?php echo $sum; ?></span><?php
                                            echo number_format($sum); ?> </td>
                                    </tr>
                                    <tr>
                                        <th style="border: none;">Shipping / Costo de envio</th>
                                        <td style="border: none;">Gs. <span id="shipping_precio" style="display: none;">
                                                20000</span> 20.000</td>
                                    </tr>
                                    <tr>
                                        <th style="border: none; color: #1677BB;">Total a pagar</th>
                                        <td style="border: none; color: #1677BB; font-weight: bold;">
                                            Gs. <?php echo number_format($sum + 20000); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-12" style="padding: 20px 0;">
                            <p style="color: #1677BB; margin-bottom: 0;">*Importante</p>
                            <span class="form-text" style="color: #909090; font-weight: 300;">Una vez realizada la compra de
                                las {{ $cant }} prendas, el método de pago se debe realizar con transferencia y/o
                                depósito bancario</span>
                        </div>
                    </div>

                    <div class="col-sm-12" style=" padding: 0 15px 15px 15px; margin-top: 20px; margin-bottom: 20px;">
                        <div style="float: left; width: 50%; text-align: left;">
                            <a href="#" class="btn btn-secondary btn-sm" style="padding: 5px 20px; border-radius: 30px;"
                                onclick="volver_ventana3()">Volver</a>
                        </div>
                        <div style="float: right; width: 50%; text-align: right;">
                            <button class="btn btn-primary btn-sm" style="padding: 5px 20px; border-radius: 30px;"
                                onclick="realizar_pedido()" class="btn btn-primary">Realizar Pedido</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        #btn_abrir_nueva_factura:focus {
            text-decoration: none;
        }

        #map {
            height: 400px;
        }

        .asignar_direccion {

            padding: 15px;
            margin-bottom: 10px;
        }

        .direccion_asignado {
            margin-bottom: 10px;
            background: #f9f9f9;
            border: 1px solid #f9f9f9;
        }

    </style>

    <script type="text/javascript">
        function agregar_factura() {
            $('#agregar_factura').toggle({
                "display": "block"
            });
        }

        function agregar_direccion_envio() {
            var city = $('#city').val();
            var direccion = $('#address').val();
            var coords = $('#coords').val();
            var id = $('#id').val();
            var referencia = $('#referencia').val();
            var dir = "";
            $.ajax({
                type: "ajax",
                url: "{{ route('caja.direccionEnvio') }}",
                type: 'POST',
                data: {
                    city: city,
                    direccion: direccion,
                    coords: coords,
                    id: id,
                    referencia: referencia,
                    _token: '{{ csrf_token() }}'
                },
                success: function(data) {
                    $.ajax({
                        type: "ajax",
                        url: "{{ route('caja.buscarDireccionEnvioLast') }}",
                        type: 'get',
                        data: {
                            id: id
                        },
                        success: function(data) {
                            $('#id_direccion_asignada').val(data.id_direccion_envio);
                        }
                    });
                    $.ajax({
                        type: "ajax",
                        url: "{{ route('caja.buscarDireccionEnvio') }}",
                        type: 'GET',
                        data: {
                            id: id
                        },
                        success: function(data) {
                            //console.log(data);

                            for (var i = 0; i < data.length; i++) {
                                if (data[i].referencia_direccion_envio) {
                                    var referencia = data[i].referencia_direccion_envio;
                                } else {
                                    var referencia = "";
                                }
                                dir += '<li class="nav-item direccion_asignado">' +
                                    '<a onclick="asignar_direccion(\'' + data[i]
                                    .id_direccion_envio +
                                    '\')" class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">' +
                                    '<p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Dirección: <span style="font-weight: 600;">' +
                                    data[i].direccion_envio + '</span></p>' +
                                    '<p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Referencias: <span style="font-weight: 600;">' +
                                    referencia + '</span></p>' +
                                    '<p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Teléfono: <span style="font-weight: 600;">' +
                                    data[i].telefono_users + '</span></p>' +
                                    '</a>' +
                                    '</li>',

                                    $("#myTab").empty();
                                $("#myTab").append(dir);
                            }
                        }
                    });
                }
            });

            $("#btn_siguiente_datos_3").prop('disabled', false);

        };

        function agregar_factura() {
            var razon = $('#razon_factura').val();
            var ruc = $('#ruc_factura').val();
            var tel = $('#telefono_factura').val();
            var dir = $('#direccion_factura').val();
            var id_user = $('#id').val();
            var table = "";
            $.ajax({
                type: "ajax",
                url: "{{ route('caja.agregarFactura') }}",
                type: 'POST',
                data: {
                    razon: razon,
                    ruc: ruc,
                    dir: dir,
                    tel: tel,
                    _token: '{{ csrf_token() }}'
                },
                success: function(data) {
                    $.ajax({
                        type: "ajax",
                        url: "{{ route('caja.buscarFacturaLast') }}",
                        type: 'get',
                        data: {
                            id_user: id_user
                        },
                        success: function(data) {
                            $('#id_factura_asignada').val(data.id_datos_factura);
                        }
                    });

                    $.ajax({
                        type: "ajax",
                        url: "{{ route('caja.buscarFactura') }}",
                        type: 'get',
                        data: {
                            id_user: id_user
                        },
                        success: function(data) {
                            for (var i = 0; i < data.length; i++) {
                                if (data[i].direccion_datos_factura) {
                                    var direccion = data[i].direccion_datos_factura;
                                } else {
                                    var direccion = "";
                                }
                                if (data[i].telefono_datos_factura) {
                                    var telefono = data[i].telefono_datos_factura;
                                } else {
                                    var telefono = "";
                                }
                                table +=
                                    '<li class="nav-item direccion_asignado" style="margin-bottom: 10px; border-radius: 10px;">' +
                                    '<a onclick="asignarFactura(\'' + data[i].id_datos_factura +
                                    '\')" class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">' +
                                    '<p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Razón Social: <span style="font-weight: 600;">' +
                                    data[i].razon_datos_factura + '</span></p>' +
                                    '<p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Referencias: <span style="font-weight: 600;">' +
                                    data[i].ruc_datos_factura + '</span></p>' +
                                    '<p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Dirección: <span style="font-weight: 600;">' +
                                    direccion + '</span></p>' +
                                    '<p style="font-size: 14px; font-weight: 300; margin-bottom: 0;">Teléfono: <span style="font-weight: 600;">' +
                                    telefono + '</span></p>' +
                                    '</a>' +
                                    '</li>',

                                    $("#myTabFactura").empty();
                                $("#myTabFactura").append(table);
                                $("#btn_siguiente_datos").prop('disabled', false);
                            }

                        }
                    });
                }
            });
        }

        function asignarFactura(id) {
            $('#id_factura_asignada').val(id);
            $("#btn_siguiente_datos").prop('disabled', false);
        }


        function asignar_direccion(id) {
            $('#id_direccion_asignada').val(id);
            $("#btn_siguiente_datos_3").prop('disabled', false);
        }


        function realizar_pedido() {
            let date = new Date()
            //informacion personal
            var fecha = $('#fecha_pedido').val();
            // var hora = $('#hora_pedido').val();
            var hora = date.getHours() + ':' + date.getMinutes();
            var id_user = $('#id').val();
            var nombre = $('#name').val();
            var razon = $('#razon').val();
            var ruc = $('#ruc').val();
            var email = $('#email').val();
            var direccion = $('#direccion').val();
            var telefono = $('#telefono').val();

            //datos de facturacion
            var razon_factura = $('#razon_factura').val();
            var ruc_factura = $('#ruc_factura').val();
            var telefono_factura = $('#telefono_factura').val();
            var direccion_factura = $('#direccion_factura').val();
            var total_factura = $('#total_factura').html();
            var shipping = $('#shipping_precio').html();
            var id_factura = $('#id_factura_asignada').val();

            //datos de envio
            var id_datos_envio = $('#id_direccion_asignada').val();

            if (id_datos_envio == null || id_datos_envio == "") {
                // $('#elegir_direccion').css({"display":"block"});
                // $('html, body').animate({scrollTop:$('#scroll').offset().top}, 'slow');
                console.log('error');

            } else {
                $.ajax({
                    type: "ajax",
                    url: "{{ route('caja.realizarPedido') }}",
                    type: 'POST',
                    data: {
                        fecha: fecha,
                        hora: hora,
                        id_user: id_user,
                        nombre: nombre,
                        razon: razon,
                        ruc: ruc,
                        email: email,
                        direccion: direccion,
                        telefono: telefono,
                        razon_factura: razon_factura,
                        telefono_factura: telefono_factura,
                        direccion_factura: direccion_factura,
                        total_factura: total_factura,
                        shipping: shipping,
                        id_factura: id_factura,
                        id_datos_envio: id_datos_envio,
                        _token: '{{ csrf_token() }}'
                    },
                    success: function(data) {
                        window.location.replace("/pedidoFinalizado");
                    }
                });
            }
        }

        function desplazar_izquierda() {
            $('#step-1').animate({
                left: -1500
            }, 300);
            $('#step-3').fadeIn('slow', function() {
                $('#step-3').css({
                    "display": "block"
                });
                $('body, html').animate({
                    scrollTop: 0
                }, 100);
            });

        }

        function desplazar_izquierda3() {
            $('#step-3').animate({
                left: -1500
            }, 300);
            $('#step-4').fadeIn('slow', function() {
                $('#step-4').css({
                    "display": "block"
                });
                $('body, html').animate({
                    scrollTop: 0
                }, 100);
            });
        }

        function volver_ventana1() {
            $('#step-1').animate({
                left: -0
            }, 300);
            $('#step-2').fadeOut('slow', function() {
                $('#step-2').css({
                    "display": "none"
                });
            });
        }

        function volver_ventana2() {
            $('#step-1').animate({
                left: -0
            }, 300);
            $('#step-3').fadeOut('slow', function() {
                $('#step-3').css({
                    "display": "none"
                });
            });
        }

        function volver_ventana3() {
            $('#step-3').animate({
                left: -0
            }, 300);
            $('#step-4').fadeOut('slow', function() {
                $('#step-4').css({
                    "display": "none"
                });
            });
        }

    </script>

@endsection
