@extends('layouts.app')

@section('content')

    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Categorias</li>
            </ul>
        </div>
    </div>

    <section>
        <div class="col-sm-12" style="padding: 10px;">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-body" style="padding: 0;">
                                <div class="form-inline">
                                    @csrf
                                    <div class="form-group">
                                        <label for="inlineFormInput" class="sr-only">Agregar categorias</label>
                                        <input id="nombre" type="text" placeholder="Ej. Jeans" class="mr-3 form-control">
                                    </div>
                                    <div class="form-group">
                                        <button onclick="agregar_categorias()" class="mr-3 btn btn-primary">Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Activos
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_categorias" id="activas">
                                        @foreach ($categoriasActivas as $c)
                                            <tr data-id="{{ $c->id_categorias }}">
                                                <th scope="row">{{ $c->id_categorias }}</th>
                                                <td contenteditable="true" class="column_name"
                                                    data-column_name="nombre_categorias"
                                                    data-id="{{ $c->id_categorias }}">
                                                    {{ $c->nombre_categorias }}</td>
                                                <td>
                                                    <i class="fa fa-arrow-right cambiar_estado p-1"
                                                        data-id="{{ $c->id_categorias }}" data-nuevo-estado="false"
                                                        data-column-name="active_categorias"></i>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Inactivos
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-categorias" id="inactivas">
                                        @foreach ($categoriasInactivas as $c)
                                            <tr data-id="{{ $c->id_categorias }}">
                                                <th scope="row">{{ $c->id_categorias }}</th>
                                                <td contenteditable="true" class="column_name"
                                                    data-column_name="nombre_categorias"
                                                    data-id="{{ $c->id_categorias }}">
                                                    {{ $c->nombre_categorias }}</td>
                                                <td>
                                                    <i class="fa fa-arrow-left cambiar_estado p-1"
                                                        data-id="{{ $c->id_categorias }}" data-nuevo-estado="true"
                                                        data-column-name="active_categorias"></i>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        const url_tabla = "/categorias/"

        function agregar_categorias() {
            var nombre = document.getElementById("nombre").value;
            var id_stock = "";
            console.log(nombre);
            $.ajax({
                type: "ajax",
                url: "{{ route('categorias.store') }}",
                type: 'POST',
                data: {
                    nombre: nombre,
                    _token: '{{ csrf_token() }}'
                },

                success: function() {

                    // var categoria = $(this).val();
                    $.get('/tabla_categorias', function(data) {
                        //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
                        console.log(data);
                        for (var i = 0; i < data.length; i++)
                            id_stock += '<tr data-id="'+data[i].id_categorias+'">' +
                            '<td>' + data[i].id_categorias + '</td>' +
                            '<td contenteditable="true" class="column_name" data-column_name="nombre_categorias" data-id="' +
                            data[i].id_categorias + '">' + data[i].nombre_categorias + '</td>' +
                            '<td><i class="fa fa-arrow-right cambiar_estado p-1" data-id="' + data[
                                i]
                            .id_categorias +
                            '" data-nuevo-estado="false" data-column-name="active_categorias"></i></td>' +
                            '</tr>',


                            $("#activas").empty();
                        $('#activas').append(id_stock);
                    })


                }
            })
        };


        //nueva fila para las tablas de estado
        function nuevaFilaEstado(data) {
            let arrow = ""
            let nuevoEstado
            let fila = ""
            if (data.active_categorias) {
                arrow = "right"
                nuevoEstado = "false"
            } else {
                arrow = "left"
                nuevoEstado = "true"
            }
            fila = `
            
            <tr data-id="${data.id_categorias}">
                <th scope="row">${data.id_categorias}</th>
                <td contenteditable="true" class="column_name"
                    data-column_name="nombre_categorias" data-id="${data.id_categorias}">
                    ${data.nombre_categorias}</td>
                <td>
                    <i class="fa fa-arrow-${arrow} cambiar_estado p-1"
                        data-id="${data.id_categorias}" data-nuevo-estado="${nuevoEstado}"
                        data-column-name="active_categorias"></i>
                </td>
            </tr>
            `;
            //se agrega la nueva fila a la tabla activos o inactivos
            if (data.active_categorias) {
                $("#activas").append(fila)
            } else {
                $("#inactivas").append(fila)
            }
        }
    </script>

@endsection
