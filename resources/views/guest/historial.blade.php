@extends('layouts.front')

@section('content')

    <div class="container-lg">
        <div class="col-sm-12 title-shop">

            <h1 style="font-weight: 900; font-family: 'Montserrat', sans-serif;">Tus Compras</h1>
            <p>Podrás ver tanto tus compras recientes, pasadas y
                también para volver a pedir lo mismo de siempre!</p>

        </div>
        <style>
            .title-shop {
                text-align: center;
                padding: 80px 0 80px 0;
            }

            .title-shop h1 {
                font-size: 100px;
                font-weight: 900;
            }

            .title-shop p {
                font-size: 17px;
                font-weight: 100;
                color: #909090;
                margin-top: 0px;
                margin-bottom: 0;
            }

            a:hover {
                text-decoration: none;
            }

            a:focus {
                box-shadow: none;
                text-decoration: none;
            }


            @media screen and (max-width: 991px) {
                .title-shop h1 {
                    font-size: 70px;
                }
            }

            @media screen and (max-width: 767px) {
                .title-shop h1 {
                    font-size: 50px;
                }
            }

            #detalle_pedido_modal {
                font-size: 15px;
            }

        </style>


    </div>

    <div clas="col-sm-12 content-shop-container" style="padding: 0px 0 100px 0; overflow: hidden;">
        <div class="container-lg">
            <div class="col-sm-12" style="padding: 0;">
                @foreach ($user as $u)
                    <div class="col-sm-12"
                        style="background: #f9f9f9; padding: 30px 20px; border-radius: 20px; overflow: hidden; margin-bottom: 20px;">

                        <div class="col-12"
                            style="border-bottom: 1px solid #e5e5e5; padding-bottom: 10px; overflow: hidden;">
                            <div class="col-6" style="float: left; padding: 0;">
                                <span style="color: #808080;">Cliente: {{ $u->name }}</span>
                            </div>
                            <div class="col-6" style="float: left; text-align: right; padding: 0;">
                                <span style="color: #808080;">N˚ de pedido: {{ $u->id_pedido }}</span>
                            </div>
                        </div>

                        <div class="col-12" style="padding: 0; overflow: hidden;">
                            <div class="col-6" style="float: left;">
                                <span style="color: #1677BB; margin-top: 10px; margin-bottom: 5px;">Dato de
                                    Facturación</span><br>
                                <span style="color: #808080; font-weight: 300;">Razón Social:
                                    {{ $u->razon_datos_factura }}</span><br>
                                <span style="color: #808080; font-weight: 300;">Dirección de envio:
                                    {{ $u->direccion_envio }}</span><br>
                                <span style="color: #808080; font-weight: 300;">Teléfono:
                                    {{ $u->telefono_datos_factura }}</span><br>

                            </div>

                            <div class="col-6" style="float: left;">
                                <span style="color: #1677BB; margin-top: 10px; margin-bottom: 5px;">Transacción</span><br>
                                <span style="color: #808080; font-weight: 300;">Fecha: {{ $u->fecha_pedido }}</span><br>
                                <span style="color: #808080; font-weight: 300;">Gasto de envio: Gs.
                                    {{ number_format($u->shipping_pedido) }}</span><br>
                                <span style="color: #808080; font-weight: 300;">Monto pagado: Gs.
                                    {{ number_format($u->total_pedido) }}</span><br>
                            </div>
                        </div>

                        @if ($u->estado_pedido == 'Pendiente')
                            @if ($u->id_documentos)
                                <div class="col-12 btn-documentos">
                                    <span style="color: #1677BB;">Estado: {{ $u->estado_pedido }}<br>
                                        Combrobante enviado</span>
                                </div>
                            @else
                                <div class="col-12 btn-documentos">
                                    <span style="color: #1677BB;">Estado: {{ $u->estado_pedido }}</span> <br>
                                    <a href="#" onclick="enviarDocumentos('{{ $u->id_pedido }}', '{{ $u->name }}')"
                                        data-toggle="modal" data-target="#modalDocumentos" class="btn btn-success btn-sm"
                                        style="border-radius: 30px; margin-top: 10px; padding: 5px 20px;">Adjuntar
                                        comprobante</a>
                                </div>
                            @endif
                        @else
                            <div class="col-12 btn-documentos">
                                <span style="color: #1677BB;">Estado: {{ $u->estado_pedido }}</span> <br>
                                <a href="#" onclick="enviarDetallePedido('{{ $u->id_pedido }}', '{{ $u->name }}')"
                                    data-toggle="modal" data-target="#exampleModal" class="btn btn-success btn-sm"
                                    style="border-radius: 30px; margin-top: 10px; padding: 5px 20px;">Volver a comprar</a>
                            </div>
                        @endif
                    </div>
                @endforeach
                <div class="col-sm-12" style="padding: 0; overflow: hidden; text-align: right;">
                    {{ $user->links() }}
                </div>

            </div>
        </div>
    </div>

    <style>
        .btn-documentos {
            float: left;
            text-align: right;
            overflow: hidden;
        }

        @media screen and (max-width: 767px) {
            .btn-documentos {
                text-align: center;
                margin-top: 20px;
            }
        }

    </style>

    <!-- Modal para subir documentos-->
    <div class="modal fade" id="modalDocumentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document" style="margin-top: 100px;">
            <div class="modal-content" style="border-radius: 20px;">
                <div class="modal-header">
                    <p style="margin: 0;">Subir imagen de su transacción</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="{{ route('guest.agregarComprobacion') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body" style="padding: 40px 30px 50px 30px;">
                        <span style="color: #808080;">N˚Pedido:<span id="span_id_pedido_documento"></span> Cliente: <span
                                id="nombre_id_pedido_documento"></span></span>
                        <input type="hidden" name="id_pedido_documento" id="id_pedido_documento" class="form-control">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="file" name="file">
                            <label class="custom-file-label" for="customFile">Subir Imagen</label>
                        </div>

                        <div id="preview"></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"
                            style="border-radius: 30px; padding: 5px 20px;">Salir</button>
                        <button type="submit" class="btn btn-success" style="border-radius: 30px; padding: 5px 20px;">Subir
                            imagen</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <style>
        #preview img {
            width: 100%;
        }

        #file~.custom-file-label::after {
            content: "Elegir";
        }

    </style>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document" style="margin-top: 60px;">
            <div class="modal-content" style="border-radius: 20px; max-height: 80vh; overflow-y: auto;z-index: 1000;">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <span>Cliente: <span id="cliente_modal"></span></span> |
                    <span>N˚ Pedido: <span id="numero_pedido_modal"></span></span>

                    <div class="col-12" style="margin-top: 20px; padding: 0;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <span class="desktop">
                                            Código
                                        </span>
                                        <span class="mobile">
                                            Cód.
                                        </span>
                                    </th>
                                    <th>Cant.</th>
                                    <th>
                                        <span class="desktop">
                                            Descripción
                                        </span>
                                        <span class="mobile">
                                            Desc.
                                        </span>
                                    </th>
                                    <th class="no-detalle" style="display: none;">Talla</th>
                                    <th class="no-detalle" style="display: none;">Color</th>
                                    {{-- <th>Talla</th>
                <th>Color</th> --}}
                                    <th style="text-align: right;">
                                        <span class="desktop">
                                            Precio unitario
                                        </span>
                                        <span class="mobile">
                                            PU
                                        </span>
                                    </th>
                                    <th style="text-align: right;">Total</th>
                                </tr>
                            </thead>
                            <tbody id="detalle_pedido_modal">

                            </tbody>
                            <tfoot id="detalle_total_pedido">

                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                        style="border-radius: 30px; padding: 5px 20px;">Salir</button>
                    <button type="button" class="btn btn-success" style="border-radius: 30px; padding: 5px 20px;">Realizar
                        pedido</button>
                </div>
            </div>
        </div>
    </div>


    <script>
        function enviarDetallePedido(id, nombre) {
            $('#cliente_modal').html(nombre);
            $('#numero_pedido_modal').html(id);
            var detalle = "";
            var detalle_total = "";
            var total_general = 0;
            $.ajax({
                type: "ajax",
                url: "{{ route('guest.buscarDetallePedido') }}",
                type: 'get',
                data: {
                    id: id
                },
                success: function(data) {
                    console.log(data.length)
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            var precio = number_format(data[i].precio_detalle_pedido);
                            var total = (data[i].precio_detalle_pedido * data[i].cantidad_detalle_pedido);
                            var total_moneda = number_format(total);
                            console.log(data[i].detalle_pedido)
                            if (data[i].detalle_pedido != null) {
                                let producto_detalles = JSON.parse(data[i].detalle_pedido);
                                detalle += `<tr>
                                <td>${data[i].cod_producto_detalle_pedido}</td>
                                <td>${data[i].cantidad_detalle_pedido}</td>
                                <td>${data[i].nombre_producto_detalle_pedido}</td>
                                <td style="text-align: right;">${precio}</td>
                                <td style="text-align: right;">${total_moneda}</td>
                                </tr>`;
                                let rows_detalles = "";
                                rows_detalles += `
                                    <tr>
                                        <td colspan="5">
                                        <div class="row" style="border-bottom: 1px solid #bbbbbb;">
                                            <div class="col-4">
                                            Color
                                            </div>
                                            <div class="col-4 text-center">
                                            Talla
                                            </div>
                                            <div class="col-4 text-center">
                                            Cantidad
                                            </div>
                                        </div>
                                `;
                                producto_detalles.forEach(detail => {
                                    rows_detalles += `
                                        <div class="row detalles-row">
                                            <div class="col-4">
                                            ${detail.nombre_color}
                                            </div>
                                            <div class="col-4 text-center">
                                            ${detail.talla}
                                            </div>
                                            <div class="col-4 text-center">
                                            ${detail.cantidad}
                                            </div>
                                        </div>
                                    `;
                                });
                                rows_detalles += `
                                    </td>
                                </tr>
                                `;
                                detalle += rows_detalles;
                                $("#detalle_pedido_modal").empty();
                                $("#detalle_pedido_modal").append(detalle);
                                $(".no-detalle").css('display', 'none')
                            } else {
                                detalle += '<tr>' +
                                    '<td>' + data[i].cod_producto_detalle_pedido + '</td>' +
                                    '<td>' + data[i].cantidad_detalle_pedido + '</td>' +
                                    '<td>' + data[i].nombre_producto_detalle_pedido + '</td>' +
                                    '<td>' + data[i].talla_detalle_pedido + '</td>' +
                                    '<td>' + data[i].color_detalle_pedido + '</td>' +
                                    '<td style="text-align: right;">' + precio + '</td>' +
                                    '<td style="text-align: right;">' + total_moneda + '</td>' +
                                    '</tr>';
                                $(".no-detalle").css('display', 'table-cell')
                            }
                        }
                        $("#detalle_pedido_modal").empty();
                        $("#detalle_pedido_modal").append(detalle);
                        total_general += total;
                        detalle_total = '<tr style="font-weight: bold">' +
                            '<td colspan = "' + data[0].detalle_pedido != null ? 4 : 6 + '">TOTAL</td>' +
                            '<td style="text-align: right;">' + number_format(total_general) + '</td>' +
                            '</tr>',
                            $("#detalle_total_pedido").empty();
                        $("#detalle_total_pedido").append(detalle_total);
                    }
                }
            });
        }

        // Mostrar documento de archivo en input file
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
        //mostrar previzualizaciones de imagen
        document.getElementById("file").onchange = function(e) {
            // Creamos el objeto de la clase FileReader
            let reader = new FileReader();

            // Leemos el archivo subido y se lo pasamos a nuestro fileReader
            reader.readAsDataURL(e.target.files[0]);

            // Le decimos que cuando este listo ejecute el código interno
            reader.onload = function() {
                let preview = document.getElementById('preview'),
                    image = document.createElement('img');

                image.src = reader.result;

                preview.innerHTML = '';
                preview.append(image);
            };
        }

        function enviarDocumentos(id, nombre) {
            $('#id_pedido_documentos').val(id);
            $('#span_id_pedido_documento').html(id);
            $('#nombre_id_pedido_documento').html(nombre);
            $('#id_pedido_documento').val(id);
        }
    </script>


@endsection
