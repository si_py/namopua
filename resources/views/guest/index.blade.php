@php
$fundacion = new DateTime('2001-01-01');
$actual = new DateTime();

$anos_trayectoria = $fundacion->diff($actual)->y;
@endphp
@extends('layouts.front')

@section('content')

    {{-- @include('preloader') --}}

    <!-- sliders -->
  <div class="col-sm-12" id="principal-slider" style="padding: 0; overflow: hidden; background: #f5f5f5;">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel_principal">
      <div class="carousel-inner">
        @foreach ($slider as $s)
          <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
            <img class="d-block w-100" src="img/{{ $s->path_sliders }}" alt="{{ $s->titulo_sliders }}">
            <div style="position: absolute; top: 15%; left: 20%; color: #000; overflow: hidden; z-index: 29999;"></div>
          </div>
        @endforeach
      </div>
    </div>
  </div>

  <!-- end sliders -->

  <!-- sliders -->
  <div class="col-sm-12" id="principal-slider-mobile" style="padding: 0;">

    <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel_principal">

      <div class="carousel-inner">
          @foreach ($slider as $s)
              <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                  <a href="{{ $s->titulo_sliders }}">
                      <img class="d-block w-100" src="img/{{ $s->path_mobile_sliders }}"
                          alt="{{ $s->titulo_sliders }}">
                      <div
                          style="position: absolute; top: 15%; left: 20%; color: #000; overflow: hidden; z-index: 29999;">

                      </div>
                  </a>
              </div>
          @endforeach
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
      </a>
    </div>

    <!-- end sliders -->
    <style>
        #principal-slider-mobile {
            display: none;
        }

        @media screen and (max-width: 991px) {
            #principal-slider {
                display: none;
            }

            #principal-slider-mobile {
                display: block;
            }
        }

        p {
            font-family: 'Montserrat', sans-serif;
        }

        a {
            font-family: 'Montserrat', sans-serif;
        }

        h1 {
            font-family: 'Montserrat', sans-serif;
        }

        .subtitulo_slider {
          font-size: 40px;
          color: #E44C65;
          font-weight: bold;
        }

        .titulo_slider {
          font-size: 60px;
          color: #E44C65;
          font-weight: bold'
        }

        .parrafo_slider {
          font-size: 25px;
          font-weight: 600;
        }

        .slider_medio { 
          border-radius: 20px; 
          padding: 30px 50px;
          height: 400px;
          display: flex;
          justify-content: center;
          align-content: center;
          flex-direction: column;
        }

        .slider_der {
          height: 400px;
        }

        .slider_izq {
          height: 340px;
          overflow-y: scroll;
        }

        @media screen and (max-width: 991px) {
          .subtitulo_slider {
            font-size: 17px;
            color: #E44C65;
            font-weight: bold;
          }

          .titulo_slider {
            font-size: 25px;
            color: #E44C65;
            font-weight: bold'
          }

          .parrafo_slider {
            font-size: 15px;
            font-weight: 600;
          }

          .slider_medio {
            height: 180px;
          }

          .texto_exportacion {
            padding-bottom: 50px;
          }

          .slider_izq {
            display: none;
          }
        }

        @media screen and (max-width: 767px) {
          .slider_medio {
            height: 160px;
            padding: 10px 20px 0 20px;
          }
        }
    </style>
  </div>

  <div class="col-12 menu_central mb-5">
    <div class="contenedor_general">
      <div class="col-12">
        <div class="row">
          <div class="col-lg-3 p-2">
            <div class="col-12 mb-3" style="background: #E44C65; color: #fff; border-radius: 20px; padding: 10px 30px;">
              <a href="/shop" class="text-white">SHOP</a>
            </div>
  
            <div class="col-12 slider_izq" style="border-radius: 20px; background: #fff; padding: 30px;">
              @foreach($subcategorias as $cat)
                @if ($cat->nombre_categorias != 'Olimpia')
                  <a class="text-dark mb-3" href="{{ route('shop.subcategorias', $cat->id_subcategorias) }}" style="width: 100%;">
                    <div class="col-12">
                      {{ $cat->nombre_subcategorias }}
                    </div>
                  </a>
                @endif
              @endforeach
            </div>
          </div>
          <div class="col-lg-9 p-2">

            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                @foreach($carrusel as $caru)
                <div class="carousel-item {{ $loop->first ? 'active' : '' }}" style="background-image: url('img/{{ $caru->path_sliders }}'); background-size: cover; border-radius: 20px;">
                  <div class="col-lg-8 col-sm-7 col-9 slider_medio">
                    <h1 class="subtitulo_slider text-dark">{{ $caru->titulo_sliders }}</h1>
                    <h1 class="titulo_slider text-dark">{{ $caru->namin_titulo_sliders }}</h1>
                    <h1 class="parrafo_slider">{{ $caru->descripcion_sliders }}</h1>
                  </div>
                </div>
                @endforeach
                {{-- <div class="carousel-item" style="background-image: url('img/bg/olimpia-bg.jpeg'); background-size: cover; border-radius: 20px;">
                  <div class="col-9 slider_medio">

                  </div>
                </div> --}}
              </div>
             <button class="carousel-control-prev" type="button" data-target="#carouselExampleControls" data-slide="prev" style="width: 25px;">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </button>
              <button class="carousel-control-next" type="button" data-target="#carouselExampleControls" data-slide="next" style="width: 25px;">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </button>
            </div>

          
          </div>
        </div>
      </div>
    </div>
  </div>

 <style>
  .box-productos-a {
    color: #101010;
  }

  .swiper-button-next:after, .swiper-button-prev:after {
    font-family: swiper-icons;
    font-size: 20px;
    text-transform: none!important;
    letter-spacing: 0;
    font-variant: initial;
    line-height: 1;
  }

  .swiper-button-next, .swiper-button-prev {
    top: 35%;
  }

  .carousel-control-next, .carousel-control-prev {
    position: absolute;
    top: 0;
    bottom: 0;
    z-index: 1;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-pack: center;
    justify-content: center;
    width: 50px;
    border: none;
    background: transparent;
    color: #fff;
    text-align: center;
    opacity: .5;
    transition: opacity .15s ease;
  }

  .historia_empresa {
    padding-left: 50px;
    padding-right: 50px;
  }

  .banner_texto {
    padding: 50px;
  }

  @media screen and (max-width: 767px) {
    .historia_empresa {
      padding-left: 20px;
      padding-right: 20px;
    }

    .banner_texto {
      padding: 25px;
    }
  }

 </style>

{{-- CLASICOS TODO EL ANO --}}
  <div class="col-12 mb-4">
    <div class="contenedor_general">
      <div class="col-lg-12 col-12 mb-3 p-0">
        <div class="col-12 mb-1 p-1">
          <div class="col-12">
            <p>
              <b>CLÁSICOS TODO EL AÑO<i class="fa fa-chevron-right" style="margin-left: 10px;"></i></b>
              {{-- <button class="btn btn-primary btn-sm" style="float: right; border-radius: 20px; padding: 5px 10px; margin-top: -5px;">
                VER MÁS PRODUCTOS <i class="fa fa-chevron-right" style="margin-left: 10px;"></i>
              </button> --}}
            </p>
          </div>
          <div class="col-12 p-0">
            <div class="swiper">
              <div class="swiper-wrapper">
                @foreach ($clasicos as $da)
                <div class="swiper-slide p-2">
                  <a class="box-productos-a" href="{{ route('shop.producto', $da['id_productos']) }}">
                    @if ($da['tipo_producto'] == 'Unisex')
                        <img class=""
                            src="img/productos/{{ count($da['imagen_principal']) > 4 ? ($da['imagen_principal'][3]['path_imagen'] ?? '') : ($da['imagen_principal'][0]['path_imagen'] ?? '') }}"
                            alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                            style="width: 100%; border-radius: 20px;" />
                    @else
                        <img class=""
                            src="img/productos/{{ $da['imagen_principal'][0]['path_imagen'] ?? '' }}"
                            alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                            style="width: 100%; border-radius: 20px;" />
                    @endif
                    
                    <div class="col-12 p-3">
                      <p class="m-0" style="font-size: 12px;">Código: {{ $da['codigo_productos'] }}</p>
                      <p class="m-0">{{ mb_strtoupper($da['nombre_productos']) }}</p>

                      <div class="col-sm-12 p-0 mt-1 mb-1" style="overflow: hidden;">

                        @foreach ($color as $col)
                          @if($col->pk_productos_color_imagen == $da['id_productos'])
                            <span style="background: {{ $col->prefijo_color }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                          @endif
                        @endforeach
    
                      </div>

                      @if (is_null($da['precio_descuento_productos']))

                        <div class="row">
                          <div class="col-lg-6 guaranies">
                            <span>G.</span>
                            {{ number_format($da['mayorista'], 0, ',', '.') }}
                          </div>
                          <div class="col-lg-6 dolares">
                            <span>$.</span>
                            <?php 
                              $dolar = $da['mayorista'] / $divisa->divisa;
                            ?>
                            {{ number_format($dolar, 2, ',', '.') }}
                          </div>
                        </div>
                          
                        @else
                            <span style="color: #F25E52;"><span>G.</span>
                                {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}</span>

                            <div class="row">
                              <div class="col-6 guaranies" style="color: #F25E52;">
                                <span style="color: #F25E52;">G.</span>
                                {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}
                              </div>
                              <div class="col-6 dolares" style="color: #F25E52;">
                                <span style="color: #F25E52;">$.</span>
                                <?php 
                                  $dolar = $da['precio_descuento_productos'] / $divisa->divisa;
                                ?>
                                {{ number_format($dolar, 2, ',', '.') }}
                              </div>
                            </div>
                        @endif

                    </div>
                  </a>
                  @if ($da['descuentos_productos'] == 1)
                    <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                        OFERTA!
                    </div>
                  @endif
                  @if ($da['premium'] == 1)
                    <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                        PREMIUM
                    </div>
                  @endif
                  @if ($da['lanzamientos_productos'] == 1)
                    <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                        LANZAMIENTO
                    </div>
                  @endif
                            
                </div>
                @endforeach
              </div>
              <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>


  <div class="col-12">
    <div class="contenedor_general">
      <div class="col-lg-12 col-12 mb-3">
        
        <div class="col-12 bg-white" style="border-radius: 20px; overflow: hidden;">
          <div class="row">
            <div class="col-md-5" style="height: 298px; background-image: url('img/photos/frabrica-de-jeans-en-paraguay.jpg'); background-size: cover; background-position: center;">

            </div>
            <div class="col-md-7 historia_empresa" style="display: flex;
                                                          justify-content: center;
                                                          align-content: center;
                                                          flex-direction: column;
                                                          height: 298px;">
              <h1>ÑAMOPU'A PARAGUAY S.A.</h1>
              <p class="m-0">
                Somos una Industria Paraguaya con más de 42 años de trayectoria familiar de fábrica de jeans en Paraguay a buen precio, 
                hechas con mano de obra 100% nacional, desde el diseño y confección de los jeans, hasta la distribución de los mismos a 
                todo el rincón del País, generando así una fuente de trabajo para cientos de personas, que prestan atención hasta al más 
                mínimo detalle de producción para ofrecer lo mejor a nuestros clientes.
              </p>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  {{-- SLIDER MUJERES --}}
  <div class="col-12 mt-5">
    <div class="contenedor_general">
      <div class="col-lg-12 col-12 mb-0 p-0">
        <div class="col-12 mb-1 p-1">
          <div class="col-12">
            <p>
              <b>DAMAS <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></b>
              <a href="/busqueda?texto=Dama" class="btn btn-primary btn-sm" style="float: right; border-radius: 20px; padding: 5px 10px; margin-top: -5px; font-size: 12px;">
                VER MÁS <i class="fa fa-chevron-right" style="margin-left: 10px;"></i>
              </a>
            </p>
          </div>

          <div class="col-12">
            <div class="row">
              <div class="col-lg-5" style="overflow: hidden;">
                <div class="swiper_dama_unit" style="position:relative;">
                  <div class="swiper-wrapper">
                    @foreach ($clasicos as $da)
                    @if( $da['tipo_producto'] == 'Dama' )
                    <div class="swiper-slide p-0">
                      <a class="box-productos-a" href="{{ route('shop.producto', $da['id_productos']) }}">
                        @if ($da['tipo_producto'] == 'Unisex')
                            <img class=""
                                src="img/productos/{{ count($da['imagen_principal']) > 4 ? ($da['imagen_principal'][3]['path_imagen'] ?? '') : ($da['imagen_principal'][0]['path_imagen'] ?? '') }}"
                                alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                style="width: 100%; border-radius: 20px; margin-bottom: 20px;" />
                        @else
                            <img class=""
                                src="img/productos/{{ $da['imagen_principal'][0]['path_imagen'] ?? '' }}"
                                alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                style="width: 100%; border-radius: 20px; margin-bottom: 20px;" />
                        @endif
                        <div class="col-12 bg-white mb-3 p-3" style="border-radius:20px;">
                          <div class="row">
                            <div class="col-lg-5 p-3">
                              <h3 class="m-0">{{ mb_strtoupper($da['nombre_productos']) }}</h3>
                              <p class="m-0">Código: {{ $da['codigo_productos'] }}</p>
                            </div>
        
                            <div class="col-lg-7 p-3" style="text-align: right;">
                              <div class="col-12 mb-3" style="overflow: hidden;">
                                {{-- @foreach ($color as $col)
                                  @if($col->pk_productos_color_productos == $da['id_productos'])
                                    <span style="background: {{ $col->prefijo_color }}; width: 15px; height: 15px; float: right; border-radius: 50px; margin-left: 5px;"></span>
                                  @endif
                                @endforeach --}}
                                @foreach ($da['colores'] as $color)
                                  <span style="border: 1px solid #b1b1b1; background: {{ $color['prefijo_color'] }}; width: 15px; height: 15px; float: right; border-radius: 50px; margin-left: 5px;"></span>
                                @endforeach
                              </div>
                              <div class="col-12">
                                <h3 class="m-0">
                                  @if (is_null($da['precio_descuento_productos']))
            
                                    <div class="row">
                                      <div class="col-lg-6">
                                        <span>G.</span>
                                        {{ number_format($da['mayorista'], 0, ',', '.') }}
                                      </div>
                                      <div class="col-lg-6" style="text-align: right;">
                                        <span>$.</span>
                                        <?php 
                                          $dolar = $da['mayorista'] / $divisa->divisa;
                                        ?>
                                        {{ number_format($dolar, 2, ',', '.') }}
                                      </div>
                                    </div>
                                      
                                  @else
                                      <span style="color: #F25E52;"><span>G.</span>
                                          {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}</span>
          
                                      <div class="row">
                                        <div class="col-lg-6" style="color: #F25E52;">
                                          <span style="color: #F25E52;">G.</span>
                                          {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}
                                        </div>
                                        <div class="col-lg-6" style="text-align: right; color: #F25E52;">
                                          <span style="color: #F25E52;">$.</span>
                                          <?php 
                                            $dolar = $da['precio_descuento_productos'] / $divisa->divisa;
                                          ?>
                                          {{ number_format($dolar, 2, ',', '.') }}
                                        </div>
                                      </div>
                                  @endif
                                </h3>
                              </div>
        
                              <button class="btn btn-sm mt-3" style="float: right; border-radius: 20px; padding: 5px 20px; margin-top: -5px; background: #E44C65; color: #fff;">
                                VER MÁS PRODUCTOS <i class="fa fa-chevron-right" style="margin-left: 10px;"></i>
                              </button>
        
                            </div>
                          </div>
                          
                        </div>
                      </a>
                      @if ($da['descuentos_productos'] == 1)
                        <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                            OFERTA!
                        </div>
                      @endif
                      @if ($da['premium'] == 1)
                        <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                            PREMIUM
                        </div>
                      @endif
                      @if ($da['lanzamientos_productos'] == 1)
                        <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                            LANZAMIENTO
                        </div>
                      @endif
                                
                    </div>
                    @endif
                    @endforeach
                  </div>
                  <div class="swiper-button-next"></div>
                  <div class="swiper-button-prev"></div>
                </div>
                

              </div>
              <div class="col-lg-7" style="overflow: hidden;">

                <div class="swiper_dama" style="position:relative;">
                  <div class="swiper-wrapper">
                    @foreach ($damas as $da)
                    <div class="swiper-slide p-0">
                      <a class="box-productos-a" href="{{ route('shop.producto', $da['id_productos']) }}">
                        @if ($da['tipo_producto'] == 'Unisex')
                            <img class=""
                                src="img/productos/{{ count($da['imagen_principal']) > 4 ? ($da['imagen_principal'][3]['path_imagen'] ?? '') : ($da['imagen_principal'][0]['path_imagen'] ?? '') }}"
                                alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                style="width: 100%; border-radius: 20px;" />
                        @else
                            <img class=""
                                src="img/productos/{{ $da['imagen_principal'][0]['path_imagen'] ?? '' }}"
                                alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                style="width: 100%; border-radius: 20px;" />
                        @endif
                        
                        <div class="col-12 p-3">
                          <p class="m-0" style="font-size: 12px;">Cód: {{ $da['codigo_productos'] }}</p>
                          <p class="m-0">{{ mb_strtoupper($da['nombre_productos']) }}</p>
    
                          <div class="col-sm-12 p-0 mt-1 mb-1" style="overflow: hidden;">
    
                            {{-- @foreach ($color as $col)
                              @if($col->pk_productos_color_productos == $da['id_productos'])
                                <span style="background: {{ $col->prefijo_color }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                              @endif
                            @endforeach --}}
                            @foreach ($da['colores'] as $color)
                              <span style="border: 1px solid #b1b1b1; background: {{ $color['prefijo_color'] }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-left: 5px;"></span>
                            @endforeach
        
                          </div>
    
                          @if (is_null($da['precio_descuento_productos']))
    
                            <div class="row">
                              <div class="col-lg-6 guaranies">
                                <span>G.</span>
                                {{ number_format($da['mayorista'], 0, ',', '.') }}
                              </div>
                              <div class="col-lg-6 dolares">
                                <span>$.</span>
                                <?php 
                                  $dolar = $da['mayorista'] / $divisa->divisa;
                                ?>
                                {{ number_format($dolar, 2, ',', '.') }}
                              </div>
                            </div>
                              
                            @else
                                <span style="color: #F25E52;"><span>G.</span>
                                    {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}</span>
    
                                <div class="row">
                                  <div class="col-lg-6 guaranies" style="color: #F25E52;">
                                    <span style="color: #F25E52;">G.</span>
                                    {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}
                                  </div>
                                  <div class="col-lg-6 dolares" style="color: #F25E52;">
                                    <span style="color: #F25E52;">$.</span>
                                    <?php 
                                      $dolar = $da['precio_descuento_productos'] / $divisa->divisa;
                                    ?>
                                    {{ number_format($dolar, 2, ',', '.') }}
                                  </div>
                                </div>
                            @endif
    
                        </div>
                      </a>
                      @if ($da['descuentos_productos'] == 1)
                        <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                            OFERTA!
                        </div>
                      @endif
                      @if ($da['premium'] == 1)
                        <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                            PREMIUM
                        </div>
                      @endif
                      @if ($da['lanzamientos_productos'] == 1)
                        <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                            LANZAMIENTO
                        </div>
                      @endif
                                
                    </div>
                    @endforeach
                  </div>
                  <div class="swiper-button-next"></div>
                  <div class="swiper-button-prev"></div>
                </div>

                <div class="swiper_dama_2" style="position:relative;">
                  <div class="swiper-wrapper">
                    @foreach ($damas2 as $da)
                    <div class="swiper-slide p-0">
                      <a class="box-productos-a" href="{{ route('shop.producto', $da['id_productos']) }}">
                        @if ($da['tipo_producto'] == 'Unisex')
                            <img class=""
                                src="img/productos/{{ count($da['imagen_principal']) > 4 ? ($da['imagen_principal'][3]['path_imagen'] ?? '') : ($da['imagen_principal'][0]['path_imagen'] ?? '') }}"
                                alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                style="width: 100%; border-radius: 20px;" />
                        @else
                            <img class=""
                                src="img/productos/{{ $da['imagen_principal'][0]['path_imagen'] ?? '' }}"
                                alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                style="width: 100%; border-radius: 20px;" />
                        @endif
                        
                        <div class="col-12 p-3">
                          <p class="m-0" style="font-size: 12px;">Cód: {{ $da['codigo_productos'] }}</p>
                          <p class="m-0">{{ mb_strtoupper($da['nombre_productos']) }}</p>
    
                          <div class="col-sm-12 p-0 mt-1 mb-1" style="overflow: hidden;">
    
                            {{-- @foreach ($color as $col)
                              @if($col->pk_productos_color_productos == $da['id_productos'])
                                <span style="background: {{ $col->prefijo_color }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                              @endif
                            @endforeach --}}
                            @foreach ($da['colores'] as $color)
                              <span style="border: 1px solid #b1b1b1; background: {{ $color['prefijo_color'] }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-left: 5px;"></span>
                            @endforeach
        
                          </div>
    
                          @if (is_null($da['precio_descuento_productos']))
    
                            <div class="row">
                              <div class="col-lg-6 guaranies">
                                <span>G.</span>
                                {{ number_format($da['mayorista'], 0, ',', '.') }}
                              </div>
                              <div class="col-lg-6 dolares">
                                <span>$.</span>
                                <?php 
                                  $dolar = $da['mayorista'] / $divisa->divisa;
                                ?>
                                {{ number_format($dolar, 2, ',', '.') }}
                              </div>
                            </div>
                              
                            @else
                                <div class="row">
                                  <div class="col-lg-6 guaranies" style="color: #F25E52;">
                                    <span style="color: #F25E52;">G.</span>
                                    {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}
                                  </div>
                                  <div class="col-lg-6 dolares" style="color: #F25E52;">
                                    <span style="color: #F25E52;">$.</span>
                                    <?php 
                                      $dolar = $da['precio_descuento_productos'] / $divisa->divisa;
                                    ?>
                                    {{ number_format($dolar, 2, ',', '.') }}
                                  </div>
                                </div>
                            @endif
    
                        </div>
                      </a>
                      @if ($da['descuentos_productos'] == 1)
                        <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                            OFERTA!
                        </div>
                      @endif
                      @if ($da['premium'] == 1)
                        <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                            PREMIUM
                        </div>
                      @endif
                      @if ($da['lanzamientos_productos'] == 1)
                        <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                            LANZAMIENTO
                        </div>
                      @endif
                                
                    </div>
                    @endforeach
                  </div>
                  <div class="swiper-button-next"></div>
                  <div class="swiper-button-prev"></div>
                </div>

              </div>
            </div>
          </div>


        </div> 
      </div>
    </div>
  </div>

  {{-- BANNER DE EXPORTACIÓN --}}

  <div class="col-12" style="margin-top: -40px !important;">
    <div class="contenedor_general">
      <div class="col-12 p-4">
        <div class="col-lg-12 col-12 mb-3 bg-white banner_texto" style="border-radius: 20px;">
          <h1 class="subtitulo_slider">ENVÍOS INTERNACIONALES A </h1>
          <h1 class="titulo_slider">TODA LATINOAMÉRICA</h1>
          <div class="row mt-4">
            <div class="col-lg-8 texto_exportacion">
              <p class="parrafo_slider m-0" style="font-weight: 400;">
                A través de contenedor, en caso de estar interesado en adquirir nuestras prendas puede mandarnos su carta de presentación
                de empresa a través del siguiente formulario
              </p>
            </div>
            <div class="col-lg-4" style="position: relative;">
              <a href="{{ route('guest.vistaExportacion') }}" class="btn btn-sm" style="position: absolute; bottom: 0px; right: 0px; border-radius: 20px; padding: 5px 20px; background: #E44C65; color: #fff;">
                CLICK ACÁ <i class="fa fa-chevron-right" style="margin-left: 10px;"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- SLIDER HOMBRES --}}

  <div class="col-12 mt-0">
    <div class="contenedor_general">
      <div class="col-lg-12 col-12 mb-3" style="overflow: hidden;">
        <div class="col-12 mb-1 p-1">
          <div class="col-12">
            <p>
              <b>CABALLEROS <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></b>
              <a  href="/busqueda?texto=Hombre" class="btn btn-primary btn-sm" style="float: right; border-radius: 20px; padding: 5px 10px; margin-top: -5px; font-size: 12px;">
                VER MÁS  <i class="fa fa-chevron-right" style="margin-left: 10px;"></i>
              </a>
            </p>
          </div>
          <div class="col-12 p-0">
            <div class="swiper" style="overflow:inherit;">
              <div class="swiper-wrapper">
                @foreach ($hombres as $da)
                <div class="swiper-slide p-2">
                  <a class="box-productos-a" href="{{ route('shop.producto', $da['id_productos']) }}">
                    @if ($da['tipo_producto'] == 'Unisex')
                        <img class=""
                            src="img/productos/{{ count($da['imagen_principal']) > 4 ? ($da['imagen_principal'][3]['path_imagen'] ?? '') : ($da['imagen_principal'][0]['path_imagen'] ?? '') }}"
                            alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                            style="width: 100%; border-radius: 20px;" />
                    @else
                        <img class=""
                            src="img/productos/{{ $da['imagen_principal'][0]['path_imagen'] ?? '' }}"
                            alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                            style="width: 100%; border-radius: 20px;" />
                    @endif
                    
                    <div class="col-12 p-3">
                      <p class="m-0" style="font-size: 12px;">Cód: {{ $da['codigo_productos'] }}</p>
                      <p class="m-0">{{ mb_strtoupper($da['nombre_productos']) }}</p>

                      <div class="col-sm-12 p-0 mt-1 mb-1" style="overflow: hidden;">

                        {{-- @foreach ($color as $col)
                          @if($col->pk_productos_color_productos == $da['id_productos'])
                            <span style="background: {{ $col->prefijo_color }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                          @endif
                        @endforeach --}}

                        @foreach ($da['colores'] as $color)
                            <span style="border: 1px solid #b1b1b1; background: {{ $color['prefijo_color'] }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                        @endforeach
    
                      </div>

                      @if (is_null($da['precio_descuento_productos']))

                        <div class="row">
                          <div class="col-lg-6 guaranies">
                            <span>G.</span>
                            {{ number_format($da['mayorista'], 0, ',', '.') }}
                          </div>
                          <div class="col-lg-6 dolares">
                            <span>$.</span>
                            <?php 
                              $dolar = $da['mayorista'] / $divisa->divisa;
                            ?>
                            {{ number_format($dolar, 2, ',', '.') }}
                          </div>
                        </div>
                          
                        @else
                            <span style="color: #F25E52;"><span>G.</span>
                                {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}</span>

                            <div class="row">
                              <div class="col-lg-6 guaranies" style="color: #F25E52;">
                                <span style="color: #F25E52;">G.</span>
                                {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}
                              </div>
                              <div class="col-lg-6 dolares" style="color: #F25E52;">
                                <span style="color: #F25E52;">$.</span>
                                <?php 
                                  $dolar = $da['precio_descuento_productos'] / $divisa->divisa;
                                ?>
                                {{ number_format($dolar, 2, ',', '.') }}
                              </div>
                            </div>
                        @endif

                    </div>
                  </a>
                  @if ($da['descuentos_productos'] == 1)
                    <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                        OFERTA!
                    </div>
                  @endif
                  @if ($da['premium'] == 1)
                    <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                        PREMIUM
                    </div>
                  @endif
                  @if ($da['lanzamientos_productos'] == 1)
                    <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                        LANZAMIENTO
                    </div>
                  @endif
                            
                </div>
                @endforeach
              </div>
              <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  {{-- TALLES ESPECIALES --}}
  <div class="col-12 mt-5">
    <div class="contenedor_general">
      <div class="col-lg-12 col-12 mb-3" style="overflow: hidden;">
        <div class="col-12 mb-1 p-1">
          <div class="col-12">
            <p>
              <b>TALLAS ESPECIALES <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></b>
              <a href="/busqueda?texto=tallesEspeciales" class="btn btn-primary btn-sm" style="float: right; border-radius: 20px; padding: 5px 10px; margin-top: -5px; font-size: 12px;">
                VER MÁS <i class="fa fa-chevron-right" style="margin-left: 10px;"></i>
              </a>
            </p>
          </div>
          <div class="col-12 p-0">
            <div class="swiper" style="overflow:inherit;">
              <div class="swiper-wrapper">
                @foreach ($especiales as $da)
                <div class="swiper-slide p-2">
                  <a class="box-productos-a" href="{{ route('shop.producto', $da['id_productos']) }}">
                    @if ($da['tipo_producto'] == 'Unisex')
                        <img class=""
                            src="img/productos/{{ count($da['imagen_principal']) > 4 ? ($da['imagen_principal'][3]['path_imagen'] ?? '') : ($da['imagen_principal'][0]['path_imagen'] ?? '') }}"
                            alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                            style="width: 100%; border-radius: 20px;" />
                    @else
                        <img class=""
                            src="img/productos/{{ $da['imagen_principal'][0]['path_imagen'] ?? '' }}"
                            alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                            style="width: 100%; border-radius: 20px;" />
                    @endif
                    
                    <div class="col-12 p-3">
                      <p class="m-0" style="font-size: 12px;">Cód: {{ $da['codigo_productos'] }}</p>
                      <p class="m-0">{{ mb_strtoupper($da['nombre_productos']) }}</p>

                      <div class="col-sm-12 p-0 mt-1 mb-1" style="overflow: hidden;">

                        {{-- @foreach ($color as $col)
                          @if($col->pk_productos_color_productos == $da['id_productos'])
                            <span style="background: {{ $col->prefijo_color }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                          @endif
                        @endforeach --}}

                        @foreach ($da['colores'] as $color)
                            <span style="border: 1px solid #b1b1b1; background: {{ $color['prefijo_color'] }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                        @endforeach
    
                      </div>

                      @if (is_null($da['precio_descuento_productos']))

                        <div class="row">
                          <div class="col-lg-6 guaranies">
                            <span>G.</span>
                            {{ number_format($da['mayorista'], 0, ',', '.') }}
                          </div>
                          <div class="col-lg-6 dolares">
                            <span>$.</span>
                            <?php 
                              $dolar = $da['mayorista'] / $divisa->divisa;
                            ?>
                            {{ number_format($dolar, 2, ',', '.') }}
                          </div>
                        </div>
                          
                        @else
                            <span style="color: #F25E52;"><span>G.</span>
                                {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}</span>

                            <div class="row">
                              <div class="col-lg-6 guaranies" style="color: #F25E52;">
                                <span style="color: #F25E52;">G.</span>
                                {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}
                              </div>
                              <div class="col-lg-6 dolares" style="color: #F25E52;">
                                <span style="color: #F25E52;">$.</span>
                                <?php 
                                  $dolar = $da['precio_descuento_productos'] / $divisa->divisa;
                                ?>
                                {{ number_format($dolar, 2, ',', '.') }}
                              </div>
                            </div>
                        @endif

                    </div>
                  </a>
                  @if ($da['descuentos_productos'] == 1)
                    <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                        OFERTA!
                    </div>
                  @endif
                  @if ($da['premium'] == 1)
                    <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                        PREMIUM
                    </div>
                  @endif
                  @if ($da['lanzamientos_productos'] == 1)
                    <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                        LANZAMIENTO
                    </div>
                  @endif
                            
                </div>
                @endforeach
              </div>
              <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  {{-- SLIDER NINOS/AS --}}
  @if (count($nina) > 0 || count($ninos) > 0)
    <div class="col-12 mt-5">
      <div class="contenedor_general">
        <div class="col-lg-12 col-12 mb-3">
          <div class="col-12 mb-1 p-1">
            <div class="col-12">
              <p>
                <b>NIÑOS/AS <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></b>
                <a href="/busqueda?texto=Ninos" class="btn btn-primary btn-sm" style="float: right; border-radius: 20px; padding: 5px 10px; margin-top: -5px; font-size: 12px;">
                  VER MÁS <i class="fa fa-chevron-right" style="margin-left: 10px;"></i>
                </a>
              </p>
            </div>
            <div class="col-12 p-0">
              <div class="swiper">
                <div class="swiper-wrapper">
                  @foreach ($ninos as $da)
                  <div class="swiper-slide p-2">
                    <a class="box-productos-a" href="{{ route('shop.producto', $da['id_productos']) }}">
                      @if ($da['tipo_producto'] == 'Unisex')
                          <img class=""
                              src="img/productos/{{ count($da['imagen_principal']) > 4 ? ($da['imagen_principal'][3]['path_imagen'] ?? '') : ($da['imagen_principal'][0]['path_imagen'] ?? '') }}"
                              alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                              style="width: 100%; border-radius: 20px;" />
                      @else
                          <img class=""
                              src="img/productos/{{ $da['imagen_principal'][0]['path_imagen'] ?? '' }}"
                              alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                              style="width: 100%; border-radius: 20px;" />
                      @endif
                      
                      <div class="col-12 p-3">
                        <p class="m-0" style="font-size: 12px;">Cód: {{ $da['codigo_productos'] }}</p>
                        <p class="m-0">{{ mb_strtoupper($da['nombre_productos']) }}</p>

                        <div class="col-sm-12 p-0 mt-1 mb-1" style="overflow: hidden;">

                          {{-- @foreach ($color as $col)
                            @if($col->pk_productos_color_productos == $da['id_productos'])
                              <span style="background: {{ $col->prefijo_color }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                            @endif
                          @endforeach --}}

                          @foreach ($da['colores'] as $color)
                              <span style="border: 1px solid #b1b1b1; background: {{ $color['prefijo_color'] }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                          @endforeach
      
                        </div>

                        @if (is_null($da['precio_descuento_productos']))

                          <div class="row">
                            <div class="col-lg-6 guaranies">
                              <span>G.</span>
                              {{ number_format($da['mayorista'], 0, ',', '.') }}
                            </div>
                            <div class="col-lg-6 dolares">
                              <span>$.</span>
                              <?php 
                                $dolar = $da['mayorista'] / $divisa->divisa;
                              ?>
                              {{ number_format($dolar, 2, ',', '.') }}
                            </div>
                          </div>
                            
                          @else
                              <span style="color: #F25E52;"><span>G.</span>
                                  {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}</span>

                              <div class="row">
                                <div class="col-lg-6 guaranies" style="color: #F25E52;">
                                  <span style="color: #F25E52;">G.</span>
                                  {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}
                                </div>
                                <div class="col-lg-6 dolares" style="color: #F25E52;">
                                  <span style="color: #F25E52;">$.</span>
                                  <?php 
                                    $dolar = $da['precio_descuento_productos'] / $divisa->divisa;
                                  ?>
                                  {{ number_format($dolar, 2, ',', '.') }}
                                </div>
                              </div>
                          @endif

                      </div>
                    </a>
                    @if ($da['descuentos_productos'] == 1)
                      <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                          OFERTA!
                      </div>
                    @endif
                    @if ($da['premium'] == 1)
                      <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                          PREMIUM
                      </div>
                    @endif
                    @if ($da['lanzamientos_productos'] == 1)
                      <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                          LANZAMIENTO
                      </div>
                    @endif
                              
                  </div>
                  @endforeach
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  @endif

  {{-- BANNER DE EXPORTACIÓN --}}

  <div class="col-12 mt-0">
    <div class="contenedor_general">
      <div class="col-12 p-4">
        <div class="col-lg-12 col-12 mb-3 bg-white banner_texto" style="border-radius: 20px;">
          <h1 class="subtitulo_slider">Nº 1 EN VENTAS CON LOS MEJORES PRECIOS A NIVEL INTERNACIONAL</h1>
        </div>
      </div>
    </div>
  </div>


  {{-- SLIDER OPLIMPIA --}}
  <div class="col-12 mt-3 mb-5">
    <div class="contenedor_general">
      <div class="col-lg-12 col-12 mb-0 p-0">
        <div class="col-12 mb-1 p-1">
          <div class="col-12">
            <div class="row">
              <div class="col-lg-6 mb-2">
                <p>
                  <b> {{strtoupper('Productos bajo licencia de Club Olimpia') }} <i class="fa fa-chevron-right" style="margin-left: 10px;"></i></b>
                </p>
              </div>
              <div class="col-lg-6 mb-2">
                <a href="/TiendaOlimpia" class="btn btn-primary btn-sm" style="float: right; border-radius: 20px; padding: 5px 10px; margin-top: -5px; font-size: 12px;">
                  VER MÁS <i class="fa fa-chevron-right" style="margin-left: 10px;"></i>
                </a>
              </div>
            </div>
          </div>

          <div class="col-12">
            <div class="row">
              <div class="col-lg-5" style="overflow: hidden;">
                <div class="swiper_dama_unit" style="position:relative;">
                  <div class="swiper-wrapper">
                    @foreach ($olimpia as $da)
                    <div class="swiper-slide p-0">
                      <a class="box-productos-a" href="{{ route('shop.producto', $da['id_productos']) }}">
                        @if ($da['tipo_producto'] == 'Unisex')
                            <img class=""
                                src="img/productos/{{ count($da['imagen_principal']) > 4 ? ($da['imagen_principal'][3]['path_imagen'] ?? '') : ($da['imagen_principal'][0]['path_imagen'] ?? '') }}"
                                alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                style="width: 100%; border-radius: 20px; margin-bottom: 20px;" />
                        @else
                            <img class=""
                                src="img/productos/{{ $da['imagen_principal'][0]['path_imagen'] ?? '' }}"
                                alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                style="width: 100%; border-radius: 20px; margin-bottom: 20px;" />
                        @endif
                        <div class="col-12 bg-white mb-3 p-3" style="border-radius:20px;">
                          <div class="row">
                            <div class="col-lg-5 p-3">
                              <h3 class="m-0">{{ mb_strtoupper($da['nombre_productos']) }}</h3>
                              <p class="m-0">Código: {{ $da['codigo_productos'] }}</p>
                            </div>
        
                            <div class="col-lg-7 p-3" style="text-align: right;">
                              <div class="col-12 mb-3" style="overflow: hidden;">
                                {{-- @foreach ($color as $col)
                                  @if($col->pk_productos_color_productos == $da['id_productos'])
                                    <span style="background: {{ $col->prefijo_color }}; width: 15px; height: 15px; float: right; border-radius: 50px; margin-left: 5px;"></span>
                                  @endif
                                @endforeach --}}

                                @foreach ($da['colores'] as $color)
                                  <span style="border: 1px solid #b1b1b1; background: {{ $color['prefijo_color'] }}; width: 15px; height: 15px; float: right; border-radius: 50px; margin-left: 5px;"></span>
                                @endforeach

                              </div>
                              <div class="col-12">
                                <h3 class="m-0">
                                  @if (is_null($da['precio_descuento_productos']))
            
                                    <div class="row">
                                      <div class="col-lg-6">
                                        <span>G.</span>
                                        {{ number_format($da['mayorista'], 0, ',', '.') }}
                                      </div>
                                      <div class="col-lg-6" style="text-align: right;">
                                        <span>$.</span>
                                        <?php 
                                          $dolar = $da['mayorista'] / $divisa->divisa;
                                        ?>
                                        {{ number_format($dolar, 2, ',', '.') }}
                                      </div>
                                    </div>
                                      
                                  @else
                                      <span style="color: #F25E52;"><span>G.</span>
                                          {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}</span>
          
                                      <div class="row">
                                        <div class="col-lg-6" style="color: #F25E52;">
                                          <span style="color: #F25E52;">G.</span>
                                          {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}
                                        </div>
                                        <div class="col-lg-6" style="text-align: right; color: #F25E52;">
                                          <span style="color: #F25E52;">$.</span>
                                          <?php 
                                            $dolar = $da['precio_descuento_productos'] / $divisa->divisa;
                                          ?>
                                          {{ number_format($dolar, 2, ',', '.') }}
                                        </div>
                                      </div>
                                  @endif
                                </h3>
                              </div>
        
                              <button class="btn btn-sm mt-3" style="float: right; border-radius: 20px; padding: 5px 20px; margin-top: -5px; background: #E44C65; color: #fff;">
                                VER MÁS PRODUCTOS <i class="fa fa-chevron-right" style="margin-left: 10px;"></i>
                              </button>
        
                            </div>
                          </div>
                          
                        </div>
                      </a>
                      @if ($da['descuentos_productos'] == 1)
                        <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                            OFERTA!
                        </div>
                      @endif
                      @if ($da['premium'] == 1)
                        <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                            PREMIUM
                        </div>
                      @endif
                      @if ($da['lanzamientos_productos'] == 1)
                        <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                            LANZAMIENTO
                        </div>
                      @endif
                                
                    </div>
                    @endforeach
                  </div>
                  <div class="swiper-button-next"></div>
                  <div class="swiper-button-prev"></div>
                </div>
                

              </div>
              <div class="col-lg-7" style="overflow: hidden;">

                <div class="swiper_dama" style="position:relative;">
                  <div class="swiper-wrapper">
                    @foreach ($olimpia as $da)
                    <div class="swiper-slide p-0">
                      <a class="box-productos-a" href="{{ route('shop.producto', $da['id_productos']) }}">
                        @if ($da['tipo_producto'] == 'Unisex')
                            <img class=""
                                src="img/productos/{{ count($da['imagen_principal']) > 4 ? ($da['imagen_principal'][3]['path_imagen'] ?? '') : ($da['imagen_principal'][0]['path_imagen'] ?? '') }}"
                                alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                style="width: 100%; border-radius: 20px;" />
                        @else
                            <img class=""
                                src="img/productos/{{ $da['imagen_principal'][0]['path_imagen'] ?? '' }}"
                                alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                style="width: 100%; border-radius: 20px;" />
                        @endif
                        
                        <div class="col-12 p-3">
                          <p class="m-0" style="font-size: 12px;">Cód: {{ $da['codigo_productos'] }}</p>
                          <p class="m-0">{{ mb_strtoupper($da['nombre_productos']) }}</p>
    
                          <div class="col-sm-12 p-0 mt-1 mb-1" style="overflow: hidden;">
    
                            {{-- @foreach ($color as $col)
                              @if($col->pk_productos_color_productos == $da['id_productos'])
                                <span style="background: {{ $col->prefijo_color }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                              @endif
                            @endforeach --}}
                            @foreach ($da['colores'] as $color)
                                  <span style="border: 1px solid #b1b1b1; background: {{ $color['prefijo_color'] }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-left: 5px;"></span>
                                @endforeach
        
                          </div>
    
                          @if (is_null($da['precio_descuento_productos']))
    
                            <div class="row">
                              <div class="col-lg-6 guaranies">
                                <span>G.</span>
                                {{ number_format($da['mayorista'], 0, ',', '.') }}
                              </div>
                              <div class="col-lg-6 dolares">
                                <span>$.</span>
                                <?php 
                                  $dolar = $da['mayorista'] / $divisa->divisa;
                                ?>
                                {{ number_format($dolar, 2, ',', '.') }}
                              </div>
                            </div>
                              
                            @else
                                <span style="color: #F25E52;"><span>G.</span>
                                    {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}</span>
    
                                <div class="row">
                                  <div class="col-lg-6 guaranies" style="color: #F25E52;">
                                    <span style="color: #F25E52;">G.</span>
                                    {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}
                                  </div>
                                  <div class="col-lg-6 dolares" style="color: #F25E52;">
                                    <span style="color: #F25E52;">$.</span>
                                    <?php 
                                      $dolar = $da['precio_descuento_productos'] / $divisa->divisa;
                                    ?>
                                    {{ number_format($dolar, 2, ',', '.') }}
                                  </div>
                                </div>
                            @endif
    
                        </div>
                      </a>
                      @if ($da['descuentos_productos'] == 1)
                        <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                            OFERTA!
                        </div>
                      @endif
                      @if ($da['premium'] == 1)
                        <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                            PREMIUM
                        </div>
                      @endif
                      @if ($da['lanzamientos_productos'] == 1)
                        <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                            LANZAMIENTO
                        </div>
                      @endif
                                
                    </div>
                    @endforeach
                  </div>
                  <div class="swiper-button-next"></div>
                  <div class="swiper-button-prev"></div>
                </div>

                <div class="swiper_dama_2" style="position:relative;">
                  <div class="swiper-wrapper">
                    @foreach ($olimpia2 as $da)
                    <div class="swiper-slide p-0">
                      <a class="box-productos-a" href="{{ route('shop.producto', $da['id_productos']) }}">
                        @if ($da['tipo_producto'] == 'Unisex')
                            <img class=""
                                src="img/productos/{{ count($da['imagen_principal']) > 4 ? ($da['imagen_principal'][3]['path_imagen'] ?? '') : ($da['imagen_principal'][0]['path_imagen'] ?? '') }}"
                                alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                style="width: 100%; border-radius: 20px;" />
                        @else
                            <img class=""
                                src="img/productos/{{ $da['imagen_principal'][0]['path_imagen'] ?? '' }}"
                                alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                style="width: 100%; border-radius: 20px;" />
                        @endif
                        
                        <div class="col-12 p-3">
                          <p class="m-0" style="font-size: 12px;">Código: {{ $da['codigo_productos'] }}</p>
                          <p class="m-0">{{ mb_strtoupper($da['nombre_productos']) }}</p>
    
                          <div class="col-sm-12 p-0 mt-1 mb-1" style="overflow: hidden;">
    
                            {{-- @foreach ($color as $col)
                              @if($col->pk_productos_color_productos == $da['id_productos'])
                                <span style="background: {{ $col->prefijo_color }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                              @endif
                            @endforeach --}}

                            @foreach ($da['colores'] as $color)
                                  <span style="border: 1px solid #b1b1b1; background: {{ $color['prefijo_color'] }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-left: 5px;"></span>
                                @endforeach
        
                          </div>
    
                          @if (is_null($da['precio_descuento_productos']))
    
                            <div class="row">
                              <div class="col-lg-6 guaranies">
                                <span>G.</span>
                                {{ number_format($da['mayorista'], 0, ',', '.') }}
                              </div>
                              <div class="col-lg-6 dolares">
                                <span>$.</span>
                                <?php 
                                  $dolar = $da['mayorista'] / $divisa->divisa;
                                ?>
                                {{ number_format($dolar, 2, ',', '.') }}
                              </div>
                            </div>
                              
                            @else
                                <span style="color: #F25E52;"><span>G.</span>
                                    {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}</span>
    
                                <div class="row">
                                  <div class="col-lg-6 guaranies" style="color: #F25E52;">
                                    <span style="color: #F25E52;">G.</span>
                                    {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}
                                  </div>
                                  <div class="col-lg-6 dolares" style="color: #F25E52;">
                                    <span style="color: #F25E52;">$.</span>
                                    <?php 
                                      $dolar = $da['precio_descuento_productos'] / $divisa->divisa;
                                    ?>
                                    {{ number_format($dolar, 2, ',', '.') }}
                                  </div>
                                </div>
                            @endif
    
                        </div>
                      </a>
                      @if ($da['descuentos_productos'] == 1)
                        <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                            OFERTA!
                        </div>
                      @endif
                      @if ($da['premium'] == 1)
                        <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                            PREMIUM
                        </div>
                      @endif
                      @if ($da['lanzamientos_productos'] == 1)
                        <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                            LANZAMIENTO
                        </div>
                      @endif
                                
                    </div>
                    @endforeach
                  </div>
                  <div class="swiper-button-next"></div>
                  <div class="swiper-button-prev"></div>
                </div>

              </div>
            </div>
          </div>


        </div> 
      </div>
    </div>
  </div>


  <section id="new-historia" style="margin-bottom: 0px; overflow: hidden; background: #fff; padding-bottom: 40px;">
    <div class="contenedor_general">
      <div class="col-sm-12" style="padding: 0; overflow: hidden;">
        <div class="col-12" style="float: left; text-align: center;">
          <h1 style="font-weight: 600;">Nuestra Historia</h1>
        </div>
      </div>

      <div class="col-12" style="margin-top: 60px;">
        <div class="row">
          @foreach($cont as $his)
            <div class="col-12 col-lg-4 p-0 text-center mb-5">
              <div class="col-12 text-center timeline_box p-0">
                <div class="timeline">
                  <div class="timeline_inside"></div>
                </div>
              </div>
              <div class="col-12 texto_descriptivo" style="padding: 0 20px; color: #808080;">
                <h4 style="font-weight: 400; margin-top: 40px; margin-bottom: 20px;">{{ $his->titulo_contenido }}</h4>
                <span style="">
                  <?php 
                    echo nl2br($his->texto_contenido); 
                  ?>
                </span>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>

<style>

  .timeline_box:before {
      content:'';
      border-top: 1px solid #c5c5c5;
      float: left;
      width: 45%;
      margin-top: 10px;
  }

  .timeline_box:after {
      content:'';
      border-top: 1px solid #c5c5c5;
      float: right;
      width: 45%;
      margin-top: -10px;
  }

  .timeline {
      width: 20px; 
      height: 20px;
      border-radius: 50%;
      margin: 0 auto;
      padding: 3px;
      border: 1px solid #c5c5c5;
  }

  .timeline_inside {
      width: 12px;
      height: 12px;
      border-radius: 50%;
      /* background: rgb(50, 135, 193); */
      background: var(--tono-principal);
  }
  .div_historia {
      float: left;
      padding: 30px;
  }

  .line-historia {
      width: 385px;

  }

  .text-historia {
      width: 340px;
  }

  .contenido_historia {
      overflow: hidden;
      height: 420px;
      padding: 50px 10px 50px 10px;
      border-radius: 30px;
      background: #f9f9f9;
      box-shadow: 15px 15px 30px 0px #cecece;
  }

  @media screen and (max-width: 1199px) {
      .line-historia {
          width: 325px;
      }

      .text-historia {
          width: 280px;
      }

      .contenido_historia {
          height: 460px;
      }
  }

  @media screen and (max-width: 991px) {
      .line-historia {
          width: 215px;
      }

      .text-historia {
          width: 260px;
      }
  }

  @media screen and (max-width: 767px) {
      .line-historia {
          width: 145px;
      }

      .text-historia {
          width: 220px;
      }

      .imagen_historia {
          display: none;
      }

      .contenido_historia {
          height: 250px;
          padding: 10px 10px;
      }

      .div_historia {
          float: left;
          padding: 10px;
      }
  }

  @media screen and (max-width: 575px) {
      .line-historia {
          width: 160px;
          text-align: center;
      }

      .text-historia {
          width: 220px;
          text-align: center;
      }
  }

  @media screen and (max-width: 569px) {
      .line-historia {
          width: 155px;
          text-align: center;
      }

      .text-historia {
          width: 220px;
          text-align: center;
      }
  }

  @media screen and (max-width: 559px) {
      .line-historia {
          width: 150px;
          text-align: center;
      }

      .text-historia {
          width: 220px;
          text-align: center;
      }
  }

  @media screen and (max-width: 549px) {
      .line-historia {
          width: 145px;
          text-align: center;
      }

      .text-historia {
          width: 220px;
          text-align: center;
      }
  }

  @media screen and (max-width: 539px) {
      .line-historia {
          width: 135px;
          text-align: center;
      }

      .text-historia {
          width: 220px;
          text-align: center;
      }
  }

  @media screen and (max-width: 519px) {
      .line-historia {
          width: 130px;
          text-align: center;
      }

      .text-historia {
          width: 220px;
          text-align: center;
      }
  }

  @media screen and (max-width: 509px) {
      .line-historia {
          width: 120px;
          text-align: center;
      }

      .text-historia {
          width: 220px;
          text-align: center;
      }
  }

  #historia {
      overflow: hidden;
  }

  .contenido-box {
      height: 400px;
  }

  .year-contenido {
      text-align: center;
      background: #e5e5e5;
      margin: 0 auto;
      width: 150px;
      padding: 20px;
  }

  .year-contenido h1 {
      font-weight: 200;
      font-size: 25px;
      margin: 0;
      color: #909090;
  }

  .contenido-icon-1 {
      width: 150px;
      float: left;
      padding: 0;
      height: 35px;
      border-bottom: 1px solid #e5e5e5;
  }

  .contenido-icon-2 {
      width: 70px;
      float: left;
      padding: 18px 0;
      text-align: center;
      background: #e5e5e5;
      border-radius: 100px;
  }

  .contenido-icon-2 img {
      width: 30px;
  }

  @media screen and (max-width: 1200px) {
      .contenido-icon-2 {
          width: 60px;
          padding: 12px 0;
      }

      .contenido-icon-1 {
          width: 124.5px;
          height: 30px;
      }
  }

  @media screen and (max-width: 991px) {

      .contenido-icon-1 {
          width: 45%;
          height: 30px;
      }

      .contenido-texto p {
          font-size: 17px;
      }
  }

  @media screen and (max-width: 767px) {
      .contenido-icon-1 {
          width: 43.5%;
          height: 30px;
      }
  }

  @media screen and (max-width: 521px) {
      .contenido-icon-1 {
          width: 43.2%;
          height: 30px;
      }
  }

  @media screen and (max-width: 500px) {
      .contenido-icon-1 {
          width: 43%;
          height: 30px;
      }
  }

  .triangulo {
      margin: 0 auto;
      text-align: center;
      width: 0px;
      height: 0px;
      border-right: 20px solid transparent;
      border-top: 20px solid #e5e5e5;
      border-left: 20px solid transparent;
      border-bottom: 20px solid transparent;
  }

  .contenido-texto {
      padding: 0 15px;
      overflow: hidden;
      margin-top: 15px;
      text-align: center;
  }

  .contenido-texto p {
      font-family: 'Montserrat', sans-serif;
      color: #909090;
  }

</style>


<section id="proyecto" style="background: #f5f5f5; overflow: hidden;">
    <div class="contenedor_general">
        <div class="col-12">
            <div class="col-lg-3" style="float: left;">
                <img src="img/industria-de-confecciones-de-jean-en-paraguay.png">
            </div>
            <div class="col-lg-9" style="float: left; padding-top: 40px;">
                <h1>DESDE UN PROYECTO EN MENTE A HACERLO REALIDAD</h1><br>
                <p>
                    Somos una <strong>industria paraguaya</strong> con una gran infraestructura. Realizamos desde la
                    importación de telas hasta la producción final de la prenda, manteniendo el estricto control de
                    calidad que nos caracteriza. Desarrollamos todo el programa de producción, la compra de tejidos e
                    insumos, recepción, elaboración, tizado, extendido de tejido y corte, loteo, confecciones, bordados,
                    procesos de lavandería, marcados, empaquetados, colocación de botones;
                    así también el aseo de prendas, control de calidad, doblado, empaquetado, almacenamientos de stock y
                    la logística de envío a sus tiendas.
                </p>
            </div>
        </div>
    </div>
</section>

<style>
  #proyecto h1 {
      font-size: 20px;
  }

  #proyecto img {
      width: 100%;
  }

  @media screen and (max-width: 991px) {
      #proyecto img {
          width: 60%;
      }

      #proyecto {
          text-align: center;
      }

      #proyecto p {
          font-size: 17px;
      }
  }

</style>

{{-- <div class="image2">
    <div class="container">
        <div class="col-lg-12" style="margin-bottom: 30px;">
            <h1>QUERÉS TRABAJAR CON NOSOTROS?</h1>
            <p>SISTEMA DE PRODUCCÍON MODULAR</p>
        </div>
        <div class="col-lg-6" style="float: left;">
            <p>Tienes la idea?</p>
            <p>Deseas emprender tu propio negocio?</p>
            <p style="margin-bottom: 0;">Querés producción para tu marca de Jeans?</p>
        </div>
        <div class="col-lg-6" style="float: left; text-align: right;">
            <p style="color: #fff; margin-top: 20px; margin-right: 10px;">PARA MÁS INFORMACIÓN</p>
            <a href="https://api.whatsapp.com/send?phone=595981143380&text=Hola%20me%20interesado%20en%20trabajar%20con%20Ustedes"
                target="_blank" class="btn btn-primary btn-informacion"
                style="padding-left: 20px; padding-right: 20px; border-radius: 30px;"><i class="fa fa-whatsapp"></i>
                Contáctanos</a>
        </div>
    </div>
</div> --}}

<style>
    .image2 {
      background: #1676BA;
      padding-top: 80px;
      padding-bottom: 80px;
      color: #fff;
      overflow: hidden;
    }

    .btn-informacion {
      background: #fff;
      color: #1676BA;
      font-weight: 400;
    }

</style>


@endsection