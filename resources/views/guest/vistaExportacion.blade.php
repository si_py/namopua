@extends('layouts.front')

@section('content')

  <div class="col-12" style="padding-bottom: 100px;">
    <div class="contenedor_general">

      <div class="col-sm-12 title-shop">
        <h1 style="font-weight: 900; font-family: 'Montserrat', sans-serif;">Exportación</h1>
        <p style="font-weight: 400;">Completá el formulario y nos ponemos en contacto contigo</p>
        <p style="font-weight: 400;"><b>Sujeto a paises: Argentina, Bolivia, Brasil, Chile, Colombia, Ecuador, Perú, Uruguay</b></p>
        <p style="font-weight: 400;">Pedidos internacionales por envío de contenedor</p>
        <p style="font-size: 35px;">
          🇦🇷 🇧🇴 🇧🇷 🇨🇱 🇨🇴 🇪🇨 🇵🇪 🇺🇾 
        </p>
      </div>
      
      <style>
          .title-shop {
              text-align: center;
              padding: 80px 0 70px 0;
          }
          .title-shop h1 {
              font-size: 70px !important;
              font-weight: 900;
          }
          .title-shop p{
              font-size: 17px;
              font-weight: 100;
              color: #909090;
              margin-top: 0px;
              margin-bottom: 0;
              font-family: 'Montserrat', sans-serif;
          }
          @media screen and (max-width: 991px){
              .title-shop {
                  text-align: center;
                  padding: 30px 0 30px 0;
              }
              .title-shop h1 {
                  font-size: 55px !important;
                  font-weight: 900;
              }
              .title-shop p{
                  font-size: 14px;
              }
          }

          label {
            color: #101010;
          }
      </style>

    </div>

    <div class="container" style="overflow: hidden;">
      <div class="row justify-content-center">
        <div class="col-md-12 contenedor">
          <div class="col-12" style="border-radius: 30px; border: 1px solid #fff; background: #fff;">
            <div class="card-body" style="overflow: hidden; padding: 40px 0 10px 0">
              <div class="col-12 text-center mensaje_enviado">
                <h2 class="text-success m-0">
                  Mensaje enviado!
                </h2>
                <p class="text-success">
                  Gracias por escribirnos, un asesor se pondrá en contacto contigo en la brevedad posible
                </p>
              </div>
              <form action="" id="agregarExportador">
                <div class="col-lg-12">

                  <label for=""><b>Datos de tu Empresa</b></label>
                  
                  <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0;">
                    <input id="empresa" type="text" class="form-control texto_input" name="name" placeholder="Empresa" autofocus required>
                    <img src="img/icon/suitcase-alt.svg" style="width: 30px; position: absolute; top: 12px; left: 20px;">
                  </div>
                  <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0;">
                    <input id="name" type="text" class="form-control texto_input" name="name" placeholder="Nombre y Apellido"  required>
                    <img src="img/icon/user-1.svg" style="width: 30px; position: absolute; top: 12px; left: 20px;">
                  </div>
                  <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0;">
                    <input id="correo" type="text" class="form-control texto_input" name="name" placeholder="Correo Electrónico"  required>
                    <img src="img/icon/envelope-alt.svg" style="width: 30px; position: absolute; top: 12px; left: 20px;">
                  </div>
                  <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0;">
                    <input id="web" type="text" class="form-control texto_input" name="name" placeholder="Página Web">
                    <img src="img/icon/envelope-alt.svg" style="width: 30px; position: absolute; top: 12px; left: 20px;">
                  </div>
                  <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0;">
                    <input id="telefono" type="text" class="form-control texto_input" name="name" placeholder="Teléfono"  required>
                    <img src="img/icon/phone.svg" style="width: 30px; position: absolute; top: 12px; left: 20px;">
                  </div>
                  <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0;">
                    <select name="" id="pais" class="form-control" style="border-radius: 30px; padding: 8px 20px; height: 50px;">
                      <option value="">Tu País</option>
                      <option value="Argentina">🇦🇷 Argentina</option>
                      <option value="Bolivia">🇧🇴 Bolivia</option>
                      <option value="Brasil">🇧🇷 Brasil</option>
                      <option value="Chile">🇨🇱 Chile</option>
                      <option value="Colombia">🇨🇴 Colombia</option>
                      <option value="Ecuador">🇪🇨 Ecuador</option>
                      <option value="Peru">🇵🇪 Perú</option>
                      <option value="Uruguay">🇺🇾 Uruguay</option>
                    </select>
                  </div>
                  <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0;">
                    <textarea id="mensaje" class="form-control texto_textarea" style="height: 200px;" placeholder="Mensaje" required> </textarea>
                  </div>

                  <div class="form-group mb-3 pt-3">
                    <label for=""><b>Presentación de tu empresa (PDF)</b></label><br>
                    <input type="file" id="file" accept="application/pdf" required>
                  </div>

                  <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0; text-align: right;">
                    <button  type="submit" class="btn btn-primary" style="padding: 10px 20px; border-radius: 30px;">Enviar Formulario</button>
                  </div>
                  
                </div>
              </form>
              
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <style>
    .mensaje_enviado {
      display: none;
    }
    .title-shop {
      text-align: center;
      padding: 80px 0 80px 0;
    }
    .title-shop h1 {
      font-size: 100px;
      font-weight: 900;
    }
    .title-shop p{
      font-size: 17px;
      font-weight: 100;
      color: #909090;
      margin-top: 0px;
      margin-bottom: 0;
      font-family: 'Montserrat', sans-serif;
    } 
    .texto_input, .texto_textarea {
      border-radius: 30px !important;
    }
    #input-search:focus, #email:focus, #password:focus {
      color: #808080 !important;
      border-color: #707070;
      background: transparent;
      outline: none !important;
      outline-width: 0 !important;
      box-shadow: none;
      -moz-box-shadow: none;
      -webkit-box-shadow: none;
    }
    @media screen and (max-width: 767px) {
      .title-shop h1 {
        font-size: 70px;
        font-weight: 900;
      }
      .title-shop p{ 
        padding-left: 25px;
        padding-right: 25px;
      }
    }
  
  
  
      section {
        padding-top: 80px; padding-bottom: 80px;
      }
      h1, label, span, h2, a, p, textarea { 
        font-family: 'Montserrat', sans-serif; 
      }
      textarea { color: #808080 !important; padding: 20px !important;}
      .btn-primary {
        background-color: #1677BB;
        border: 1px solid #1677BB;
      }
      .btn-primary:hover {
        background-color: #175b89;
        border: 1px solid #175b89;
      }
  
      a{
        color: #1677BB;
      }
      a:hover {
        text-decoration: none;
        color: #175b89;
      }
      .texto_input {
          border-radius: 30px !important;
          padding: 25px !important;
          font-family: 'Montserrat', sans-serif;
          margin-bottom: 0  !important;
          padding-left: 70px !important;
      }
      .texto_input::placeholder, textarea::placeholder {
          font-size: 15px !important;
          font-family: 'Montserrat', sans-serif !important;
          font-weight: 400 !important;
      }
      
      .btn-outline-primary {
          border-radius: 30px;
          padding: 5px 20px 5px 20px;
          border: 2px solid #1677BB;
          color: #1677BB;
          font-weight: 600;
      }
      .btn-outline-primary:hover {
          background: #1677BB;
          border: 2px solid #1677BB;
      }
      .derecha {
          float: left; 
          overflow: hidden; 
          padding-top: 0px;
          color: #808080;
      }
      .izquierda {
  
      }
      .izquierda h1{
          font-weight: 600; 
          font-size: 60px; 
          margin-bottom: 0;
          color: #000;
      }
      .title {
          margin-bottom: 30px; 
          margin-top: 0px; 
          overflow: hidden; 
          text-align: right;
      }
      .btn {
          text-align: right;
      }
      .footer-izquierda {
          width: 80%;
          float: left;
      }
      .footer-derecha {
          width: 20%;
          float: left;
          text-align: right;
      }
      .card-body {
          overflow: hidden; 
          padding: 50px 20px;
      }
      .contenedor { 
        padding-top: 0px; 
        margin-bottom: 100px;
      }
  
      .form-control:focus {
          box-shadow: none;
      }
  
      @media screen and (max-width: 991px){
        .derecha {
            float: left; 
            overflow: hidden; 
            padding-top: 25px;
        }
        .izquierda {text-align: center;}
        .izquierda h1{ font-size: 40px; margin-bottom: 10px;}
        .title {text-align: center;}
        .btn {text-align: center;}
        .footer-izquierda { width: 40%; }
        .footer-derecha { width: 60%; }
        .card-body {
            overflow: hidden; padding: 50px 0px;
        }
        .contenedor { padding-top: 0px; }
      }
  
      @media screen and (max-width: 767px){
        .derecha span {
          font-size: 15px;
        }
      }
  </style>

  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>

  <script>
    $('#agregarExportador').submit(function(e){
      e.preventDefault();

      var empresa = $('#empresa').val();
      var name = $('#name').val();
      var correo = $('#correo').val();
      var web = $('#web').val();
      var telefono = $('#telefono').val();
      var pais = $('#pais').val();
      var mensaje = $('#mensaje').val();

      var formData = new FormData();
      formData.append('_token', "{{ csrf_token() }}");
      formData.append('empresa', empresa);
      formData.append('name', name);
      formData.append('correo', correo);
      formData.append('web', web);
      formData.append('telefono', telefono);
      formData.append('pais', pais);
      formData.append('mensaje', mensaje);

      var pdf = $('#file')[0].files[0];
      pdf = pdf ? pdf : null;
      formData.append('pdf', pdf);

      $.ajax({
          url: "{{ route('guest.agregarExportacion') }}",
          type: "POST",
          data: formData,
          processData: false,
          contentType: false,
          success: function (data){
              $('.mensaje_enviado').fadeIn(); 

              setTimeout(function(){
                $('.mensaje_enviado').fadeOut();
                $('#agregarExportador').trigger('reset'); 
              }, 5000);

          } 
      });
      

    });
  </script>

@endsection