@extends('layouts.front')

@section('content')
    <style>
        .title-shop {
            text-align: center;
            padding: 40px 0 40px 0;
        }

        .micuenta-card {
            border-radius: 30px;
            border: 1px solid #cecece;
        }

        .micuenta-field {
            position: relative;
            margin-bottom: 15px;
            padding: 0;
        }

        .title-shop h1 {
            font-size: 100px;
            font-weight: 900;
        }

        .title-shop p {
            font-size: 17px;
            font-weight: 100;
            color: #909090;
            margin-top: 0px;
            margin-bottom: 0;
            font-family: 'Montserrat', sans-serif;
        }

        .texto_input,
        .texto_textarea {
            border-radius: 30px !important;
        }

        #input-search:focus,
        #email:focus,
        #password:focus {
            color: #808080 !important;
            border-color: #707070;
            background: transparent;
            outline: none !important;
            outline-width: 0 !important;
            box-shadow: none;
            -moz-box-shadow: none;
            -webkit-box-shadow: none;
        }

        @media screen and (max-width: 767px) {
            .title-shop h1 {
                font-size: 70px;
                font-weight: 900;
            }

            .title-shop p {
                padding-left: 25px;
                padding-right: 25px;
            }
        }



        section {
            padding-top: 80px;
            padding-bottom: 80px;
        }

        h1,
        label,
        span,
        h2,
        a,
        p,
        textarea {
            font-family: 'Montserrat', sans-serif;
        }

        textarea {
            color: #808080 !important;
            padding: 20px !important;
        }

        .btn-primary {
            background-color: #1677BB;
            border: 1px solid #1677BB;
        }

        .btn-primary:hover {
            background-color: #175b89;
            border: 1px solid #175b89;
        }

        a {
            color: #1677BB;
        }

        a:hover {
            text-decoration: none;
            color: #175b89;
        }

        .texto_input {
            border-radius: 30px !important;
            padding: 25px !important;
            font-family: 'Montserrat', sans-serif;
            margin-bottom: 0 !important;
            padding-left: 70px !important;
        }

        .texto_input::placeholder,
        textarea::placeholder {
            font-size: 17px !important;
            font-family: 'Montserrat', sans-serif !important;
            font-weight: 400 !important;
        }

        .btn-outline-primary {
            border-radius: 30px;
            padding: 5px 20px 5px 20px;
            border: 2px solid #1677BB;
            color: #1677BB;
            font-weight: 600;
        }

        .btn-outline-primary:hover {
            background: #1677BB;
            border: 2px solid #1677BB;
        }

        .derecha {
            float: left;
            overflow: hidden;
            padding-top: 0px;
            color: #808080;
        }

        .izquierda {}

        .izquierda h1 {
            font-weight: 600;
            font-size: 60px;
            margin-bottom: 0;
            color: #000;
        }

        .title {
            margin-bottom: 30px;
            margin-top: 0px;
            overflow: hidden;
            text-align: right;
        }

        .btn {
            text-align: right;
        }

        .footer-izquierda {
            width: 80%;
            float: left;
        }

        .footer-derecha {
            width: 20%;
            float: left;
            text-align: right;
        }

        .card-body {
            overflow: hidden;
            padding: 50px 20px;
        }

        .contenedor {
            padding-top: 0px;
            margin-bottom: 100px;
        }

        .form-control:focus {
            box-shadow: none;
        }

        @media screen and (max-width: 991px) {
            .derecha {
                float: left;
                overflow: hidden;
                padding-top: 25px;
            }

            .izquierda {
                text-align: center;
            }

            .izquierda h1 {
                font-size: 40px;
                margin-bottom: 10px;
            }

            .title {
                text-align: center;
            }

            .btn {
                text-align: center;
            }

            .footer-izquierda {
                width: 40%;
            }

            .footer-derecha {
                width: 60%;
            }

            .card-body {
                overflow: hidden;
                padding: 50px 0px;
            }

            .contenedor {
                padding-top: 0px;
            }
        }

        @media screen and (max-width: 767px) {
            .derecha span {
                font-size: 15px;
            }
        }

    </style>
    <div class="container-lg">
        <div class="col-sm-12 title-shop">
            <h1 style="font-weight: 900; font-family: 'Montserrat', sans-serif;">Mi cuenta</h1>
            {{-- <p style="font-weight: 400;">Puedes modificar tus datos personales</p> --}}
        </div>
    </div>

    <div class="container" style="overflow: hidden;" id="mi-cuenta-container">
        <div class="row justify-content-center">
            <div class="col-md-12 contenedor">
                <div class="row">
                    <div class="col-4">
                        <div class="card-body micuenta-card" style="overflow: hidden;">
                            <div class="col-12">
                                <h4>
                                    Mis Datos
                                </h4>
                            </div>
                            <div class="col-lg-12">
                                <div class="col-md-12 micuenta-field">
                                    <span>Nombre y Apellido:</span> <span id="user_name">{{ Auth::user()->name }}</span>
                                </div>
                                <div class="col-md-12 micuenta-field">
                                    <span>Empresa:</span> <span id="user_razon">{{ Auth::user()->razon_users }}</span>
                                </div>
                                <div class="col-md-12 micuenta-field">
                                    <span>RUC:</span> <span id="user_ruc">{{ Auth::user()->ruc_users }}</span>
                                </div>
                                <div class="col-md-12 micuenta-field">
                                    <span>Correo:</span> <span id="user_email">{{ Auth::user()->email }}</span>
                                </div>
                                <div class="col-md-12 micuenta-field">
                                    <span>Teléfono:</span> <span
                                        id="user_telefono">{{ Auth::user()->telefono_users }}</span>
                                </div>
                                <div class="col-md-12 micuenta-field">
                                    <span>Dirección:</span> <span
                                        id="user_direccion">{{ Auth::user()->direccion_users }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form method="POST" id="modificar-datos-form" class="col-8 micuenta-card"
                        onsubmit="editar_datos(event)">
                        @csrf
                        <div class="card-body" style="overflow: hidden;">
                            <div class="col-lg-12">
                                <div class="col-md-12 micuenta-field">
                                    <input id="nombre" type="text" class="form-control texto_input" name="name"
                                        placeholder="Nombre y Apellido" autofocus>
                                    <img src="img/icon/user-1.svg"
                                        style="width: 30px; position: absolute; top: 12px; left: 20px;">
                                </div>
                                <div class="col-md-12 micuenta-field">
                                    <input id="empresa" type="text" class="form-control texto_input" name="razon_users"
                                        placeholder="Empresa">
                                    <img src="img/icon/suitcase-alt.svg"
                                        style="width: 30px; position: absolute; top: 12px; left: 20px;">
                                </div>
                                <div class="col-md-12 micuenta-field">
                                    <input id="ruc" type="text" class="form-control texto_input" name="ruc_users"
                                        placeholder="RUC">
                                    <img src="img/icon/new.svg"
                                        style="width: 30px; position: absolute; top: 12px; left: 20px;">
                                </div>
                                <div class="col-md-12 micuenta-field">
                                    <input id="email_field" type="text" class="form-control texto_input" name="email"
                                        placeholder="Correo Electrónico">
                                    <img src="img/icon/envelope-alt.svg"
                                        style="width: 30px; position: absolute; top: 12px; left: 20px;">
                                </div>
                                <div class="col-md-12 micuenta-field">
                                    <input id="telefono" type="text" class="form-control texto_input" name="telefono_users"
                                        placeholder="Teléfono">
                                    <img src="img/icon/phone.svg"
                                        style="width: 30px; position: absolute; top: 12px; left: 20px;">
                                </div>
                                <div class="col-md-12 micuenta-field">
                                    <input id="direccion" type="text" class="form-control texto_input"
                                        name="direccion_users" placeholder="Dirección">
                                    <img src="img/icon/location.svg"
                                        style="width: 20px; position: absolute; top: 12px; left: 20px;">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-md-12 micuenta-field">
                                    <label for="#pass_confirm">Confirme su contraseña</label>
                                    <input id="pass_confirm" type="password" class="form-control"
                                        style="border-radius: 30px; padding: 25px" name="pass_confirm"
                                        placeholder="Confirmar contraseña" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-md-12 micuenta-field"
                                    style="position: relative; margin-bottom: 15px; padding: 0; text-align: right;">
                                    <button class="btn btn-primary"
                                        style="padding: 5px 20px; border-radius: 30px;">Editar</button>
                                    <button class="btn btn-danger"
                                        style="padding: 5px 20px; border-radius: 30px;">Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        function editar_datos(e) {
            e.preventDefault();

            let data = $("#modificar-datos-form").serialize()
            $.ajax({
                url: "{{ route('guest.editar.datos') }}",
                data: data,
                method: 'POST',
                success: function(resp) {
                    if (resp.success) {
                        let data = resp.data;
                        $('#user_name').html(data.name)
                        $('#user_email').html(data.email)
                        $('#user_razon').html(data.razon_users)
                        $('#user_ruc').html(data.ruc_users)
                        $('#user_direccion').html(data.direccion_users)
                        $('#user_telefono').html(data.telefono_users)
                        alert('Datos actualizados');
                        $('#modificar-datos-form').trigger('reset')
                    } else {
                        alert('Ha ocurrido un error')
                    }
                }
            })
        }
    </script>

@endsection
