@php
$fundacion = new DateTime('2001-01-01');
$actual = new DateTime();

$anos_trayectoria = $fundacion->diff($actual)->y;
@endphp
@extends('layouts.front')

@section('content')

    @include('preloader')

    <!-- sliders -->
    <!-- <div class="col-sm-12" id="principal-slider" style="padding: 0; height: 500px; overflow: hidden;">
                                                              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="height: 500px;">

                                                                <div class="carousel-inner">
                                                                  @foreach ($sliderUnd as $s)
                                                                  @if ($s->pk_secciones_sliders == 1)
                                                                  <div class="carousel-item active">
                                                                    <img class="d-block w-100" src="img/{{ $s->path_sliders }}" alt="First slide">
                                                                  </div>
                                                                  <div style="position: absolute; top: 15%; left: 20%; color: #000; overflow: hidden; z-index: 29999;">
                                                                    <p style="margin-bottom: 0; font-size: 30px; color: #000; font-weight: 400;">
                                                                      COLECCIÓN PRIMAVERA - VERANO
                                                                    </p>
                                                                    <h1 style="padding-left: 10px; padding-right: 10px; background: #1676BA; color: #fff; font-size: 80px; font-family: 'Montserrat', sans-serif; margin-bottom: 0px;">
                                                                      DESCUENTOS
                                                                    </h1>
                                                                    <p style="margin-bottom: 10px; font-size: 30px; color: #000;">
                                                                      <span>con tus</span> <span style="font-size: 40px; font-weight: 600;">COMPRAS!</span>
                                                                    </p>
                                                                    <a href="/shop" class="btn btn-primary" style="padding: 5px 20px; border-radius: 30px; font-size: 20px; color: #fff;">Comprar</a>
                                                                  </div>
                                                                  @endif
                                                                  @endforeach
                                                                </div>
                                                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                                                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                                  <span class="sr-only">Previous</span>
                                                                </a>
                                                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                                                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                                  <span class="sr-only">Next</span>
                                                                </a>
                                                              </div>
                                                            </div> -->
    <div class="col-sm-12" id="principal-slider" style="padding: 0; overflow: hidden;">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel_principal">
            <ol class="carousel-indicators">
                @foreach ($slider as $s)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}"
                        class="{{ $loop->first ? 'active' : '' }}"></li>
                @endforeach
            </ol>
            <div class="carousel-inner">
                @foreach ($slider as $s)
                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                        <a href="{{ $s->titulo_sliders }}">
                            <img class="d-block w-100" src="img/{{ $s->path_sliders }}" alt="{{ $s->titulo_sliders }}">
                            <div
                                style="position: absolute; top: 15%; left: 20%; color: #000; overflow: hidden; z-index: 29999;">

                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <!-- end sliders -->
    <style>
        #principal-slider-mobile {
            display: none;
        }

        @media screen and (max-width: 991px) {
            #principal-slider {
                display: none;
            }

            #principal-slider-mobile {
                display: block;
            }
        }

        p {
            font-family: 'Montserrat', sans-serif;
        }

        a {
            font-family: 'Montserrat', sans-serif;
        }

        h1 {
            font-family: 'Montserrat', sans-serif;
        }

    </style>

    <!-- sliders -->
    <div class="col-sm-12" id="principal-slider-mobile" style="padding: 0;">





        <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel_principal">
            <ol class="carousel-indicators">
                @foreach ($slider as $s)
                    <li data-target="#carouselExampleIndicators1" data-slide-to="{{ $loop->index }}"
                        class="{{ $loop->first ? 'active' : '' }}"></li>
                @endforeach
            </ol>
            <div class="carousel-inner">
                @foreach ($slider as $s)
                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                        <a href="{{ $s->titulo_sliders }}">
                            <img class="d-block w-100" src="img/{{ $s->path_mobile_sliders }}"
                                alt="{{ $s->titulo_sliders }}">
                            <div
                                style="position: absolute; top: 15%; left: 20%; color: #000; overflow: hidden; z-index: 29999;">

                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <!-- end sliders -->
        <style>
            #principal-slider-mobile {
                display: none;
            }

            @media screen and (max-width: 991px) {
                #principal-slider {
                    display: none;
                }

                #principal-slider-mobile {
                    display: block;
                }
            }

            p {
                font-family: 'Montserrat', sans-serif;
            }

            a {
                font-family: 'Montserrat', sans-serif;
            }

            h1 {
                font-family: 'Montserrat', sans-serif;
            }

        </style>




    </div>

    <!-- line -->
    {{-- <div class="col-sm-12" style="padding: 0; overflow: hidden; margin-top: -7px;">

        <div class="col-4 line" style="float: left; padding: 0; background: #F25E52;">

        </div>
        <div class="col-4 line" style="float: left; padding: 0; background: #FCB541;">

        </div>
        <div class="col-4 line" style="float: left; padding: 0; background: #FF0188;">

        </div>
    </div> --}}
    {{-- <style>
        .line {
            height: 10px;
        }

    </style> --}}
    <!-- end line -->


    <section id="presentacion">
        <div class="col-sm-12">
            <h1 style="color: #fff;">PRODUCCIÓN <span>100%</span> NACIONAL</h1>
            <p style="font-weight: 400;">
                N° 1 en ventas y producción mayorista, con {{ $anos_trayectoria }} años de trayectoria
            </p>
        </div>
    </section>
    <section id="excelencia">
        <div class="col-sm-12">
            <p class="texto-excelencia">
                "LA EXCELENCIA SE LOGRA CON EL TIEMPO Y ESFUERZO - SINCE 2001"
            </p>
        </div>
    </section>

    <style>
        #excelencia {
            text-align: center;
            overflow: hidden;
            padding-top: 10px;
            padding-bottom: 10px;
            background: white;
            color: #f5f5f5;
        }

        #presentacion {
            text-align: center;
            overflow: hidden;
            background: var(--tono-barra);
            padding-top: 40px;
            padding-bottom: 30px;
            margin-top: -7px;
        }

        #presentacion h1 {
            font-size: 60px;
            margin-bottom: 0;
        }

        #presentacion p {
            font-size: 25px;
            font-weight: 100;
            color: #fff;
            margin-top: 0px;
            margin-bottom: 0;
            font-weight: bold;
        }

        #presentacion button {
            margin-top: 20px;
            font-size: 25px;
            padding: 10px 30px;
        }

        @media screen and (max-width: 1150px) {
            #presentacion h1 {
                font-size: 60px;
            }

            #presentacion p {
                font-size: 19px;
            }

            #presentacion button {
                font-size: 17px;
            }
        }

        @media screen and (max-width: 991px) {
            #presentacion h1 {
                font-size: 40px;
            }
        }

        @media screen and (max-width: 683px) {
            #presentacion {
                padding-top: 13px;
                padding-bottom: 10px;
            }

            #presentacion h1 {
                font-size: 20px;
            }

            #presentacion p {
                font-size: 15px;
                padding: 0 20px;
            }
        }

    </style>


    <!-- <section style="padding: 10px 0;">

                                                              <div class="container">
                                                                <div class="row">
                                                                  <div class="col-sm-12">
                                                                    <div class="col-sm-6" style=" padding: 10px; float: left;">
                                                                      <div class="col-sm-12" style="border: 1px solid #e5e5e5; padding: 0px;">
                                                                        <img src="{{ asset('img/fabrica_de_jeans_en_paraguay_tapabocas.jpg') }}" style="width: 100%;">
                                                                      </div>
                                                                    </div>
                                                                    <div class="col-sm-6" style="padding: 10px; float: left;">
                                                                      <div class="col-sm-12" style="border: 1px solid #e5e5e5; padding: 0px;">
                                                                        <img src="{{ asset('img/fabrica_de_jeans_en_paraguay_olimpia_tienda_online.jpg') }}" style="width: 100%;">
                                                                      </div>
                                                                    </div>

                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </section> -->




    {{-- <section id="los-mas-vendidos" style="overflow: hidden;"> --}}
    {{-- <div class="container"> --}}
    {{-- <div class="row"> --}}

    {{-- <div class="col-lg-4 masVendidos_title" style="float: left;"> --}}
    {{-- <div class="col-12"> --}}
    {{-- <h1>Los más</h1> --}}
    {{-- <h1 style="font-size: 40px;">VENDIDOS <i class="fa fa-chevron-right"></i></h1> --}}
    {{-- </div> --}}
    {{-- </div> --}}

    {{-- <div class="col-lg-8"> --}}

    {{-- <div class="MultiCarousel" data-items="1,2,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000"> --}}
    {{-- <h1 class="masVendidos_title_responsive" style="font-weight: 600; color: #808080;">Los más vendidos</h1> --}}

    {{-- <div class="MultiCarousel-inner"> --}}

    {{-- @foreach ($masVendidos as $p) --}}
    {{-- @if ($p->principal_color_imagen == 'true') --}}
    {{-- @if ($p->seccion_color_imagen == 1) --}}
    {{-- <div class="item" style="position: relative;"> --}}
    {{-- <a id="box-productos-a" href="{{route('shop.producto', $p->id_productos)}}"> --}}
    {{-- <img class="lazy " data-src="img/productos/{{$p->path_imagen}}" alt="{{ $p->nombre_productos }} {{ $p->codigo_productos }}" style="width: 100%;"> --}}
    {{-- </a> --}}
    {{-- @if ($p->descuentos_productos == 1) --}}
    {{-- <div class="cartel" style="padding: 5px 20px; color: #fff; background: #F25E52; position: absolute; top: 5px; right: 5px; border-radius: 30px;"> --}}
    {{-- Oferta --}}
    {{-- </div> --}}
    {{-- @endif --}}

    {{-- @if ($p->premium == 1) --}}
    {{-- <div class="cartel" style="padding: 5px 20px; color: #fff; background: #1677BB; position: absolute; top: 5px; right: 5px; border-radius: 30px;"> --}}
    {{-- Lanzamiento --}}
    {{-- </div> --}}
    {{-- @endif --}}

    {{-- <div class="pad15"> --}}
    {{-- <p class="lead" style=" margin-top: 20px; margin-bottom: 0; color: #808080; font-size: 18px; font-weight: 400;"> --}}
    {{-- {{$p->nombre_productos}} --}}
    {{-- </p> --}}
    {{-- <p style="margin-bottom: 10px; font-size: 20px; color: #606060; font-weight: 600;"> --}}
    {{-- @if (is_null($p->precio_descuento_productos)) --}}
    {{-- <span style="font-size: 14px">Gs.</span> {{ number_format($p->mayorista,  0, ',', '.')}} --}}
    {{-- @else --}}
    {{-- <span style="font-size: 14px; margin-right: 3px; text-decoration: line-through; color: #808080;"><span style="font-size: 14px">Gs.</span> {{number_format($p->mayorista,  0, ',', '.')}}</span> --}}
    {{-- <span style="color: #F25E52;"><span style="font-size: 14px">Gs.</span> {{ number_format($p->precio_descuento_productos,  0, ',', '.') }}</span> --}}
    {{-- @endif --}}
    {{-- </p> --}}

    {{-- <!-- @if ($p->descuentos_productos == 1) --}}
    {{-- <div class="oferta" style=" background: #F25E52; border-radius: 30px;"> --}}
    {{-- OFERTA! --}}
    {{-- </div> --}}
    {{-- @endif --}}
    {{-- @if ($p->premium == 1) --}}
    {{-- <div class="oferta" style=" background: #1677BB; border-radius: 30px;"> --}}
    {{-- LANZAMIENTO! --}}
    {{-- </div> --}}
    {{-- @endif --> --}}

    {{-- <p> --}}
    {{-- <div class="col-sm-12" style="margin-bottom: 10px; padding: 0px; overflow: hidden; display: flex; justify-content: center; align-content: center;"> --}}
    {{-- @foreach ($masVendidos as $pc) --}}
    {{-- @if ($pc->pk_productos_color_imagen == $p->id_productos) --}}
    {{-- @if ($pc->principal_color_imagen == 'true') --}}
    {{-- <span style="background: {{$pc->prefijo_color}}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span> --}}
    {{-- @endif --}}
    {{-- @endif --}}
    {{-- @endforeach --}}
    {{-- </div> --}}
    {{-- </p> --}}
    {{-- <p style="color: #808080; font-size: 15px;">Código {{$p->codigo_productos}}</p> --}}
    {{-- </div> --}}

    {{-- </div> --}}
    {{-- @endif --}}
    {{-- @endif --}}
    {{-- @endforeach --}}

    {{-- </div> --}}
    {{-- <a class="leftLst"><img class="lazy" data-src="img/icon/chevron-left.svg" alt="Chevron Left" style="width: 20px;"></a> --}}
    {{-- <a class="rightLst"><img class="lazy" data-src="img/icon/chevron-right.svg" alt="Chevron Right" style="width: 20px;"> </a> --}}
    {{-- </div> --}}

    {{-- </div> --}}
    {{-- </div> --}}
    {{-- </div> --}}
    {{-- </section> --}}

    <style>
        .masVendidos_title {
            padding: 150px 0 0 0;
            color: #808080;
        }

        .masVendidos_title_responsive {
            display: none;
        }

        .MultiCarousel {
            float: left;
            overflow: hidden;
            padding: 15px;
            width: 100%;
            position: relative;
        }

        .MultiCarousel .MultiCarousel-inner {
            transition: 1s ease all;
            float: left;
        }

        .MultiCarousel .MultiCarousel-inner .item {
            float: left;
        }

        .MultiCarousel .MultiCarousel-inner .item>div {
            text-align: center;
        }

        .MultiCarousel .leftLst,
        .MultiCarousel .rightLst {
            position: absolute;
            top: calc(50% - 70px);
        }

        .MultiCarousel .leftLst {
            left: 10px;
            bottom: 15px;
            color: #c3c3c3;
            cursor: pointer;
        }

        .MultiCarousel .rightLst {
            right: 10px;
            bottom: 15px;
            color: #c3c3c3;
            cursor: pointer;
        }

        .MultiCarousel .leftLst.over,
        .MultiCarousel .rightLst.over {
            pointer-events: none;
            ;
        }

        .pad15 p {
            margin-bottom: 0;
        }

        @media screen and (max-width: 991px) {
            .masVendidos_title {
                display: none;
            }

            .masVendidos_title_responsive {
                display: block;
            }
        }

    </style>


    <section id="los-mas-vendidos" style="overflow: hidden;">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 masVendidos_title" style="float: left;">
                    <div class="col-12">
                        <h1>OFERTAS</h1>
                        <h1 style="font-size: 40px;">IMPERDIBLES <i class="fa fa-chevron-right"></i></h1>
                    </div>
                </div>

                <div class="col-lg-8">

                    <div class="MultiCarousel" data-items="1,2,3,5,6" data-slide="1" id="MultiCarousel"
                        data-interval="1000">
                        <h1 class="masVendidos_title_responsive" style="font-weight: 600; color: #808080;">OFERTAS
                            IMPERDIBLES</h1>

                        <div class="MultiCarousel-inner">

                            @foreach ($ofertas as $o)
                                <div class="item" style="position: relative;">
                                    <a id="box-productos-a" href="{{ route('shop.producto', $o->id_productos) }}">
                                        <img src="img/productos/{{ $o->path_imagen }}"
                                            alt="{{ $o->nombre_productos }} {{ $o->codigo_productos }}"
                                            style="width: 100%;">
                                    </a>
                                    @if ($o->descuentos_productos == 1)
                                        <div class="cartel"
                                            style="padding: 5px 20px; color: #fff; background: #F25E52; position: absolute; top: 5px; right: 5px; border-radius: 30px;">
                                            Oferta
                                        </div>
                                    @endif

                                    @if ($o->premium == 1)
                                        <div class="cartel"
                                            style="padding: 5px 20px; color: #fff; background: #1677BB; position: absolute; top: 5px; right: 5px; border-radius: 30px;">
                                            PREMIUM
                                        </div>
                                    @endif
                                    @if ($o->lanzamientos_productos == 1)
                                        <div class="cartel"
                                            style="padding: 5px 20px; color: #fff; background: #94B53D; position: absolute; top: 5px; right: 5px; border-radius: 30px;">
                                            LANZAMIENTO
                                        </div>
                                    @endif

                                    <div class="pad15" style="text-align: center;">
                                        <p class="lead"
                                            style=" margin-top: 20px; margin-bottom: 0; color: #808080; font-size: 18px; font-weight: 400;">
                                            {{ $o->nombre_productos }}
                                        </p>
                                        <p style="margin-bottom: 10px; font-size: 20px; color: #606060; font-weight: 600;">
                                            @if (is_null($o->precio_descuento_productos))
                                                <span style="font-size: 14px">Gs.</span>
                                                {{ number_format($o->mayorista, 0, ',', '.') }}
                                            @else
                                                {{-- <span style="font-size: 14px; margin-right: 3px; text-decoration: line-through; color: #808080;"><span style="font-size: 14px">Gs.</span> {{number_format($o->mayorista,  0, ',', '.')}}</span> --}}
                                                <span style="color: #F25E52;"><span style="font-size: 14px">Gs.</span>
                                                    {{ number_format($o->precio_descuento_productos, 0, ',', '.') }}</span>
                                            @endif
                                        </p>

                                        <!-- @if ($o->descuentos_productos == 1) <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                                                                                            OFERTA!
                                                                                          </div> @endif
                                                                                          @if ($o->premium == 1)
                                                                                          <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                                                                                            LANZAMIENTO!
                                                                                          </div>
                                                                                          @endif -->

                                        <p>
                                        <div class="col-sm-12"
                                            style="margin-bottom: 10px; padding: 0px; overflow: hidden; display: flex; justify-content: center; align-content: center;">
                                            @foreach ($colorImagen as $pc)
                                                @if ($pc->pk_productos_color_imagen == $o->id_productos)
                                                    @if ($pc->principal_color_imagen == 'true')
                                                        <span
                                                            style="border: 1px solid #b1b1b1; background: {{ $pc->prefijo_color }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </div>
                                        </p>
                                        <p style="color: #808080; font-size: 15px;">Código {{ $o->codigo_productos }}
                                        </p>
                                    </div>

                                </div>
                            @endforeach

                        </div>
                        <a class="leftLst"><img class="lazy" data-src="img/icon/chevron-left.svg"
                                alt="Chevron Left" style="width: 20px;"></a>
                        <a class="rightLst"><img class="lazy" data-src="img/icon/chevron-right.svg"
                                alt="Chevron Right" style="width: 20px;"> </a>
                    </div>

                </div>
            </div>
        </div>
    </section>

    {{-- <!-- <section id="los-mas-vendidos" style="overflow: hidden;">
              <div class="container">
                <div class="col-sm-12">
                  <div class="col-sm-12" style="t border-bottom: 1px solid #e5e5e5; margin-bottom: 0px; padding: 0 0 10px 0;">
                    <h1 style="font-weight: 600; color: #808080;">Los más vendidos</h1>
                  </div>

                  @foreach ($masVendidos as $p)
                  @if ($p->principal_color_imagen == 'true')
                  @if ($p->seccion_color_imagen == 1)
                  <div class="box-productos" style="float: left; padding: 5px 10px; text-align: center;">
                    <div class="box-productos-img">
                      <a id="box-productos-a" href="{{ route('shop.producto', $p->id_productos) }}">
                        <img src="img/productos/{{ $p->path_imagen }}" style="width: 100%;"/>
                      </a>
                    </div>
                    <div class="col-sm-12 box-productos-title" style="float: left; padding: 10px 0;">
                      <p style="margin-bottom: 0; color: #808080; font-size: 18px; font-weight: 400;">
                        {{ $p->nombre_productos }}
                      </p>
                      <p style="margin-bottom: 0; font-size: 20px; color: #606060; font-weight: 600;">
                        @if (is_null($p->precio_descuento_productos))
                          <span style="font-size: 14px">Gs.</span> {{ number_format($p->mayorista, 0, ',', '.') }}
            @else
                          <span style="font-size: 14px; margin-right: 3px; text-decoration: line-through; color: #808080;"><span style="font-size: 14px">Gs.</span> {{ number_format($p->mayorista, 0, ',', '.') }}</span>
                          <span style="color: #F25E52;"><span style="font-size: 14px">Gs.</span> {{ number_format($p->precio_descuento_productos, 0, ',', '.') }}</span>
                        @endif
                      </p>

                    </div>

                      @if ($p->descuentos_productos == 1)
                      <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                        OFERTA!
                      </div>
                      @endif
                      @if ($p->premium == 1)
                      <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                        LANZAMIENTO!
                      </div>
                      @endif
                      <div class="col-sm-12" style="padding: 0px; overflow: hidden; display: flex; justify-content: center; align-content: center;">

                        @foreach ($masVendidos as $pc)
                          @if ($pc->pk_productos_color_imagen == $p->id_productos)
                            @if ($pc->principal_color_imagen == 'true')
                              <span style="background: {{ $pc->prefijo_color }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                            @endif
                          @endif
                        @endforeach

                      </div>
                    <div class="col-12" style="padding: 0; margin-top: 10px; color: #808080; font-size: 15px;">
                      <p>Codigo. {{ $p->codigo_productos }}</p>
                    </div>
                  </div>

                  @endif
                  @endif
                  @endforeach
                </div>
              </div>
            </section> --> --}}

    <style>
        .oferta {
            font-size: 14px;
            position: absolute;
            top: 10px;
            right: 10px;
            color: #fff;
            padding: 10px 20px;
        }

        .container-productos {
            width: 1300px;
            margin: 0 auto;
        }

        .box-productos {
            width: 25%;
            position: relative;
            overflow: hidden;
            margin-bottom: 10px;
        }

        .box-productos-img {
            overflow: hidden;
        }

        .box-productos img {
            -webkit-transition: all 700ms ease;
            -o-transition: all 700ms ease;
            transition: all 700ms ease;
        }

        .box-productos img:hover {
            transform: scale(1.3);
            -webkit-transition: all 700ms ease;
            -o-transition: all 700ms ease;
            transition: all 700ms ease;
        }

        .box-productos-title {
            font-size: 12px;
        }

        .box-productos-carrito {
            font-size: 12px;
        }

        .box-productos.ninos {
            width: 16.5%;
        }

        @media screen and (max-width: 1439px) {
            .container-productos {
                width: 1100px;
            }
        }

        @media screen and (max-width: 1200px) {
            .container-productos {
                width: 991px;
            }
        }

        @media screen and (max-width: 991px) {
            .container-productos {
                width: 600px;
            }

            .box-productos {
                width: 50%;
            }

            .box-productos-title {
                font-size: 15px;
            }

            .box-productos-carrito {
                font-size: 15px;
            }

            .box-productos.ninos {
                width: 50%;
            }
        }

        @media screen and (max-width: 575px) {
            .container-productos {
                width: 100%;
                ;
            }

            .box-productos {
                width: 50%;
            }

            .box-productos-title {
                width: 100%;
                font-size: 15px;
            }

            .box-productos-carrito {
                width: 40%;
                font-size: 15px;
            }

            .box-productos.ninos {
                width: 50%;
            }
        }

    </style>

    <section id="new-productos">
        <div class="container">
            <div class="col-sm-12">
                <div class="col-lg-4 new-productos-title">
                    <img src="img/logo/logo-namopua-paraguay-octubre-rosa-cancer-de-mamas=paraguay.svg"
                        alt="Logo Ñamopu'a Paraguay" />
                </div>
                <div class="col-lg-8 new-productos-imagen">
                    <h1 style="color: #fff;">ÑAMOPU'A PARAGUAY S.A.</h1>
                    <p style="font-size: 18px; font-weight: 300; color: #fff;">
                        Somos una <strong>Industria Paraguaya</strong> con {{ $anos_trayectoria }} años de trayectoria de
                        <strong>fábrica de jeans en Paraguay</strong> a buen precio, hechas con mano de obra <strong>100%
                            nacional</strong>, desde
                        el diseño y confección de los jeans, hasta la distribución de los mismos a todo el rincón del
                        País, generando así una fuente de trabajo para cientos de personas, que prestan
                        atención hasta al más mínimo detalle de producción para ofrecer lo mejor a nuestros clientes.
                    </p>
                    <!-- <a class="btn btn-primary" style="color: #fff; border-radius: 30px;">Conózcanos</a> -->
                </div>
            </div>
        </div>
    </section>

    <style>
        #new-productos {
            padding: 80px 0 80px 0;
            overflow: hidden;
            background: #E5B3BE;
        }

        .new-productos-title {
            float: left;
            text-align: left;
            padding: 0;
        }

        .new-productos-imagen {
            text-align: left;
            padding-top: 0px;
            color: #373435;
            float: left;
        }

        .new-productos-title img {
            width: 70%;
        }

        .new-productos-imagen h1 {
            font-size: 40px;
            color: #000;
        }

        @media screen and (max-width: 991px) {
            .new-productos-title {
                text-align: center;
            }

            .new-productos-title img {
                width: 40%;
                margin-bottom: 30px;
            }

            .new-productos-imagen {
                text-align: center;
            }
        }

        @media screen and (max-width: 767px) {

            .new-productos-imagen h1,
            .new-productos-imagen p {
                text-align: center;
            }

            .new-productos-imagen h1 {
                font-size: 25px;
            }
        }


        .MultiCarousel2 {
            float: left;
            overflow: hidden;
            padding: 15px 0 0 0;
            width: 100%;
            position: relative;
        }

        .MultiCarousel2 .MultiCarousel-inner2 {
            transition: 1s ease all;
            float: left;
        }

        .MultiCarousel2 .MultiCarousel-inner2 .item2 {
            float: left;
        }

        .MultiCarousel2 .MultiCarousel-inner2 .item2>div {
            text-align: center;
            padding: 10px;
            margin: 0px;
        }

        .MultiCarousel2 .leftLst2,
        .MultiCarousel2 .rightLst2 {
            position: absolute;
            border-radius: 50%;
            top: calc(50% - 20px);
        }

        .MultiCarousel2 .leftLst2 {
            left: 0;
        }

        .MultiCarousel2 .rightLst2 {
            right: 0;
        }

        .MultiCarousel2 .leftLst2.over,
        .MultiCarousel2 .rightLst2.over {
            pointer-events: none;
        }

        .ofertasImperdibles_title {}

        @media screen and (max-width: 991px) {
            .ofertasImperdibles_title {
                display: none;
            }
        }

    </style>

    <section style="background: #f9f9f9;">
        <div class="container">
            <div class="col-sm-12" style="overflow: hidden;">
                <div class="col-sm-12" style="padding: 0;">
                    <h1 style="color: #808080; font-size: 30px; margin-bottom: 20px;"><i class="fa fa-building"></i>
                        Oportunidad Laboral</h1>
                    <p style="font-size: 18px; font-weight: 300; color: #808080;">
                        Envianos tus datos y curriculum en el rubro al que te dedicas, completando el formulario
                        correspondiente
                    </p>
                </div>
                <div class="col-sm-12" style="padding: 0; margin-top: 20px;">
                    <div class="row">

                        <div class="col-sm-12 empleos" style="margin-bottom: 30px;">
                            <ul style="margin-left: -40px; margin-bottom: 0;">
                                @foreach ($empleo as $em)
                                    <li
                                        style="padding-bottom: 10px; list-style: none; font-famity: 'Montserrat', sans-serif; font-weight: 300;">
                                        <a class="enlace_trabajo" href="javascript:void(0);"><i class="fa fa-check"
                                                style="color: #808080;"></i> {{ $em->nombre }}</a>
                                    </li>
                                @endforeach
                            </ul>

                            <a class="btn btn-outline-primary" href="javascript:void(0)" onclick="abrir_empleo()"
                                style="margin-top: 20px; border-radius: 50px;">
                                Completar formulario acá
                                <i class="fa fa-chevron-right" style="margin-left: 10px;"></i>
                            </a>
                        </div>

                        <div class="col-sm-12 acordeon_formulario_empleo" style="background: #fff; padding: 20px 30px;">
                            <form id="form_empleo" multiple="multiple" method="post">
                                <div class="col-sm-12">
                                    <p style="font-weight: 300; color: #808080; margin: 0;">Subir Curriculum en formato PDF
                                    </p>
                                </div>
                                <div class="col-sm-12">
                                    <div class="container-input">
                                        <input type="file" name="cv" id="cv" required accept="application/pdf"
                                            class="inputfile inputfile-5"
                                            data-multiple-caption="{count} archivos seleccionados" multiple />
                                        <label for="cv">
                                            <figure>
                                                <svg xmlns="http://www.w3.org/2000/svg" class="iborrainputfile" width="20"
                                                    height="17" viewBox="0 0 20 17">
                                                    <path
                                                        d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z">
                                                    </path>
                                                </svg>
                                            </figure>
                                            <p class="iborrainputfile">Seleccionar archivo</p>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-6" style="float: left;">
                                    <div class="col-12"
                                        style="padding: 0; border-bottom: 1px solid #e5e5e5; margin-bottom: 20px;">
                                        <p style="color: #808080; font-weight: 300; margin:0;">Tus Datos</p>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0;">
                                        <p for="" style="color: #808080; font-weight: 300; margin:0;">Listado de empleos
                                            vigentes</p>
                                        <select name="vigentes_empleo" id="vigentes_empleo" class="form-control"
                                            onchange="buscarEmpleo()" required="required">
                                            <option value="" selected disabled>Seleccione..</option>
                                            @foreach ($empleo as $emp)
                                                <option value="{{ $emp->id }}">{{ $emp->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0;">
                                        <p for="" style="color: #808080; font-weight: 300; margin:0;">Nombre y Apellido</p>
                                        <input type="text" class="form-control" name="nombre_empleo" required>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0;">
                                        <p for="" style="color: #808080; font-weight: 300; margin:0;">Nº Cedula de Identidad
                                        </p>
                                        <input type="text" class="form-control" name="ci_empleo" required>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0;">
                                        <p for="" style="color: #808080; font-weight: 300; margin:0;">Teléfono (Celular)</p>
                                        <input type="text" class="form-control" name="telefono_empleo" required>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0;">
                                        <p for="" style="color: #808080; font-weight: 300; margin:0;">Dirección</p>
                                        <input type="text" class="form-control" name="direccion_empleo" required>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0;">
                                        <p for="" style="color: #808080; font-weight: 300; margin:0;">Ciudad</p>
                                        <input type="text" class="form-control" name="ciudad_empleo" required>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0;">
                                        <p for="" style="color: #808080; font-weight: 300; margin:0;">Barrio</p>
                                        <input type="text" class="form-control" name="barrio_empleo" required>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0;">
                                        <p for="" style="color: #808080; font-weight: 300; margin:0;">Año de Experiencia</p>
                                        <input type="number" class="form-control" name="experiencia_empleo" required>
                                    </div>
                                    <div class="form-group col-sm-12" style="padding: 0;">
                                        <p for="" style="color: #808080; font-weight: 300; margin:0;">Descripción</p>
                                        <textarea class="form-control" name="descripcion_empleo" required></textarea>
                                    </div>

                                    <div class="form-group col-sm-12" style="padding: 0; margin-top: 20px;">
                                        <button type="submit" class="btn btn-success" id="btn_empleo"
                                            style="padding-left: 30px; padding-right: 30px; border-radius: 50px;">Enviar</button>
                                    </div>

                                </div>

                                <div class="col-sm-6" style="float: left;">
                                    <div class="col-sm-12"
                                        style="padding: 0; border-bottom: 1px solid #e5e5e5; margin-bottom: 20px;">
                                        <p style="color: #808080; font-weight: 300; margin:0;">Requisitos</p>
                                    </div>

                                    <div class="col-sm-12 requisitos_empleo">

                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="snackbar">
        <div class="col-sm-12" style="border-bottom: 1px solid #e5e5e5; padding: 10px 20px 7px 30px;">
            <p style="margin: 0; color: #1677BB;"><i class="fa fa-bell" style=" "></i> NOTIFICACIÓN</p>
        </div>
        <div class="col-sm-12" style="padding: 15px 20px 15px 30px; background: #f5f5f5;">
            <p style="margin: 0;">Gracias! En caso de quedar seleccionado/a, nos estaremos comunicando contigo en la
                brevedad posible.</p>
        </div>
    </div>
    <style>
        #snackbar {
            visibility: hidden;
            background-color: #fff;
            color: #808080;
            text-align: left;
            border-radius: 2px;
            padding: 0px;
            position: fixed;
            z-index: 1;
            right: 0;
            bottom: 80px;
            width: 400px;
            border-top-left-radius: 20px;
            font-size: 14px;
            box-shadow: -3px 3px 10px #c5c5c5;
        }

        @media screen and (max-width: 767px) {
            #snackbar {
                visibility: hidden;
                background-color: #fff;
                color: #808080;
                text-align: left;
                border-radius: 2px;
                padding: 0px;
                position: fixed;
                z-index: 99999999;
                right: 0px;
                bottom: 80px;
                width: 90%;
                border-top-left-radius: 0px;
                font-size: 14px !important;
                box-shadow: -3px 3px 10px #c5c5c5;
            }

            #snackbar p {
                font-size: 14px !important;
            }
        }

        #snackbar.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 4.5s;
            animation: fadein 0.5s, fadeout 0.5s 4.5s;
        }

        @-webkit-keyframes fadein {
            from {
                bottom: 0;
                opacity: 0;
            }

            to {
                bottom: 80px;
                opacity: 1;
            }
        }


        @-webkit-keyframes fadeout {
            from {
                bottom: 80px;
                opacity: 1;
            }

            to {
                bottom: 0;
                opacity: 0;
            }
        }

    </style>
    <style>
        .enlace_trabajo {
            color: #808080;
        }

        .acordeon_formulario_empleo {
            display: none;
        }

        /**********File Inputs**********/
        .container-input {
            text-align: center;
            padding: 40px 0 20px 0;
            border-radius: 6px;
            width: 100%;
            margin: 0 auto;
            margin-bottom: 20px;
            border: 2px dotted #b1b1b1;
        }

        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

        .inputfile+label {
            max-width: 80%;
            font-size: 1.25rem;
            font-weight: 700;
            text-overflow: ellipsis;
            white-space: nowrap;
            cursor: pointer;
            display: inline-block;
            overflow: hidden;
            padding: 0.625rem 1.25rem;
        }

        .inputfile+label svg {
            width: 1em;
            height: 1em;
            vertical-align: middle;
            fill: currentColor;
            margin-top: -0.25em;
            margin-right: 0.25em;
        }

        .iborrainputfile {
            font-size: 16px;
            font-weight: 300;
        }


        /* style 5 */

        .inputfile-5+label {
            color: #b1b1b1;
        }

        .inputfile-5:focus+label,
        .inputfile-5.has-focus+label,
        .inputfile-5+label:hover {
            color: #000;
        }

        .inputfile-5+label figure {
            width: 50px;
            height: 50px;
            border-radius: 50%;
            display: block;
            padding: 5px;
            margin: 0 auto;
        }

        .inputfile-5:focus+label figure,
        .inputfile-5.has-focus+label figure,
        .inputfile-5+label:hover figure {
            color: #000;
        }

        .inputfile-5+label svg {
            width: 100%;
            height: 100%;
            fill: #b1b1b1;
        }

    </style>



    {{-- <section id="ofertas" style="overflow: hidden;"> --}}
    {{-- <div class="container"> --}}
    {{-- <div class="col-sm-12"> --}}
    {{-- <div class="col-sm-12 ofertasImperdibles_title" style="t border-bottom: 1px solid #e5e5e5; margin-bottom: 0px; padding: 0 0 10px 0;"> --}}
    {{-- <h1 style="font-weight: 600; color: #808080;">Ofertas imperdibles</h1> --}}
    {{-- </div> --}}
    {{-- <div class="MultiCarousel2" data-items="1,2,3,5,6" data-slide-a="1" id="MultiCarousel2"  data-interval="1000"> --}}
    {{-- <h1 class="masVendidos_title_responsive" style="font-weight: 600; color: #808080;">Ofertas Imperdibles</h1> --}}

    {{-- <div class="MultiCarousel-inner2"> --}}

    {{-- @foreach ($ofertas as $o) --}}
    {{-- <div class="item2" style="position: relative;"> --}}
    {{-- <a id="box-productos-a" href="{{route('shop.producto', $o->id_productos)}}"> --}}
    {{-- <img class="lazy" data-src="img/productos/{{$o->path_imagen}}" alt="{{ $o->nombre_productos }} {{ $o->codigo_productos }}" style="width: 100%;"> --}}
    {{-- </a> --}}
    {{-- @if ($o->descuentos_productos == 1) --}}
    {{-- <div class="cartel" style="padding: 5px 20px; color: #fff; background: #F25E52; position: absolute; top: 5px; right: 5px; border-radius: 30px;"> --}}
    {{-- Oferta --}}
    {{-- </div> --}}
    {{-- @endif --}}

    {{-- @if ($o->premium == 1) --}}
    {{-- <div class="cartel" style="padding: 5px 20px; color: #fff; background: #1677BB; position: absolute; top: 5px; right: 5px; border-radius: 30px;"> --}}
    {{-- Lanzamiento --}}
    {{-- </div> --}}
    {{-- @endif --}}

    {{-- <div class="pad15" style="text-align: center;"> --}}
    {{-- <p class="lead" style=" margin-top: 20px; margin-bottom: 0; color: #808080; font-size: 18px; font-weight: 400;"> --}}
    {{-- {{$o->nombre_productos}} --}}
    {{-- </p> --}}
    {{-- <p style="margin-bottom: 10px; font-size: 20px; color: #606060; font-weight: 600;"> --}}
    {{-- @if (is_null($o->precio_descuento_productos)) --}}
    {{-- <span style="font-size: 14px">Gs.</span> {{ number_format($o->mayorista,  0, ',', '.')}} --}}
    {{-- @else --}}
    {{-- <span style="font-size: 14px; margin-right: 3px; text-decoration: line-through; color: #808080;"><span style="font-size: 14px">Gs.</span> {{number_format($o->mayorista,  0, ',', '.')}}</span> --}}
    {{-- <span style="color: #F25E52;"><span style="font-size: 14px">Gs.</span> {{ number_format($o->precio_descuento_productos,  0, ',', '.') }}</span> --}}
    {{-- @endif --}}
    {{-- </p> --}}

    {{-- <!-- @if ($o->descuentos_productos == 1) --}}
    {{-- <div class="oferta" style=" background: #F25E52; border-radius: 30px;"> --}}
    {{-- OFERTA! --}}
    {{-- </div> --}}
    {{-- @endif --}}
    {{-- @if ($o->premium == 1) --}}
    {{-- <div class="oferta" style=" background: #1677BB; border-radius: 30px;"> --}}
    {{-- LANZAMIENTO! --}}
    {{-- </div> --}}
    {{-- @endif --> --}}

    {{-- <p> --}}
    {{-- <div class="col-sm-12" style="margin-bottom: 10px; padding: 0px; overflow: hidden; display: flex; justify-content: center; align-content: center;"> --}}
    {{-- @foreach ($colorImagen as $pc) --}}
    {{-- @if ($pc->pk_productos_color_imagen == $o->id_productos) --}}
    {{-- @if ($pc->principal_color_imagen == 'true') --}}
    {{-- <span style="background: {{$pc->prefijo_color}}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span> --}}
    {{-- @endif --}}
    {{-- @endif --}}
    {{-- @endforeach --}}
    {{-- </div> --}}
    {{-- </p> --}}
    {{-- <p style="color: #808080; font-size: 15px;">Código {{$o->codigo_productos}}</p> --}}
    {{-- </div> --}}

    {{-- </div> --}}
    {{-- @endforeach --}}

    {{-- </div> --}}
    {{-- <a class="leftLst2"><img src="img/icon/chevron-left.svg" style="width: 20px;"></a> --}}
    {{-- <a class="rightLst2"><img src="img/icon/chevron-right.svg" style="width: 20px;"> </a> --}}
    {{-- </div> --}}
    {{-- </div> --}}
    {{-- </div> --}}
    {{-- </section> --}}






    <!-- <section id="covid" style="background: #f9f9f9;">
                                                              <div class="container">
                                                                <div class="col-sm-12">
                                                                  <div class="col-sm-12" style=" margin-bottom: 0px; padding: 0 0 10px 0;">
                                                                    <h1 style="font-weight: 600; color: #808080;">Modo Covid</h1>
                                                                  </div>

                                                                  <div class="col-sm-12" style="padding: 0;">
                                                                    @foreach ($cont as $con)
                                                                    @if ($con->pk_secciones_contenido == 6)
                                                                    <div class="col-sm-6" style="padding: 10px; float: left; ">
                                                                      <img src="img/{{ $con->path_contenido }}" style="width: 100%; margin-bottom: 20px;"/>
                                                                      <p>{{ $con->titulo_contenido }}</p>
                                                                      <h1><?php
                                                                      //echo nl2br($con->texto_contenido)
                                                                      ?></h1>

                                                                      <button class="btn btn-primary" style="margin-top: 20px; border-radius: 15px; padding: 10px 40px;">Ver más</button>
                                                                    </div>
                                                                    @endif
                                                                    @endforeach
                                                                  </div>

                                                                </div>
                                                              </div>
                                                            </section> -->

    <style>
        #covid {
            overflow: hidden;
        }

        #covid p {
            font-family: 'Montserrat', sans-serif;
            margin-top: 5px;
            margin-bottom: 0;
            color: #808080;
        }

        #covid h1 {
            font-family: 'Montserrat', sans-serif;
            margin-top: 5px;
            margin-bottom: 0;
            color: #606060;
        }

    </style>



    <section id="damas" style="overflow: hidden;">
        <div class="container">
            <div class="col-sm-12">
                <div class="col-sm-12" style="margin-bottom: 0px; padding: 0 0 10px 0;">
                    <h1 style="font-weight: 600; color: #808080;">Damas</h1>
                </div>
                <div class="row">
                    @foreach ($damas as $da)
                        <div class="box-productos" style="float: left; padding: 5px 10px; text-align: center;">
                            <div class="box-productos-img">
                                <a id="box-productos-a" href="{{ route('shop.producto', $da->id_productos) }}">
                                    <img class="lazy" data-src="img/productos/{{ $da->path_imagen }}"
                                        alt="{{ $da->nombre_productos }} {{ $da->codigo_productos }}"
                                        style="width: 100%;" />
                                </a>
                            </div>
                            <div class="col-sm-12 box-productos-title" style="float: left; padding: 10px 0;">
                                <p style="margin-bottom: 0; color: #808080; font-size: 18px; font-weight: 400;">
                                    {{ $da->nombre_productos }}
                                </p>
                                <p style="margin-bottom: 0; font-size: 20px; color: #606060; font-weight: 600;">
                                    @if (is_null($da->precio_descuento_productos))
                                        <span style="font-size: 14px">Gs.</span>
                                        {{ number_format($da->mayorista, 0, ',', '.') }}
                                    @else
                                        <span style="color: #F25E52;"><span style="font-size: 14px">Gs.</span>
                                            {{ number_format($da->precio_descuento_productos, 0, ',', '.') }}</span>
                                    @endif
                                </p>
                            </div>

                            @if ($da->descuentos_productos == 1)
                                <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                                    OFERTA!
                                </div>
                            @endif
                            @if ($da->premium == 1)
                                <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                                    PREMIUM
                                </div>
                            @endif
                            @if ($da->lanzamientos_productos == 1)
                                <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                                    LANZAMIENTO
                                </div>
                            @endif
                            <div class="col-sm-12"
                                style="padding: 0px; overflow: hidden; display: flex; justify-content: center; align-content: center;">

                                @foreach ($colorImagen as $pc)
                                    {{-- {{ var_dump($pc) }} --}}
                                    @if ($pc->pk_productos_color_imagen == $da->id_productos)
                                        @if ($pc->principal_color_imagen == 'true')
                                            @if ($pc->estado_color == true)
                                                <span
                                                    style="border: 1px solid #b1b1b1; background: {{ $pc->prefijo_color }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                                            @endif
                                        @endif
                                    @endif
                                @endforeach

                            </div>
                            <div class="col-12"
                                style="padding: 0; margin-top: 10px; color: #808080; font-size: 15px;">
                                <p>Código {{ $da->codigo_productos }}</p>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-12 ver_mas" style="overflow: hidden;">
                <a class="btn btn-primary" href="/busqueda?texto=Dama">Ver más productos <i class="fa fa-chevron-right"
                        style="margin-left: 10px;"></i></a>
            </div>
        </div>
    </section>

    <style>
        .ver_mas {
            text-align: right;
        }

        .ver_mas a {
            border-radius: 30px;
        }

        .ver_mas a:hover {
            color: #1676BA;
        }

        @media screen and (max-width: 991px) {
            .ver_mas {
                text-align: center;
            }
        }

    </style>






    <section id="caballeros" style="overflow: hidden;">
        <div class="container">
            <div class="col-sm-12">

                <div class="col-md-6" style=" float: left; padding: 5px 10px; text-align: center;">
                    <div class="box-productos-img" style="border: 1px solid #f5f5f5;">
                        <a id="box-productos-a" href="/">
                            <img class="lazy"
                                data-src="{{ asset('img/fabrica_de_jeans_en_paraguay_covid_bg.jpg') }}" alt=""
                                style="width: 100%;" />
                        </a>
                    </div>
                </div>

                <div class="col-md-6" style=" float: left; padding: 5px 10px; text-align: center;">
                    <div class="box-productos-img" style="border: 1px solid #f5f5f5;">
                        <a id="box-productos-a" href="{{ route('shop.tiendaOlimpia') }}">
                            <img class="lazy"
                                data-src="{{ asset('img/fabrica_de_jeans_en_paraguay_olimpia_bg.jpg') }}" alt=""
                                style="width: 100%;" />
                        </a>
                    </div>
                </div>

            </div>

        </div>
    </section>









    <section id="caballeros" style="overflow: hidden;">
        <div class="container">
            <div class="col-sm-12">
                <div class="col-sm-12"
                    style="t border-bottom: 1px solid #e5e5e5; margin-bottom: 0px; padding: 0 0 10px 0;">
                    <h1 style="font-weight: 600; color: #808080;">Caballeros</h1>
                </div>
                <div class="row">
                    @foreach ($hombres as $hom)
                        <div class="box-productos" style="float: left; padding: 5px 10px; text-align: center;">
                            <div class="box-productos-img">
                                <a id="box-productos-a" href="{{ route('shop.producto', $hom->id_productos) }}">
                                    <img class="lazy" data-src="img/productos/{{ $hom->path_imagen }}"
                                        alt="{{ $hom->nombre_productos }} {{ $hom->codigo_productos }}"
                                        style="width: 100%;" />
                                </a>
                            </div>
                            <div class="col-sm-12 box-productos-title" style="float: left; padding: 10px 0;">
                                <p style="margin-bottom: 0; color: #808080; font-size: 18px; font-weight: 400;">
                                    {{ $hom->nombre_productos }}
                                </p>
                                <p style="margin-bottom: 0; font-size: 20px; color: #606060; font-weight: 600;">
                                    @if (is_null($hom->precio_descuento_productos))
                                        <span style="font-size: 14px">Gs.</span>
                                        {{ number_format($hom->mayorista, 0, ',', '.') }}
                                    @else
                                        <span style="color: #F25E52;"><span style="font-size: 14px">Gs.</span>
                                            {{ number_format($hom->precio_descuento_productos, 0, ',', '.') }}</span>
                                    @endif
                                </p>
                            </div>

                            @if ($hom->descuentos_productos == 1)
                                <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                                    OFERTA!
                                </div>
                            @endif
                            @if ($hom->premium == 1)
                                <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                                    PREMIUM
                                </div>
                            @endif
                            @if ($hom->lanzamientos_productos == 1)
                                <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                                    LANZAMIENTO
                                </div>
                            @endif
                            <div class="col-sm-12"
                                style="padding: 0px; overflow: hidden; display: flex; justify-content: center; align-content: center;">

                                @foreach ($colorImagen as $pc)
                                    @if ($pc->pk_productos_color_imagen == $hom->id_productos)
                                        @if ($pc->principal_color_imagen == 'true')
                                            @if ($pc->estado_color == true)
                                            <span
                                                style="border: 1px solid #b1b1b1; background: {{ $pc->prefijo_color }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                                        @endif
                                        @endif
                                    @endif
                                @endforeach

                            </div>
                            <div class="col-12"
                                style="padding: 0; margin-top: 10px; color: #808080; font-size: 15px;">
                                <p>Código {{ $hom->codigo_productos }}</p>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-12 ver_mas" style="overflow: hidden;">
                <a class="btn btn-primary" href="/busqueda?texto=Hombre">Ver más productos <i class="fa fa-chevron-right"
                        style="margin-left: 10px;"></i></a>
            </div>
        </div>
    </section>
    @if ($nina->count() > 0 || $ninos->count() > 0)

        <section id="ninos" style="overflow: hidden;">
            <div class="container">
                <div class="col-sm-12">
                    <div class="col-sm-12"
                        style="t border-bottom: 1px solid #e5e5e5; margin-bottom: 0px; padding: 0 0 10px 0;">
                        <h1 style="font-weight: 600; color: #808080;">Niños</h1>
                    </div>
                    <div class="row">
                        @foreach ($ninos as $nino)
                            <div class="box-productos ninos" style="float: left; padding: 5px 10px; text-align: center;">
                                <div class="box-productos-img">
                                    <a id="box-productos-a"
                                        href="{{ route('shop.producto', $nino->id_productos) }}?color={{ $nino->id_color }}">
                                        <img class="lazy" data-src="img/productos/{{ $nino->path_imagen }}"
                                            alt="{{ $nino->nombre_productos }} {{ $nino->codigo_productos }}"
                                            style="width: 100%;" />
                                    </a>
                                </div>
                                <div class="col-sm-12 box-productos-title" style="float: left; padding: 10px 0;">
                                    <p style="margin-bottom: 0; color: #808080; font-size: 15px; font-weight: 400;">
                                        {{ $nino->nombre_productos }}
                                    </p>
                                    <p style="margin-bottom: 0; font-size: 20px; color: #606060; font-weight: 600;">
                                        @if (is_null($nino->precio_descuento_productos))
                                            <span style="font-size: 14px">Gs.</span>
                                            {{ number_format($nino->mayorista, 0, ',', '.') }}
                                        @else
                                            <span style="color: #F25E52;"><span style="font-size: 14px">Gs.</span>
                                                {{ number_format($nino->precio_descuento_productos, 0, ',', '.') }}</span>
                                        @endif
                                    </p>
                                </div>

                                @if ($nino->descuentos_productos == 1)
                                    <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                                        OFERTA!
                                    </div>
                                @endif
                                @if ($nino->premium == 1)
                                    <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                                        PREMIUM
                                    </div>
                                @endif
                                @if ($nino->lanzamientos_productos == 1)
                                    <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                                        LANZAMIENTO
                                    </div>
                                @endif
                                <div class="col-sm-12"
                                    style="padding: 0px; overflow: hidden; display: flex; justify-content: center; align-content: center;">

                                    @foreach ($colorImagen as $pc)
                                        @if ($pc->pk_productos_color_imagen == $nino->id_productos)
                                            @if ($pc->principal_color_imagen == 'true' && $pc->pk_color_color_imagen == $nino->id_color)
                                                @if ($pc->estado_color == true)
                                                    <span style="border: 1px solid #b1b1b1; background: {{ $pc->prefijo_color }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                                                        @break
                                                @endif
                                            @endif
                                        @endif
                                    @endforeach
                    </div>
                    <div class="col-12" style="padding: 0; margin-top: 10px; color: #808080; font-size: 15px;">
                        <p>Código {{ $nino->codigo_productos }}</p>
                    </div>

                </div>
    @endforeach

    @foreach ($nina as $ni)
        <div class="box-productos ninos" style="float: left; padding: 5px 10px; text-align: center;">
            <div class="box-productos-img">
                <a id="box-productos-a"
                    href="{{ route('shop.producto', $ni->id_productos) }}?color={{ $ni->id_color }}">
                    <img class="lazy" data-src="img/productos/{{ $ni->path_imagen }}"
                        alt="{{ $ni->nombre_productos }} {{ $ni->codigo_productos }}" style="width: 100%;" />
                </a>
            </div>
            <div class="col-sm-12 box-productos-title" style="float: left; padding: 10px 0;">
                <p style="margin-bottom: 0; color: #808080; font-size: 15px; font-weight: 400;">
                    {{ $ni->nombre_productos }}
                </p>
                <p style="margin-bottom: 0; font-size: 20px; color: #606060; font-weight: 600;">
                    @if (is_null($ni->precio_descuento_productos))
                        <span style="font-size: 14px">Gs.</span>
                        {{ number_format($ni->mayorista, 0, ',', '.') }}
                    @else
                        <span style="color: #F25E52;"><span style="font-size: 14px">Gs.</span>
                            {{ number_format($ni->precio_descuento_productos, 0, ',', '.') }}</span>
                    @endif
                </p>
            </div>

            @if ($ni->descuentos_productos == 1)
                <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                    OFERTA!
                </div>
            @endif
            @if ($ni->premium == 1)
                <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                    PREMIUM
                </div>
            @endif
            @if ($ni->lanzamientos_productos == 1)
                <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                    LANZAMIENTO
                </div>
            @endif
            <div class="col-sm-12"
                style="padding: 0px; overflow: hidden; display: flex; justify-content: center; align-content: center;">

                @foreach ($colorImagen as $pc)
                    @if ($pc->pk_productos_color_imagen == $ni->id_productos)
                        @if ($pc->principal_color_imagen == 'true' && $pc->pk_color_color_imagen == $nino->id_color)
                        @if ($pc->estado_color == true)
                            <span
                                style="border: 1px solid #b1b1b1; background: {{ $pc->prefijo_color }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                            @break
                        @endif
                    @endif
                @endif
    @endforeach

    </div>
    <div class="col-12" style="padding: 0; margin-top: 10px; color: #808080; font-size: 15px;">
        <p>Código {{ $ni->codigo_productos }}</p>
    </div>

    </div>
    @endforeach
    </div>
    </div>

    <div class="col-12 ver_mas" style="overflow: hidden;">
        <a class="btn btn-primary" href="/busqueda?texto=Ninos">Ver más productos <i class="fa fa-chevron-right"
                style="margin-left: 10px;"></i></a>
    </div>
    </div>
    </section>

    @endif


    <section id="especiales" style="overflow: hidden;">
        <div class="container">
            <div class="col-sm-12">
                <div class="col-sm-12"
                    style="t border-bottom: 1px solid #e5e5e5; margin-bottom: 0px; padding: 0 0 10px 0;">
                    <h1 style="font-weight: 600; color: #808080;">Tallas Especiales</h1>
                </div>
                <div class="row">
                    @foreach ($especiales as $hom)
                        <div class="box-productos" style="float: left; padding: 5px 10px; text-align: center;">
                            <div class="box-productos-img">
                                <a id="box-productos-a" href="{{ route('shop.producto', $hom->id_productos) }}">
                                    <img class="lazy" data-src="img/productos/{{ $hom->path_imagen }}"
                                        alt="{{ $hom->nombre_productos }} {{ $hom->codigo_productos }}"
                                        style="width: 100%;" />
                                </a>
                            </div>
                            <div class="col-sm-12 box-productos-title" style="float: left; padding: 10px 0;">
                                <p style="margin-bottom: 0; color: #808080; font-size: 18px; font-weight: 400;">
                                    {{ $hom->nombre_productos }}
                                </p>
                                <p style="margin-bottom: 0; font-size: 20px; color: #606060; font-weight: 600;">
                                    @if (is_null($hom->precio_descuento_productos))
                                        <span style="font-size: 14px">Gs.</span>
                                        {{ number_format($hom->mayorista, 0, ',', '.') }}
                                    @else
                                        <span style="color: #F25E52;"><span style="font-size: 14px">Gs.</span>
                                            {{ number_format($hom->precio_descuento_productos, 0, ',', '.') }}</span>
                                    @endif
                                </p>
                            </div>

                            @if ($hom->descuentos_productos == 1)
                                <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                                    OFERTA!
                                </div>
                            @endif
                            @if ($hom->premium == 1)
                                <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                                    PREMIUM
                                </div>
                            @endif
                            @if ($hom->lanzamientos_productos == 1)
                                <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                                    LANZAMIENTO
                                </div>
                            @endif
                            <div class="col-sm-12"
                                style="padding: 0px; overflow: hidden; display: flex; justify-content: center; align-content: center;">

                                @foreach ($colorImagen as $pc)
                                    @if ($pc->pk_productos_color_imagen == $hom->id_productos)
                                        @if ($pc->principal_color_imagen == 'true')
                                            <span
                                                style="border: 1px solid #b1b1b1; background: {{ $pc->prefijo_color }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                                        @endif
                                    @endif
                                @endforeach

                            </div>
                            <div class="col-12"
                                style="padding: 0; margin-top: 10px; color: #808080; font-size: 15px;">
                                <p>Código {{ $hom->codigo_productos }}</p>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-12 ver_mas" style="overflow: hidden;">
                <a class="btn btn-primary" href="/busqueda?texto=tallesEspeciales">Ver más productos <i
                        class="fa fa-chevron-right" style="margin-left: 10px;"></i></a>
            </div>
        </div>
    </section>

    <section id="nina" style="overflow: hidden; background: #f5f5f5;">
        <div class="container">

            <style>
                .olimpia-box {
                    width: 25%;
                }

                @media screen and (max-width: 767px) {
                    .olimpia-box {
                        width: 50%;
                    }

                    .nino {
                        margin-top: 40px;
                    }
                }

            </style>
            <div class="col-sm-12 nino" style="float: left; padding: 0;">
                <div class="col-sm-12"
                    style="t border-bottom: 1px solid #e5e5e5; margin-bottom: 0px; padding: 0 0 10px 0;">
                    <h1 style="font-weight: 600; color: #808080;">Productos bajo licencia de Club Olimpia</h1>
                </div>

                @foreach ($olimpia as $no)
                    <div class="olimpia-box" style="float: left; padding: 5px 10px; text-align: center;">
                        <div class="box-productos-img">
                            <a id="box-productos-a" href="{{ route('shop.producto', $no->id_productos) }}">
                                <img class="lazy" data-src="img/productos/{{ $no->path_imagen }}"
                                    alt="{{ $no->nombre_productos }} {{ $no->codigo_productos }}"
                                    style="width: 100%;" />
                            </a>
                        </div>
                        <div class="col-sm-12 box-productos-title" style="float: left; padding: 10px 0;">
                            <p style="margin-bottom: 0; color: #808080; font-size: 18px; font-weight: 400;">
                                {{ $no->nombre_productos }}
                            </p>
                            <p style="margin-bottom: 0; font-size: 20px; color: #606060; font-weight: 600;">
                                @if (is_null($no->precio_descuento_productos))
                                    <span style="font-size: 14px">Gs.</span>
                                    {{ number_format($no->mayorista, 0, ',', '.') }}
                                @else
                                    <span style="color: #F25E52;"><span style="font-size: 14px">Gs.</span>
                                        {{ number_format($no->precio_descuento_productos, 0, ',', '.') }}</span>
                                @endif
                            </p>

                        </div>

                        @if ($no->descuentos_productos == 1)
                            <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                                OFERTA!
                            </div>
                        @endif
                        @if ($no->premium == 1)
                            <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                                PREMIUM
                            </div>
                        @endif
                        @if ($no->lanzamientos_productos == 1)
                            <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                                LANZAMIENTO
                            </div>
                        @endif
                        <div class="col-sm-12"
                            style="padding: 0px; overflow: hidden; display: flex; justify-content: center; align-content: center;">

                            @foreach ($colorImagen as $pc)
                                @if ($pc->pk_productos_color_imagen == $no->id_productos)
                                    @if ($pc->principal_color_imagen == 'true')
                                        <span
                                            style="border: 1px solid #b1b1b1; background: {{ $pc->prefijo_color }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                                    @endif
                                @endif
                            @endforeach

                        </div>

                        <div class="col-12" style="padding: 0; margin-top: 10px; color: #808080; font-size: 15px;">
                            <p>Código {{ $no->codigo_productos }}</p>
                        </div>
                    </div>
                @endforeach
                <div class="col-12 ver_mas" style="overflow: hidden;">
                    <a class="btn btn-primary" style="background: #000;" href="/TiendaOlimpia">Ver más productos <i
                            class="fa fa-chevron-right" style="margin-left: 10px;"></i></a>
                </div>
            </div>


        </div>
    </section>






    <section id="new-historia" style="margin-bottom: 50px; overflow: hidden;">
        <div class="container">
            <div class="col-sm-12" style="padding: 0; overflow: hidden;">
                <!-- <div class="line-historia" style="float: left;">
                                                                    <div class="col-12" style="border-bottom: 2px solid #cecece; height: 15px;">

                                                                    </div>
                                                                  </div> -->
                <div class="col-12" style="float: left; text-align: center;">
                    <h1 style="font-weight: 600; color: #808080">Nuestra Historia</h1>
                </div>
                <!-- <div class="line-historia" style="float: left;">
                                                                    <div class="col-12" style="border-bottom: 2px solid #cecece; height: 15px;">

                                                                    </div>
                                                                  </div> -->
            </div>

            <div class="col-sm-12" style="padding: 0; margin-top: 40px;">
                @foreach ($cont as $co)
                    <div class="col-lg-4 col-md-6 div_historia">
                        <div class="col-12 contenido_historia">
                            <div class="col-12 imagen_historia" style="text-align: center;">
                                <img src="../img/{{ $co->path_contenido }}" style="width: 70px;" />
                            </div>
                            <div class="col-12" style="text-align: center; margin-top: 20px; color: #3287C1;">
                                <h1>{{ $co->titulo_contenido }}</h1>
                            </div>

                            <div class="col-sm-12 contenido-texto">
                                <?php echo nl2br($co->texto_contenido); ?>
                            </div>

                        </div>


                    </div>
                @endforeach

            </div>










        </div>
    </section>

    <style>
        .div_historia {
            float: left;
            padding: 30px;
        }

        .line-historia {
            width: 385px;

        }

        .text-historia {
            width: 340px;
        }

        .contenido_historia {
            overflow: hidden;
            height: 420px;
            padding: 50px 10px 50px 10px;
            border-radius: 30px;
            background: #f9f9f9;
            box-shadow: 15px 15px 30px 0px #cecece;
        }

        @media screen and (max-width: 1199px) {
            .line-historia {
                width: 325px;
            }

            .text-historia {
                width: 280px;
            }

            .contenido_historia {
                height: 460px;
            }
        }

        @media screen and (max-width: 991px) {
            .line-historia {
                width: 215px;
            }

            .text-historia {
                width: 260px;
            }
        }

        @media screen and (max-width: 767px) {
            .line-historia {
                width: 145px;
            }

            .text-historia {
                width: 220px;
            }

            .imagen_historia {
                display: none;
            }

            .contenido_historia {
                height: 250px;
                padding: 10px 10px;
            }

            .div_historia {
                float: left;
                padding: 10px;
            }
        }

        @media screen and (max-width: 575px) {
            .line-historia {
                width: 160px;
                text-align: center;
            }

            .text-historia {
                width: 220px;
                text-align: center;
            }
        }

        @media screen and (max-width: 569px) {
            .line-historia {
                width: 155px;
                text-align: center;
            }

            .text-historia {
                width: 220px;
                text-align: center;
            }
        }

        @media screen and (max-width: 559px) {
            .line-historia {
                width: 150px;
                text-align: center;
            }

            .text-historia {
                width: 220px;
                text-align: center;
            }
        }

        @media screen and (max-width: 549px) {
            .line-historia {
                width: 145px;
                text-align: center;
            }

            .text-historia {
                width: 220px;
                text-align: center;
            }
        }

        @media screen and (max-width: 539px) {
            .line-historia {
                width: 135px;
                text-align: center;
            }

            .text-historia {
                width: 220px;
                text-align: center;
            }
        }

        @media screen and (max-width: 519px) {
            .line-historia {
                width: 130px;
                text-align: center;
            }

            .text-historia {
                width: 220px;
                text-align: center;
            }
        }

        @media screen and (max-width: 509px) {
            .line-historia {
                width: 120px;
                text-align: center;
            }

            .text-historia {
                width: 220px;
                text-align: center;
            }
        }

        #historia {
            overflow: hidden;
        }

        .contenido-box {
            height: 400px;
        }

        .year-contenido {
            text-align: center;
            background: #e5e5e5;
            margin: 0 auto;
            width: 150px;
            padding: 20px;
        }

        .year-contenido h1 {
            font-weight: 200;
            font-size: 25px;
            margin: 0;
            color: #909090;
        }

        .contenido-icon-1 {
            width: 150px;
            float: left;
            padding: 0;
            height: 35px;
            border-bottom: 1px solid #e5e5e5;
        }

        .contenido-icon-2 {
            width: 70px;
            float: left;
            padding: 18px 0;
            text-align: center;
            background: #e5e5e5;
            border-radius: 100px;
        }

        .contenido-icon-2 img {
            width: 30px;
        }

        @media screen and (max-width: 1200px) {
            .contenido-icon-2 {
                width: 60px;
                padding: 12px 0;
            }

            .contenido-icon-1 {
                width: 124.5px;
                height: 30px;
            }
        }

        @media screen and (max-width: 991px) {

            .contenido-icon-1 {
                width: 45%;
                height: 30px;
            }

            .contenido-texto p {
                font-size: 17px;
            }
        }

        @media screen and (max-width: 767px) {
            .contenido-icon-1 {
                width: 43.5%;
                height: 30px;
            }
        }

        @media screen and (max-width: 521px) {
            .contenido-icon-1 {
                width: 43.2%;
                height: 30px;
            }
        }

        @media screen and (max-width: 500px) {
            .contenido-icon-1 {
                width: 43%;
                height: 30px;
            }
        }

        .triangulo {
            margin: 0 auto;
            text-align: center;
            width: 0px;
            height: 0px;
            border-right: 20px solid transparent;
            border-top: 20px solid #e5e5e5;
            border-left: 20px solid transparent;
            border-bottom: 20px solid transparent;
        }

        .contenido-texto {
            padding: 0 15px;
            overflow: hidden;
            margin-top: 15px;
            text-align: center;
        }

        .contenido-texto p {
            font-family: 'Montserrat', sans-serif;
            color: #909090;
        }

    </style>


    <section id="proyecto" style="background: #f9f9f9; overflow: hidden;">
        <div class="container">
            <div class="col-12">
                <div class="col-lg-4" style="float: left;">
                    <img src="img/industria-de-confecciones-de-jean-en-paraguay.png">
                </div>
                <div class="col-lg-8" style="float: left; padding-top: 30px;">
                    <h1>DESDE UN PROYECTO EN MENTE A HACERLO REALIDAD</h1><br>
                    <p>
                        Somos una <strong>industria paraguaya</strong> con una gran infraestructura. Realizamos desde la
                        importación de telas hasta la producción final de la prenda, manteniendo el estricto control de
                        calidad que nos caracteriza. Desarrollamos todo el programa de producción, la compra de tejidos e
                        insumos, recepción, elaboración, tizado, extendido de tejido y corte, loteo, confecciones, bordados,
                        procesos de lavandería, marcados, empaquetados, colocación de botones;
                        así también el aseo de prendas, control de calidad, doblado, empaquetado, almacenamientos de stock y
                        la logística de envío a sus tiendas.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <style>
        #proyecto h1 {
            font-size: 20px;
        }

        #proyecto img {
            width: 100%;
        }

        @media screen and (max-width: 991px) {
            #proyecto img {
                width: 60%;
            }

            #proyecto {
                text-align: center;
            }

            #proyecto p {
                font-size: 17px;
            }
        }

    </style>

    {{-- <div class="image2">
        <div class="container">
            <div class="col-lg-12" style="margin-bottom: 30px;">
                <h1>QUERÉS TRABAJAR CON NOSOTROS?</h1>
                <p>SISTEMA DE PRODUCCÍON MODULAR</p>
            </div>
            <div class="col-lg-6" style="float: left;">
                <p>Tienes la idea?</p>
                <p>Deseas emprender tu propio negocio?</p>
                <p style="margin-bottom: 0;">Querés producción para tu marca de Jeans?</p>
            </div>
            <div class="col-lg-6" style="float: left; text-align: right;">
                <p style="color: #fff; margin-top: 20px; margin-right: 10px;">PARA MÁS INFORMACIÓN</p>
                <a href="https://api.whatsapp.com/send?phone=595981143380&text=Hola%20me%20interesado%20en%20trabajar%20con%20Ustedes"
                    target="_blank" class="btn btn-primary btn-informacion"
                    style="padding-left: 20px; padding-right: 20px; border-radius: 30px;"><i class="fa fa-whatsapp"></i>
                    Contáctanos</a>
            </div>
        </div>
    </div> --}}

    <style>
        .image2 {
            /* 
                                                    background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(img/industria-de-jeans-namopua.jpg) no-repeat fixed;
                                                    background-attachment: fixed;
                                                    background-position: top center;
                                                    background-repeat: no-repeat;
                                                    background-size: cover;
                                                    */
            background: #1676BA;
            padding-top: 80px;
            padding-bottom: 80px;
            color: #fff;
            overflow: hidden;
        }

        .btn-informacion {
            background: #fff;
            color: #1676BA;
            font-weight: 400;
        }

    </style>

    <script>
        function myFunction() {
            $('#snackbar').addClass("show");

            setTimeout(function() {
                $('#snackbar').removeClass("show");
            }, 5000);
        }


        function abrir_empleo() {
            $('.acordeon_formulario_empleo').toggle({
                "display": "block"
            });
            $("#form_empleo")[0].reset();
            $('.requisitos_empleo').empty();
            $('#btn_empleo').removeClass('disabled');
        }

        //INPUT FILE PERSONALIZADO

        'use strict';

        ;
        (function(document, window, index) {
            var inputs = document.querySelectorAll('.inputfile');
            Array.prototype.forEach.call(inputs, function(input) {
                var label = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener('change', function(e) {
                    var fileName = '';
                    if (this.files && this.files.length > 1)
                        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}',
                            this.files.length);
                    else
                        fileName = e.target.value.split('\\').pop();

                    if (fileName)
                        label.querySelector('p').innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });
            });
        }(document, window, 0));

        function buscarEmpleo() {
            var empleo = $('#vigentes_empleo').val();
            $.ajax({
                url: "{{ route('guest.buscarEmpleo') }}",
                method: "GET",
                data: {
                    empleo: empleo
                },
                success: function(data) {
                    $('.requisitos_empleo').empty();
                    $('.requisitos_empleo').html(data);
                }
            });
        }

        $('#form_empleo').on('submit', function(e) {
            e.preventDefault();
            $('#btn_empleo').addClass('disabled');
            var formData = new FormData($('#form_empleo')[0]);
            formData.append('_token', "{{ csrf_token() }}");
            $.ajax({
                url: "{{ route('guest.agregarEmpleoGuest') }}",
                method: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                    //myFunction();
                    if (data.code == 200) {
                        console.log(data.message);
                        myFunction();
                        $('.acordeon_formulario_empleo').toggle({
                            "display": "block"
                        });
                    } else if (data.code == 500) {
                        console.log(data.message);
                    }
                }
            });
        });


        $(document).ready(function() {
            var itemsMainDiv = ('.MultiCarousel');
            var itemsDiv = ('.MultiCarousel-inner');
            var itemWidth = "";

            $('.leftLst, .rightLst').click(function() {
                var condition = $(this).hasClass("leftLst");
                if (condition)
                    click(0, this);
                else
                    click(1, this)
            });

            ResCarouselSize();




            $(window).resize(function() {
                ResCarouselSize();
            });

            //this function define the size of the items
            function ResCarouselSize() {
                var incno = 0;
                var dataItems = ("data-items");
                var itemClass = ('.item');
                var id = 0;
                var btnParentSb = '';
                var itemsSplit = '';
                var sampwidth = $(itemsMainDiv).width();
                var bodyWidth = $('body').width();
                $(itemsDiv).each(function() {
                    id = id + 1;
                    var itemNumbers = $(this).find(itemClass).length;
                    btnParentSb = $(this).parent().attr(dataItems);
                    itemsSplit = btnParentSb.split(',');
                    $(this).parent().attr("id", "MultiCarousel" + id);


                    if (bodyWidth >= 2600) {
                        incno = itemsSplit[3];
                        itemWidth = sampwidth / incno;
                    } else if (bodyWidth >= 992) {
                        incno = itemsSplit[2];
                        itemWidth = sampwidth / incno;
                    } else if (bodyWidth >= 768) {
                        incno = itemsSplit[1];
                        itemWidth = sampwidth / incno;
                    } else {
                        incno = itemsSplit[1];
                        itemWidth = sampwidth / incno;
                    }
                    $(this).css({
                        'transform': 'translateX(0px)',
                        'width': itemWidth * itemNumbers
                    });
                    $(this).find(itemClass).each(function() {
                        $(this).outerWidth(itemWidth);
                    });

                    $(".leftLst").addClass("over");
                    $(".rightLst").removeClass("over");

                });
            }


            //this function used to move the items
            function ResCarousel(e, el, s) {
                var leftBtn = ('.leftLst');
                var rightBtn = ('.rightLst');
                var translateXval = '';
                var divStyle = $(el + ' ' + itemsDiv).css('transform');
                var values = divStyle.match(/-?[\d\.]+/g);
                var xds = Math.abs(values[4]);
                if (e == 0) {
                    translateXval = parseInt(xds) - parseInt(itemWidth * s);
                    $(el + ' ' + rightBtn).removeClass("over");

                    if (translateXval <= itemWidth / 2) {
                        translateXval = 0;
                        $(el + ' ' + leftBtn).addClass("over");
                    }
                } else if (e == 1) {
                    var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
                    translateXval = parseInt(xds) + parseInt(itemWidth * s);
                    $(el + ' ' + leftBtn).removeClass("over");

                    if (translateXval >= itemsCondition - itemWidth / 2) {
                        translateXval = itemsCondition;
                        $(el + ' ' + rightBtn).addClass("over");
                    }
                }
                $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
            }

            //It is used to get some elements from btn
            function click(ell, ee) {
                var Parent = "#" + $(ee).parent().attr("id");
                var slide = $(Parent).attr("data-slide");
                ResCarousel(ell, Parent, slide);
            }

        });

        $(document).ready(function() {
            var itemsMainDiv = ('.MultiCarousel2');
            var itemsDiv = ('.MultiCarousel-inner2');
            var itemWidth = "";

            $('.leftLst2, .rightLst2').click(function() {
                var condition = $(this).hasClass("leftLst2");
                if (condition)
                    click(0, this);
                else
                    click(1, this)
            });

            ResCarouselSize();




            $(window).resize(function() {
                ResCarouselSize();
            });

            //this function define the size of the items
            function ResCarouselSize() {
                var incno = 0;
                var dataItems = ("data-items");
                var itemClass = ('.item2');
                var id = 0;
                var btnParentSb = '';
                var itemsSplit = '';
                var sampwidth = $(itemsMainDiv).width();
                var bodyWidth = $('body').width();
                $(itemsDiv).each(function() {
                    id = id + 1;
                    var itemNumbers = $(this).find(itemClass).length;
                    btnParentSb = $(this).parent().attr(dataItems);
                    itemsSplit = btnParentSb.split(',');
                    $(this).parent().attr("id", "MultiCarousel2");


                    if (bodyWidth >= 1900) {
                        incno = itemsSplit[3];
                        itemWidth = sampwidth / incno;
                    } else if (bodyWidth >= 992) {
                        incno = itemsSplit[3];
                        itemWidth = sampwidth / incno;
                    } else if (bodyWidth >= 768) {
                        incno = itemsSplit[1];
                        itemWidth = sampwidth / incno;
                    } else {
                        incno = itemsSplit[1];
                        itemWidth = sampwidth / incno;
                    }
                    $(this).css({
                        'transform': 'translateX(0px)',
                        'width': itemWidth * itemNumbers
                    });
                    $(this).find(itemClass).each(function() {
                        $(this).outerWidth(itemWidth);
                    });

                    $(".leftLst2").addClass("over");
                    $(".rightLst2").removeClass("over");

                });
            }


            //this function used to move the items
            function ResCarousel(e, el, s) {
                var leftBtn = ('.leftLst2');
                var rightBtn = ('.rightLst2');
                var translateXval = '';
                var divStyle = $(el + ' ' + itemsDiv).css('transform');
                var values = divStyle.match(/-?[\d\.]+/g);
                var xds = Math.abs(values[4]);
                if (e == 0) {
                    translateXval = parseInt(xds) - parseInt(itemWidth * s);
                    $(el + ' ' + rightBtn).removeClass("over");

                    if (translateXval <= itemWidth / 2) {
                        translateXval = 0;
                        $(el + ' ' + leftBtn).addClass("over");
                    }
                } else if (e == 1) {
                    var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
                    translateXval = parseInt(xds) + parseInt(itemWidth * s);
                    $(el + ' ' + leftBtn).removeClass("over");

                    if (translateXval >= itemsCondition - itemWidth / 2) {
                        translateXval = itemsCondition;
                        $(el + ' ' + rightBtn).addClass("over");
                    }
                }
                $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
            }

            //It is used to get some elements from btn
            function click(ell, ee) {
                var Parent = "#" + $(ee).parent().attr("id");
                var slide = $(Parent).attr("data-slide-a");
                ResCarousel(ell, Parent, slide);
            }

        });



        // $('#carouselExampleIndicators').carousel({
        //   interval: 4000
        // });
    </script>



@endsection
