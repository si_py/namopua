<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Registro | Ñamopu'a Py</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css')}}">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{ asset('css/fontastic.css')}}">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="{{ asset('css/grasp_mobile_progress_circle-1.0.0.min.css')}}">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css')}}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('css/style.default.css') }}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;600;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
  </head>
  <body>

  <style>
    section {padding-top: 80px; padding-bottom: 80px;}
    h1, label, span, h2, a { font-family: 'Montserrat', sans-serif; }
    .btn-primary {
    background-color: #1677BB;
    border: 1px solid #1677BB;
    }
    .btn-primary:hover {
    background-color: #175b89;
    border: 1px solid #175b89;
    }

    a{
    color: #1677BB;
    }
    a:hover {
    text-decoration: none;
    color: #175b89;
    }
    input {
        border-radius: 30px !important;
        padding: 15px !important;
        font-family: 'Montserrat', sans-serif;
        margin-bottom: 0  !important;
        padding-left: 70px !important;
    }
    input::placeholder {
        font-size: 17px !important;
        font-family: 'Montserrat', sans-serif !important;
        font-weight: 400 !important;
    }
    .btn-outline-primary {
        border-radius: 30px;
        padding: 5px 20px 5px 20px;
        border: 2px solid #1677BB;
        color: #1677BB;
        font-weight: 600;
    }
    .btn-outline-primary:hover {
        background: #1677BB;
        border: 2px solid #1677BB;
    }
    .btn-primary {
        border-radius: 30px;
        padding: 5px 20px 5px 20px;
    }
    .contenedor {
        padding-top: 80px;
    }
    .title {
        margin-bottom: 30px; 
        margin-top: 0px; 
        overflow: hidden; 
        text-align: right;
    }
    .izquierda {

    }
    .izquierda h1{
        font-weight: 600; 
        font-size: 60px; 
        margin-bottom: 0;
        color: #000;
    }
    .derecha {
        float: left; 
        overflow: hidden; 
        padding-top: 10px;
    }
    .card-body {
        overflow: hidden; padding: 50px 50px;
    }
    .footer-izquierda {
        width: 80%;
        float: left;
    }
    .footer-derecha {
        width: 20%;
        float: left;
        text-align: right;
    }
    .btn-register {
        margin-top: 20px;
        text-align: right;
    }
    #iniciar-sesion {
        font-size: 14px; 
        color: #808080; 
        margin-top: 90px;
    }

    @media screen and (max-width: 991px){
        .derecha {
            float: left; 
            overflow: hidden; 
            padding-top: 25px;
        }
        .izquierda {text-align: center;}
        .izquierda h1{ font-size: 40px; margin-bottom: 10px;}
        .title {text-align: center;}
        .btn {text-align: center;}
        .footer-izquierda { width: 40%; }
        .footer-derecha { width: 60%; }
        .card-body {
            overflow: hidden; padding: 50px 20px;
        }
        .contenedor { padding-top: 20px; }
        #iniciar-sesion { margin-top: 20px; }
    }
    </style>

  
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 contenedor">
                <div class="col-12" style="border-radius: 30px; border: 1px solid #cecece; overflow: hidden;">

                    <div class="card-body">

                        <div class="col-12 title">
                            <h2 style="font-weight: 400; color: #808080;">{{ __('Registrar') }}</h2>
                        </div>

                        <div class="col-lg-6 izquierda" style="float: left;">
                            <img src="img/logo/Icono-namopua-paraguay.svg" style="width: 150px;">
                            <h1>Hola,</h1>
                            <h1 style="font-weight: 900; margin-top: -10px;">bienvenido!</h1>
                            <span id="iniciar-sesion">Ya tenés una cuenta?</span>
                            <a class="btn btn-outline-primary" href="/login"> Iniciar Sesión </a>
                        </div>

                        <div class="col-lg-6 derecha">

                            <form id="register_form" method="POST" action="{{ route('register') }}">
                                @csrf

                                <div class="form-group row">

                                    <div class="col-md-12" style="position: relative; margin-bottom: 0;">
                                        <input id="name" type="text" placeholder="Nombre y Apellido" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        <img src="img/icon/user-1.svg" style="width: 25px; position: absolute; top: 8px; left: 40px;">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                <!-- email -->

                                    <div class="col-md-12" style="position: relative; margin-bottom: 0;">
                                        <input id="email" placeholder="Correo Electrónico" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                        <img src="img/icon/envelope-alt.svg" style="width: 25px; position: absolute; top: 13px; left: 40px;">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-md-12" style="position: relative; margin-bottom: 0;">
                                        <input id="razon" type="text" placeholder="Razón Social" class="form-control @error('razon_users') is-invalid @enderror" name="razon" value="{{ old('razon') }}" required autocomplete="razon" autofocus>
                                        <img src="img/icon/suitcase-alt.svg" style="width: 25px; position: absolute; top: 8px; left: 40px;">
                                        @error('razon')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-md-12" style="position: relative; margin-bottom: 0;">
                                        <input id="ruc" type="text" placeholder="Ruc / C.I.N˚" class="form-control @error('ruc_users') is-invalid @enderror" name="ruc" value="{{ old('ruc_users') }}" required autocomplete="ruc" autofocus>
                                        <img src="img/icon/new.svg" style="width: 25px; position: absolute; top: 8px; left: 40px;">
                                        @error('ruc_users')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-md-12" style="position: relative; margin-bottom: 0;">
                                        <input id="direccion" type="text" placeholder="Dirección" class="form-control @error('direccion_users') is-invalid @enderror" name="direccion" value="{{ old('direccion_users') }}" required autocomplete="direccion" autofocus>
                                        <img src="img/icon/location.svg" style="width: 17px; position: absolute; top: 8px; left: 45px;">
                                        @error('direccion_users')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-md-12" style="position: relative; margin-bottom: 0;">
                                        <input id="telefono" type="number" placeholder="Teléfono Celular. Ej: 0981999999" class="form-control @error('telefono_users') is-invalid @enderror" name="telefono_users" value="{{ old('telefono_users') }}" required autocomplete="phone" autofocus>
                                        <img src="img/icon/phone.svg" style="width: 25px; position: absolute; top: 8px; left: 40px;">
                                        <span class="invalid-feedback" style="display: none" id="error_msg" role="alert">
                                            <strong></strong>
                                        </span>
                                        @error('telefono_users')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-md-12" style="position: relative; margin-bottom: 0;">
                                        <input id="password" placeholder="Contraseña" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                        <img src="img/icon/lock-open.svg" style="width: 17px; position: absolute; top: 10px; left: 45px;">
                                        </span>
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12" style="position: relative; margin-bottom: 0;">
                                        <input id="password-confirm" placeholder="Confirmar contraseña" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        <img src="img/icon/lock-open.svg" style="width: 17px; position: absolute; top: 10px; left: 45px;">
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-12 btn-register">
                                        <a href="/" style="margin-right: 20px; font-size: 14px; color: #808080;">
                                            <i class="fa fa-chevron-left" style="margin-right: 5px;"></i> Volver al inicio
                                        </a>
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Registrarme') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-12" style="float: left; overflow: hidden; margin-top: 30px;">

                            <div class="footer-izquierda">
                                <div class="col-12" style="border-bottom: 2px solid #cecece; height: 25px;"></div>
                            </div>

                            <div class="footer-derecha">
                                <span style="font-size: 20px; color: #808080; margin-right: 10px;">Seguinos</span>
                                <a href="https://www.facebook.com/namopuaparaguaysa" target="_blank" title="Facebook Ñamopu'a Paraguay S.A." style="color: #808080">
                                    <i class="fa fa-facebook-square" style="margin-right: 5px; font-size: 30px;"></i>
                                </a>
                                <a href="https://instagram.com/namopuaparaguay?igshid=1o3tora5vknk1" target="_blank" title="Instagram Ñamopu'a Paraguay S.A." style="color: #808080">
                                    <i class="fa fa-instagram" style="font-size: 30px;"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('js/register.js')}}"></script>
  </body>
</html>
