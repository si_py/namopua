<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Restablecer contraseña | Ñamopu'a Py</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css')}}">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{ asset('css/fontastic.css')}}">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="{{ asset('css/grasp_mobile_progress_circle-1.0.0.min.css')}}">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css')}}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('css/style.default.css') }}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;600;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
  </head>
  <body>

    <style>
        section {padding-top: 80px; padding-bottom: 80px;}
        h1, label, span, h2, a { font-family: 'Montserrat', sans-serif; }
        .btn-primary {
        background-color: #1677BB;
        border: 1px solid #1677BB;
        }
        .btn-primary:hover {
        background-color: #175b89;
        border: 1px solid #175b89;
        }

        a{
        color: #1677BB;
        }
        a:hover {
        text-decoration: none;
        color: #175b89;
        }
        input {
            border-radius: 30px !important;
            padding: 20px !important;
            font-family: 'Montserrat', sans-serif;
            margin-bottom: 0  !important;
            padding-left: 70px !important;
        }
        input::placeholder {
            font-size: 17px !important;
            font-family: 'Montserrat', sans-serif !important;
            font-weight: 400 !important;
        }
        .btn-outline-primary {
            border-radius: 30px;
            padding: 5px 20px 5px 20px;
            border: 2px solid #1677BB;
            color: #1677BB;
            font-weight: 600;
        }
        .btn-outline-primary:hover {
            background: #1677BB;
            border: 2px solid #1677BB;
        }
        .btn-primary {
            border-radius: 30px;
            padding: 5px 20px 5px 20px;
        }
        .contenedor {
            padding-top: 80px;
        }
        .title {
            margin-bottom: 30px; 
            margin-top: 0px; 
            overflow: hidden; 
            text-align: right;
        }
        .logo-div h1{ font-size: 70px; }

        .footer-izquierda {
            width: 80%;
            float: left;
        }
        .footer-derecha {
            width: 20%;
            float: left;
            text-align: right;
        }

        .contenedor-2 {
            padding: 100px;
        }
        @media screen and (max-width: 991px){
            .logo-div h1{ font-size: 40px; }
            .footer-izquierda { width: 40%; }
            .footer-derecha { width: 60%; }
            .contenedor-2 {padding: 50px;}
            .contenedor {padding-top: 20px;}
        }
    </style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 contenedor">
                <div class="col-12 contenedor-2" style="border-radius: 30px; border: 1px solid #cecece; overflow: hidden;">

                    <div class="card-body">
                        <div class="col-12 logo-div" style="text-align: center;">
                            <img src="../img/logo/Icono-namopua-paraguay.svg" style="width: 150px; margin-bottom: 20px;">
                            <h1 style="font-weight: 600;">Olvidaste</h1>
                            <h1 style="font-weight: bold; margin-top: -10px;">tu contraseña?</h1>
                            <!-- <span id="iniciar-sesion">Ya tenés una cuenta?</span>
                            <a class="btn btn-outline-primary" href="/login"> Iniciar Sesión </a> -->
                        </div>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="form-group row">

                                <div class="col-md-5" style="margin: 20px auto 0 auto;">
                                    <input id="email" placeholder="Correo electrónico" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    <img src="../img/icon/envelope-alt.svg" style="width: 30px; position: absolute; top: 10px; left: 40px;">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12" style="text-align: center;">
                                    <a href="/" style="margin-right: 20px; font-size: 14px; color: #808080;">
                                        <i class="fa fa-chevron-left" style="margin-right: 5px;"></i> Volver al inicio
                                    </a>
                                </div>
                                <div class="col-md-12" style="text-align: center; margin-top: 20px;">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Restablecer contraseña') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-12" style="float: left; overflow: hidden; margin-top: 30px;">

                        <div class="footer-izquierda">
                            <div class="col-12" style="border-bottom: 2px solid #cecece; height: 25px;"></div>
                        </div>

                        <div class="footer-derecha">
                            <span style="font-size: 20px; color: #808080; margin-right: 10px;">Seguinos</span>
                            <a href="https://www.facebook.com/namopuaparaguaysa" target="_blank" title="Facebook Ñamopu'a Paraguay S.A." style="color: #808080">
                                <i class="fa fa-facebook-square" style="margin-right: 5px; font-size: 30px;"></i>
                            </a>
                            <a href="https://instagram.com/namopuaparaguay?igshid=1o3tora5vknk1" target="_blank" title="Instagram Ñamopu'a Paraguay S.A." style="color: #808080">
                                <i class="fa fa-instagram" style="font-size: 30px;"></i>
                            </a>
                        </div>



                    </div>

                </div>
            </div>
        </div>
    </div>
  </body>
</html>
