@extends('layouts.app')

@section('content')
<!-- Breadcrumb-->
<div class="breadcrumb-holder">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Home</a></li>
      <li class="breadcrumb-item"><a href="/sliders">Sliders</a></li>
      <li class="breadcrumb-item active">Crear</li>
    </ul>
  </div>
</div>

<section>
  <div class="col-sm-12" style="padding: 10px;">
    <div class="row">

      <div class="col-lg-4">
        <div class="card">
          <div class="card-header d-flex align-items-center">
            <h4>Agregar Sliders</h4>
          </div>
          <div class="card-body">
            <form class="form-horizontal" action="{{route('sliders.images')}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="form-group row">
                <div class="col-sm-2">
                  Orden
                </div>
                <div class="col-sm-10">
                  <input type="number" value="1" class="form-control" name="orden">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-2">
                  <label>Sección</label>
                </div>
                <div class="col-sm-10">
                  <Select name="pk_secciones_sliders" class="form-control form-control-warning">
                    <option disabled selected>Seleccione..</option>
                  @foreach($secciones as $sec)
                    <option value="{{$sec->id_secciones}}">{{$sec->nombre_secciones}} | {{$sec->referencias_secciones}}</option>
                  @endforeach
                  </Select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-2">
                  <label>Imagen</label>
                </div>
                <div class="col-sm-10">
                  <input id="file" name="file" type="file" class="form-control form-control-warning">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-2">
                  <label>Imagen Mobile</label>
                </div>
                <div class="col-sm-10">
                  <input id="file-mobile" name="file-mobile" type="file" class="form-control form-control-warning">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-2">
                  <label>Titulo</label>
                </div>
                <div class="col-sm-10">
                  <input name="titulo_sliders" type="text" class="form-control">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-2">
                  <label>Titulo Main</label>
                </div>
                <div class="col-sm-10">
                  <input name="titulo_main_sliders" type="text" class="form-control">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-2">
                  <label>Descripción</label>
                </div>
                <div class="col-sm-10">
                  <input name="descripcion_sliders" type="text" class="form-control">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-10 offset-sm-2">
                  <button type="submit" class="btn btn-primary">Agregar Sección</button>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
@endsection
