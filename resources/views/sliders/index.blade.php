@extends('layouts.app')

@section('content')

    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Sliders</li>
            </ul>
        </div>
    </div>

    <section>
        <div class="col-sm-12" style="padding: 10px;">
            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="/sliders/create">
                                + Crear
                            </a>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Orden</th>
                                            <th>Imagen</th>
                                            <th>Referencias</th>
                                            <th>Imagen Mobile</th>
                                            <th>Referencias Mobile</th>
                                            <th>Titulo</th>
                                            <th>Main Titulo</th>
                                            <th>Descripción</th>
                                            <th>Secciones</th>
                                            <th><i class="fa fa-trash"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_slides">
                                        @foreach ($sliders as $sl)
                                            <tr data-id="{{ $sl->id_sliders }}">
                                                <th scope="row">{{ $sl->orden }}</th>
                                                <td>
                                                    <img src="img/{{ $sl->path_sliders }}" style="width: 100px;" />
                                                </td>
                                                <td>{{ $sl->path_sliders }}</td>
                                                <td>
                                                    <img src="img/{{ $sl->path_mobile_sliders }}" style="width: 50px;" />
                                                </td>
                                                <td>{{ $sl->path_mobile_sliders }}</td>
                                                <td>{{ $sl->titulo_sliders }}</td>
                                                <td>{{ $sl->namin_titulo_sliders }}</td>
                                                <td>{{ $sl->descripcion_sliders }}</td>
                                                <td>
                                                    @foreach ($secciones as $sec)
                                                        @if ($sl->pk_secciones_sliders == $sec->id_secciones)
                                                            {{ $sec->nombre_secciones }}
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>
                                                    <i class="fa fa-pencil editar-slider" data-id="{{ $sl->id_sliders }}"
                                                        data-path-desk="{{ asset('img/' . $sl->path_sliders) }}"
                                                        data-path-mob="{{ asset('img/' . $sl->path_mobile_sliders) }}"
                                                        data-seccion="{{ $sl->pk_secciones_sliders }}"
                                                        data-titulo="{{ $sl->titulo_sliders }}"
                                                        data-titulo-main="{{ $sl->namin_titulo_sliders }}"
                                                        data-descripcion="{{ $sl->descripcion_sliders }}"
                                                        data-orden="{{ $sl->orden }}" style="font-size: 20px"
                                                        onclick="editarSliderBtn(event)"></i>
                                                    <a href="#" onclick="Eliminar({{ $sl->id_sliders }})"
                                                        title="Eliminar: {{ $sl->path_sliders }}">
                                                        <i class="fa fa-trash"
                                                            style="cursor: pointer; font-size: 20px;"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modalEditarSlider" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form class="form-horizontal modal-content" id="editarSliderForm" method="post"
                onsubmit="submitEditarSlider(event)" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar slider</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label>Sección</label>
                        </div>
                        <div class="col-sm-10">
                            <Select name="pk_secciones_sliders" class="form-control form-control-warning">
                                <option disabled selected>Seleccione..</option>
                                @foreach ($secciones as $sec)
                                    <option value="{{ $sec->id_secciones }}">{{ $sec->nombre_secciones }} |
                                        {{ $sec->referencias_secciones }}</option>
                                @endforeach
                            </Select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label>Imagen</label>
                        </div>
                        <div class="col-sm-10">
                            <input name="file" type="file" class="form-control form-control-warning"
                                onchange="fileChange(event)">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-4">
                            <img src="#" alt="#" id="desk-preview" width="100%">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label>Imagen Mobile</label>
                        </div>
                        <div class="col-sm-10">
                            <input name="file-mobile" type="file" class="form-control form-control-warning"
                                onchange="fileChange(event)">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-4">
                            <img src="#" alt="#" id="mob-preview" width="100%">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label>Orden</label>
                        </div>
                        <div class="col-sm-10">
                            <input name="orden" type="number" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label>Titulo</label>
                        </div>
                        <div class="col-sm-10">
                            <input name="titulo_sliders" type="text" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label>Titulo Main</label>
                        </div>
                        <div class="col-sm-10">
                            <input name="titulo_main_sliders" type="text" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label>Descripción</label>
                        </div>
                        <div class="col-sm-10">
                            <input name="descripcion_sliders" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>

    <script>
        function Eliminar(id) {
            var route = "eliminarSliders/" + id + "";
            //alert(route);
            $.ajax({
                url: route,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'PUT',
                success: function() {
                    location.reload();
                }
            })
        };

        function editarSliderBtn(e) {
            $("#modalEditarSlider").modal('toggle')
            $("#modalEditarSlider select").val($(e.currentTarget).data('seccion')).change();
            $("#modalEditarSlider form").attr('action', "/editar-slider/" + $(e.currentTarget).data('id'))
            $("#modalEditarSlider input[name=titulo_sliders]").val($(e.currentTarget).data('titulo')).change()
            $("#modalEditarSlider input[name=titulo_main_sliders]").val($(e.currentTarget).data('titulo-main'))
            $("#modalEditarSlider input[name=orden]").val($(e.currentTarget).data('orden'))
                .change()
            $("#modalEditarSlider input[name=descripcion_sliders]").val($(e.currentTarget).data('descripcion'))
                .change()

            $("#desk-preview").attr('src', $(e.currentTarget).data('path-desk'))
            $("#mob-preview").attr('src', $(e.currentTarget).data('path-mob'))

        }

        function fileChange(e) {
            if (e.currentTarget.getAttribute('name') == 'file') {
                readFile(e.currentTarget.files[0], '#desk-preview')
            } else {
                readFile(e.currentTarget.files[0], '#mob-preview')
            }
        }

        function submitEditarSlider(e) {
            e.preventDefault();
            var fd = new FormData();
            fd.append('_token', $("#modalEditarSlider input[name=_token]").val());
            fd.append('titulo_sliders', $("#modalEditarSlider input[name=titulo_sliders]").val());
            fd.append('titulo_main_sliders', $("#modalEditarSlider input[name=titulo_main_sliders]").val());
            fd.append('descripcion_sliders', $("#modalEditarSlider input[name=descripcion_sliders]").val());
            fd.append('pk_secciones_sliders', $("#modalEditarSlider select[name=pk_secciones_sliders]")
                .val());
            fd.append('orden', document.querySelector("#modalEditarSlider input[name=orden]").value);
            fd.append('file', document.querySelector("#modalEditarSlider input[name=file]").files[0]);
            fd.append('file-mobile', document.querySelector("#modalEditarSlider input[name=file-mobile]")
                .files[0]);

            $.ajax({
                url: $(e.currentTarget).attr('action'),
                method: 'POST',
                processData: false,
                contentType: false,
                data: fd,
                success: function(resp) {
                    if (resp.success) {
                        const data = resp.data
                        $(`tr[data-id=${data.id_sliders}]`).replaceWith(nuevaFila(data))
                    }
                    $("#modalEditarSlider").modal('toggle')
                }
            })
        }

        function nuevaFila(data) {
            return `
                    <tr data-id="${data.id_sliders}">
                        <th scope="row">${data.orden}</th>
                        <td>
                            <img src="/img/${data.path_sliders}" style="width: 100px;" />
                        </td>
                        <td>${data.path_sliders}</td>
                        <td>
                            <img src="/img/${data.path_mobile_sliders}" style="width: 50px;" />
                        </td>
                        <td>${data.path_mobile_sliders}</td>
                        <td>${data.titulo_sliders != null ? data.titulo_sliders : ""}</td>
                        <td>${data.namin_titulo_sliders != null ? data.namin_titulo_sliders : ""}</td>
                        <td>${data.descripcion_sliders != null ? data.descripcion_sliders : ""}</td>
                        <td>
                            ${data.nombre_secciones}
                        </td>
                        <td>
                            <i class="fa fa-pencil editar-slider"
                                data-id="${data.id_sliders}"
                                data-path-desk="/img/${data.path_sliders}"
                                data-path-mob="/img/${data.path_mobile_sliders}"
                                data-seccion="${data.pk_secciones_sliders}"
                                data-titulo="${data.titulo_sliders}"
                                data-titulo-main="${data.namin_titulo_sliders}"
                                data-orden="${data.orden}"
                                data-descripcion="${data.descripcion_sliders}" style="font-size: 20px" onclick="editarSliderBtn(event)"></i>
                            <a href="#" onclick="Eliminar(${data.id_sliders})"
                                title="Eliminar: ${data.path_sliders}">
                                <i class="fa fa-trash"
                                    style="cursor: pointer; font-size: 20px;"></i>
                            </a>
                        </td>
                    </tr>
                `
        }

        function readFile(file, target) {
            const reader = new FileReader()
            reader.onload = function(event) {
                $(target).attr('src', event.target.result)
            }
            reader.readAsDataURL(file);
        }
    </script>

@endsection
