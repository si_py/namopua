@extends('layouts.front')

<?php
$sum = 0;
$sum_cant = 0;
$cantidad = 0;
$sum1 = 0;
$sum_cant1 = 0;
$cantidad1 = 0;
function cmp($a, $b)
{
    return strcmp($a['color'], $b['color']);
}
?>
@section('content')

    <div class="container-lg">
        <div class="col-sm-12 title-shop">

            <h1 style="font-weight: 900; font-family: 'Montserrat', sans-serif;">Carrito</h1>
            <p id="cantidad_title" style="margin-bottom: 0; font-weight: normal;">

                @if (session('cart'))

                    <?php
                    $cant = 0;
                    $cant1 = 0;
                    ?>
                    @foreach (session('cart') as $position => $y)
                        @if ($y['tipo'] == 'Olimpia')
                            <?php
                            $cant1 += intval($y['cantidad']);
                            $oli = $y['tipo'];
                            ?>
                        @else
                            <?php $cant += intval($y['cantidad']); ?>
                        @endif
                    @endforeach

                    Tenés
                    <span id="item-letter" style="color: #1677BB; font-weight: normal">
                        @if ($cant > 0 || $cant1 > 0)
                            @if ($cant > 0)
                                {{ $cant }} prenda(s) {{ $cant1 > 0 ? ' y ' : '' }}
                            @endif
                            @if ($y['tipo'] == 'Olimpia')
                                @if ($cant1 > 0)
                                    {{ $cant1 }} productos de {{ $oli }}
                                @endif
                            @endif
                        @else
                            0 prenda(s)
                        @endif
                    </span>
                    en tu carrito
                @endif
            </p>
            <p style="margin-bottom: 0;">
                @if (session('cart'))
                    @if ($cant > 0)
                        @if ($cant > 30)
                            Felicidades! Completaste la cantidad minima requerida para la compra
                        @else
                            Faltan
                            <span id="item-letter" style="color: #1677BB; font-weight: normal">
                                {{ 30 - $cant }} prenda(s)
                            </span> para completar tu pedido
                        @endif
                    @endif
                @endif
            </p>

        </div>

        <style>
            .title-shop {
                text-align: center;
                padding: 80px 0 80px 0;
            }

            .title-shop h1 {
                font-size: 100px;
                font-weight: 900;
            }

            .title-shop p {
                font-size: 17px;
                font-weight: 100;
                color: #909090;
                margin-top: 0px;
                margin-bottom: 0;
            }

            .title-compra {
                font-size: 17px;
                font-weight: 100;
                color: #909090;
                margin-top: 0px;
                margin-bottom: 0;
            }

            #cantidad_title {
                font-size: 22px;
            }

            .tabla_carrito {
                border: 1px solid #cecece;
                border-radius: 30px;
                padding: 20px 40px;
            }

            .tabla_carrito table td img {
                width: 78px;
            }

            @media screen and (max-width: 991px) {
                .title-shop {
                    text-align: center;
                    padding: 30px 0 30px 0;
                }

                .title-shop h1 {
                    font-size: 40px;
                }

                .title-shop p {
                    font-size: 14px;
                    font-weight: 100;
                    color: #909090;
                    margin-top: 0px;
                    margin-bottom: 0;
                }

                #item-letter,
                #cantidad_title {
                    font-size: 14px;
                }

                .tabla_carrito {
                    padding: 0px 10px;
                }

                .tabla_carrito table th {
                    padding: 5px;
                }

                .tabla_carrito table td img {
                    width: 45px;
                }
            }

            .table td,
            .table th {
                vertical-align: middle;
            }

        </style>

        <div class="col-sm-12">
            @if (Auth::guest())
            @else
                <p style="margin-bottom: 0; color: #808080;">Hola, <span
                        style="color: #000">{{ Auth::user()->name }}</span>, la descripción de tu pedido: </p>
            @endif
        </div>


        <div class="col-sm-12 tabla_carrito">
            <table class="table" style="width: 100%;" id="tabla_carrito">
                <thead style=" color: #1677BB;">
                    {{-- <tr> --}}
                    <th>Producto</th>
                    {{-- <th>Cant.</th> --}}
                    <th>Descripción</th>
                    {{-- <th>Total</th> --}}
                    <th></th>
                    {{-- </tr> --}}
                </thead>

                <tbody>
                    @if (session('cart'))
                        @foreach (session('cart') as $position => $y)
                            @php
                                usort($y['detalles'], 'cmp');
                            @endphp
                            @if ($y['tipo'] == 'Olimpia')
                            @else
                                <tr style="vertical-align: middle;">
                                    <td style="width: 10%;">
                                        <img src="../img/productos/{{ $y['imagen_producto'] }}" />
                                    </td>
                                    {{-- <td style="width: 10%;"> <span class="texto_carrito">{{ $y['cantidad'] }}</span></td> --}}
                                    <td style="width: 100%;">
                                        <span class="texto_carrito" style="font-weight: 300;">Codigo:</span>
                                        {{ $y['codigo_productos'] }} <br>
                                        <span class="texto_carrito" style="font-weight: 300;">Producto:</span>
                                        {{ $y['nombre_productos'] }}<br>
                                        <span class="texto_carrito" style="font-weight: 300;">Cantidad:</span>
                                        <span class="texto_carrito">{{ $y['cantidad'] }}</span><br>
                                        <span class="texto_carrito" style="font-weight: 300;">Precio unitario:</span>
                                        <span class="texto_carrito">
                                            @if ($y['precio_descuento'] == null)
                                                Gs. {{ number_format($y['mayoristas'], 0, ',', '.') }}
                                            @else
                                                Gs. {{ number_format($y['precio_descuento'], 0, ',', '.') }}
                                            @endif
                                        </span><br>
                                        <span class="texto_carrito" style="font-weight: 300;">Total:</span>
                                        <span class="texto_carrito">
                                            <span class="texto_carrito">
                                                Gs. {{ number_format($y['total'], 0, ',', '.') }}
                                            </span>
                                        </span>
                                        <div class="row">
                                            <div class="col-12">
                                                Detalles
                                            </div>
                                            <div class="col-2 text-center">
                                                Talla
                                            </div>
                                            <div class="col-6 text-center">
                                                Color
                                            </div>
                                            <div class="col-4 text-center">
                                                Cantidad
                                            </div>
                                        </div>
                                        @foreach ($y['detalles'] as $detalle)
                                            <div class="row detalles-row">
                                                <div class="col-2 text-center">
                                                    {{ $detalle['talla'] }}
                                                </div>
                                                <div class="col-6 text-center">
                                                    {{ $detalle['nombre_color'] }}
                                                </div>
                                                <div class="col-4 text-center">
                                                    {{ $detalle['cantidad'] }}
                                                </div>
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <td style="width: 10%;">
                                        <span class="texto_carrito">
                                            {{ number_format($y['total'], 0, ',', '.') }}
                                        </span>
                                    </td> --}}
                                    <td
                                        style="position: relative; display: flex; justify-content: center; align-items: start; margin: 0; padding-top: 50px;">
                                        <a class="trash-carrito" href="#"
                                            onclick="eliminarCarrito('{{ $y['id_token'] }}')" {{-- style="padding: 0; position: absolute; bottom: 40%; font-size: 20px;"> --}}>
                                            <img src="{{ asset('img/icon/trash.svg') }}" style="width: 20px;" />
                                        </a>
                                    </td>
                                </tr>
                                <?php
                                $sum += $y['total'];
                                $sum_cant += $y['cantidad'];
                                ?>
                            @endif
                        @endforeach
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <td style="text-align: right;" colspan="2">SUB TOTAL Gs.:</td>
                        <td>
                            <?php if (!empty($sum)) {
                                echo number_format($sum, 0, ',', '.');
                            } else {
                                echo 0;
                            } ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>


        <div class="col-sm-12 tabla_carrito" style="margin-top: 20px;">
            <table class="table" style="width: 100%;" id="tabla_carrito">
                <thead style=" color: #1677BB;">
                    <tr>
                        <th>Producto</th>
                        {{-- <th>Cant.</th> --}}
                        <th>Descripción</th>
                        {{-- <th>Total</th> --}}
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @if (session('cart'))
                        @php
                            $cart = session('cart');
                        @endphp
                        @foreach ($cart as $position => $y)
                            @php
                                usort($y['detalles'], 'cmp');
                            @endphp
                            @if ($y['tipo'] == 'Olimpia')
                                {{-- <tr style="vertical-align: middle;">
                                    <td style="width: 10%;">
                                        <img src="../img/productos/{{ $y['imagen_producto'] }}" />
                                    </td>
                                    <td style="width: 10%;"> <span class="texto_carrito">{{ $y['cantidad'] }}</span>
                                    </td>
                                    <td style="width: 60%;">
                                        <span class="texto_carrito" style="font-weight: 300;">Codigo:</span>
                                        {{ $y['codigo_productos'] }} <br>
                                        <span class="texto_carrito" style="font-weight: 300;">Producto:</span>
                                        {{ $y['nombre_productos'] }}<br>
                                        <span class="texto_carrito" style="font-weight: 300;">Color:</span>
                                        {{ $y['color'] }}<br>
                                        <span class="texto_carrito" style="font-weight: 300;">Talla:</span>
                                        {{ $y['talla'] }}<br>
                                        <span class="texto_carrito" style="font-weight: 300;">Precio unitario:</span>
                                        <span class="texto_carrito">
                                            @if ($y['precio_descuento'] == null)
                                                {{ number_format($y['mayoristas'], 0, ',', '.') }}
                                            @else
                                                {{ number_format($y['precio_descuento'], 0, ',', '.') }}
                                            @endif
                                        </span>
                                    </td>
                                    <td style="width: 10%;">
                                        <span class="texto_carrito">
                                            {{ number_format($y['total'], 0, ',', '.') }}
                                        </span>
                                    </td>
                                    <td style="width: 10%; position: relative;">
                                        <a class="trash-carrito" href="#"
                                            onclick="eliminarCarrito('{{ $y['id_token'] }}')"
                                            style="padding: 0; position: absolute; bottom: 40%; font-size: 20px;">
                                            <img src="{{ asset('img/icon/trash.svg') }}" style="width: 20px;" />
                                        </a>
                                    </td>
                                </tr> --}}

                                <tr style="vertical-align: middle;">
                                    <td style="width: 10%;">
                                        <img src="../img/productos/{{ $y['imagen_producto'] }}" />
                                    </td>
                                    {{-- <td style="width: 10%;"> <span class="texto_carrito">{{ $y['cantidad'] }}</span></td> --}}
                                    <td style="width: 100%;">
                                        <span class="texto_carrito" style="font-weight: 300;">Codigo:</span>
                                        {{ $y['codigo_productos'] }} <br>
                                        <span class="texto_carrito" style="font-weight: 300;">Producto:</span>
                                        {{ $y['nombre_productos'] }}<br>
                                        <span class="texto_carrito" style="font-weight: 300;">Cantidad:</span>
                                        <span class="texto_carrito">{{ $y['cantidad'] }}</span><br>
                                        <span class="texto_carrito" style="font-weight: 300;">Precio unitario:</span>
                                        <span class="texto_carrito">
                                            @if ($y['precio_descuento'] == null)
                                                Gs. {{ number_format($y['mayoristas'], 0, ',', '.') }}
                                            @else
                                                Gs. {{ number_format($y['precio_descuento'], 0, ',', '.') }}
                                            @endif
                                        </span><br>
                                        <span class="texto_carrito" style="font-weight: 300;">Total:</span>
                                        <span class="texto_carrito">
                                            <span class="texto_carrito">
                                                Gs. {{ number_format($y['total'], 0, ',', '.') }}
                                            </span>
                                        </span>
                                        <div class="row">
                                            <div class="col-12">
                                                Detalles
                                            </div>
                                            <div class="col-2 text-center">
                                                Talla
                                            </div>
                                            <div class="col-6 text-center">
                                                Color
                                            </div>
                                            <div class="col-4 text-center">
                                                Cantidad
                                            </div>
                                        </div>
                                        @foreach ($y['detalles'] as $detalle)
                                            <div class="row detalles-row">
                                                <div class="col-2 text-center">
                                                    {{ $detalle['talla'] }}
                                                </div>
                                                <div class="col-6 text-center">
                                                    {{ $detalle['nombre_color'] }}
                                                </div>
                                                <div class="col-4 text-center">
                                                    {{ $detalle['cantidad'] }}
                                                </div>
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <td style="width: 10%;">
                                        <span class="texto_carrito">
                                            {{ number_format($y['total'], 0, ',', '.') }}
                                        </span>
                                    </td> --}}
                                    <td
                                        style="position: relative; display: flex; justify-content: center; align-items: start; margin: 0; padding-top: 50px;">
                                        <a class="trash-carrito" href="#"
                                            onclick="eliminarCarrito('{{ $y['id_token'] }}')" {{-- style="padding: 0; position: absolute; bottom: 40%; font-size: 20px;"> --}}>
                                            <img src="{{ asset('img/icon/trash.svg') }}" style="width: 20px;" />
                                        </a>
                                    </td>
                                </tr>
                                <?php
                                $sum1 += $y['total'];
                                $sum_cant1 += $y['cantidad'];
                                ?>
                            @endif
                        @endforeach
                    @endif
                </tbody>

                {{-- <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td style="text-align: right;">SUB TOTAL Gs.:</td>
                        <td>
                            <?php if (!empty($sum1)) {
                                echo number_format($sum1, 0, ',', '.');
                            } else {
                                echo 0;
                            } ?>
                        </td>
                        <td></td>
                    </tr>
                </tfoot> --}}

                <tfoot>
                    <tr>
                        <td style="text-align: right;" colspan="2">SUB TOTAL Gs.:</td>
                        <td>
                            <?php if (!empty($sum1)) {
                                echo number_format($sum1, 0, ',', '.');
                            } else {
                                echo 0;
                            } ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>

        <div class="col-sm-12 tabla_carrito" style="margin-top: 20px;">
            <table class="table" style="width: 100%;" id="tabla_carrito">
                <thead style=" color: #1677BB;">
                    <tr>
                        <th style="font-size: 20px;">TOTAL</th>
                        <th></th>
                        <th></th>
                        <th style="font-size: 20px; text-align: right; padding-right: 30px;">Gs. <?php echo number_format($sum + $sum1); ?> </th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>

                </tbody>
            </table>
        </div>

        <div class="col-sm-12">
            <?php if (!empty($sum_cant) || !empty($sum_cant1)) {
                $sum_cant += $sum_cant1;
                $cantidad = number_format($sum_cant, 0, ',', '.');
            } else {
                $cantidad = 0;
            } ?>
            @if (session('cart'))
                @if ($cantidad < 30)
                    <div class="col-sm-12" style="text-align: center; margin-top: 20px;">
                        <p class="title-compra">
                            (*) Debes completar treinta unidades de prendas para prodecer al pago de su pedido, faltan
                            {{ 30 - $cantidad }} prenda(s) <br>
                            (*) OBS. Los articulos de Olimpia y Covid no incluyen dentro de la compra mayorista.

                        </p>
                        <a class="btn btn-primary" href="/shop" style="color: #fff; margin-top: 30px;">Ir a la Tienda</a>
                    </div>
                @else
                    @if (Auth::guest())
                        <div class="col-sm-12" style="text-align: center; padding: 0; margin-top: 20px;">
                            <a class="btn btn-primary" class="btn btn-primary" data-toggle="modal"
                                data-target="#modalIniciarSesion"
                                style="color: #fff; padding-left: 30px; padding-right: 30px; border-radius: 30px;">
                                Iniciar Sesión para efectuar la compra
                            </a>
                        </div>
                    @else
                        <div class="col-sm-12" style="text-align: center; padding: 0; margin-top: 30px;">
                            <a href="/caja" class="btn btn-primary"
                                style="color: #fff; padding-left: 30px; padding-right: 30px; border-radius: 30px;">Comprar</a>
                        </div>
                    @endif
                @endif
            @endif
        </div>

        <div class="col-sm-12" style="text-align: center; padding-top: 20px;">
            @error('email')
                <span class="invalid-feedback" style="display: block;" role="alert"><strong>{{ $message }}</strong></span>
            @enderror

            @error('password')
                <span class="invalid-feedback" style="display: block;" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
        </div>

        <div class="col-sm-12" style="margin-top: 20px; margin-bottom: 140px; text-align: center;">
            <p class="title-compra">Problemas con tus compras? </p>
            <a href="https://api.whatsapp.com/send?phone=595981143380&text=Desde%20el%20Carrito%20de%20la%20WEB:%20Hola!%20tengo%20una%20consulta%20podria%20dar%20información?"
                target="_blank">
                Contacte con nosotros <i class="fa fa-whatsapp"></i>
            </a>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalIniciarSesion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="margin-top: 50px; margin-bottom: 80px;">
                <div class="modal-header" style="border: none;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="text-shadow: none;">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="modal-body" style="overflow: hidden;">
                        <div class="col-sm-12">
                            <div class="col-lg-4 imagen_login" style="float: left;">
                                <img src="img/logo/Icono-namopua-paraguay.svg" />
                            </div>
                            <div class="col-lg-8" style="float: left; padding-top: 15px;">

                                <div class="form-group">
                                    <label for="email" class="col-md-12 col-form-label"
                                        style="text-align: left; padding-bottom: 0;">{{ __('Correo Electrónico') }}</label>

                                    <div class="col-md-12">
                                        <input id="email1" type="email" style="color: #000;"
                                            class="form-control @error('email') is-invalid @enderror @error('password') is-invalid @enderror"
                                            name="email" value="{{ old('email') }}" required autocomplete="email"
                                            autofocus>


                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="col-md-12 col-form-label"
                                        style="padding-bottom: 0;">{{ __('Contraseña') }}</label>

                                    <div class="col-md-12">
                                        <input id="password1" type="password" style="color: #000;"
                                            class="form-control @error('email') is-invalid @enderror @error('password') is-invalid @enderror"
                                            name="password" required autocomplete="current-password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                                {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Recuerdame') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        @error('email')
                                            <span class="invalid-feedback" style="display: block;"
                                                role="alert"><strong>{{ $message }}</strong></span>
                                        @enderror

                                        @error('password')
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        @if (Route::has('password.request'))
                                            <a class="" href=" {{ route('password.request') }}"
                                                style="color: #1677BB; font-size: 13px; margin-top: 10px;">
                                                {{ __('¿Olvidaste tu contraseña?') }}
                                            </a>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">

                                        <a class="" href=" {{ route('register') }}"
                                            style="color: #1677BB; font-size: 13px; margin-top: 10px;">
                                            {{ __('Registrarme') }}
                                        </a>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="margin-top: 30px; overflow: hidden;">
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Iniciar Sesión') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <style>
        .imagen_login {
            text-align: center;
        }

        .imagen_login img {
            width: 200px;
        }

        @media screen and (max-width: 991px) {
            .imagen_login {
                margin-bottom: 30px;
            }
        }

    </style>


    <script>
        function eliminarCarrito(key) {
            $.ajax({
                type: "ajax",
                url: "{{ route('carrito.removerItemCarrito') }}",
                type: 'GET',
                data: {
                    key: key
                },
                success: function(data) {
                    console.log(data);
                    location.reload();
                }
            });
        }
    </script>

@endsection
