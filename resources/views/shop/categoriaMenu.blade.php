<div class="side-shop p-2">
    <div class="col-12" style="background: #fff; border-radius: 15px; padding: 20px;">
        <ul>
            <li style="color: #000; margin-bottom: 5px;">Categorías</li>
            @foreach ($cat as $cate)
                @if ($cate->nombre_categorias != 'Olimpia')
                    <li><a href="{{ route('shop.categorias', $cate->id_categorias) }}">{{ $cate->nombre_categorias }}</a>
                    </li>
                @endif
            @endforeach
            <li style="color: #000; margin-bottom: 5px; margin-top: 10px;">SubCategorías</li>
            @foreach ($categorias as $sub)
                @if ($sub->nombre_categorias != 'Olimpia')
                    <li><a
                            href="{{ route('shop.subcategorias', $sub->id_subcategorias) }}">{{ $sub->nombre_subcategorias }}</a>
                    </li>
                @endif
            @endforeach
            <li style="color: #000; margin-bottom: 5px; margin-top: 10px;">Marca</li>
            @foreach ($marca as $m)
                @if ($m->nombre_marcas != 'Olimpia')
                    <li><a href="{{ route('shop.marcas', $m->id_marcas) }}">{{ $m->nombre_marcas }}</a></li>
                @endif
            @endforeach
            <li style="color: #000; margin-bottom: 5px; margin-top: 10px;">Tallas especiales</li>
            <li><a href="{{ route('shop.genero')."?texto=EspecialesHombre" }}">Plus+ Hombre</a></li>
            <li><a href="{{ route('shop.genero')."?texto=EspecialMujer" }}">Plus+ Mujer</a></li>
            {{-- @foreach ($marca as $m)
                @if ($m->nombre_marcas != 'Olimpia')
                    <li><a href="{{ route('shop.marcas', $m->id_marcas) }}">{{ $m->nombre_marcas }}</a></li>
                @endif
            @endforeach --}}
            @if ($premiumCount > 0)
                <li
                    style="margin-bottom: 5px; margin-top: 10px; background: #1677BB; color: #fff;  padding: 3px 15px 5px 15px; border-radius: 30px; width: fit-content;">
                    <a href="{{ route('shop.premium') }}" style="color: #fff;">
                        Premium
                    </a>
                </li>
            @endif
            @if ($lanzamientosCount > 0)
                <li
                    style="margin-bottom: 5px; margin-top: 10px; background: #94B53D; color: #fff;  padding: 3px 15px 5px 15px; border-radius: 30px; width: fit-content;">
                    <a href="{{ route('shop.lanzamientos') }}" style="color: #fff;">
                        Lanzamientos
                    </a>
                </li>
            @endif
            @if ($descuentosCount > 0)
                <li
                    style="margin-bottom: 5px; margin-top: 10px; background: #F25E52; color: #fff; padding: 3px 15px 5px 15px; border-radius: 30px; width: 80px;">
                    <a href="{{ route('shop.ofertas') }}" style="color: #fff;">
                        Ofertas
                    </a>
                </li>
            @endif
        </ul>
    
        <ul style="margin-top: 20px; border-top: 1px solid #e5e5e5;">
            <li style="color: #000; margin-bottom: 5px; margin-top: 20px;">Otros Productos</li>
            <li
                style="margin-bottom: 5px; margin-top: 10px; padding: 3px 15px 5px 0px; border-radius: 30px; color: #353535;">
                <a href="/TiendaOlimpia">
    
                    <img src="{{ asset('img/olimpia-logo.svg') }}" style="width: 40px; margin-right: 5px;"> Olimpia
                </a>
            </li>
    
            <li
                style="margin-bottom: 5px; margin-top: 10px; padding: 3px 15px 5px 0px; border-radius: 30px; color: #353535;">
                <a href="{{ route('shop.ofertas') }}">
                    <img src="{{ asset('img/mascarilla.svg') }}" style="width: 30px; margin-right: 10px;"> Tapabocas
                </a>
            </li>
    
    
        </ul>
    </div>
</div>

<style>
    .title-shop {
        text-align: center;
        padding: 80px 0 80px 0;
    }

    .title-shop h1 {
        font-size: 100px;
        font-weight: 900;
    }

    .title-shop p {
        font-size: 17px;
        font-weight: 100;
        color: #909090;
        margin-top: 0px;
        margin-bottom: 0;
    }

</style>

<style>
    .side-shop {
        float: left;
        width: 15%;
    }

    .side-shop ul {
        margin-left: -40px;
    }

    .side-shop ul li {
        list-style: none;
        font-family: 'Montserrat', sans-serif;
        font-weight: 400;
        font-size: 14px;
        color: #909090;
    }

    .side-shop ul li a {
        color: #808080;
    }

    .side-shop ul li a:hover {
        color: #1677BB;
    }

    .content-shop {
        float: left;
        width: 85%;
        font-family: 'Montserrat', sans-serif;
    }

    .content-shop select {
        border: 1px solid #e5e5e5;
        color: #909090;
        font-size: 14px;
        padding: 0px 20px 0px 10px;
        border-radius: 3px;
    }

    @media screen and (max-width: 1150px) {
        .side-shop {
            display: none;
        }

        .content-shop {
            width: 100%;
        }
    }

</style>
