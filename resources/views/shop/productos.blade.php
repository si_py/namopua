@extends('layouts.front')

@php
$producto_en_carrito = null;
if (session('cart')) {
    foreach (session('cart') as $item) {
        // dd($item);
        if ($item['id_productos'] == $productos->id_productos) {
            $producto_en_carrito = $item;
            // dd($producto_en_carrito);
            break;
        }
    }
}
function get_value($producto_en_carrito, $color, $talla)
{
    // dd($producto_en_carrito);
    $value = null;
    foreach ($producto_en_carrito['detalles'] as $detalle) {
        if ($talla == $detalle['talla'] && $color == $detalle['color']) {
            $value = $detalle['cantidad'];
            break;
        }
    }
    return $value;
}
$colores = array_unique(array_column($colorImagen->toArray(), 'nombre_color'));
$tallesDisponibles = [];
$tabindex = 1;

@endphp
@section('content')

    <style>
        .btn-wp {
            border-radius: 50px;
            color: #fff !important;
            background: #46C153;
            border: none;
        }

        .btn-wp:hover {
            background: #489350;
            border: none;
        }

        .btn-primary:not([disabled]):not(.disabled):active,
        .btn-primary:not([disabled]):not(.disabled).active,
        .show>.btn-primary.dropdown-toggle {
            background-color: #000;
            border: none;
        }

        .btn-primary.focus,
        .btn-primary:focus {
            background-color: #000;
            border: none;
        }

        .mensaje_color {
            display: none;
        }

        .nav-tabs .nav-link.active,
        .nav-tabs .nav-item.show .nav-link {
            border: 2px solid #1676BA;
            border-radius: 10px;
        }

        .content-producto {
            padding: 75px 0 50px 0;
            overflow: hidden;
        }

        .content-producto p {
            font-family: 'Montserrat', sans-serif;
            font-weight: 100;
            font-style: italic;
            color: #909090;
        }

        .content-producto-imagen-left {
            width: 60%;
            float: left;
        }

        .content-producto-imagen-right {
            width: 40%;
            float: left;
            padding-left: 20px;
        }

        .content-producto-imagen-principal {
            width: 71.4%;
            padding: 20px 0 0 0;
            float: left;
        }

        .content-producto-imagen-secundario {
            width: 28.6%;
            padding: 10px;
            float: left;
        }

        .content-producto-imagen-secundario-int {
            float: left;
            width: 100%;
            padding: 10px;
        }

        .content-producto-texto {
            padding: 10px 10px 0 10px;
        }

        .content-producto-texto h1 {
            font-size: 40px;
        }

        .content-producto-texto h3,
        .content-producto-imagen h3 {
            font-family: 'Montserrat', sans-serif;
            font-weight: 100;
        }

        .content-producto-texto p {
            font-family: 'Montserrat', sans-serif;
            font-weight: 400;
            color: #808080;
        }

        .color-contain {
            float: left;
            width: 100%;
            padding: 0 10px 10px 10px;
        }

        .color-interno {
            padding: 0;
            width: 80px;
            height: 80px;
            text-align: center;
            border-radius: 10px;
            /* margin-right: 10px; */
            float: left;
        }

        .btn-carrito {
            color: #fff;
            padding: 15px 0;
            border: 1px solid #e9e9e9;
            border-radius: 10px;
            font-family: 'Montserrat', sans-serif;
            margin-bottom: 15px;
        }

        .btn-carrito:not(.btn-danger) {
            background: #1676BA;
        }

        .btn-carrito:not(.btn-danger):hover {
            background: #25648e;
            border: 1px solid #505050;
        }

        .btn-comprar {
            background: #f9f9f9;
            color: #353535;
            padding: 15px 0;
            border: 1px solid #e9e9e9;
            border-radius: 10px;
            font-family: 'Montserrat', sans-serif;
        }

        .btn-comprar:hover {
            background: #e5e5e5;
            border: 1px solid #f5f5f5;
            color: #000;
        }

        @media screen and (max-width: 991px) {
            .content-producto-imagen-left {
                width: 100%;
                margin-bottom: 30px;
            }

            .content-producto-imagen-right {
                width: 100%;
                margin-bottom: 50px;
            }
        }

        .MultiCarousel {
            float: left;
            overflow: hidden;
            padding: 15px;
            width: 100%;
            position: relative;
        }

        .MultiCarousel .MultiCarousel-inner {
            transition: 1s ease all;
            float: left;
        }

        .MultiCarousel .MultiCarousel-inner .item {
            float: left;
        }

        .MultiCarousel .MultiCarousel-inner .item>div {
            text-align: center;
        }

        .MultiCarousel .leftLst,
        .MultiCarousel .rightLst {
            position: absolute;
            top: calc(50% - 70px);
        }

        .MultiCarousel .leftLst {
            left: 10px;
            bottom: 15px;
            color: #c3c3c3;
            cursor: pointer;
        }

        .MultiCarousel .rightLst {
            right: 10px;
            bottom: 15px;
            color: #c3c3c3;
            cursor: pointer;
        }

        .MultiCarousel .leftLst.over,
        .MultiCarousel .rightLst.over {
            pointer-events: none;
        }

        .pad15 p {
            margin-bottom: 0;
        }

        .title-shop {
            text-align: center;
            padding: 15px 0;
            height: 55px;
            overflow: hidden;
            border-bottom: 1px solid #e5e5e5;
            font-family: 'Montserrat', sans-serif;
            font-weight: 100;
            font-size: 15px;
        }

        .content-producto a {
            color: #808080;
            font-family: 'Montserrat', sans-serif;
            font-weight: 400;
        }

        .content-producto a:hover {
            color: #000;
        }

        #aviso {
            color: #175b89;
        }

        .principal-imagen {
            /* width: 82%; */
            width: 100%;
            float: left;
            padding: 10px;
        }

        .principal-imagen img {
            width: 100%;
        }

        .secundario-imagen {
            width: 18%;
            float: left;
        }

        .secundario-imagen img {
            width: 100%;
            padding: 0px;
            margin-top: 10px;
        }

        .box-otras-marcas-hausmann {
            padding: 50px;
            float: left;
            text-align: center;
        }

        .box-otras-marcas {
            float: left;
            text-align: center;
            padding: 0 50px;
        }

        @media screen and (max-width: 767px) {
            .box-otras-marcas {
                width: 33.3%;
                padding: 0 15px;
            }

            .cantidad-pedido-input {
                min-width: 20px;
                max-width: 70px;
            }

            .box-otras-marcas-hausmann {
                padding-top: 30px !important;
            }

        }

        @media screen and (max-width: 991px) {
            .box-otras-marcas-hausmann {
                padding: 15px;
                width: 33%;
            }
        }

        .swiper-button-next:after, .swiper-button-prev:after {
            font-family: swiper-icons;
            font-size: 20px;
            text-transform: none!important;
            letter-spacing: 0;
            font-variant: initial;
            line-height: 1;
        }

        .swiper-button-next, .swiper-button-prev {
            top: 35%;
        }

    </style>
    {{-- <div class="col-sm-12 title-shop">
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div style="width: 100%; text-align: center; float: left; font-weight: 400;">
                        <img src="../img/icon/icon/credit-card.svg" style="margin-top: -4px;" />
                        Descuentos
                        <!-- | <a href="#">Click aquí! Como obtener tu carnet</a> -->
                    </div>
                </div>
                <div class="carousel-item">
                    <div style="width: 100%; text-align: center; float: left; font-weight: 400;">
                        <img src="../img/icon/icon/danger.svg" style="margin-top: -4px;" />
                        Modo COVID-19 | <a href="#">Conocė nuestros productos</a>
                    </div>
                </div>
                <div class="carousel-item">
                    <div style="width: 100%; text-align: center; float: left; font-weight: 400;">
                        <img src="../img/icon/icon/transport.svg" style="margin-top: -4px;" />
                        Delivery | Asunción y Gran Asunción
                    </div>
                </div>
                <div class="carousel-item">
                    <div style="width: 100%; text-align: center; float: left; font-weight: 400;">
                        <img src="../img/icon/icon/box.svg" style="margin-top: -4px; width: 23px;" />
                        Encomienda | para todo territorio nacional
                    </div>
                </div>
            </div>
        </div>
    </div> --}}


    <div class="content-producto">
        <div class="contenedor_general">
            <div class="col-12 mb-4">
                <div class="col-12">
                    <p style="font-style: normal; margin-bottom: 0; color: #000; font-weight: 400;">

                        @foreach ($cat as $cate)
                            @if ($cate->id_categorias == $productos->pk_categorias_productos)
                                @if ($cate->nombre_categorias == 'Olimpia')
                                    <a href="{{ route('shop.tiendaOlimpia') }}">Olimpia /</a>
                                @else
                                    <a href="/">Home /</a><a href="/shop">Tienda /</a><a
                                        href="{{ route('shop.categorias', $cate->id_categorias) }}">{{ $cate->nombre_categorias }}
                                        /</a>
                                @endif
                            @endif
                        @endforeach
        
                        @foreach ($categorias as $cat)
                            @if ($cat->id_subcategorias == $productos->pk_subcategorias_productos)
                                <a href="{{ route('shop.subcategorias', $cat->id_subcategorias) }}">{{ $cat->nombre_subcategorias }}
                                    /</a>
                            @endif
                        @endforeach
        
                        Cod. {{ $productos->codigo_productos }}
                    </p>
                </div>
            </div>

            <div class="col-12">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="col-12 principal-imagen" id="principal-imagen">
                            <a href="javascript:void(0);" class="full">
                                <img src="../img/productos/{{ $imagen_principal->path_imagen }}" style="border: 1px solid #e5e5e5; border-radius: 15px; width: 100%;">
                            </a>
                        </div>
                        <div class="col-12 miniatura p-0" style="overflow: hidden;">
                            @foreach ($imagenesProductos as $imagene)
                                <div class="col-3" style="float: left;">
                                    <div class="preview">
                                        <a href="javascript:void(0);" class="selected"
                                            data-full="../img/productos/{{ $imagene->path_imagen }}">
                                            <img src="../img/productos/{{ $imagene->path_imagen }}" style="width: 100%; border: 1px solid #e5e5e5; border-radius: 10px;">
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="content-producto-texto">
                            <p style="margin-bottom: 10px;">
        
                                <input type="hidden" id="tipo_producto" value="{{ $productos->tipo_producto }}">
        
                                @if ($productos->descuentos_productos == 1)
                                    <span
                                        style="padding: 5px 10px; background: #F25E52; color: #fff; border-radius: 5px; font-style: initial; font-size: 13px;">OFERTA!</span>
                                @endif
                                @if ($productos->lanzamientos_productos == 1)
                                    <span
                                        style="padding: 5px 10px; background: #94B53D; color: #fff; border-radius: 5px; font-style: initial; font-size: 13px;">LANZAMIENTO!</span>
                                @endif
                                @if ($productos->premium == 1)
                                    <span style="padding: 5px 10px; background: #1677BB; color: #fff; border-radius: 5px; font-style: initial; font-size: 13px;">LINEA PREMIUM!</span>
                                @endif
                            </p>
                            <h1 style="margin-bottom: 0;">{{ $productos->nombre_productos }}</h1>
                            <p style="margin-bottom: 0; font-style: normal;">Cod. {{ $productos->codigo_productos }}</p>
                            <p style="margin-bottom: 0; font-style: normal;">{{ $productos->descripcion_productos }}</p>
                            <span style="margin-bottom: 0; margin-top: -5px; font-size: 40px; font-style: normal; color: #000; font-family: 'Montserrat', sans-serif;">
                                @if (is_null($productos->precio_descuento_productos))
                                     {{-- <span id="precio_producto">🇵🇾 Gs.
                                        {{ number_format($productos->mayorista, 0, ',', '.') }}
                                    </span><br>
                                    <span>
                                        @php 
                                            $preRes = $productos->mayorista / $divisa->divisa;
                                        @endphp
                                        🇺🇸 $. {{number_format($preRes, 2, ',', '.') }}
                                    </span> --}}
                                    <table>
                                        <tr>
                                            <td style="width: 60px;">🇵🇾 </td>
                                            <td style="text-align: center;">G. </td>
                                            <td>{{ number_format($productos->mayorista, 0, ',', '.') }}</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 60px;">🇺🇸 </td>
                                            <td style="text-align: center;">$. </td>
                                            <td>
                                                @php 
                                                    $preRes = $productos->mayorista / $divisa->divisa;
                                                @endphp
                                                {{number_format($preRes, 2, ',', '.') }}
                                            </td>
                                        </tr>
                                    </table>
                                @else
                                    <span
                                        style="font-size: 14px; margin-right: 3px; text-decoration: line-through; color: #808080;">Gs.
                                        {{ $productos->mayorista }}
                                    </span>
                                    <span>Gs. <span id="precio_producto">{{ number_format($productos->precio_descuento_productos, 0, ',', '.') }}</span></span>
                                    <span>
                                        @php 
                                            $preRes = $productos->precio_descuento_productos / $divisa->divisa;
                                        @endphp
                                        $. {{number_format($preRes, 2, ',', '.') }}
                                    </span>
                                @endif
                            </span>

                            
                        </div>
                        <div class="col-sm-12 mt-4" style="padding: 0px 10px 10px 0; overflow: hidden;">
                            <p style="margin-bottom: 0; margin-left: 10px; font-style: normal; #808080; font-weight: 400;">
                                Colores Disponibles
                            </p>
                            <div class="color-contain">
                                <ul class="nav nav-tabs" id="myTab" role="tablist" style="border: none;">
                                    @php
                                        $colores = array_column($colorImagen->toArray(), 'pk_imagen_color_imagen');
                                    @endphp
                                    {{-- @foreach ($imagenesProductos as $imagen) --}}
        
                                    @foreach ($colorImagen as $color)
                                        {{-- @if ($imagen->principal_color_imagen == 1 && in_array($imagen->pk_imagen_color_imagen, $colores)) --}}
                                        <li class="nav-item"
                                            onclick="buscar_color_producto( '{{ $color->id_color_imagen }}',
                                                                            '{{ $color->pk_color_color_imagen }}',
                                                                            '{{ $color->pk_imagen_color_imagen }}',
                                                                            '{{ $color->pk_productos_color_imagen }}')"
                                            style="overflow: hidden; text-align: center; max-width: 100px">
                                            <a class="nav-link" id="tab-materia-{{ $color->id_color_imagen }}"
                                                data-toggle="tab" href="#materia" role="tab" aria-controls=""
                                                style="overflow: hidden; padding: 10px;">
                                                <div class="color-interno"
                                                    style=" background: {{ $color->prefijo_color }}; 
                                                            background-image: url('../img/productos/{{ $color->path_imagen }}'); 
                                                            background-position: center; 
                                                            background-size: 100%;
                                                            border: 1px solid #e5e5e5;">
                                                </div>
                                                <p style="margin-bottom: 0; margin-left: 10px; font-style: normal; #808080; font-weight: 400; font-size: 13px;">
                                                    {{ $color->nombre_color }}</p>
                                            </a>
                                        </li>
                                        {{-- @endif --}}
                                    @endforeach
                                    {{-- @endforeach --}}
                                </ul>
                                <input type="text" id="id_productos" value="{{ $productos->id_productos }}" hidden>
                                <input id="id_color_imagen" type="hidden">
        
                                <div class="col-sm-12 mensaje_color" style="overflow: hidden;">
                                    <p style="font-style: normal; padding-top: 20px; padding-bottom: none;">
                                        <img src="../img/icon/info.svg" style="width: 20px; margin-top: -4px;">
                                        <span style="color: red;">Debe elegir un color</span>
                                    </p>
                                </div>
                            </div>
                        </div>
        
                        <div style="width: 100%; float: left; padding: 15px 10px 0 10px;">
        
                            @foreach (App\categorias::where('active_categorias', true)->get() as $ca)
                                @if ($ca->id_categorias == $productos->pk_categorias_productos)
                                    @if ($ca->nombre_categorias == 'Olimpia')
                                        <a class="btn btn-primary btn-wp" style="font-size: 22px; padding: 0px 30px 5px;"
                                            href="https://api.whatsapp.com/send?phone=595981143380&text=Hola%20me%20interesa%20este%20producto%20Código:%20+{{ $productos->codigo_productos }}+"
                                            target="_blank">
                                            <div class="col-12">
                                                <span style="font-size: 14px;">Contacte con nosotros al</span>
                                            </div>
                                            <div class="col-12" style="margin-top: -10px;">
                                                <i class="fa fa-whatsapp"></i> 0986 650 595
                                            </div>
                                        </a>
                                    @else
                                        <a class="btn btn-primary btn-wp" style="font-size: 22px; padding: 0px 30px 5px;"
                                            href="https://api.whatsapp.com/send?phone=595981143380&text=Hola%20me%20interesa%20este%20producto%20Código:%20+{{ $productos->codigo_productos }}+"
                                            target="_blank">
                                            <div class="col-12">
                                                <span style="font-size: 14px;">Contacte con nosotros al</span>
                                            </div>
                                            <div class="col-12" style="margin-top: -10px;">
                                                <i class="fa fa-whatsapp"></i> 0981 143 380
                                            </div>
                                        </a>
                                    @endif
                                @endif
                            @endforeach
        
        
                        </div>

                        <div class="col-12">
                            @if (strtolower($productos->tipo_producto) == 'olimpia')
                                @if (count($talles) > 0)

                                    <div class="col-sm-12 p-0 desktop" style="clear: both;">
                                        @include('shop.tabla_pedido')
                                    </div>
                                    <div class="col-sm-12 mobile p-0" style="clear: both;">
                                        @include('shop.tabla_pedido_mobile')
                                    </div>
                                @else
                                    <div class="clearfix"></div>
                                    <div class="col-sm-12 p-0 mb-4">
                                        <div class="col-sm-12 p-0 mt-4" style="overflow: hidden; text-align: left;">
                                            <p style="border-bottom: 1px solid #b1b1b1;"><b>Pedido</b></p>
                                        </div>
                                        <div class="row">
                                            @foreach ($colorImagen as $color)
                                                <div class="col-2">
                                                    <div class="form-group">
                                                        <label for="cantidad_color_{{ $color->id_color }}" class="form-label">
                                                            <p class="m-0">
                                                                <b>{{ $color->nombre_color }}</b>
                                                            </p>
                                                        </label>
                                                        <input type="number" min="1" id="cantidad_color_{{ $color->id_color }}"
                                                            class="form-control cantidad-pedido-input"
                                                            data-color="{{ $color->id_color }}"
                                                            data-nombre-color="{{ $color->nombre_color }}">
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>


                                        <div class="row" style="padding: 0;">
                                            <div class="col p-0">
                                                <button onclick="agregar_carrito()" class="btn btn-primary btn-carrito"
                                                    style="width:100%; border-radius: 30px;">
                                                    Agregar al carrito
                                                </button>
                                            </div>
                                            <div class="col p-0">
                                                <a href="/shop" class="btn btn-primary btn-comprar"
                                                    style="width:100%; float: left; margin-right: 3px; border-radius: 30px;">
                                                    Comprar otros modelos</a>
                                            </div>
                                            <div class="col p-0">
                                                <a href="/carrito" class="btn btn-primary btn-comprar"
                                                    style="width:100%; float: left; margin-left: 3px; border-radius: 30px;">
                                                    <img src="../img/icon/bag.svg" style="width: 20px; margin-top: -10px;"> Ir al carrito
                                                </a>

                                            </div>
                                        </div>

                                    </div>
                                @endif
                            @else
                                <div class="col-sm-12 desktop" style="clear: both; padding: 40px 0 0 0;">
                                    @include('shop.tabla_pedido')
                                </div>
                                <div class="col-sm-12 mobile p-0" style="clear: both;">
                                    @include('shop.tabla_pedido_mobile')
                                </div>
                            @endif
                        </div>

                        <!-- Aviso -->
                        <div class="col-sm-12"
                            style="margin-top: 0px; text-align: left; padding: 0px 10px 0 10px; overflow: hidden;">
                            <div clas="col-sm-12" style="padding: 20px 0 20px 0">
                                <p style="font-style:normal; margin-bottom: 0;">Precio a partir de treinta unidades, pueden ser modelos
                                    surtidos
                                    <a href="#" id="aviso" style="font-weight: 600;">Leer nuestra política de venta</a>
                                </p>
                            </div>
                        </div>

                        <div style="width: 150px; float: left; padding: 10px 10px 0 10px; text-align: left;">
                            <div id="fb-root"></div>
                            <script async defer crossorigin="anonymous"
                                            src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v8.0&appId=211278076364804&autoLogAppEvents=1"
                                            nonce="TuefmuAe"></script>
                            <div class="fb-share-button" data-href="https://www.namopuapy.com" data-layout="button_count"
                                data-size="large"><a target="_blank"
                                    href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.namopuapy.com%2F&amp;src=sdkpreparse"
                                    class="fb-xfbml-parse-ignore">Compartir</a></div>
                        </div>

                        <div style="width: 170px; float: left; padding: 10px 10px 0 10px;">
                            <a style="background: #46C153; color: #fff; font-size: 13px; padding: 4px 25px; border-radius: 5px;"
                                href="whatsapp://send?text=http://www.namopuapy.com"
                                data-text="BOTÓN COMPARTIR EN WHATSAPP EN MI PÁGINA WEB" data-action="share/whatsapp/share">
                                <i class="fa fa-whatsapp" style="font-size: 18px;"></i> Compartir
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 mb-5">
        <div class="contenedor_general">
            <div class="col-12" style="border-bottom: 1px solid #e5e5e5;">

            </div>
        </div>
    </div>


    {{-- <div class="contenedor_general">
        <div class="col-lg-12 col-12 mb-3 p-0">
            <div class="col-12 mb-1 p-1">
                <div class="col-12">
                    <p><b>PRODUCTOS RELACIONADOS<i class="fa fa-chevron-right" style="margin-left: 10px;"></i></b></p>
                </div>
                <div class="col-12 p-0">
                    <div class="swiper">
                        <div class="swiper-wrapper">
                            @foreach ($masVendidos as $p)
                                <div class="swiper-slide p-2">
                                    <a id="box-productos-a" href="{{ route('shop.producto', $p->id_productos) }}">
                                        <img src="/img/productos/{{ $p->path_imagen }}" alt="{{ $p->nombre_productos }} {{ $p->codigo_productos }}" style="width: 100%; border: 1px solid #e5e5e5; border-radius: 15px;" />
                                    </a>
                                    
                                    <div class="col-12 p-3">
                                        <p class="m-0" style="font-size: 12px;">Código: {{ $p->codigo_productos }}</p>
                                        <p class="m-0">{{ strtoupper($p->nombre_productos) }}</p>
                
                                        <div class="col-sm-12 p-0 mt-1 mb-1" style="overflow: hidden;">
                                            @foreach($coloresRelacionados as $colo)
                                                @if($p->id_productos == $colo->pk_productos_color_imagen)
                                                    @if ($colo->principal_color_imagen == 'true')
                                                        <span style="border: 1px solid #b1b1b1; background: {{ $colo->prefijo_color }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </div>
                
                                        @if (is_null($p->precio_descuento_productos))
                
                                            <div class="row">
                                                <div class="col-lg-6 guaranies">
                                                    <span>G.</span>
                                                    {{ number_format($p->mayorista, 0, ',', '.') }}
                                                </div>
                                                <div class="col-lg-6 dolares">
                                                    <span>$.</span>
                                                    <?php 
                                                    $dolar = $p->mayorista / 6900;
                                                    ?>
                                                    {{ number_format($dolar, 2, ',', '.') }}
                                                </div>
                                            </div>
                                        
                                        @else
                                            <span style="color: #F25E52;"><span>G.</span>
                                                {{ number_format($p->precio_descuento_productos, 0, ',', '.') }}</span>
                
                                            <div class="row">
                                                <div class="col-6 guaranies" style="color: #F25E52;">
                                                    <span style="color: #F25E52;">G.</span>
                                                    {{ number_format($p->precio_descuento_productos, 0, ',', '.') }}
                                                </div>
                                                <div class="col-6 dolares" style="color: #F25E52;">
                                                    <span style="color: #F25E52;">$.</span>
                                                    <?php 
                                                    $dolar = $p->precio_descuento_productos / 6900;
                                                    ?>
                                                    {{ number_format($dolar, 2, ',', '.') }}
                                                </div>
                                            </div>
                                        @endif
                
                                    </div>
                                    @if ($p->descuentos_productos == 1)
                                        <div class="oferta"
                                            style=" background: #F25E52; border-radius: 30px; padding: 5px 10px;">
                                            OFERTA!
                                        </div>
                                    @endif
                                    @if ($p->premium == 1)
                                        <div class="oferta"
                                            style=" background: #1677BB; border-radius: 30px; padding: 5px 10px;">
                                            PREMIUM
                                        </div>
                                    @endif
                                    @if ($p->lanzamientos_productos == 1)
                                        <div class="oferta"
                                            style=" background: #94B53D; border-radius: 30px; padding: 5px 10px;">
                                            LANZAMIENTO
                                        </div>
                                    @endif
                                            
                                </div>
                            @endforeach
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="contenedor_general">
        <div class="col-lg-12 col-12 mb-3 p-0">
            <div class="col-12 mb-1 p-1">
                <div class="col-12">
                    <p><b>PRODUCTOS RELACIONADOS<i class="fa fa-chevron-right" style="margin-left: 10px;"></i></b></p>
                </div>
                <div class="col-12" style="padding: 0 10px;">
                    <div class="swiper" style="position:relative;">
                        <div class="swiper-wrapper">
                          @foreach ($damas as $da)
                          <div class="swiper-slide p-0">
                            <a class="box-productos-a" href="{{ route('shop.producto', $da['id_productos']) }}" style="color: #101010;">
                              @if ($da['tipo_producto'] == 'Unisex')
                                  <img class=""
                                      src="/img/productos/{{ count($da['imagen_principal']) > 4 ? ($da['imagen_principal'][3]['path_imagen'] ?? '') : ($da['imagen_principal'][0]['path_imagen'] ?? '') }}"
                                      alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                      style="width: 100%; border-radius: 20px;" />
                              @else
                                  <img class=""
                                      src="/img/productos/{{ $da['imagen_principal'][0]['path_imagen'] ?? '' }}"
                                      alt="{{ $da['nombre_productos'] }} {{ $da['codigo_productos'] }}"
                                      style="width: 100%; border-radius: 20px;" />
                              @endif
                              
                              <div class="col-12 p-3">
                                <p class="m-0" style="font-size: 12px;">Cód: {{ $da['codigo_productos'] }}</p>
                                <p class="m-0">{{ mb_strtoupper($da['nombre_productos']) }}</p>
            
                                <div class="col-sm-12 p-0 mt-1 mb-1" style="overflow: hidden;">
            
                                  {{-- @foreach ($color as $col)
                                    @if($col->pk_productos_color_productos == $da['id_productos'])
                                      <span style="background: {{ $col->prefijo_color }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                                    @endif
                                  @endforeach --}}
                                  @foreach ($da['colores'] as $color)
                                    <span style="border: 1px solid #b1b1b1; background: {{ $color['prefijo_color'] }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-left: 5px;"></span>
                                  @endforeach
              
                                </div>
            
                                @if (is_null($da['precio_descuento_productos']))
            
                                  <div class="row">
                                    <div class="col-lg-6 guaranies">
                                      <span>G.</span>
                                      {{ number_format($da['mayorista'], 0, ',', '.') }}
                                    </div>
                                    <div class="col-lg-6 dolares">
                                      <span>$.</span>
                                      <?php 
                                        $dolar = $da['mayorista'] / $divisa->divisa;
                                      ?>
                                      {{ number_format($dolar, 2, ',', '.') }}
                                    </div>
                                  </div>
                                    
                                  @else
                                      <span style="color: #F25E52;"><span>G.</span>
                                          {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}</span>
            
                                      <div class="row">
                                        <div class="col-lg-6 guaranies" style="color: #F25E52;">
                                          <span style="color: #F25E52;">G.</span>
                                          {{ number_format($da['precio_descuento_productos'], 0, ',', '.') }}
                                        </div>
                                        <div class="col-lg-6 dolares" style="color: #F25E52;">
                                          <span style="color: #F25E52;">$.</span>
                                          <?php 
                                            $dolar = $da['precio_descuento_productos'] / $divisa->divisa;
                                          ?>
                                          {{ number_format($dolar, 2, ',', '.') }}
                                        </div>
                                      </div>
                                  @endif
            
                              </div>
                            </a>
                            @if ($da['descuentos_productos'] == 1)
                              <div class="oferta" style=" background: #F25E52; border-radius: 30px;">
                                  OFERTA!
                              </div>
                            @endif
                            @if ($da['premium'] == 1)
                              <div class="oferta" style=" background: #1677BB; border-radius: 30px;">
                                  PREMIUM
                              </div>
                            @endif
                            @if ($da['lanzamientos_productos'] == 1)
                              <div class="oferta" style="background: #94B53D; border-radius: 30px;">
                                  LANZAMIENTO
                              </div>
                            @endif
                                      
                          </div>
                          @endforeach
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>


    <div class="aviso_cantidad_vacio">
        <div class="row">
            <div class="col-3">
                <img src="/img/icon/info.png" alt="" style="width: 100%;">
            </div>
            <div class="col-9 pt-1">
                <p class=" m-0">Debes agregar la cantidad para agregar al carrito</p>
            </div>
        </div>
    </div>

    <style>
        .aviso_cantidad_vacio {
            width: 350px;
            background: #fff;
            position: fixed;
            top: 80px; 
            right: 10px;
            border-radius: 10px;
            box-shadow:  0 0 5px 5px #e5e5e5;
            padding: 20px;
            display: none;
        }
    </style>


    <script>
        function buscar_color_producto(id, color, imagen, productos) {
            var id_stock = "";
            var id_stock2 = "";

            $.get('/busqueda_imagenes/' + color + '/' + productos, function(data) {
                //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
                let imagenes = data.images;
                if ($("#id_color_imagen").val() != color) {
                    let tallas = data.tallas;
                    let tallasOptions = ""
                    tallas.forEach(talla => {
                        tallasOptions +=
                            `<option value="${talla.id_talles}">${talla.nombre_talles}</option>`
                    });
                    $("#talla_producto").html(tallasOptions)
                }
                id_stock = '<a href="javascript:void(0);" id="imagen" class="full" title="">' +
                    '<img src="../img/productos/' + imagenes[0].path_imagen + '" style="border: 1px solid #e5e5e5; border-radius: 15px;">' +
                    '</a>'
                for (var i = 0; i < imagenes.length; i++) {
                    id_stock2 += '<div class="col-3" style="float: left;">' +
                        '<div class="preview">' +
                        '<a href="javascript:void(0);" class="selected" data-full="../img/productos/' + imagenes[i]
                        .path_imagen + '" >' +
                        '<img src="../img/productos/' + imagenes[i].path_imagen + '" style="width: 100%; border: 1px solid #e5e5e5; border-radius: 10px;">' +
                        '</a>' +
                        '</div>' +
                        '</div>'
                    $('#id_productos').val(imagenes[i].id_productos)
                    $('#id_color_imagen').val(imagenes[i].id_color_imagen);
                }

                $("#principal-imagen").empty();
                $('#principal-imagen').append(id_stock);

                $(".miniatura").empty();
                $('.miniatura').append(id_stock2);


                //funcion para hacer funcionar el fancybox, elegir imagenes para mostrar
                $(".preview a").on("click", function() {
                    $(".selected").removeClass("selected");
                    $(this).addClass("selected");
                    var picture = $(this).data();

                    event.preventDefault(); //prevents page from reloading every time you click a thumbnail

                    $(".full img").fadeOut(100, function() {
                        $(".full img").attr("src", picture.full);
                        $(".full").attr("href", picture.full);
                        $(".full").attr("title", picture.title);

                    }).fadeIn();

                }); // end on click
                $('.mensaje_color').css({
                    "display": "none"
                });
            })

        };


        function agregar_carrito() {
            var id_productos = $('#id_productos').val();
            var precio = $('#precio_producto').html();
            var cantidad = 0;
            var tipo = $('#tipo_producto').val();
            // var id_color_imagen = $('#id_color_imagen').val();
            // var talla = $('#talla_producto').val();
            let detalles = [];
            $(".cantidad-pedido-input:not(:disabled)").each(function(index, input) {
                if (input.value > 0) {
                    detalles.push({
                        talla: tipo != 'Olimpia' ? input.dataset.talla : (input.dataset.talla != null ?
                            input.dataset.talla : null),
                        color: input.dataset.color,
                        nombre_color: input.dataset.nombreColor,
                        cantidad: input.value
                    });
                    cantidad += parseInt(input.value);
                }
            });

            if(detalles.length == 0) {
                $('.aviso_cantidad_vacio').fadeIn();

                $('#mensaje_alerta_pedido').text('Debes agregar la cantidad para agregar al carrito');


                setTimeout(function(){
                    $('.aviso_cantidad_vacio').fadeOut();
                    $('#mensaje_alerta_pedido').text('');
                }, 5000);

                return;
            } 

            $.ajax({
                url: "{{ route('carrito.agregarCarrito') }}",
                type: 'GET',
                data: {
                    id_productos: id_productos,
                    precio: precio,
                    cantidad: cantidad,
                    tipo: tipo,
                    detalles: detalles
                },
                success: function(data) {
                    // console.log(data)
                    location.reload();
                }
            });
        }

        $(document).ready(function() {
            var itemsMainDiv = ('.MultiCarousel');
            var itemsDiv = ('.MultiCarousel-inner');
            var itemWidth = "";

            $('.leftLst, .rightLst').click(function() {
                var condition = $(this).hasClass("leftLst");
                if (condition)
                    click(0, this);
                else
                    click(1, this)
            });

            ResCarouselSize();




            $(window).resize(function() {
                ResCarouselSize();
            });

            //this function define the size of the items
            function ResCarouselSize() {
                var incno = 0;
                var dataItems = ("data-items");
                var itemClass = ('.item');
                var id = 0;
                var btnParentSb = '';
                var itemsSplit = '';
                var sampwidth = $(itemsMainDiv).width();
                var bodyWidth = $('body').width();
                $(itemsDiv).each(function() {
                    id = id + 1;
                    var itemNumbers = $(this).find(itemClass).length;
                    btnParentSb = $(this).parent().attr(dataItems);
                    itemsSplit = btnParentSb.split(',');
                    $(this).parent().attr("id", "MultiCarousel" + id);


                    if (bodyWidth >= 1920) {
                        incno = itemsSplit[4];
                        itemWidth = sampwidth / incno;
                    } else if (bodyWidth >= 992) {
                        incno = itemsSplit[3];
                        itemWidth = sampwidth / incno;
                    } else if (bodyWidth >= 768) {
                        incno = itemsSplit[1];
                        itemWidth = sampwidth / incno;
                    } else {
                        incno = itemsSplit[1];
                        itemWidth = sampwidth / incno;
                    }
                    $(this).css({
                        'transform': 'translateX(0px)',
                        'width': itemWidth * itemNumbers
                    });
                    $(this).find(itemClass).each(function() {
                        $(this).outerWidth(itemWidth);
                    });

                    $(".leftLst").addClass("over");
                    $(".rightLst").removeClass("over");

                });
            }


            //this function used to move the items
            function ResCarousel(e, el, s) {
                var leftBtn = ('.leftLst');
                var rightBtn = ('.rightLst');
                var translateXval = '';
                var divStyle = $(el + ' ' + itemsDiv).css('transform');
                var values = divStyle.match(/-?[\d\.]+/g);
                var xds = Math.abs(values[4]);
                if (e == 0) {
                    translateXval = parseInt(xds) - parseInt(itemWidth * s);
                    $(el + ' ' + rightBtn).removeClass("over");

                    if (translateXval <= itemWidth / 2) {
                        translateXval = 0;
                        $(el + ' ' + leftBtn).addClass("over");
                    }
                } else if (e == 1) {
                    var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
                    translateXval = parseInt(xds) + parseInt(itemWidth * s);
                    $(el + ' ' + leftBtn).removeClass("over");

                    if (translateXval >= itemsCondition - itemWidth / 2) {
                        translateXval = itemsCondition;
                        $(el + ' ' + rightBtn).addClass("over");
                    }
                }
                $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
            }

            //It is used to get some elements from btn
            function click(ell, ee) {
                var Parent = "#" + $(ee).parent().attr("id");
                var slide = $(Parent).attr("data-slide");
                ResCarousel(ell, Parent, slide);
            }

        });
    </script>

@endsection
