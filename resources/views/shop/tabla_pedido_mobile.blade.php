@php
$tabindex = 1;
@endphp

<div class="col-sm-12 p-0 mb-4">
    <div class="col-sm-12 p-0" style="overflow: hidden; text-align: left;">
        <p style="border-bottom: 1px solid #b1b1b1;"><b>Pedido</b></p>
    </div>
    <div class="col-sm-12">
        <table>
            <thead>
                <th class="text-center">
                </th>
                @foreach ($colorImagen as $color)
                    <th class="text-center">
                        <b>
                            {{ $color->nombre_color }}
                        </b>
                    </th>
                @endforeach
            </thead>
            <tbody>
                @if (count($talles) <= 0)
                    <tr>
                        <td></td>
                        <td colspan="{{ count($colorImagen) }}">
                            <span class="p-5">
                                No hay tallas disponibles
                            </span>
                        </td>
                    </tr>
                @else
                    @foreach ($talles as $talle)
                        <tr>
                            <td class="text-center">
                                <p>
                                    <b>
                                        {{ $talle->nombre_talles }}&nbsp;&nbsp;
                                    </b>
                                </p>
                            </td>
                            @foreach ($colorImagen as $color)
                                @php
                                    $value = null;
                                    if ($producto_en_carrito != null) {
                                        $value = get_value($producto_en_carrito, $color->id_color, $talle->nombre_talles);
                                    }
                                @endphp
                                <td>
                                    <input type="number" name="cantidad_producto" min="1"
                                        class="form-control form-control-sm text-center cantidad-pedido-input"
                                        tabindex="{{ $tabindex }}"
                                        {{ preg_match(" /\b$talle->id_talles\b/i", $tallesProducto[$loop->index]->tamanos ?? '') ? '' : 'disabled' }}
                                        data-color="{{ $color->id_color }}" data-talla="{{ $talle->nombre_talles }}"
                                        data-nombre-color="{{ $color->nombre_color }}" @if ($value != null) value="{{ $value }}" @endif>
                                </td>
                                @php
                                    $tabindex++;
                                @endphp
                            @endforeach
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>


<div class="row" style="padding: 0px 10px 0 10px;">
    <div class="col">
        <button onclick="agregar_carrito()" class="btn btn-primary btn-carrito"
            style="width:100%; border-radius: 30px;">Agregar
            al
            carrito</button>
    </div>
    <div class="col">
        <a href="/shop" class="btn btn-primary btn-comprar"
            style="width:100%; float: left; margin-right: 3px; border-radius: 30px;">Comprar otros modelos</a>
    </div>
</div>


@section('bottom-menu-section')
    <div class="col-sm-12 bg-light mobile" style="padding: 10px;">
        <div class="row">
            <div class="col d-flex align-items-center">
                <button onclick="agregar_carrito()" class="btn btn-primary btn-carrito"
                    style="width: 100%; border-radius: 30px; margin: 0; padding-top: 5px; padding-bottom: 5px;">Agregar al
                    carrito</button>
            </div>
            <div class="col d-flex align-items-center">
                <a href="/shop" class="btn btn-primary btn-comprar"
                    style="width: 100%; margin-right: 3px; border-radius: 30px; padding-top: 5px; padding-bottom: 5px;">Otros
                    modelos</a>
            </div>
        </div>
    </div>
@endsection
