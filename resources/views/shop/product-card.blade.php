<div class="box-productos"
    style="float: left; padding: 10px; margin-bottom: 20px; font-family: 'Montserrat', sans-serif;">
    <a href="{{ route('shop.producto', $p->id_productos) }}">

        <img src="{{ asset('img/productos/'.$p->path_imagen) }}"
            alt="{{ $p->nombre_productos }} {{ $p->codigo_productos }}" style="width: 100%; border-radius: 15px;" />

    </a>
    <div class="col-sm-12 box-productos-title" style="float: left; padding: 10px 10px;">

        <p class="m-0" style="font-size: 12px; text-align: left;">Código: {{ $p->codigo_productos }}</p>
        <p class="m-0" style="text-align: left; font-size: 17px;">{{ mb_strtoupper($p->nombre_productos) }}</p>
        <div class="col-sm-12" style="margin-bottom: 10px; padding: 0px; overflow: hidden;">
            @foreach ($colorImagen as $pc)
                @if ($pc->pk_productos_color_imagen == $p->id_productos)
                    @if ($pc->principal_color_imagen == 'true')
                        <span style="border: 1px solid #b1b1b1; background: {{ $pc->prefijo_color }}; width: 15px; height: 15px; float: left; border-radius: 50px; margin-right: 5px;""></span>
                    @endif
                @endif
            @endforeach
        </div>
        {{-- <p style="margin-bottom: 0; font-size: 20px; font-weight: 600; color: #808080;">
            @if (is_null($p->precio_descuento_productos))
                <span style="font-size: 14px;">Gs.</span>
                {{ number_format($p->mayorista, 0, ',', '.') }}
            @else
                <span style="color: #F25E52;"><span style="font-size: 14px;">Gs.</span>
                    {{ number_format($p->precio_descuento_productos, 0, ',', '.') }}</span>
            @endif
        </p> --}}


        @if (is_null($p->precio_descuento_productos))

        <div class="row">
            <div class="col-lg-6 guaranies" style="text-align: left; font-size: 17px;">
            <span>G.</span>
            {{ number_format($p->mayorista, 0, ',', '.') }}
            </div>
            <div class="col-lg-6 dolares" style="font-size: 17px;">
            <span>$.</span>
            <?php 
                $dolar = $p->mayorista / 6900;
            ?>
            {{ number_format($dolar, 2, ',', '.') }}
            </div>
        </div>
            
        @else
            <span style="color: #F25E52;"><span>G.</span>
                {{ number_format($p->precio_descuento_productos, 0, ',', '.') }}</span>

            <div class="row">
                <div class="col-6 guaranies p-0" style="color: #F25E52; text-align: left; font-size: 17px;">
                <span style="color: #F25E52;">G.</span>
                {{ number_format($p->precio_descuento_productos, 0, ',', '.') }}
                </div>
                <div class="col-6 dolares" style="color: #F25E52; font-size: 17px;">
                <span style="color: #F25E52;">$.</span>
                <?php 
                    $dolar = $p->precio_descuento_productos / 6900;
                ?>
                {{ number_format($dolar, 2, ',', '.') }}
                </div>
            </div>
        @endif




    </div>
    @if ($p->descuentos_productos == 1)
        <div class="oferta" style=" background: #F25E52; border-radius: 30px; padding: 5px 10px;">
            OFERTA!
        </div>
    @endif
    @if ($p->premium == 1)
        <div class="oferta" style=" background: #1677BB; border-radius: 30px; padding: 5px 10px;">
            PREMIUM
        </div>
    @endif
    @if ($p->lanzamientos_productos == 1)
        <div class="oferta" style=" background: #94B53D; border-radius: 30px; padding: 5px 10px;">
            LANZAMIENTO
        </div>
    @endif
    {{-- <p>
    
    </p>
    <p style="color: #808080; font-size: 15px; font-family: 'Montserrat', sans-serif;">Código.
        {{ $p->codigo_productos }}</p> --}}

</div>
