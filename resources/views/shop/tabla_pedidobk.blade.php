{{-- <div class="col-sm-12" style="overflow: hidden; padding: 0px 5px 15px 5px;">
    <div class="col-sm-6" style="float: left; padding: 0 5px 0 5px;">
        <p style="margin-bottom: 0; font-weight: 400; color: #808080; font-style: normal;">Tallas</p>
        <select id="talla_producto" class="form-control"
            style="border-radius: 30px; padding-left: 10px; padding-right: 10px;">
            @if (isset($tallesProducto->tamanos))
                @foreach ($talles as $ta)
                    @if (preg_match(" /\b$ta->id_talles\b/i", $tallesProducto->tamanos))
                        <option value="{{ $ta->id_talles }}">{{ $ta->nombre_talles }}</option>
                    @endif
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6" style="float: left; padding: 0 5px 0 5px;">
        <p style="margin-bottom: 0; font-weight: 400; color: #808080; font-style: normal;">Cantidad</p>
        <input type="number" id="cantidad_productos" min="1" max="500" class="form-control" value="1"
            style="border-radius: 30px; padding-left: 20px;" />
    </div>
</div> --}}
@php
$producto_en_carrito = null;
if (session('cart')) {
    foreach (session('cart') as $item) {
        // dd($item);
        if ($item['id_productos'] == $productos->id_productos) {
            $producto_en_carrito = $item;
            // dd($producto_en_carrito);
            break;
        }
    }
}
function get_value($producto_en_carrito, $color, $talla)
{
    // dd($producto_en_carrito);
    $value = null;
    foreach ($producto_en_carrito['detalles'] as $detalle) {
        if ($talla == $detalle['talla'] && $color == $detalle['color']) {
            $value = $detalle['cantidad'];
            break;
        }
    }
    return $value;
}
@endphp

<div class="col-sm-12 p-0 mb-4">
    <div class="col-sm-12 p-0" style="overflow: hidden; text-align: left;">
        <p style="border-bottom: 1px solid #b1b1b1;"><b>Pedido</b></p>
    </div>
    <div class="col-sm-12">
        {{-- row de titulos --}}
        <div class="row">
            <div class="col-3 text-center">
                <p>
                    <b>
                        Tallas
                    </b>
                </p>
            </div>

            {{-- Si es jeans, crece horizontalmente con los colores, si no, muestra solo cantidad --}}
            @php
                $colores = array_unique(array_column($colorImagen->toArray(), 'nombre_color'));
                $tallesDisponibles = [];
                $tabindex = 1;
            @endphp
            @foreach ($colores as $color)
                <div class="col text-center">
                    <p>
                        <b>
                            {{ $color }}
                        </b>
                    </p>
                </div>
            @endforeach
        </div>
        {{-- @foreach ($talles as $talle)
            @foreach ($talles as $ta)
                @php
                    if (preg_match(" /\b$ta->id_talles\b/i", $talle->tamanos)) {
                        $tallesDisponibles[] = $ta;
                    }
                    // $tallesProducto = json_decode($tallesProducto);
                @endphp
            @endforeach
        @endforeach --}}
        @foreach ($talles as $talle)
            <div class="row">
                <div class="col-3 text-center">
                    <p>
                        <b>
                            {{ $talle->nombre_talles }}
                        </b>
                    </p>
                </div>
                {{-- Input para ingresar la cantidad: talla-color --}}
                {{-- {{dd($tallesProducto)}} --}}
                @foreach ($colorImagen as $color)
                    @php
                        $value = null;
                        if ($producto_en_carrito != null) {
                            $value = get_value($producto_en_carrito, $color->id_color, $talle->nombre_talles);
                        }
                    @endphp
                    <div class="col text-center">
                        <input type="number" name="cantidad_producto" min="1" {{-- {{ in_array($talle->id_tamanos, $talles_color) && !isset($producto_en_carrito) ? '' : 'disabled' }} --}}
                            {{-- data-has-tamano="{{ in_array($talle->id_tamanos, $talles_color) ? 1 : 0 }}" --}} {{-- data-color="{{ $color->nombre_color }}" data-tamano="{{ $talle->nombre_tamanos }}" --}}
                            class="form-control form-control-sm text-center cantidad-pedido-input" tabindex="{{ $tabindex }}"
                            {{ preg_match(" /\b$talle->id_talles\b/i", $tallesProducto[$loop->index]->tamanos) ? '' : 'disabled' }}
                            data-color="{{ $color->id_color }}" data-talla="{{ $talle->nombre_talles }}"
                            data-nombre-color="{{ $color->nombre_color }}"
                            @if($value != null) value="{{$value}}" @endif>
                    </div>
                    {{-- si el color tiene el tamaño, crea un input modificable --}}
                    {{-- si el color no tiene el tamano, agrega estado desabilitado --}}
                    @php
                        $tabindex++;
                    @endphp
                @endforeach
            </div>
        @endforeach
    </div>
</div>


<div class="col-sm-12" style="padding: 0px 10px 0 10px;">
    @if ($producto_en_carrito != null)
        <button onclick="eliminarCarrito('{{ $producto_en_carrito['id_token'] }}')" class="btn btn-danger btn-carrito"
            style="width: 100%; border-radius: 30px;">Remover del Carrito</button>
    @else
        <button onclick="agregar_carrito()" class="btn btn-primary btn-carrito"
            style="width: 100%; border-radius: 30px;">Agregar al carrito</button>
    @endif
    <a href="/shop" class="btn btn-primary btn-comprar"
        style="width: 49%; float: left; margin-right: 3px; border-radius: 30px;">Comprar otros modelos</a>
    <a href="/carrito" class="btn btn-primary btn-comprar"
        style="width: 49%; float: left; margin-left: 3px; border-radius: 30px;">
        <img src="../img/icon/bag.svg" style="width: 20px; margin-top: -10px;"> Ir al carrito
    </a>
</div>
