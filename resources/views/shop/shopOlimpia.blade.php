@extends('layouts.front')

@section('content')
    @include('preloader')

    <style>
        p {
            font-family: 'Montserrat', sans-serif;
        }

        a {
            font-family: 'Montserrat', sans-serif;
        }

        h1 {
            font-family: 'Montserrat', sans-serif;
        }

    </style>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach ($slider as $s)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}"
                    class="{{ $loop->first ? 'active' : '' }}"></li>
            @endforeach
        </ol>
        <div class="carousel-inner">
            @foreach ($slider as $s)
                <div class="carousel-item active">
                    <img class="d-block w-100" src="img/{{ $s->path_sliders }}" alt="First slide">
                </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <div class="contenedor_general" style="margin-top: 80px;">

        <div class="col-sm-12 content-shop-container " style="padding: 0px 0 50px 0; overflow: hidden;">
            {{-- <div class="container-lg"> --}}

                <div class="content-shop p-2" style="text-align: center;">
                    {{-- @foreach ($imagenesProductos as $p)
                        @if ($p->tipo_producto == 'Olimpia')

                            @if ($p->principal_color_imagen == 'true')
                                @if ($p->seccion_color_imagen == 1)

                                    <div class="box-productos"
                                        style="float: left; padding: 5px; margin-bottom: 20px; font-family: 'Montserrat', sans-serif;">
                                        <a href="{{ route('shop.producto', $p->id_productos) }}">

                                            <img class="lazy" data-src="/img/productos/{{ $p->path_imagen }}"
                                                alt="{{ $p->nombre_productos }} {{ $p->codigo_productos }}"
                                                style="width: 100%;" />

                                        </a>
                                        <div class="col-sm-12 box-productos-title" style="float: left; padding: 10px 0;">
                                            <p style="margin-bottom: 0; color: #909090; font-size: 20px;">
                                                {{ $p->nombre_productos }}
                                            </p>
                                            <p style="margin-bottom: 0; font-size: 20px; font-weight: 600; color: #808080;">
                                                @if (is_null($p->precio_descuento_productos))
                                                    <span style="font-size: 14px;">Gs.</span>
                                                    {{ number_format($p->mayorista, 0, ',', '.') }}
                                                @else
                                                    <span style="color: #F25E52;"><span style="font-size: 14px;">Gs.</span>
                                                        {{ number_format($p->precio_descuento_productos, 0, ',', '.') }}</span>
                                                @endif
                                            </p>

                                        </div>

                                        @if ($p->descuentos_productos == 1)
                                            <div class="oferta"
                                                style=" background: #F25E52; border-radius: 30px; padding: 5px 10px;">
                                                OFERTA!
                                            </div>
                                        @endif
                                        @if ($p->premium == 1)
                                            <div class="oferta"
                                                style=" background: #1677BB; border-radius: 30px; padding: 5px 10px;">
                                                PREMIUM
                                            </div>
                                        @endif
                                        @if ($p->lanzamientos_productos == 1)
                                            <div class="oferta"
                                                style=" background: #94B53D; border-radius: 30px; padding: 5px 10px;">
                                                LANZAMIENTO
                                            </div>
                                        @endif
                                        <p>
                                        <div class="col-sm-12"
                                            style="margin-bottom: 10px; padding: 0px; overflow: hidden; display: flex; justify-content: center; align-content: center;">
                                            @foreach ($colorImagen as $pc)
                                                @if ($pc->pk_productos_color_imagen == $p->id_productos)
                                                    @if ($pc->principal_color_imagen == 'true')
                                                        <span
                                                            style="border: 1px solid #b1b1b1; background: {{ $pc->prefijo_color }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </div>
                                        </p>
                                        <p style="color: #808080; font-size: 15px; font-family: 'Montserrat', sans-serif;">
                                            Código. {{ $p->codigo_productos }}</p>

                                    </div>
                                @endif
                            @endif
                        @endif
                    @endforeach --}}
                    <div class="row shop-productos">
                        @foreach ($imagenesProductos as $p)
                            @if ($p->principal_color_imagen == 'true')
                                @if ($p->seccion_color_imagen == 1)
                                    @include('shop.product-card')
                                @endif
                            @endif
                        @endforeach
                    </div>
                    <div class="row" id="loading-products" style="display: none;">
                        <div class="col-sm-12 text-center">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>

                </div>
            {{-- </div> --}}
        </div>
    </div>
    <script>
        var can_make_query = true
        var scrollHeight = $(document).height();
        var footerHeight = $(document).width() < 720 ? 1000 : 500;
        let page = 2
        $(window).scroll(function() {
            var scrollPos = $(window).height() + $(window).scrollTop();
            if (((scrollHeight - footerHeight) >= scrollPos) / scrollHeight == 0) {
                if (can_make_query) {
                    can_make_query = false
                    makeQuery()
                }
            }
        });

        $(window).resize(function() {
            scrollHeight = $(document).height();
            footerHeight = $(document).width() < 720 ? 1000 : 500;
        })

        function makeQuery() {
            if (page <= {!! $imagenesProductos->lastPage() !!}) {
                $("#loading-products").show()
                $.ajax({
                    url: "{{ route('shop.get.chunk') }}?tipo=Olimpia&page=" + page + "",
                    data: {
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(resp) {
                        $(".shop-productos").append(resp)
                        setTimeout(() => {
                            page += 1
                            can_make_query = true
                            scrollHeight = $(document).height()
                        }, 500);
                        $("#loading-products").hide()
                        $(window).trigger('scroll');
                    }
                })
            }
        }
    </script>

@endsection
