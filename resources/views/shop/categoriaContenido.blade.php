<div class="col-sm-12" style="padding: 0 10px;">
    <div class="col-12" style="border-radius: 15px; overflow: hidden; margin-bottom: 20px; background: #fff; padding: 10px;">
        <div class="row">
        
            <div class="col-lg-12" style="text-align: right; color: #909090;">
                <form action="{{ route('shop.genero') }}">
                    <div class="row">
                        <div class="col-lg-6 col-2 pt-2" style="text-align: right;">
                            Filtrar:
                        </div>
                        <div class="col-lg-4 col-6 p-0">
                            <select id="texto" name="texto" class="form-control" style="border-radius: 10px;">
                                <option disabled selected>Seleccione...</option>
                                <option value="Dama">Damas</option>
                                <option value="Hombre">Caballeros</option>
                                <option value="Niña">Niñas</option>
                                <option value="Niño">Niños</option>
                                <option value="EspecialMujer">Plus+ Damas</option>
                                <option value="EspecialesHombre">Plus+ Caballeros</option>
                                <option value="tallesEspeciales">Tallas Especiales</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-4">
                            <button type="submit" class="btn btn-primary" style="font-size: 14px; border-radius: 10px; width: 100%;">
                            <i class="fa fa-search"></i>
                            Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</div>

<div class="col-12 p-0">
    <div class="col-lg-12" style="color: #909090; text-align: left;">

        @if (isset($resultado))
            Resultado:
            <span style="color: #1677bb;">
                @if ($resultado == 'Hombre')
                    Caballeros
                @elseif($resultado == "Dama")
                    Damas
                @elseif($resultado == "Niño")
                    Niños
                @elseif($resultado == "Niña")
                    Niñas
                @elseif($resultado == "Lanzamientos")
                    Lanzamientos
                @elseif($resultado == "Ofertas")
                    Ofertas
                @endif
            </span>
        @else
            @if (isset($marc->nombre_marcas))
                Resultado:
                <span style="color: #1677bb;">{{ $marc->nombre_marcas }}</span>
            @else
                @if (isset($category->nombre_categorias))
                    Resultado:
                    <span style="color: #1677bb;">{{ $category->nombre_categorias }}</span>
                @else
                    @if (isset($subcat->nombre_subcategorias))
                        Resultado:
                        <span style="color: #1677bb;">{{ $subcat->nombre_subcategorias }}</span>
                    @else
                        Resultado:
                        <span style="color: #1677bb;"> {{ count($productos) }}</span> productos
                    @endif
                @endif
            @endif
        @endif

    </div>
</div>


<div class="col-sm-12">
    <div class="row shop-productos">
        @foreach ($imagenesProductos as $index=>$p)
            {{-- @if($index == 0) --}}
                @if ($p->tipo_producto != 'Olimpia')
                    @if ($p->principal_color_imagen == 'true')
                        @if ($p->seccion_color_imagen == 1)
                            @include('shop.product-card')
                        @endif
                    @endif
                @endif
            {{-- @endif --}}
        @endforeach
    </div>
    <div class="row" id="loading-products" style="display: none;">
        <div class="col-sm-12 text-center">
            <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
</div>

<style>

</style>
