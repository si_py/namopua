@extends('layouts.front')

@section('content')
    {{-- @include('preloader') --}}

    <div class="contenedor_general">

        @include('shop.categoriaTitulo')

        <div class="col-sm-12 content-shop-container" style="padding: 0px 0 50px 0; overflow: hidden;">
            {{-- <div class="container-lg"> --}}

                @include('shop.categoriaMenu')

                <div class="content-shop p-2" style="text-align: center;">

                    @include('shop.categoriaContenido')

                </div>
            {{-- </div> --}}
        </div>
    </div>
    <script>
        var can_make_query = true
        var scrollHeight = $(document).height();
        var footerHeight = $(document).width() < 720 ? 1000 : 500;
        let page = 2
        $(window).scroll(function() {
            var scrollPos = $(window).height() + $(window).scrollTop();
            if (((scrollHeight - footerHeight) >= scrollPos) / scrollHeight == 0) {
                if (can_make_query) {
                    can_make_query = false
                    makeQuery()
                }
            }
        });

        $(window).resize(function() {
            scrollHeight = $(document).height();
            footerHeight = $(document).width() < 720 ? 1000 : 500;
        })

        function makeQuery() {
            if (page <= {!! $imagenesProductos->lastPage() !!}) {
                $("#loading-products").show()
                $.ajax({
                    url: "{{ route('shop.get.chunk') }}?page=" + page,
                    data: {
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(resp) {
                        $(".shop-productos").append(resp)
                        setTimeout(() => {
                            page += 1
                            can_make_query = true
                            scrollHeight = $(document).height()
                        }, 500);
                        $("#loading-products").hide()
                        $(window).trigger('scroll');
                    }
                })
            }
        }
    </script>
@endsection
