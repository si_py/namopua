@extends('layouts.front')

    @section('content')

        @include('preloader')

        <div class="contenedor_general">

            @include('shop.categoriaTitulo')

            <div class="col-sm-12 content-shop-container" style="padding: 0px 0 50px 0; overflow: hidden;">
                {{-- <div class="container-lg"> --}}

                    @include('shop.categoriaMenu')

                    <div class="content-shop p-2" style="text-align: center;">

                        @include('shop.categoriaContenido')

                    </div>
                {{-- </div> --}}
            </div>
        </div>

    @endsection
