@extends('layouts.front')

@section('content')

    <div class="container-lg">
        <div class="col-sm-12 title-shop">

            <h1 style="font-weight: 900; font-family: 'Montserrat', sans-serif;">Shop</h1>
            <p>para mayoristas apartir de 30 unidades<br />modelos surtidos</p>

        </div>
        <style>
            .title-shop {
                text-align: center;
                padding: 80px 0 80px 0;
            }

            .title-shop h1 {
                font-size: 100px;
                font-weight: 900;
            }

            .title-shop p {
                font-size: 17px;
                font-weight: 100;
                color: #909090;
                margin-top: 0px;
                margin-bottom: 0;
            }

        </style>
    </div>

    <div class="col-sm-12 content-shop-container" style="padding: 0px 0 50px 0; overflow: hidden;">
        <div class="container-lg">
            <div class="side-shop">
                <ul>
                    <li style="color: #000; margin-bottom: 5px;">Categorías</li>
                    @foreach ($cat as $cate)
                        @php
                            dd($cate);
                        @endphp
                        @if ($cate->nombre_categorias != 'Olimpia')
                            <li><a
                                    href="{{ route('shop.categorias', $cate->id_categorias) }}">{{ $cate->nombre_categorias }}</a>
                            </li>
                        @endif
                    @endforeach
                    <li style="color: #000; margin-bottom: 5px; margin-top: 10px;">SubCategorías</li>
                    @foreach ($categorias as $sub)
                        @if ($sub->nombre_categorias != 'Olimpia')
                            <li><a
                                    href="{{ route('shop.subcategorias', $sub->id_subcategorias) }}">{{ $sub->nombre_subcategorias }}</a>
                            </li>
                        @endif
                    @endforeach
                    <li style="color: #000; margin-bottom: 5px; margin-top: 10px;">Marca</li>
                    @foreach ($marca as $m)
                        @if ($m->nombre_marcas != 'Olimpia')
                            <li><a href="{{ route('shop.marcas', $m->id_marcas) }}">{{ $m->nombre_marcas }}</a></li>
                        @endif
                    @endforeach
                    <li
                        style="margin-bottom: 5px; margin-top: 10px; background: #1677BB; color: #fff;  padding: 3px 15px 5px 15px; border-radius: 30px; width: 130px;">
                        <a href="{{ route('shop.premium') }}" style="color: #fff;">
                            Premium
                        </a>
                    </li>
                    <li
                        style="margin-bottom: 5px; margin-top: 10px; background: #94B53D; color: #fff;  padding: 3px 15px 5px 15px; border-radius: 30px; width: 130px;">
                        <a href="{{ route('shop.premium') }}" style="color: #fff;">
                            Lanzamientos
                        </a>
                    </li>
                    <li
                        style="margin-bottom: 5px; margin-top: 10px; background: #F25E52; color: #fff; padding: 3px 15px 5px 15px; border-radius: 30px; width: 80px;">
                        <a href="{{ route('shop.ofertas') }}" style="color: #fff;">
                            Ofertas
                        </a>
                    </li>
                </ul>
            </div>

            <div class="content-shop" style="text-align: center;">
                <div class="col-sm-12" style="padding: 0; overflow: hidden; margin-bottom: 20px;">
                    <div style="width: 40%; float: left; color: #909090; text-align: left;">
                        <i class="fa fa-search"> </i> Resultado:
                        <span style="color: #1677bb;">
                            @if ($resultado == 'Hombre')
                                Caballeros
                            @elseif($resultado == "Dama")
                                Damas
                            @elseif($resultado == "Niño")
                                Niños
                            @elseif($resultado == "Niña")
                                Niñas
                            @endif
                        </span>
                    </div>
                    <div style="width: 60%; float: left; text-align: right; color: #909090;">
                        <form action="{{ route('shop.genero') }}">
                            Filtrar por:
                            <select id="texto" name="texto" style="border-radius: 30px;">
                                <option disabled selected>Seleccione...</option>
                                <option value="Dama">Damas</option>
                                <option value="Hombre">Caballeros</option>
                                <option value="Niña">Niñas</option>
                                <option value="Niño">Niños</option>
                            </select>
                            <button type="submit" class="btn btn-sm btn-primary"
                                style="padding: 0 10px 0 10px; font-size: 14px; border-radius: 30px;"><i
                                    class="fa fa-search"></i> Buscar</button>
                        </form>
                    </div>
                    <div style="width: 100%; height: 5px; border-bottom: 1px solid #e5e5e5; overflow: hidden;">

                    </div>
                </div>
                @foreach ($colorImagen as $p)
                    @if ($p->principal_color_imagen == 'true')
                        @if ($p->seccion_color_imagen == 1)

                            <div class="box-productos"
                                style="float: left; padding: 5px; margin-bottom: 20px; font-family: 'Montserrat', sans-serif;">
                                <a href="{{ route('shop.producto', $p->id_productos) }}">

                                    <img class="lazy" data-src="/img/productos/{{ $p->path_imagen }}"
                                        alt="{{ $p->nombre_productos }} {{ $p->codigo_productos }}"
                                        style="width: 100%;" />

                                </a>
                                <div class="col-sm-12 box-productos-title" style="float: left; padding: 10px 0;">
                                    <p style="margin-bottom: 0; color: #909090; font-size: 20px;">
                                        {{ $p->nombre_productos }}
                                    </p>
                                    <p style="margin-bottom: 0; font-size: 20px; font-weight: 600; color: #808080;">
                                        @if (is_null($p->precio_descuento_productos))
                                            <span style="font-size: 14px;">Gs.</span>
                                            {{ number_format($p->mayorista, 0, ',', '.') }}
                                        @else
                                            {{-- <span style="font-size: 14px; margin-right: 3px; text-decoration: line-through; color: #808080;">{{number_format($p->mayorista,  0, ',', '.')}}</span> --}}
                                            <span style="color: #F25E52;"><span style="font-size: 14px;">Gs.</span>
                                                {{ number_format($p->precio_descuento_productos, 0, ',', '.') }}</span>
                                        @endif
                                    </p>

                                </div>

                                @if ($p->descuentos_productos == 1)
                                    <div class="oferta"
                                        style=" background: #F25E52; border-radius: 30px; padding: 5px 10px;">
                                        OFERTA!
                                    </div>
                                @endif
                                @if ($p->lanzamientos_productos == 1)
                                    <div class="oferta"
                                        style=" background: #1677BB; border-radius: 30px; padding: 5px 10px;">
                                        PREMIUM
                                    </div>
                                @endif
                                <!-- <div class="col-sm-12" style="padding: 0px; overflow: hidden;">
                @foreach ($colorImagen as $pc)
                @if ($pc->pk_productos_color_imagen == $p->id_productos)
                @if ($pc->principal_color_imagen == 'true')
                <span style="background: {{ $pc->prefijo_color }}; width: 20px; height: 20px; border-radius: 50px; float: left; margin-right: 5px;">

                </span>

                @endif
                @endif
                @endforeach
              </div> -->
                                <p>
                                <div class="col-sm-12"
                                    style="margin-bottom: 10px; padding: 0px; overflow: hidden; display: flex; justify-content: center; align-content: center;">
                                    @foreach ($colorImagen as $pc)
                                        @if ($pc->pk_productos_color_imagen == $p->id_productos)
                                            @if ($pc->principal_color_imagen == 'true')
                                                <span
                                                    style="border: 1px solid #b1b1b1; background: {{ $pc->prefijo_color }}; width: 20px; height: 20px; float: left; border-radius: 50px; margin-right: 5px;"></span>
                                            @endif
                                        @endif
                                    @endforeach
                                </div>
                                </p>
                                <p style="color: #808080; font-size: 15px; font-family: 'Montserrat', sans-serif;">Codigo.
                                    {{ $p->codigo_productos }}</p>


                                <!-- <div class="col-sm-4 box-productos-carrito" style="float: left; padding: 20px; text-align: right;">
              <a href="#">
                <img src="img/icon/cart.svg" style="width: 50px; padding: 10px;"/>
              </a>
            </div> -->
                            </div>
                        @endif
                    @endif
                @endforeach

            </div>
        </div>
    </div>

    <style>
        .content-shop-container {
            width: 1150px;
        }

        .side-shop {
            float: left;
            width: 15%;
        }

        .side-shop ul {
            margin-left: -40px;
        }

        .side-shop ul li {
            list-style: none;
            font-family: 'Montserrat', sans-serif;
            font-size: 14px;
            color: #909090;
        }

        .side-shop ul li a {
            color: #808080;
        }

        .side-shop ul li a:hover {
            color: #1677BB;
        }

        .content-shop {
            float: left;
            width: 85%;
        }

        .content-shop select {
            border: 1px solid #e5e5e5;
            color: #909090;
            font-size: 15px;
        }

        @media screen and (max-width: 1150px) {
            .side-shop {
                display: none;
            }

            .content-shop {
                width: 100%;
            }
        }

    </style>


@endsection
