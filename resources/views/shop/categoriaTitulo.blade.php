<div class="col-sm-12 title-shop">
    <h1 style="font-weight: 900; font-family: 'Montserrat', sans-serif;">Shop</h1>
    <p style="font-weight: 400;">para mayoristas apartir de treinta unidades<br />modelos surtidos</p>
</div>

<style>
    .title-shop {
        text-align: center;
        padding: 80px 0 80px 0;
    }
    .title-shop h1 {
        font-size: 100px;
        font-weight: 900;
    }
    .title-shop p{
        font-size: 17px;
        font-weight: 100;
        color: #909090;
        margin-top: 0px;
        margin-bottom: 0;
        font-family: 'Montserrat', sans-serif;
    }
    @media screen and (max-width: 991px){
        .title-shop {
            text-align: center;
            padding: 30px 0 30px 0;
        }
        .title-shop h1 {
            font-size: 60px;
            font-weight: 900;
        }
        .title-shop p{
            font-size: 14px;
        }
    }
</style>
