@php
$rows = 0;
@endphp
<div class="col-sm-12 p-0 mb-4">
    <div class="col-sm-12 p-0 mb-3" style="overflow: hidden; text-align: left;">
        <span><b>Realizá tu Pedido</b> <span id="mensaje_alerta_pedido" class="text-danger" style="margin-left: 10px;"></span></span>
    </div>
    <div class="col-sm-12 px-0">
        <table class="table border">
            <thead>
                <th class="text-center">
                </th>
                @foreach ($talles as $talle)
                    <th class="text-center">
                        <b>
                            {{ $talle->nombre_talles }}
                        </b>
                    </th>
                @endforeach
            </thead>
            <tbody>
                @foreach ($colorImagen as $color)
                    @php
                        $index = $loop->index;
                        $rows++;
                    @endphp
                    <tr>
                        <td style="vertical-align: middle;">
                            <span>
                                <b>
                                    {{ $color->nombre_color }}&nbsp;&nbsp;
                                </b>
                            </span>
                        </td>
                        @if (count($talles) > 0)
                            @foreach ($talles as $talle)
                                @php
                                    $value = null;
                                    if ($producto_en_carrito != null) {
                                        $value = get_value($producto_en_carrito, $color->id_color, $talle->nombre_talles);
                                    }
                                @endphp
                                <td style="vertical-align: middle;">
                                    <input  type="number" name="cantidad_producto" min="1"
                                            class="form-control form-control-sm text-center cantidad-pedido-input p-0"
                                            tabindex="{{ $tabindex }}"
                                            {{ preg_match(" /\b$talle->id_talles\b/i", $tallesProducto[$index]->tamanos ?? '') ? '' : 'disabled' }}
                                            data-color="{{ $color->id_color }}" data-talla="{{ $talle->nombre_talles }}"
                                            data-nombre-color="{{ $color->nombre_color }}" @if ($value != null) value="{{ $value }}" @endif>
                                </td>
                                @php
                                    $tabindex++;
                                @endphp
                            @endforeach
                        @else
                            @if ($loop->index == 0)
                                @if (count($talles) <= 0)
                                    <td rowspan="{{ count($colorImagen) }}">
                                        <span class="p-5">
                                            No hay tallas disponibles
                                        </span>
                                    </td>
                                @endif
                            @endif
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


<div class="row" style="padding: 0px 10px 0 10px;">
    <div class="col p-0">
        <button onclick="agregar_carrito()" class="btn btn-primary btn-carrito"
            style="width:100%; border-radius: 30px;">Agregar al carrito</button>
    </div>
    <div class="col p-0">
        <a href="/shop" class="btn btn-primary btn-comprar"
            style="width:100%; float: left; margin-right: 3px; border-radius: 30px;">Comprar otros modelos</a>
    </div>
    <div class="col p-0">
        <a href="/carrito" class="btn btn-primary btn-comprar"
            style="width:100%; float: left; margin-left: 3px; border-radius: 30px;">
            <img src="../img/icon/bag.svg" style="width: 20px; margin-top: -10px;"> Ir al carrito
        </a>

    </div>
</div>
