@extends('layouts.app')

@section('content')

<!-- Breadcrumb-->
<div class="breadcrumb-holder">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Home</a></li>
      <li class="breadcrumb-item active">Secciones</li>
    </ul>
  </div>
</div>

<section>
  <div class="col-sm-12" style="padding: 10px;">
    <div class="row">

      <div class="col-lg-4">
        <div class="card">
          <div class="card-header d-flex align-items-center">
            <h4>Agregar Secciones</h4>
          </div>
          <div class="card-body">
            <form class="form-horizontal">
              <div class="form-group row">
                <div class="col-sm-2">
                  <label>Sección</label>
                </div>
                <div class="col-sm-10">
                  <input id="nombre_secciones" type="text" placeholder="Nombre seccion" class="form-control form-control-success">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-2">
                  <label>Referencias</label>
                </div>
                <div class="col-sm-10">
                  <input id="referencias_secciones" type="text" placeholder="Ej. Pagina principal" class="form-control form-control-warning">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-10 offset-sm-2">
                  <button onclick="agregar_secciones()" type="button" class="btn btn-primary">Agregar Sección</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="col-lg-8">
        <div class="card">

          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Sección</th>
                    <th>Referencias</th>
                    <th>Activo</th>
                  </tr>
                </thead>
                <tbody class="table_secciones">
                  @foreach($seccion as $s)
                  <tr>
                    <th scope="row">{{$s->id_secciones}}</th>
                    <td contenteditable="true" class="column_name" data-column_name="nombre_secciones" data-id="{{$s->id_secciones}}">{{$s->nombre_secciones}}</td>
                    <td contenteditable="true" class="column_name" data-column_name="referencias_secciones" data-id="{{$s->id_secciones}}">{{$s->referencias_secciones}}</td>
                    <td contenteditable="true" class="column_name" data-column_name="activo_secciones" data-id="{{$s->id_secciones}}">{{$s->activo_secciones}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>



    </div>
  </div>
</section>

<script>
function agregar_secciones() {
  var nombre = document.getElementById("nombre_secciones").value;
  var referencia = document.getElementById("referencias_secciones").value;
  var id_stock="";
  console.log(nombre);
  $.ajax({
    type:"ajax",
    url: "{{route('secciones.store')}}",
    type: 'POST',
    data: {nombre: nombre, referencia: referencia, _token:'{{csrf_token()}}'},

    success: function(){

      // var categoria = $(this).val();
    $.get('/tabla_secciones', function(data){
//esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
      console.log(data);
      for (var i=0; i<data.length;i++)
      id_stock+= '<tr>'+
                 '<td>'+data[i].id_secciones+'</td>'+
                 '<td contenteditable="true" class="column_name" data-column_name="nombre_secciones" data-id="'+data[i].id_secciones+'">'+data[i].nombre_secciones+'</td>'+
                 '<td contenteditable="true" class="column_name" data-column_name="referencias_secciones" data-id="'+data[i].id_secciones+'">'+data[i].referencias_secciones+'</td>'+
                 '<td contenteditable="true" class="column_name" data-column_name="activo_secciones" data-id="'+data[i].id_secciones+'">'+data[i].activo_secciones+'</td>'+
                 '</tr>',

                 $(".table_secciones").empty();
                 $('.table_secciones').append(id_stock);
         })


    }
  })
};

</script>

@endsection
