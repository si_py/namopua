@extends('layouts.app')

@section('content')
    <style>
        .my-custom-scrollbar {
            position: relative;
            height: 200px;
            overflow: auto;
        }

        .table-wrapper-scroll-y {
            display: block;
        }

    </style>
    <!-- Counts Section -->
    <section class="dashboard-counts section-padding">
        <div class="container-fluid">
            <div class="row">
                <!-- Count item widget-->
                <div class="col-xl-3 col-md-4 col-6">
                    <div class="wrapper count-title d-flex" style="background: #fff; padding: 20px; border-radius: 20px;">
                        <div class="icon"><i class="fa fa-clock"></i></div>
                        <div class="name"><strong class="text-uppercase">Pendientes</strong>

                            <div class="count-number">{{ count($pendiente) }}</div>
                        </div>
                    </div>
                </div>

                <!-- Count item widget-->
                <div class="col-xl-3 col-md-4 col-6">
                    <div class="wrapper count-title d-flex" style="background: #fff; padding: 20px; border-radius: 20px;">
                        <div class="icon"><i class="fa fa-briefcase"></i></div>
                        <div class="name"><strong class="text-uppercase">Pasar a Buscar</strong>
                            <div class="count-number">{{ count($buscar) }}</div>
                        </div>
                    </div>
                </div>

                <!-- Count item widget-->
                <div class="col-xl-3 col-md-4 col-6">
                    <div class="wrapper count-title d-flex" style="background: #fff; padding: 20px; border-radius: 20px;">
                        <div class="icon"><i class="fa fa-send"></i></div>
                        <div class="name"><strong class="text-uppercase">Enviando</strong>
                            <div class="count-number">{{ count($enviando) }}</div>
                        </div>
                    </div>
                </div>
                <!-- Count item widget-->
                <div class="col-xl-3 col-md-4 col-6">
                    <div class="wrapper count-title d-flex" style="background: #fff; padding: 20px; border-radius: 20px;">
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <div class="name"><strong class="text-uppercase">Concretados</strong>
                            <div class="count-number">{{ count($concretado) }}</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- Updates Section -->
    <section>
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12 col-md-12">
                    <!-- Recent Activities Widget      -->
                    <div id="recent-activities-wrapper" class="card updates activities" style="margin-bottom:0;">
                        <div id="activites-header" class="card-header d-flex justify-content-between align-items-center">
                            <h2 class="h5 display"><a data-toggle="collapse" data-parent="#recent-activities-wrapper"
                                    href="#activities-box" aria-expanded="true" aria-controls="activities-box">Procesos de
                                    pedidos</a></h2><a data-toggle="collapse" data-parent="#recent-activities-wrapper"
                                href="#activities-box" aria-expanded="true" aria-controls="activities-box"><i
                                    class="fa fa-angle-down"></i></a>
                        </div>
                        <div id="activities-box" role="tabpanel" class="collapse show">
                            <ul class="activities list-unstyled">
                                <!-- Item-->
                                @foreach ($pedido as $pe)
                                    <li>
                                        <div class="row">
                                            <a href="#"
                                                onclick="enviarDetalle('{{ $pe->id_pedido }}','{{ $pe->name }}')"
                                                data-toggle="modal" data-target="#modalDetalles" style="width: 100%;">
                                                <div class="col-1 date-holder text-right" style="float: left;">
                                                    <div class="icon"><i class="icon-clock"></i></div>
                                                    <div class="date">
                                                        <span>{{ $pe->fecha_pedido }}</span>
                                                        <!-- <span class="text-info">6 hours ago</span> -->
                                                        <span
                                                            class="text-info">{{ substr($pe->hora_pedido, 0, 6) }}</span>
                                                    </div>
                                                </div>

                                                <div class="col-9 content"
                                                    style="float: left; border-right: 1px solid #eee">

                                                    <strong>Cliente: {{ $pe->name }}</strong>
                                                    <p style="margin-bottom: 0;">N˚Pedido: {{ $pe->id_pedido }}</p>
                                                    <p style="margin-bottom: 0;">Direccion: {{ $pe->direccion_users }}
                                                    </p>
                                                    <p style="margin-bottom: 0;">Teléfono: {{ $pe->telefono_users }}</p>

                                                </div>

                                                <div class="col-2 content" style="float: left; border: none;">
                                                    <strong>Situación</strong>

                                                    <p>{{ $pe->estado_pedido }}</p>

                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    {{ $pedido->links() }}
                </div>
            </div>
        </div>

    </section>


    <!-- Modal detalle de pedido-->
    <div class="modal fade" id="modalDetalles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" style="max-width: 70%;" role="document">
            <div class="modal-content" style="width: 100%;">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cliente: <span id="nombre_cliente"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="col-sm-4" style="float: left;">
                        <p>Comprobante </p>
                        <div class="col-sm-12" id="comprobante_modal">

                        </div>
                    </div>

                    <div class="col-sm-8" style="float: left;">
                        <p>Detalle de pedido </p>
                        <input type="hidden" class="form-control" id="id_pedido_cliente">
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Cod.</th>
                                        <th>Cantidad</th>
                                        <th>Descripción</th>
                                        <th class="no-detalle" style="display: none;">talla</th>
                                        <th class="no-detalle" style="display: none;">Color</th>
                                        <th>Precio unitario</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody class="tabla_detalles">

                                </tbody>
                            </table>
                        </div>
                        <div id="footer_detalles"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" id="descargar-pdf-detalle">
                        Descargar
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button onclick="pasarIdProceso()" type="button" id="btn-procesar" class="btn btn-primary"
                        data-toggle="modal" data-target="#modalProceso" data-dismiss="modal">Siguiente</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal detalle de pedido-->
    <div class="modal fade" id="modalProceso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" style="max-width: 70%;" role="document">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <input type="hidden" class="form-control" id="id_pedido_cliente_proceso">
                    <label style="margin-top: 40px;">Proceso</label>
                    <select id="procesos" class="form-control" style="margin-bottom: 40px;">
                        <option value="Pendiente">Pendiente</option>
                        <option value="Pasar a Buscar">Pasar a Buscar</option>
                        <option value="Enviando">Enviando</option>
                        <option value="Concretado">Concretado</option>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalDetalles"
                        data-dismiss="modal">Volver</button>
                    <button type="button" onclick="procesar_pedido()" id="btn-procesar"
                        class="btn btn-primary">Procesar</button>
                </div>

            </div>
        </div>
    </div>

    <script>
        function enviarDetalle(id, nombre) {
            $('#nombre_cliente').html(nombre);
            $('#id_pedido_cliente').val(id);
            var imagen = "";
            $("#comprobante_modal").empty();
            $('#btn-procesar').css({
                "display": "none"
            });
            $.ajax({
                type: "ajax",
                url: "{{ route('admin.enviarComprobante') }}",
                type: 'GET',
                data: {
                    id: id
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        console.log(data[i].id_pedido_documentos);
                        imagen += '<img src="../img/comprobantes/' + data[i].path_documentos +
                            '" style="width: 100%;">',
                            $("#comprobante_modal").empty();
                        $("#comprobante_modal").append(imagen);
                        $('#btn-procesar').css({
                            "display": "block"
                        });
                    }
                }
            });

            $.ajax({
                type: "ajax",
                url: "{{ route('admin.enviarDetalle') }}",
                type: 'GET',
                data: {
                    id: id
                },
                success: function(data) {
                    let productos = []
                    if (data.length > 0) {
                        productos = formatArray(data);
                    }
                    let build = build_detalle_table(productos)
                    let total_general = build.total
                    let detalle = build.detalle
                    var detalle_total = "";

                    $(".tabla_detalles").empty();
                    $(".tabla_detalles").append(detalle);
                    detalle_total = `
                            <div class="row" style="font-weight: bold">
                                <div class="col-2">
                                    TOTAL: 
                                </div>
                                <div class="col">
                                    Gs. ${number_format(total_general)}
                                </div>
                            </div>
                        `
                    $("#footer_detalles").empty();
                    $("#footer_detalles").append(detalle_total);

                    $("#descargar-pdf-detalle").attr('onclick', 'exportarPDF(' + id + ', event)')
                }
            });
        }

        function exportarPDF(id, ev) {
            let button = ev.currentTarget
            $(button).attr('disabled', true)
            $.ajax({
                type: "ajax",
                url: "{{ route('admin.enviarDetalle') }}",
                type: 'GET',
                data: {
                    id: id
                },
                success: function(data) {
                    let productos = []
                    if (data.length > 0) {
                        productos = formatArray(data);
                        let detalle = build_detalle_table(productos, false, true)
                        $.ajax({
                            url: "{{ route('admin.descargarDetallesPDF') }}",
                            method: 'POST',
                            data: {
                                _token: "{{ csrf_token() }}",
                                table: detalle,
                                id: id
                            },
                            success: function(resp) {
                                console.log(resp)
                                var element = document.createElement('a');
                                element.setAttribute('href', resp.file);
                                element.setAttribute('download', resp.file_name);
                                element.setAttribute('target', '_blank');
                                element.style.display = 'none';

                                document.body.appendChild(element);
                                
                                element.click();
                                
                                document.body.removeChild(element);
                                $(button).removeAttr('disabled')
                            }
                        })
                    }
                }
            });
        }

        function download(filename) {
            var element = document.createElement('a');
            element.setAttribute('download', filename);
            element.setAttribute('target', '_blank');
            element.innerHTML = "Descargar pdf"

            // element.style.display = 'none';
            document.body.appendChild(element);

            element.click();

            // document.body.removeChild(element);
        }

        function formatArray(data) {
            let productos = []
            if (data[0].detalle_pedido != null) {
                for (var i = 0; i < data.length; i++) {
                    let producto_detalles = JSON.parse(data[i].detalle_pedido);
                    producto_detalles.forEach(detail => {
                        productos.push({
                            ...data[i],
                            color_detalle_pedido: detail.nombre_color,
                            talla_detalle_pedido: detail.talla,
                            cantidad_detalle_pedido: detail.cantidad
                        })
                    })
                }
            } else {
                productos = data
            }
            return productos
        }

        function build_detalle_table(data, returnTotalGral = true, borders = false) {

            var detalle = "";
            var total_general = 0;
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    var precio = number_format(data[i].precio_detalle_pedido);
                    var total = (data[i].precio_detalle_pedido * data[i].cantidad_detalle_pedido);
                    var total_moneda = number_format(total);

                    detalle += `
                        <tr ${borders ? 'style="background-color: '+ (i % 2 == 0 ? '' : '#eeeeee') +'"' : '' }>
                            <td ${borders ? 'style="padding: 0.75rem"' : ''}>
                                ${data[i].cod_producto_detalle_pedido}
                            </td>
                            <td ${borders ? 'style="padding: 0.75rem"' : ''}>
                                ${data[i].cantidad_detalle_pedido}
                            </td>
                            <td ${borders ? 'style="padding: 0.75rem"' : ''}>
                                ${data[i].nombre_producto_detalle_pedido}
                            </td>
                            <td ${borders ? 'style="padding: 0.75rem"' : ''}>
                                ${data[i].talla_detalle_pedido ?? ''}
                            </td>
                            <td ${borders ? 'style="padding: 0.75rem"' : ''}>
                                ${data[i].color_detalle_pedido}
                            </td>
                            <td ${borders ? 'style="padding: 0.75rem"' : ''}>
                                ${precio}
                            </td>
                            <td ${borders ? 'style="padding: 0.75rem"' : ''}>
                                ${total_moneda}
                            </td>
                        </tr>`;
                    total_general += total;
                }
                $(".no-detalle").css('display', 'table-cell')
            }
            if (returnTotalGral) {
                return {
                    total: total_general,
                    detalle: detalle
                }
            } else {
                return detalle
            }
        }

        function pasarIdProceso() {
            var id_pedido = $('#id_pedido_cliente').val();
            $('#id_pedido_cliente_proceso').val(id_pedido);
        }

        function procesar_pedido() {
            var id_pedido = $('#id_pedido_cliente_proceso').val();
            var proceso = $('#procesos').val();
            $.ajax({
                type: "ajax",
                url: "{{ route('admin.modificarProceso') }}",
                type: 'GET',
                data: {
                    id_pedido: id_pedido,
                    proceso: proceso
                },
                success: function(data) {
                    location.reload();
                }

            });
        }
    </script>

@endsection
