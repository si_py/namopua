@php
$fundacion = new DateTime('2001-01-01');
$actual = new DateTime();

$anos_trayectoria = $fundacion->diff($actual)->y;
@endphp

@extends('layouts.front')

@section('content')

    <div class="container-lg">
        <div class="col-sm-12 title-shop">
            <h1 style="font-weight: 900; font-family: 'Montserrat', sans-serif;">ÑAMOPU'A PARAGUAY S.A.</h1>
        </div>
        <style>
            .title-shop {
                text-align: center;
                padding: 80px 0 80px 0;
            }

            .title-shop h1 {
                font-size: 50px;
                font-weight: 900;
                color: #000;
                margin-bottom: 0;
            }

            .title-shop p {
                font-size: 17px;
                font-weight: 100;
                color: #909090;
                margin-top: 0px;
                margin-bottom: 0;
                font-family: 'Montserrat', sans-serif;
            }

            p,
            h1 {
                font-family: 'Montserrat', sans-serif;
                color: #808080;
            }

            p {
                font-weight: 400;
                text-align: justify;
            }

        </style>
        <div class="container-lg" style="margin-bottom: 100px;">
            <div class="col-12">
                <p>
                    Somos una <strong>Empresa Paraguaya</strong> con {{ $anos_trayectoria }} años de trayectoria en
                    <strong>confecciones de prendas de jeans en Paraguay</strong>
                    a buen precio, hechas con mano de obra <strong>100% nacional</strong>, desde el diseño y confección de
                    los jeans, hasta
                    la distribución de los mismos a todo el rincón del País, generando así una fuente de trabajo para
                    cientos
                    de personas, que prestan atención hasta al más mínimo detalle de producción para ofrecer lo mejor a
                    nuestros clientes.
                </p>
            </div>
        </div>
    @endsection
