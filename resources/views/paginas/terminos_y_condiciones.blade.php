@extends('layouts.front')

@section('content')

<div class="container-lg">
  <div class="col-sm-12 title-shop">

    <h1 style="font-weight: 900; font-family: 'Montserrat', sans-serif;">Términos y Condiciones</h1>
    <p style="font-weight: 400;">
    CUALQUIER PERSONA QUE NO ACEPTE ESTOS TÉRMINOS Y CONDICIONES GENERALES, LOS CUALES TIENEN UN 
    CARÁCTER OBLIGATORIO Y VINCULANTE, DEBERÁ ABSTENERSE DE UTILIZAR EL SITIO Y/O LOS SERVICIOS.
    </p>
  </div>
  <style>
  .title-shop {
    text-align: center;
    padding: 80px 0 80px 0;
  }
  .title-shop h1 {
    font-size: 60px;
    font-weight: 900;
  }
  .title-shop p{
    font-size: 17px;
    font-weight: 100;
    color: #909090;
    margin-top: 0px;
    margin-bottom: 0;
    font-family: 'Montserrat', sans-serif;
  }
  p {
    font-family: 'Montserrat', sans-serif;
    color: #808080;
  }
  </style>

  <div class="col-12" style="padding: 0; text-align: justify; margin-bottom: 100px;">
    <p>
    El Usuario debe leer, entender y aceptar todas las condiciones establecidas en los Términos y 
    Condiciones Generales y en las Políticas de Privacidad así como en los demás documentos incorporados 
    a los mismos por referencia, previo al registro como Usuario de namopuapy.com , optando de forma expresa 
    por recibir los mismos y toda otra información por medios digitales.
    </p>
    <p>
    <span style="font-weight: 600;">01 – Capacidad</span><br>
    Los Servicios sólo están disponibles para personas que tengan capacidad legal para contratar. 
    No podrán utilizar los servicios las personas que no tengan esa capacidad, los menores de edad o 
    Usuarios de Namopuapy.com que hayan sido suspendidos temporalmente o inhabilitados definitivamente. 
    Si estás registrando un Usuario como Empresa, debes tener capacidad para contratar a nombre de tal 
    entidad y de obligar a la misma en los términos de este Acuerdo.
    </p>
    <p>
    <span style="font-weight: 600;">02 – Registro</span><br>
    Es obligatorio completar el formulario de registro en todos sus campos con datos válidos para poder utilizar 
    los servicios que brinda Namopuapy.com El futuro Usuario deberá completarlo con su información personal de manera 
    exacta, precisa y verdadera (“Datos Personales”) y asume el compromiso de actualizar los Datos Personales conforme 
    resulte necesario. El Usuario presta expresa conformidad con que Namopuapy.com utilice diversos medios para identificar 
    sus datos personales, asumiendo el Usuario la obligación de revisarlos y mantenerlos actualizados. Namopuapy.com NO se 
    responsabiliza por la certeza de los Datos Personales de los Usuarios. Los Usuarios garantizan y responden, en cualquier 
    caso, de la veracidad, exactitud, vigencia y autenticidad de sus Datos Personales. La Cuenta es personal, única e 
    intransferible, y está prohibido que un mismo Usuario registre o posea más de una Cuenta. En caso que Namopuapy.com 
    detecte distintas Cuentas que contengan datos coincidentes o relacionados, podrá cancelar, suspender o inhabilitarlas.<br>

    El Usuario será responsable por todas las operaciones efectuadas en su Cuenta, pues el acceso a la misma está restringido 
    al ingreso y uso de su Clave de Seguridad, de conocimiento exclusivo del Usuario. El Usuario se compromete a notificar a 
    Namopuapy.com en forma inmediata y por medio idóneo y fehaciente, cualquier uso no autorizado de su Cuenta, así como el 
    ingreso por terceros no autorizados a la misma. Se aclara que está prohibida la venta, cesión o transferencia de la Cuenta 
    (incluyendo la reputación) bajo ningún título.<br>

    Namopuapy.com se reserva el derecho de rechazar cualquier solicitud de registro o de cancelar un registro previamente aceptada, 
    sin que esté obligado a comunicar o exponer las razones de su decisión y sin que ello genere algún derecho a indemnización o resarcimiento.
    </p>
    <p>
    <span style="font-weight: 600;">03 – Modificaciones del Acuerdo</span><br>
    Namopuapy.com podrá modificar los Términos y Condiciones Generales en cualquier momento haciendo públicos en el Sitio los términos modificados. 
    Todos los términos modificados entrarán en vigor a los 10 (diez) días de su publicación. Dichas modificaciones serán comunicadas por Namopuapy.com 
    a sus usuarios y todo usuario que no esté de acuerdo con las modificaciones efectuadas por Namopuapy.com podrá solicitar la baja de la cuenta.<br>
    El uso del sitio y/o sus servicios implica la aceptación de estos Términos y Condiciones generales de uso de Namopuapy.com.
    </p>
    <p>
    <span style="font-weight: 600;">04 – Obligaciones de los Usuarios</span><br>
    <span style="font-weight: 600;">4.1.</span> Obligaciones del comprador<br>
    Durante el plazo fijado por Namopuapy.com , los Usuarios interesados podrán comprar Los productos y servicios que permitan la 
    contratación en el Sitio. La oferta de venta se cierra una vez que vence el plazo de la publicación o se acaban las cantidades 
    estipuladas por el Vendedor y la promoción del servicio culmina con el vencimiento del plazo de la publicación.<br>
    Al comprar un producto o contratar un servicio, el Usuario acepta quedar obligado por las condiciones de venta incluidas en la 
    publicación, en la medida en que las mismas no infrinjan las leyes o los Términos y Condiciones Generales y demás políticas de 
    Namopuapy.com La oferta de compra de un bien o contratación de un servicio es irrevocable salvo en circunstancias excepcionales.<br>
    Las compras sólo serán consideradas válidas una vez que hayan sido concretadas, verificadas y procesadas por el sistema informático de Namopuapy.com
    Impuestos. Tal como lo establece la normativa fiscal vigente, el comprador debe exigir factura o ticket como comprobante de la operación.<br>
    <span style="font-weight: 600;">4.2.</span> Obligaciones del vendedor<br>
    El vendedor debe tener capacidad legal para vender el producto o servicio objeto de su oferta. Asimismo, debe cumplir con todas las 
    obligaciones regulatorias pertinentes y contar con los registros, habilitaciones, permisos y/o autorizaciones exigidos por la normativa 
    aplicable para la venta de los productos y servicios ofrecidos. Si el vendedor ha recibido una compra queda obligado a intentar comunicarse 
    con el comprador y completar la operación. La cancelación de una venta por parte del vendedor impactará en su reputación.<br>
    Dado que Namopuapy.com es un punto de encuentro entre comprador y vendedor y no participa de las operaciones que se realizan entre ellos, 
    el vendedor será responsable por todas las obligaciones y cargas impositivas que correspondan por la venta de sus bienes y/o servicios, 
    sin que pudiera imputársele a Llevauno.com algún tipo de responsabilidad por incumplimientos en tal sentido.
    </p>
    <p>
    <span style="font-weight: 600;">05 – Violaciones del Sistema o Bases de Datos</span><br>
    No está permitida ninguna acción o uso de dispositivo, software, u otro medio tendiente a interferir tanto en las actividades y operatoria de 
    Namopuapy.com como en las ofertas, descripciones, cuentas o bases de datos de Namopuapy.com . Cualquier intromisión, tentativa o actividad 
    violatoria o contraria a las leyes sobre derecho de propiedad intelectual y/o a las prohibiciones estipuladas en este contrato harán pasible 
    a su responsable de las acciones legales pertinentes, y a las sanciones previstas por este acuerdo, así como lo hará responsable de indemnizar los daños ocasionados.
    </p>
    <p>
    <span style="font-weight: 600;">06 – Sanciones. Suspensión de operaciones</span><br>
    Sin perjuicio de otras medidas, Namopuapy.com podrá advertir, suspender en forma temporal o definitivamente la Cuenta de un Usuario o una publicación, 
    aplicar una sanción que impacte negativamente en la reputación de un Usuario, iniciar las acciones que estime pertinentes y/o suspender la prestación 
    de sus Servicios si (a) se quebrantara alguna ley, o cualquiera de las estipulaciones de los Términos y Condiciones Generales y demás políticas de 
    Namopuapy.com; (b) si incumpliera sus compromisos como Usuario; (c) si se incurriera a criterio de Namopuapy.com en conductas o actos dolosos o fraudulentos; 
    (d) no pudiera verificarse la identidad del Usuario o cualquier información proporcionada por el mismo fuere errónea; (e) Namopuapy.com entendiera que las 
    publicaciones u otras acciones pueden ser causa de responsabilidad para el Usuario que las publicó, para Namopuapy.com o para los demás Usuarios en general. 
    En el caso de la suspensión de un Usuario, sea temporal o definitiva, todos los artículos que tuviera publicados serán removidos del sistema.
    </p>
    <p>
    <span style="font-weight: 600;">07– Políticas de Google</span><br>
    Google no quiere que los usuarios se sientan engañados por el contenido que se promociona en los anuncios de Shopping, así que nuestro objetivo 
    es ser claros, sinceros y ofrecerles la información que necesitan para tomar decisiones con conocimiento de causa. Por este motivo, no permitimos 
    lo siguiente: <br>
    <span style="font-weight: 600;">Ejemplos de lo que no está permitido: Omisión de información relevante</span><br>
    No divulgar de forma clara y visible el modelo de pago y el gasto completo que representará para el usuario antes y después de la compra. Ejemplos: 
    los precios de los productos (precio total, moneda) dependen de condiciones adicionales que influyen en el coste total para el usuario. Es decir, 
    los precios de subasta, las tarifas de suscripción, los contratos, las formas de pago o los requisitos adicionales de compra que crean obligaciones 
    de pago adicionales no reveladas durante el proceso de pago.<br>
    Consulte los atributos de la Especificación de Feeds relativos a la disponibilidad, el precio, los impuestos y los gastos de envío para obtener 
    indicaciones que le ayuden a cumplir esta política.<br>
    No revelar de forma clara y visible todas las condiciones relacionadas antes y después de la compra. Ejemplos: No incluir los términos y condiciones 
    del comerciante, la información de envío o la política de devoluciones y reembolsos (o que esta última sea confusa o no se encuentre fácilmente).<br>
    Omitir información importante al promocionar contenido a favor de una organización benéfica o política<br>
    Ejemplos: no mostrar el número de organización benéfica o de exención fiscal en las donaciones benéficas; no indicar si las donaciones políticas están exentas de impuestos.<br>
    Promociones no disponibles.
    Prometer productos u ofertas promocionales no disponibles para los usuarios<br>
    Ejemplos: promocionar productos que no están en stock; promocionar ofertas que ya no están activas; crear llamadas a la acción en una promoción que no se puede realizar fácilmente desde la página de destino.<br>
    Consulte los atributos de las especificaciones de feeds relativos a la disponibilidad y los precios para obtener indicaciones concretas que le ayuden a cumplir esta política.<br>
    Nos tomamos muy en serio las infracciones de esta política y las consideramos graves. Se consideran infracciones flagrantes de las políticas de anuncios de shopping 
    aquellas que son tan graves que son ilegales o que suponen un perjuicio significativo para nuestros usuarios. Para determinar si un comerciante o destino infringe estas 
    políticas, es posible que revisemos información procedente de varias fuentes, como su sitio web, sus productos, sus cuentas e incluso fuentes de terceros. En el momento 
    en que detectemos infracciones de estas políticas, suspenderemos su cuenta sin previo aviso, y no podrá volver a participar en los anuncios de shopping ni en las fichas orgánicas.<br>
    Si considera que ha habido un error y que no ha incumplido nuestras políticas, puede enviar una apelación junto con una explicación de los motivos a través del enlace que aparece en 
    la parte inferior de esta página. Solo restauramos cuentas si consideramos que las circunstancias son las adecuadas y hay razones de peso para hacerlo, por lo que es importante que 
    nos facilite información detallada, precisa y veraz. Más información acerca de la suspensión de cuentas.<br>
    Lea la política anterior para saber qué no permitimos. Por ejemplo:<br>
    Información de contacto insuficiente: su sitio web debe ofrecer a los usuarios al menos dos de los siguientes tipos de datos de contacto: dirección física, 
    número de teléfono o correo electrónico. No basta con tener un formulario de contacto.<br>
    Información de pago insuficiente: antes de la tramitación de la compra:<br>
    Debe mostrar claramente en su sitio web todos los métodos de pago aceptados.<br>
    Debe informar de todos los gastos que supondrá para el usuario antes y después de la compra.<br>
    Métodos de pago no admitidos: permita que los usuarios puedan seleccionar al menos un método de pago convencional en la tramitación de la compra (como tarjeta de crédito, tarjeta de débito, factura o envío contrarreembolso).<br>
    Si su cuenta aún está en periodo de advertencia, se revisará automáticamente al finalizar dicho periodo.<br>
    Si su cuenta está suspendida, tendrá que solicitar que la revisemos.<br>
    Solicitar una revisión<br>
    La mayoría de las cuentas se revisan en 3 días laborables, pero algunas pueden tardar más si requieren una revisión más compleja. En cuanto comprobemos, 
    eliminaremos la advertencia o, en caso de suspensión de la cuenta, la aprobaremos y podrá ponerla en marcha de nuevo.<br>
    </p>

    <p>
    <span style="font-weight: 600;">08 – Fallas en el sistema</span><br>
    Namopuapy.com no se responsabiliza por cualquier daño, perjuicio o pérdida al usuario causados por fallas en el sistema, en el servidor o en Internet. Namopuapy.com 
    tampoco será responsable por cualquier virus que pudiera infectar el equipo del usuario como consecuencia del acceso, uso o examen de su sitio web o a raíz de cualquier 
    transferencia de datos, archivos, imágenes, textos, o audio contenidos en el mismo. Los usuarios NO podrán imputar responsabilidad alguna ni exigir pago por lucro cesante, 
    en virtud de perjuicios resultantes de dificultades técnicas o fallas en los sistemas o en Internet. Namopuapy.com no garantiza el acceso y uso continuado o ininterrumpido de su sitio. 
    El sistema puede eventualmente no estar disponible debido a dificultades técnicas o fallas de Internet, o por cualquier otra circunstancia ajena a Namopuapy.com; en tales casos se procurará 
    restablecerlo con la mayor celeridad posible sin que por ello pueda imputarse algún tipo de responsabilidad. Namopuapy.com no será responsable por ningún error u omisión contenidos en su sitio web.<br>
    </p>
    <p>
    <span style="font-weight: 600;">09 – Tarifas. Facturación</span><br>
    El usuario vendedor solo deberá pagar a Namopuapy.com un costo por la venta cuando la operación se concrete. Namopuapy.com se reserva el derecho de tomar las medidas judiciales y 
    extrajudiciales que estime pertinentes para obtener el pago del monto debido. Namopuapy.com se reserva el derecho de modificar, cambiar, agregar, o eliminar las tarifas vigentes, 
    en cualquier momento, lo cual será notificado a los Usuarios, en la forma establecida en la Cláusula 3. Sin embargo, Namopuapy.com podrá modificar temporalmente la Política de Tarifas 
    y las tarifas por sus servicios por razón de promociones, siendo efectivas estas modificaciones cuando se haga pública la promoción o se realice el anuncio. En caso de haberse facturado 
    cargos que no hubiesen correspondido, el usuario deberá comunicarse con nuestro equipo de Atención al Cliente para resolver dicha cuestión.<br>
    </p>
    <p>
    <span style="font-weight: 600;">10 – Sistema de reputación</span><br>
    Debido a que la verificación de la identidad de los Usuarios en Internet es difícil, Namopuapy.com no puede confirmar ni confirma la identidad pretendida de cada Usuario. 
    Por ello el Usuario Vendedor cuenta con un sistema de reputación de Usuarios que es actualizado periódicamente en base a datos vinculados con su actividad en el sitio. Este sistema de reputación, 
    además constará de un espacio donde los Usuarios compradores podrán hacer comentarios sobre la experiencia de compra. Dichos comentarios serán incluidos bajo exclusiva responsabilidad de los 
    Usuarios que los emitan. Namopuapy.com no tiene obligación de verificar la veracidad o exactitud de los mismos y NO se responsabiliza por los dichos allí vertidos por cualquier Usuario, por las 
    ofertas de compras o ventas que los Usuarios realicen teniéndolos en cuenta o por la confianza depositada en las opiniones de los compradores o por cualquier otro comentario expresado dentro del 
    sitio o a través de cualquier otro medio incluido el correo electrónico. Namopuapy.com se reserva el derecho de editar y/o eliminar aquellos comentarios que sean considerados
    inadecuados u ofensivos. Namopuapy.com mantiene el derecho de excluir a aquellos usuarios que sean objeto de comentarios negativos provenientes de fuentes distintas.
    </p>
    <p>
    <span style="font-weight: 600;">11 – Propiedad intelectual – Licencia – Enlaces</span><br>
    Namopuapy.com y/o sus sociedades controlantes, controladas, filiales o subsidiarias se reservan todos los derechos, incluyendo los derechos de propiedad intelectual e industrial, 
    asociados con los servicios de Namopuapy.com, sus sitios web, los contenidos de sus pantallas, programas, bases de datos, redes, códigos, desarrollo, software, arquitectura, hardware, 
    contenidos, información, tecnología, fases de integración, funcionalidades, dominios, archivos que permiten al Usuario acceder y crear su Cuenta, herramientas de venta, marcas, patentes, 
    derechos de autor, diseños y modelos industriales, nombres comerciales, entre otros, y declara que están protegidos por leyes nacionales e internacionales vigentes. En ningún caso se 
    entenderá que el Usuario tendrá algún tipo de derecho sobre los mismos excepto para utilizar el servicio de Namopuapy.com conforme a lo previsto en estos Términos y Condiciones Generales. 
    El uso indebido o contrario a la normativa vigente de los derechos de propiedad intelectual e industrial de Namopuapy.com, así como su reproducción total o parcial, queda prohibido, salvo 
    autorización expresa y por escrito de Namopuapy.com<br>
    </p>
    <p>
    <span style="font-weight: 600;">12 – Indemnidad</span><br>
    El Usuario mantendrá indemne a Namopuapy.com, así como a sus filiales, empresas controladas y/o controlantes, funcionarios, directivos, sucesores, administradores, representantes y empleados, 
    por cualquier reclamo iniciado por otros Usuarios, terceros o por cualquier Organismo, relacionado con sus actividades en el Sitio, el cumplimiento y/o el incumplimiento de los Términos y 
    Condiciones Generales o demás Políticas, así como respecto de cualquier violación de leyes o derechos de terceros.<br>
    </p>
    <p>
    <span style="font-weight: 600;">13 – Jurisdicción y Ley Aplicable</span><br>
    uerdo estará regido en todos sus puntos por las leyes vigentes en la República del Paraguay. Cualquier controversia derivada del presente acuerdo, su existencia, validez, interpretación, 
    alcance o cumplimiento, será sometida ante la Justicia Nacional Ordinaria.<br>
    </p>
    <p>

    <span style="font-weight: 600;">14 – Domicilio</span><br>
    Se fija como domicilio de Namopuapy.com la calle Pizzarro 912 casi García de Zuñiga , Asunción – Paraguay. Si tenés alguna duda o consulta sobre los Términos y Condiciones Generales o 
    demás políticas y principios que rigen Namopuapy.com podés contactarnos.
    </p>
  </div>
</div>

@endsection