<table class="table table-striped">

    <thead>
        <tr>
            <th>#</th>
            <th>Cod.</th>
            <th>Producto</th>
            <th>Tipo</th>
            <th>Precios</th>
            <th style="width: 150px;">Color</th>
            <th style="width: 70px;">Tamaño</th>
            <th style="width: 80px;">Imagenes</th>
            <th style="width: 80px;">Disponible</th>
            <th style="width: 80px;">Acción</th>
        </tr>
    </thead>
    <tbody class="table-productos">
        @foreach ($productoFull as $p)
        <tr>
            <td>{{ $p->id_productos }}</td>
            <td>{{ $p->codigo_productos }}</td>
            <td>
                <table class="table table-sm table-borderless m-0 p-0" style="font-size: 15px;">
                    <tr style="background-color: transparent;">
                        <td class="p-0" style="width: 80px;">Nombre: </td>
                        <td class="p-0">{{ $p->nombre_productos }}</td>
                    </tr>
                    <tr style="background-color: transparent;">
                        <td class="p-0" style="width: 80px;">Genero: </td>
                        <td class="p-0">{{ $p->tipo_producto }}</td>
                    </tr>
                    <tr style="background-color: transparent;">
                        <td class="p-0" style="width: 80px;">Marca: </td>
                        <td class="p-0">{{ $p->nombre_marcas }}</td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-sm table-borderless m-0 p-0" style="font-size: 15px;">
                    <tr style="background-color: transparent;">
                        <td class="p-0" style="width: 100px;">Lanzamiento: </td>
                        <td class="p-0">
                            @if ($p->lanzamientos_productos == 1)
                                Si
                            @else
                                No
                            @endif
                        </td>
                    </tr>
                    <tr style="background-color: transparent;">
                        <td class="p-0" style="width: 100px;">Premium: </td>
                        <td class="p-0">
                            @if ($p->premium == 1)
                                Si
                            @else
                                No
                            @endif
                        </td>
                    </tr>
                    <tr style="background-color: transparent;">
                        <td class="p-0" style="width: 100px;">Descuento: </td>
                        <td class="p-0">
                            @if ($p->descuentos_productos == 1)
                                Si
                            @else
                                No
                            @endif
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-sm table-borderless m-0 p-0" style="font-size: 15px;">
                    <tr style="background-color: transparent;">
                        <td class="p-0" style="width: 120px;">Precio Desc: </td>
                        <td class="p-0">{{ $p->precio_descuento_productos }}</td>
                    </tr>
                    <tr style="background-color: transparent;">
                        <td class="p-0" style="width: 120px;">Mayorista: </td>
                        <td class="p-0">{{ $p->mayorista }}</td>
                    </tr>
                    <tr style="background-color: transparent;">
                        <td class="p-0" style="width: 120px;">Minorista: </td>
                        <td class="p-0">{{ $p->minorista }}</td>
                    </tr>
                </table>
            </td>
            <td>
                @foreach ($colorProductos as $cp)
                    @if ($cp->pk_productos_color_productos == $p->id_productos)
                        <a href="{{ route('productos.deleteColor', $cp->id_color_productos) }}"
                            style="width: 100%;">
                            <div class="col-sm-12"
                                style="padding: 0; background: {{ $cp->prefijo_color }}; text-align: center; color: {{ strtolower($cp->nombre_color) == 'blanco' || strtolower($cp->nombre_color) == 'celeste' || strtolower($cp->nombre_color) == 'transparente' ? '#000' : '#fff' }}; margin-bottom: 2px;">
                                {{ $cp->nombre_color }}
                            </div>
                        </a>
                    @endif
                @endforeach
                <a href="#"
                    onclick="abrirModalColor('{{ $p->id_productos }}', '{{ $p->codigo_productos }}', 'namopua-table')"
                    data-toggle="modal" data-target="#modal-agregar-color"
                    title="Agregar Imagenes">
                    <div id="add-box" style=" padding: 2px;float: left;">
                        +
                    </div>
                </a>
            </td>
            <td>
                <div class="col-sm-12"
                    style="text-align: center; padding: 13px; float: left; border: 1px solid #e5e5e5;">
                    <a href="#"
                        onclick="abrirModalTalleColor('{{ $p->id_productos }}', '{{ $p->codigo_productos }}', 'namopua-table')"
                        {{-- data-toggle="modal" data-target="#color-talla-modal" --}} title="Agregar Imagenes">
                        <div id="add-box" style="padding: 0px;">
                            +
                        </div>
                    </a>
                </div>
            </td>
            <td>
                <a href="#"
                    onclick="abrirModalImagen('{{ $p->id_productos }}', '{{ $p->codigo_productos }}', 'namopua-table')"
                    data-toggle="modal" data-target="#modal-agregar-imagenes"
                    title="Agregar Imagenes">
                    <div id="add-box"
                        style="width: 50px; height: 50px; padding: 2px; border: 1px solid #e5e5e5; background: #e5e5e5; float: left;">
                        <div class="col-sm-12"
                            style="border: 2px dotted #d1d1d1; text-align: center; display: flex; justify-content: center; align-content: center; flex-direction: column;">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </a>
            </td>

            <td>
                @if ($p->activo_productos == true)
                    <a href="#"
                        onclick="modificarExistencia('{{ $p->id_productos }}', 
                                                    '{{ $p->codigo_productos }}',
                                                    '{{ $p->nombre_productos }}' , 
                                                    '{{ $p->activo_productos }}')"
                        style="text-align: center;" data-toggle="modal"
                        data-target="#modal_modificar_existencia">
                        <i class="fa fa-thumbs-up" style="font-size: 25px;"></i><br>Existente
                    </a>
                @else
                    <a href="#"
                        onclick="modificarExistencia('{{ $p->id_productos }}', 
                                                    '{{ $p->codigo_productos }}',
                                                    '{{ $p->nombre_productos }}' , 
                                                    '{{ $p->activo_productos }}')"
                        style="text-align: center;" data-toggle="modal"
                        data-target="#modal_modificar_existencia">
                        <i class="fa fa-thumbs-down" style="font-size: 25px;"></i><br>Sin stock
                    </a>
                @endif
            </td>

            <td>
                <a href="#"
                    onclick="modificarProductos( '{{ $p->id_productos }}',
                                                '{{ $p->codigo_productos }}', 
                                                '{{ $p->nombre_productos }}',
                                                '{{ $p->tipo_producto }}',
                                                '{{ $p->pk_categorias_productos }}',
                                                '{{ $p->pk_subcategorias_productos }}', 
                                                '{{ $p->pk_marcas_productos }}',
                                                '{{ $p->lanzamientos_productos }}', 
                                                '{{ $p->descuentos_productos }}', 
                                                '{{ $p->precio_descuento_productos }}', 
                                                '{{ $p->mayorista }}', 
                                                '{{ $p->minorista }}', 
                                                '{{ $p->descripcion_productos }}', 
                                                '{{ $p->premium }}', 
                                                '{{ $p->clasicos }}')"
                    style="text-align: center;" data-toggle="modal"
                    data-target="#modal_modificar_productos">
                    <i class="fa fa-edit" style="font-size: 25px;"></i><br>Editar
                </a>
            </td>

        </tr>
        @endforeach
    </tbody>

</table>

<div class="col-12 paginate_productos" style="padding: 0; position: relative;">
    {{ $productoFull->links() }}
</div>

<style>
    .paginate_productos nav {
        position: absolute;
        right: 0;
    }

</style>
