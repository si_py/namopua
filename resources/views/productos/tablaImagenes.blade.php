<div class="row">
  <input type="hidden" id="cantidadDeColor" value="{{count($tipoColor)}}" class="form-control">
  @foreach($tipoColor as $index=>$t)
    <div class="col-3">
      <p class="mb-2" style="padding: 0px;">
        <span style="background-color: {{$t->prefijo_color}}; color: #fff; padding: 5px 10px; border-radius: 3px;">{{ $t->nombre_color }}</span>
        @if($t->activo == true)
          <button class="btn btn-sm" type="button" onclick="inhabilitarColor('{{ $t->id_color }}', 'inhabilitar')" style="float: right; margin-top: -3px;">
            <svg class="activoColor" fill="#2CA797" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                width="30px" height="30px" viewBox="0 0 301.153 301.153" style="enable-background:new 0 0 301.153 301.153;"
                xml:space="preserve">
              <g>
                <path d="M257.098,44.055c-58.738-58.736-154.311-58.742-213.047,0c-58.733,58.738-58.727,154.319,0,213.047
                  c58.742,58.739,154.314,58.733,213.047,0C315.831,198.362,315.837,102.793,257.098,44.055z M148.952,189.476
                  c-2.402,2.402-6.29,2.402-8.695,0l-50.008-50.005c-1.186-1.198-1.79-2.771-1.801-4.348c0-1.573,0.604-3.146,1.801-4.348
                  c2.405-2.401,6.29-2.401,8.695,0l45.648,45.657l66.305-66.299c2.414-2.411,6.3-2.401,8.707,0c2.402,2.405,2.402,6.29,0,8.695
                  L148.952,189.476z"/>
              </g>
            </svg>
          </button>
        @else 
          <button class="btn btn-sm" type="button" onclick="inhabilitarColor('{{ $t->id_color }}', 'habilitar')" style="float: right; margin-top: -3px;">
            <svg class="activoColor" fill="#dc3545" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                width="30px" height="30px" viewBox="0 0 301.153 301.153" style="enable-background:new 0 0 301.153 301.153;"
                xml:space="preserve">
              <g>
                <path d="M257.098,44.055c-58.738-58.736-154.311-58.742-213.047,0c-58.733,58.738-58.727,154.319,0,213.047
                  c58.742,58.739,154.314,58.733,213.047,0C315.831,198.362,315.837,102.793,257.098,44.055z M148.952,189.476
                  c-2.402,2.402-6.29,2.402-8.695,0l-50.008-50.005c-1.186-1.198-1.79-2.771-1.801-4.348c0-1.573,0.604-3.146,1.801-4.348
                  c2.405-2.401,6.29-2.401,8.695,0l45.648,45.657l66.305-66.299c2.414-2.411,6.3-2.401,8.707,0c2.402,2.405,2.402,6.29,0,8.695
                  L148.952,189.476z"/>
              </g>
            </svg>
          </button>
          @endif
      </p>
      <table class="table table-sm table-borderless">
        <thead>
          <tr class="bg-secondary text-white">
            <th style="width: 90px; text-align: center;">Orden</th>
            <th style="width: 120px; text-align: center;">Imagenes</th>
            <th style="text-align: center;">Principal</th>
          </tr>
        </thead>
      </table>
      <ul id="sortable-{{$index}}" class="sortable" style="border: 1px solid #e5e5e5;">
        @foreach($color as $contador=>$c)
          @if($t->pk_color_color_imagen == $c->pk_color_color_imagen)
            <li class="default" id="{{$c->id_color_imagen}}" data-id="{{$c->pk_productos_color_imagen}}">
              <table class="table table-borderless table-sm">
                <tbody>
                  <tr>
                    <td style="width: 60px; text-align: center;">{{$c->orden}}</td>
                    <td style="width: 120px;">
                      <img src="img/productos/{{ $c->path_imagen }}" style="width: 100%;">
                    </td>
                    <td style="text-align: center;">
                      <p>
                        @if($c->principal_color_imagen == 1)
                        Si
                      @else
                        No
                      @endif
                      </p>

                      <p>
                        <button class="btn" type="button" onclick="borrarImagenModal('{{$c->id_color_imagen}}')">
                          <svg version="1.1" id="Capa_1" width="20px" height="20px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 59 59" style="enable-background:new 0 0 59 59;" xml:space="preserve">
                            <g>
                              <path d="M52.5,6H38.456c-0.11-1.25-0.495-3.358-1.813-4.711C35.809,0.434,34.751,0,33.499,0H23.5c-1.252,0-2.31,0.434-3.144,1.289
                                C19.038,2.642,18.653,4.75,18.543,6H6.5c-0.552,0-1,0.447-1,1s0.448,1,1,1h46c0.552,0,1-0.447,1-1S53.052,6,52.5,6z M21.792,2.681
                                C22.24,2.223,22.799,2,23.5,2h9.999c0.701,0,1.26,0.223,1.708,0.681c0.805,0.823,1.128,2.271,1.24,3.319H20.553
                                C20.665,4.952,20.988,3.504,21.792,2.681z"/>
                              <path d="M10.456,54.021C10.493,55.743,11.565,59,15.364,59h28.272c3.799,0,4.871-3.257,4.907-4.958L50.376,10H8.624L10.456,54.021z
                                M48.291,12l-1.747,41.979C46.538,54.288,46.4,57,43.636,57H15.364c-2.734,0-2.898-2.717-2.909-3.042L10.709,12H48.291z"/>
                              <path d="M17.5,54h24c0.552,0,1-0.447,1-1s-0.448-1-1-1h-24c-0.552,0-1,0.447-1,1S16.948,54,17.5,54z"/>
                              <path d="M17.5,49h24c0.552,0,1-0.447,1-1s-0.448-1-1-1h-24c-0.552,0-1,0.447-1,1S16.948,49,17.5,49z"/>
                              <path d="M17.5,44h24c0.552,0,1-0.447,1-1s-0.448-1-1-1h-24c-0.552,0-1,0.447-1,1S16.948,44,17.5,44z"/>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                          </svg>
                        </button>
                      </p>
                      
                    </td>
                  </tr>
                </tbody>
              </table>
            </li>
          @endif
        @endforeach
      </ul>
    </div>
  @endforeach
</div>


<script>
  var cantidad = $('#cantidadDeColor').val();
  $(function() {
    
    for(var i = 0; i < cantidad; i++) {
      $( "#sortable-"+i ).sortable({
        placeholder: "highlight", 
        update: function(event, ui) {
          console.log($( this ).sortable('toArray'));
          var order = $( this ).sortable('toArray');

          var id = $('#pk_productos_imagen_productos').val();
          var codigo = $('#modal_codigo_producto_imagen').text(); 
          var target = 'namopua-table';

          $.ajax({
            url: "{{ route('productos.ordenarImagenProducto') }}",
            type: 'POST',
            data: {
                order: order, _token: "{{ csrf_token() }}", 
            },
            success: function (data) {
              abrirModalImagen(id, codigo, target);
            }

          });
        }
      });
    }
  });


  function inhabilitarColor(id_color, habilitar) {
    var id = $('#pk_productos_imagen_productos').val();
    var codigo = $('#modal_codigo_producto_imagen').text(); 
    var target = 'namopua-table';
    $.ajax({
      url: "{{ route('productos.habilitarColor') }}",
      type: 'POST',
      data: {
          id: id, id_color: id_color, habilitar: habilitar, _token: "{{ csrf_token() }}", 
      },
      success: function (data) {
        abrirModalImagen(id, codigo, target);
      }

    });
  }
</script>