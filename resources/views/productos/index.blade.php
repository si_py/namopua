@extends('layouts.app')

@section('content')
    <style>
        .delete-talle {
            border: 1px solid black;
            padding: 10px;
        }

        .delete-talle:hover {
            background-color: rgba(231, 231, 231, 0.767);
            cursor: pointer;
        }
    </style>
    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb" style="padding-bottom: 10px;">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Productos</li>

                <li style="margin-top: -5px; margin-left: 20px;">
                    <div class="input-group">
                        <input type="text" id="buscador_productos" class="form-control form-control-sm"
                            style="margin-bottom: 0;">
                        <div class="input-group-append">
                            <button class="btn btn-secondary btn-sm" onclick="buscarProductos()" type="button"
                                style="border: none;"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <section>
        <div class="col-sm-12" style="padding: 10px;">
            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body" style="padding: 0;">

                            <table class="table table-striped">

                                <thead>
                                    <tr>
                                        <th style="width: 7%;">Cod.</th>
                                        <th style="width: 16%;">Producto</th>
                                        <th style="width: 7%;">Tipo</th>
                                        <th style="width: 7%;">Categoría</th>
                                        <th style="width: 7%;">SubCategoría</th>
                                        <th style="width: 7%;">Marca</th>
                                        <th style="width: 7%;">Clásico</th>
                                        <th style="width: 7%;">Lanzamiento</th>
                                        <th style="width: 7%;">Premium</th>
                                        <th style="width: 7%;">Descuento</th>
                                        <th style="width: 7%;">Precio Desc.</th>
                                        <th style="width: 7%;">Mayorista</th>
                                        <th style="width: 7%;">Minorista</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <input id="codigo_productos" class="form-control form-control-sm" type="text"
                                                placeholder="Codigo del producto" />
                                        </td>
                                        <td>
                                            <input id="nombre_productos" class="form-control form-control-sm" type="text"
                                                placeholder="Nombre del producto" />
                                        </td>
                                        <td>
                                            <select id="tipo_producto" class="form-control form-control-sm">
                                                <option>Seleccione..</option>
                                                <option value="Dama">Dama</option>
                                                <option value="Hombre">Hombre</option>
                                                <option value="Niño">Niño</option>
                                                <option value="Niña">Niña</option>
                                                <option value="Unisex">Unisex</option>
                                                <option value="EspecialesHombre">Especial Hombre</option>
                                                <option value="EspecialMujer">Especial Mujer</option>
                                                <option value="Olimpia">Olimpia</option>
                                                <option value="tapaBocas">Tapabocas</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="pk_categorias_productos" class="form-control form-control-sm">
                                                <option>Seleccione..</option>
                                                @foreach ($categoria as $ca)
                                                    <option value="{{ $ca->id_categorias }}">
                                                        {{ $ca->nombre_categorias }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select id="pk_subcategorias_productos" class="form-control form-control-sm">
                                                <option>Seleccione..</option>
                                                @foreach ($subcategoria as $su)
                                                    <option value="{{ $su->id_subcategorias }}">
                                                        {{ $su->nombre_subcategorias }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select id="pk_marcas_productos" class="form-control form-control-sm">
                                                <option>Seleccione..</option>
                                                @foreach ($marcas as $ma)
                                                    <option value="{{ $ma->id_marcas }}">{{ $ma->nombre_marcas }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select id="clasico_productos" class="form-control form-control-sm">
                                                <option>Seleccione..</option>
                                                <option value="1">Si</option>
                                                <option value=0>No</option>
                                            </select>
                                        </td>

                                        <td>
                                            <select id="lanzamientos_productos" class="form-control form-control-sm">
                                                <option>Seleccione..</option>
                                                <option value="1">Si</option>
                                                <option value=0>No</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="descuentos_productos" class="form-control form-control-sm">
                                                <option>Seleccione..</option>
                                                <option value="1">Si</option>
                                                <option value=0>No</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="premium_productos" class="form-control form-control-sm">
                                                <option>Seleccione..</option>
                                                <option value="1">Si</option>
                                                <option value=0>No</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input id="precio_descuento_productos" class="form-control form-control-sm"
                                                type="number" />
                                        </td>
                                        <td>
                                            <input id="mayorista" class="form-control form-control-sm" type="number" />
                                        </td>
                                        <td>
                                            <input id="minorista" class="form-control form-control-sm" type="number" />
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="12">
                                            <input type="text" id="descripcion_producto"
                                                placeholder="Descripción del producto" class="form-control form-control-sm">
                                        </td>
                                        <td style=" padding-top: 18px; text-align: center;">
                                            <a href="#" onclick="agregar_productos()" title="Agregar Producto">
                                                <i class="fa fa-plus" style="font-size: 20px;"></i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>


    <section>
        <div class="col-sm-12" style="padding: 10px;">
            <div class="row">

                <div class="col-lg-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="namopua-tab" data-toggle="tab" href="#productos-namopua"
                                role="tab" aria-controls="productos-namopua" aria-selected="true">Ñamopu'a Paraguay
                                S.A.</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="olimpia-tab" data-toggle="tab" href="#productos-olimpia"
                                role="tab" aria-controls="productos-olimpia" aria-selected="false">Olimpia</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="productos-namopua" role="tabpanel"
                            aria-labelledby="namopua-tab">
                            <div class="card">
                                <div class="card-body tabla_productos" id="namopua-table" style="padding: 0;">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Cod.</th>
                                                <th>Producto</th>
                                                <th>Tipo</th>
                                                <th>Precios</th>
                                                <th style="width: 150px;">Color</th>
                                                <th style="width: 70px;">Tamaño</th>
                                                <th style="width: 80px;">Imagenes</th>
                                                <th style="width: 80px;">Disponible</th>
                                                <th style="width: 80px;">Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody class="table-productos" data-table="namopua">
                                            @foreach ($namopuaproductoFull as $p)
                                                <tr>
                                                    <td>{{ $p->id_productos }}</td>
                                                    <td>{{ $p->codigo_productos }}</td>
                                                    <td>
                                                        <table class="table table-sm table-borderless m-0 p-0"
                                                            style="font-size: 15px;">
                                                            <tr style="background-color: transparent;">
                                                                <td class="p-0" style="width: 80px;">Nombre: </td>
                                                                <td class="p-0">{{ $p->nombre_productos }}</td>
                                                            </tr>
                                                            <tr style="background-color: transparent;">
                                                                <td class="p-0" style="width: 80px;">Genero: </td>
                                                                <td class="p-0">{{ $p->tipo_producto }}</td>
                                                            </tr>
                                                            <tr style="background-color: transparent;">
                                                                <td class="p-0" style="width: 80px;">Marca: </td>
                                                                <td class="p-0">{{ $p->nombre_marcas }}</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table class="table table-sm table-borderless m-0 p-0"
                                                            style="font-size: 15px;">
                                                            <tr style="background-color: transparent;">
                                                                <td class="p-0" style="width: 100px;">Lanzamiento:
                                                                </td>
                                                                <td class="p-0">
                                                                    @if ($p->lanzamientos_productos == 1)
                                                                        Si
                                                                    @else
                                                                        No
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: transparent;">
                                                                <td class="p-0" style="width: 100px;">Premium: </td>
                                                                <td class="p-0">
                                                                    @if ($p->premium == 1)
                                                                        Si
                                                                    @else
                                                                        No
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: transparent;">
                                                                <td class="p-0" style="width: 100px;">Descuento: </td>
                                                                <td class="p-0">
                                                                    @if ($p->descuentos_productos == 1)
                                                                        Si
                                                                    @else
                                                                        No
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table class="table table-sm table-borderless m-0 p-0"
                                                            style="font-size: 15px;">
                                                            <tr style="background-color: transparent;">
                                                                <td class="p-0" style="width: 120px;">Precio Desc:
                                                                </td>
                                                                <td class="p-0">{{ $p->precio_descuento_productos }}
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color: transparent;">
                                                                <td class="p-0" style="width: 120px;">Mayorista: </td>
                                                                <td class="p-0">{{ $p->mayorista }}</td>
                                                            </tr>
                                                            <tr style="background-color: transparent;">
                                                                <td class="p-0" style="width: 120px;">Minorista: </td>
                                                                <td class="p-0">{{ $p->minorista }}</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        @foreach ($colorProductos as $cp)
                                                            @if ($cp->pk_productos_color_productos == $p->id_productos)
                                                                <a href="{{ route('productos.deleteColor', $cp->id_color_productos) }}"
                                                                    style="width: 100%; float: left; margin-right: 5px;">
                                                                    <div class="col-sm-12"
                                                                        style="padding: 0; background: {{ $cp->prefijo_color }}; text-align: center; color: {{ strtolower($cp->nombre_color) == 'blanco' || strtolower($cp->nombre_color) == 'celeste' || strtolower($cp->nombre_color) == 'transparente' ? '#000' : '#fff' }}; margin-bottom: 2px;">
                                                                        {{ $cp->nombre_color }}
                                                                    </div>
                                                                </a>
                                                            @endif
                                                        @endforeach
                                                        <a href="#"
                                                            onclick="abrirModalColor('{{ $p->id_productos }}', '{{ $p->codigo_productos }}', 'namopua-table')"
                                                            data-toggle="modal" data-target="#modal-agregar-color"
                                                            title="Agregar Imagenes">
                                                            <div id="add-box" style=" padding: 2px;float: left;">
                                                                +
                                                            </div>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-12"
                                                            style="text-align: center; padding: 13px; float: left; border: 1px solid #e5e5e5;">
                                                            <a href="#"
                                                                onclick="abrirModalTalleColor('{{ $p->id_productos }}', '{{ $p->codigo_productos }}', 'namopua-table')"
                                                                {{-- data-toggle="modal" data-target="#color-talla-modal" --}} title="Agregar Imagenes">
                                                                <div id="add-box" style="padding: 0px;">
                                                                    +
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="#"
                                                            onclick="abrirModalImagen('{{ $p->id_productos }}', '{{ $p->codigo_productos }}', 'namopua-table')"
                                                            data-toggle="modal" data-target="#modal-agregar-imagenes"
                                                            title="Agregar Imagenes">
                                                            <div id="add-box"
                                                                style="width: 50px; height: 50px; padding: 2px; border: 1px solid #e5e5e5; background: #e5e5e5; float: left;">
                                                                <div class="col-sm-12"
                                                                    style="border: 2px dotted #d1d1d1; text-align: center; display: flex; justify-content: center; align-content: center; flex-direction: column;">
                                                                    <i class="fa fa-search"></i>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </td>

                                                    <td>
                                                        @if ($p->activo_productos == true)
                                                            <a href="#"
                                                                onclick="modificarExistencia('{{ $p->id_productos }}', 
                                                                                            '{{ $p->codigo_productos }}',
                                                                                            '{{ $p->nombre_productos }}' , 
                                                                                            '{{ $p->activo_productos }}')"
                                                                style="text-align: center;" data-toggle="modal"
                                                                data-target="#modal_modificar_existencia">
                                                                <i class="fa fa-thumbs-up"
                                                                    style="font-size: 25px;"></i><br>Existente
                                                            </a>
                                                        @else
                                                            <a href="#"
                                                                onclick="modificarExistencia('{{ $p->id_productos }}', 
                                                                                            '{{ $p->codigo_productos }}',
                                                                                            '{{ $p->nombre_productos }}' , 
                                                                                            '{{ $p->activo_productos }}')"
                                                                style="text-align: center;" data-toggle="modal"
                                                                data-target="#modal_modificar_existencia">
                                                                <i class="fa fa-thumbs-down"
                                                                    style="font-size: 25px;"></i><br>Sin stock
                                                            </a>
                                                        @endif
                                                    </td>

                                                    <td>
                                                        <a href="#"
                                                            onclick="modificarProductos( '{{ $p->id_productos }}',
                                                                                        '{{ $p->codigo_productos }}', 
                                                                                        '{{ $p->nombre_productos }}',
                                                                                        '{{ $p->tipo_producto }}',
                                                                                        '{{ $p->pk_categorias_productos }}',
                                                                                        '{{ $p->pk_subcategorias_productos }}', 
                                                                                        '{{ $p->pk_marcas_productos }}',
                                                                                        '{{ $p->lanzamientos_productos }}', 
                                                                                        '{{ $p->descuentos_productos }}', 
                                                                                        '{{ $p->precio_descuento_productos }}', 
                                                                                        '{{ $p->mayorista }}', 
                                                                                        '{{ $p->minorista }}', 
                                                                                        '{{ $p->descripcion_productos }}', 
                                                                                        '{{ $p->premium }}', 
                                                                                        '{{ $p->clasicos }}')"
                                                            style="text-align: center;" data-toggle="modal"
                                                            data-target="#modal_modificar_productos">
                                                            <i class="fa fa-edit" style="font-size: 25px;"></i><br>Editar
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        </tbody>

                                    </table>

                                    <div class="col-12 paginate_productos" data-table="namopua"
                                        style="padding: 0; position: relative;">
                                        {{ $namopuaproductoFull->links() }}
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="productos-olimpia" role="tabpanel" aria-labelledby="olimpia-tab">

                            <div class="card">
                                <div class="card-body tabla_productos" id="olimpia-table" style="padding: 0;">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Cod.</th>
                                                <th>Producto</th>
                                                <th>Tipo</th>
                                                <th>Marca</th>
                                                <th>Lanzamiento</th>
                                                <th>Premium</th>
                                                <th>Descuento</th>
                                                <th>Precio Desc.</th>
                                                <th>Mayorista</th>
                                                <th>Minorista</th>
                                                <th>Color</th>
                                                <th>Tamaño</th>
                                                <th>Imagenes</th>
                                                <th>Disponible</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody class="table-productos" data-table="olimpia">
                                            @foreach ($olimpiaproductoFull as $p)
                                                <tr>
                                                    <td>{{ $p->id_productos }}</td>
                                                    <td>{{ $p->codigo_productos }}</td>
                                                    <td>{{ $p->nombre_productos }}</td>
                                                    <td>{{ $p->tipo_producto }}</td>
                                                    <td>{{ $p->nombre_marcas }}</td>
                                                    <td>
                                                        @if ($p->lanzamientos_productos == 1)
                                                            Si
                                                        @else
                                                            No
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($p->premium == 1)
                                                            Si
                                                        @else
                                                            No
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($p->descuentos_productos == 1)
                                                            Si
                                                        @else
                                                            No
                                                        @endif
                                                    </td>
                                                    <td>{{ $p->precio_descuento_productos }}</td>
                                                    <td>{{ $p->mayorista }}</td>
                                                    <td>{{ $p->minorista }}</td>
                                                    <td>
                                                        @foreach ($colorProductos as $cp)
                                                            @if ($cp->pk_productos_color_productos == $p->id_productos)
                                                                <a href="{{ route('productos.deleteColor', $cp->id_color_productos) }}"
                                                                    style="width: 100%;">
                                                                    <div class="col-sm-12"
                                                                        style="padding: 0; background: {{ $cp->prefijo_color }}; text-align: center; color: #fff; margin-bottom: 2px;">
                                                                        {{ $cp->nombre_color }}
                                                                    </div>
                                                                </a>
                                                            @endif
                                                        @endforeach
                                                        <a href="#"
                                                            onclick="abrirModalColor('{{ $p->id_productos }}', '{{ $p->codigo_productos }}', 'olimpia-table')"
                                                            data-toggle="modal" data-target="#modal-agregar-color"
                                                            title="Agregar Imagenes">
                                                            <div id="add-box" style=" padding: 2px;float: left;">
                                                                +
                                                            </div>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        {{-- @foreach ($tallesProductos as $tp)
                                                            @if ($tp->pk_productos_talles_productos == $p->id_productos)
                                                                <div class="col-sm-4"
                                                                    style="text-align: center; padding: 5px 0 0; float: left; border: 1px solid #e5e5e5;">
                                                                    <a href="{{ route('productos.deleteTalles', $tp->id_talles_productos) }}"
                                                                        style=" overflow: hidden;">
                                                                        {{ $tp->nombre_talles }}
                                                                    </a>
                                                                </div>
                                                            @endif
                                                        @endforeach --}}
                                                        {{-- <div class="col-sm-4"
                                                            style="text-align: center; padding: 5px 0; float: left; border: 1px solid #e5e5e5;">
                                                            <a href="#"
                                                                onclick="abrirModalTalle('{{ $p->id_productos }}', '{{ $p->codigo_productos }}', 'olimpia-table')"
                                                                data-toggle="modal" data-target="#modal-agregar-talles"
                                                                title="Agregar Imagenes">
                                                                <div id="add-box" style="padding: 0px;">
                                                                    +
                                                                </div>
                                                            </a>
                                                        </div> --}}

                                                        <div class="col-sm-4"
                                                            style="text-align: center; padding: 5px 0; float: left; border: 1px solid #e5e5e5;">
                                                            <a href="#"
                                                                onclick="abrirModalTalleColor('{{ $p->id_productos }}', '{{ $p->codigo_productos }}', 'olimpia-table')"
                                                                {{-- data-toggle="modal" data-target="#modal-agregar-talles" --}} title="Agregar Imagenes">
                                                                <div id="add-box" style="padding: 0px;">
                                                                    +
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="#"
                                                            onclick="abrirModalImagen('{{ $p->id_productos }}', '{{ $p->codigo_productos }}', 'olimpia-table')"
                                                            data-toggle="modal" data-target="#modal-agregar-imagenes"
                                                            title="Agregar Imagenes">
                                                            <div id="add-box"
                                                                style="width: 50px; height: 50px; padding: 2px; border: 1px solid #e5e5e5; background: #e5e5e5; float: left;">
                                                                <div class="col-sm-12"
                                                                    style="border: 2px dotted #d1d1d1; text-align: center; display: flex; justify-content: center; align-content: center; flex-direction: column;">
                                                                    <i class="fa fa-search"></i>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </td>

                                                    <td>
                                                        @if ($p->activo_productos == true)
                                                            <a href="#"
                                                                onclick="modificarExistencia('{{ $p->id_productos }}', 
                                                                                            '{{ $p->codigo_productos }}',
                                                                                            '{{ $p->nombre_productos }}' , 
                                                                                            '{{ $p->activo_productos }}')"
                                                                style="text-align: center;" data-toggle="modal"
                                                                data-target="#modal_modificar_existencia">
                                                                <i class="fa fa-thumbs-up"></i> Existente
                                                            </a>
                                                        @else
                                                            <a href="#"
                                                                onclick="modificarExistencia('{{ $p->id_productos }}', 
                                                                                            '{{ $p->codigo_productos }}',
                                                                                            '{{ $p->nombre_productos }}' , 
                                                                                            '{{ $p->activo_productos }}')"
                                                                style="text-align: center;" data-toggle="modal"
                                                                data-target="#modal_modificar_existencia">
                                                                <i class="fa fa-thumbs-down"></i><br> Sin stock
                                                            </a>
                                                        @endif
                                                    </td>

                                                    <td>
                                                        <a href="#"
                                                            onclick="modificarProductos( '{{ $p->id_productos }}',
                                                                                        '{{ $p->codigo_productos }}', 
                                                                                        '{{ $p->nombre_productos }}',
                                                                                        '{{ $p->tipo_producto }}',
                                                                                        '{{ $p->pk_categorias_productos }}',
                                                                                        '{{ $p->pk_subcategorias_productos }}', 
                                                                                        '{{ $p->pk_marcas_productos }}',
                                                                                        '{{ $p->lanzamientos_productos }}', 
                                                                                        '{{ $p->descuentos_productos }}', 
                                                                                        '{{ $p->precio_descuento_productos }}', 
                                                                                        '{{ $p->mayorista }}', 
                                                                                        '{{ $p->minorista }}', 
                                                                                        '{{ $p->descripcion_productos }}', 
                                                                                        '{{ $p->premium }}', 
                                                                                        '{{ $p->clasicos }}')"
                                                            style="text-align: center;" data-toggle="modal"
                                                            data-target="#modal_modificar_productos">
                                                            <i class="fa fa-edit"></i> Editar
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        </tbody>

                                    </table>

                                    <div class="col-12 paginate_productos" data-table="olimpia"
                                        style="padding: 0; position: relative;">
                                        {{ $olimpiaproductoFull->links() }}
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <style>
        .paginate_productos nav {
            position: absolute;
            right: 0;
        }

        #add-box {
            text-align: center;
            display: flex;
            justify-content: center;
            align-content: center;
            flex-direction: column;
        }

        #file-preview-zone img {
            height: 100%;
        }

        #modal_modificar_productos label {
            margin-bottom: 0;
            margin-top: 10px;
        }
    </style>
    <div class="modal fade" id="modal_modificar_existencia">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <input type="hidden" class="form-control" id="id_existente_producto">
                    <span>Código: <span id="codigo_mod_existencia"></span> | <span
                            id="nombre_mod_existencia"></span></span>
                    <select class="form-control" id="select_existente" style="margin: 10px 0;">
                        <option value="">Dar de baja</option>
                        <option value="1">Existente</option>
                    </select>
                    <button onclick="modificarProductoExistente()" class="btn btn-primary">Modificar</button>
                </div>
            </div>
        </div>
    </div>


    <!-- modificar producto -->
    <div class="modal fade" id="modal_modificar_productos">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 style="margin-bottom: 0;">
                        Código: <span id="modal_nombre_productos"></span>
                    </h6>
                </div>

                <div class="modal-body">
                    <p>Cargar Colores del Producto</p>
                    @csrf
                    <input id="modal_id_productos" type="hidden" class="form-control form-control-sm">
                    <label>Nombre Producto</label>
                    <input id="modal_nombre_productos_input" type="text" class="form-control form-control-sm">
                    <label>Código</label>
                    <input id="modal_codigo_productos" type="text" class="form-control form-control-sm">
                    <label>Tipo</label>
                    <select id="modal_tipo_productos" class="form-control form-control-sm">
                        <option value="Dama">Damas</option>
                        <option value="Hombre">Caballeros</option>
                        <option value="Niña">Niñas</option>
                        <option value="Niño">Niños</option>
                        <option value="EspecialesHombre">Especial Hombre</option>
                        <option value="EspecialMujer">Especial Mujer</option>
                    </select>
                    <label>Categoria</label>
                    <select id="modal_id_categorias" class="form-control form-control-sm">
                        @foreach ($categoria as $Mcat)
                            <option value="{{ $Mcat->id_categorias }}">{{ $Mcat->nombre_categorias }}</option>
                        @endforeach
                    </select>
                    <label>Subcategoria</label>
                    <select id="modal_id_subcategorias" class="form-control form-control-sm">
                        @foreach ($subcategoria as $Msubcate)
                            <option value="{{ $Msubcate->id_subcategorias }}">{{ $Msubcate->nombre_subcategorias }}
                            </option>
                        @endforeach
                    </select>
                    <label>Marca</label>
                    <select id="modal_id_marcas" class="form-control form-control-sm">
                        @foreach ($marcas as $Mma)
                            <option value="{{ $Mma->id_marcas }}">{{ $Mma->nombre_marcas }}</option>
                        @endforeach
                    </select>
                    <label>Clasicos</label>
                    <select id="modal_clasicos" class="form-control form-control-sm">
                        <option value="1">Si</option>
                        <option value="0">No</option>
                    </select>
                    <label>Lanzamiento</label>
                    <select id="modal_lanzamientos" class="form-control form-control-sm">
                        <option value="1">Si</option>
                        <option value="0">No</option>
                    </select>
                    <label>Descuento</label>
                    <select id="modal_descuentos" class="form-control form-control-sm">
                        <option value="1">Si</option>
                        <option value="0">No</option>
                    </select>
                    <label>Premium</label>
                    <select id="modal_premium" class="form-control form-control-sm">
                        <option value="1">Si</option>
                        <option value="0">No</option>
                    </select>
                    <label>Precio Descuento</label>
                    <input id="modal_precio_descuentos" type="number" class="form-control form-control-sm">
                    <label>Precio Mayorista</label>
                    <input id="modal_precio_mayorista" type="number" class="form-control form-control-sm">
                    <label>Precio Minorista</label>
                    <input id="modal_precio_minoristas" type="number" class="form-control form-control-sm">
                    <label>Descripción</label>
                    <input id="modal_descripcion" type="text" class="form-control form-control-sm">
                    <button class="btn btn-primary" onclick="modificar_productos()"
                        style="margin-top: 20px;">Modificar</button>

                </div>

            </div>
        </div>
    </div>

    <!-- modal color -->

    <div class="modal fade" id="modal-agregar-color">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 style="margin-bottom: 0;">
                        Cargar Colores del Producto || Código: <span id="titulo_color"></span>
                    </h6>
                </div>

                <div class="modal-body">
                    {{-- <form method="post" action="{{route('productos.color')}}">
            @csrf
            <select name="pk_color_productos" class="form-control form-control-sm">
              @foreach ($color as $col)
              <option value="{{$col->id_color}}" style="background: {{$col->prefijo_color}}">{{$col->nombre_color}} <span>{{$col->prefijo_color}}</span></option>
              @endforeach
            </select>
            

            <button type="submit" class="btn btn-primary" style="margin-top: 20px;">Agregar</button>
          </form> --}}


                    <input type="hidden" name="pk_productos_color_productos" id="pk_productos_color_productos"
                        class="form-control form-control-sm" />
                    <input type="text" class="target-table" hidden>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input todos_color" id="todos_color"
                            onclick="todos_color()">
                        <label class="form-check-label" for="todos_color">Todos</label>
                    </div>

                    @foreach ($color as $col)
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input color_select" name="color_select"
                                id="color_select" value="{{ $col->id_color }}">
                            <span style="width: 50px; height: 15px; background: {{ $col->prefijo_color }}"></span>
                            <label class="form-check-label" for="color_select">{{ $col->nombre_color }}</label>
                        </div>
                    @endforeach

                </div>

                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button id="btnEnviarColor" onclick="agregarColor()" data-dismiss="modal"
                        class="btn btn-primary">Agregar</button>
                </div>

            </div>
        </div>
    </div>


    <!-- modal talles -->

    {{-- <div class="modal fade" id="modal-agregar-talles">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 style="margin-bottom: 0;">
                        Cargar Tallas del Producto || Código: <span id="titulo_talle"></span>
                    </h6>
                </div>

                <div class="modal-body">

                    <input type="hidden" id="pk_productos_talles_productos" name="pk_productos_talles_productos"
                        class="form-control form-control-sm" />
                    <input type="text" class="target-table" hidden>

                    <div class="col-12" style="padding: 0;">
                        <div class="row">
                            <div class="col-4">
                                <span style="margin-bottom: 10px;">Niños & Niñas</span>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input todos_ninos" id="todos_ninos"
                                        onclick="todos_ninos()">
                                    <label class="form-check-label" for="todos_ninos">Todos</label>
                                </div>
                                @foreach ($talles as $tal)
                                    @if ($tal->nombre_talles < 18 && $tal->nombre_talles > 0)
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input talles_ninos" name="talles_ninos"
                                                id="talles_ninos" value="{{ $tal->id_talles }}">
                                            <label class="form-check-label"
                                                for="talles_ninos">{{ $tal->nombre_talles }}</label>
                                        </div>
                                    @endif
                                @endforeach
                            </div>

                            <div class="col-4">
                                <span style="margin-bottom: 10px;">Adultos</span>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input todos_adultos" id="todos_adultos"
                                        onclick="todos_adultos()">
                                    <label class="form-check-label" for="todos_adultos">Todos</label>
                                </div>
                                @foreach ($talles as $tal)
                                    @if (($tal->nombre_talles > 18 && $tal->nombre_talles < 47) || $tal->nombre_talles == 'P' || $tal->nombre_talles == 'M' || $tal->nombre_talles == 'L' || $tal->nombre_talles == 'G' || $tal->nombre_talles == 'XG' || $tal->nombre_talles == 'XXG')
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input talles_adultos"
                                                name="talles_adultos" value="{{ $tal->id_talles }}">
                                            <label class="form-check-label"
                                                for="talles_adultos">{{ $tal->nombre_talles }}</label>
                                        </div>
                                    @endif
                                @endforeach


                            </div>

                            <div class="col-4">
                                <span style="margin-bottom: 10px;">Tallas Especiales</span>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input todos_especiales" id="todos_especiales"
                                        onclick="todos_especiales()">
                                    <label class="form-check-label" for="todos_especiales">Todos</label>
                                </div>
                                @foreach ($talles as $tal)
                                    @if ($tal->nombre_talles > 47)
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input talles_especiales"
                                                name="talles_especiales" value="{{ $tal->id_talles }}">
                                            <label class="form-check-label"
                                                for="talles_especiales">{{ $tal->nombre_talles }}</label>
                                        </div>
                                    @endif
                                @endforeach
                            </div>

                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button id="btnEnviar" onclick="agregarTalla()" data-dismiss="modal"
                        class="btn btn-primary">Agregar</button>
                </div>

            </div>
        </div>
    </div> --}}

    {{-- Modal de color - talla --}}
    <div class="modal fade" id="color-talla-modal" tabindex="-1" role="dialog"
        aria-labelledby="color-talla-modalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 700px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Color y talla: <span
                            class="codigo_productos"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" id="id_producto_talla_color" class="form-control" hidden>
                    <div class="form-row">
                        <div class="col">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <select name="" id="colores_modal_colores_tallas" class="form-control">
                                            @foreach ($color as $co)
                                                <option value="{{ $co->id_color }}" data-id-color="{{ $co->id_color }}">
                                                    {{ $co->id_color }} -
                                                    {{ $co->nombre_color }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row" id="lista_tallas_colores"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col">

                            <div class="row">
                                <div class="col-4">
                                    <span style="margin-bottom: 10px;">Niños & Niñas</span>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input todos_ninos" id="todos_ninos"
                                            onclick="todos_ninos()">
                                        <label class="form-check-label" for="todos_ninos">Todos</label>
                                    </div>
                                    @foreach ($talles as $tal)
                                        @if ($tal->nombre_talles < 18 && $tal->nombre_talles > 0)
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input talles_ninos"
                                                    name="talles_ninos" id="talles_ninos"
                                                    value="{{ $tal->id_talles }}">
                                                <label class="form-check-label"
                                                    for="talles_ninos">{{ $tal->nombre_talles }}</label>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                                <div class="col-4">
                                    <span style="margin-bottom: 10px;">Adultos</span>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input todos_adultos" id="todos_adultos"
                                            onclick="todos_adultos()">
                                        <label class="form-check-label" for="todos_adultos">Todos</label>
                                    </div>
                                    @foreach ($talles as $tal)
                                        @if (
                                            ($tal->nombre_talles > 18 && $tal->nombre_talles < 47) ||
                                                $tal->nombre_talles == 'P' ||
                                                $tal->nombre_talles == 'M' ||
                                                $tal->nombre_talles == 'L' ||
                                                $tal->nombre_talles == 'G' ||
                                                $tal->nombre_talles == 'XG' ||
                                                $tal->nombre_talles == 'XXG' ||
                                                str_contains($tal->nombre_talles, 'XG') ||
                                                str_contains($tal->nombre_talles, 'G') ||
                                                str_contains($tal->nombre_talles, 'X'))
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input talles_adultos"
                                                    name="talles_adultos" value="{{ $tal->id_talles }}">
                                                <label class="form-check-label"
                                                    for="talles_adultos">{{ $tal->nombre_talles }}</label>
                                            </div>
                                        @endif
                                    @endforeach


                                </div>

                                <div class="col-4">
                                    <span style="margin-bottom: 10px;">Tallas Especiales</span>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input todos_especiales"
                                            id="todos_especiales" onclick="todos_especiales()">
                                        <label class="form-check-label" for="todos_especiales">Todos</label>
                                    </div>
                                    @foreach ($talles as $tal)
                                        @if ($tal->nombre_talles > 47)
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input talles_especiales"
                                                    name="talles_especiales" value="{{ $tal->id_talles }}">
                                                <label class="form-check-label"
                                                    for="talles_especiales">{{ $tal->nombre_talles }}</label>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="agregarColorTalla()">Save changes</button>
                </div>
            </div>
        </div>
    </div>



    <!--modal imagenes -->
    <div class="modal fade" id="modal-agregar-imagenes">
        <div class="modal-dialog modal-xl" style="max-width: 95%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 style="margin-bottom: 0;">
                        Cargar Imagen del Producto || Código: <span id="modal_codigo_producto_imagen"></span>
                    </h6>
                </div>
                <!-- <form method="post" action="{{ route('productos.imagenes') }}" enctype="multipart/form-data"> -->
                <form method="post" id="formulario" action="" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body" style="overflow: hidden;">
                        <div class="row">
                            <div class="col-9">
                                <div class="row">
                                    <div class="col-6 mb-3">
                                        <div class="col-12 p-3"
                                            style="height: 145px; border: 1px solid #e5e5e5; background: #f5f5f5;">
                                            <p class="m-0">Cargar imagenes en la página web</p>
                                            <p class="m-0" style="color: #808080;">1. Seleccionar la imagen</p>
                                            <p class="m-0" style="color: #808080;">2. Elegir si es imagen principal</p>
                                            <p class="m-0" style="color: #808080;">3. Ordenar para mostrar en la página
                                                web</p>
                                        </div>
                                    </div>
                                    <div class="col-6 mb-3">
                                        <div class="col-12 p-3" style="border: 1px solid #e5e5e5; background: #f5f5f5;">
                                            <p class="m-0">Ordenar imagenes</p>
                                            <p class="m-0" style="color: #808080;">1. Arrastrar y soltar</p>
                                            <p class="mb-0 mt-3">Habilitar inhabilitar</p>
                                            <p class="m-0" style="color: #808080;">1. Click en el icono
                                                <svg class="activoColor" fill="#808080" version="1.1" id="Capa_1"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                    y="0px" width="17px" height="17px"
                                                    viewBox="0 0 301.153 301.153"
                                                    style="enable-background:new 0 0 301.153 301.153;"
                                                    xml:space="preserve">
                                                    <g>
                                                        <path
                                                            d="M257.098,44.055c-58.738-58.736-154.311-58.742-213.047,0c-58.733,58.738-58.727,154.319,0,213.047
                                                    c58.742,58.739,154.314,58.733,213.047,0C315.831,198.362,315.837,102.793,257.098,44.055z M148.952,189.476
                                                    c-2.402,2.402-6.29,2.402-8.695,0l-50.008-50.005c-1.186-1.198-1.79-2.771-1.801-4.348c0-1.573,0.604-3.146,1.801-4.348
                                                    c2.405-2.401,6.29-2.401,8.695,0l45.648,45.657l66.305-66.299c2.414-2.411,6.3-2.401,8.707,0c2.402,2.405,2.402,6.29,0,8.695
                                                    L148.952,189.476z" />
                                                    </g>
                                                </svg>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <input type="hidden" name="pk_productos_imagen_productos"
                                            id="pk_productos_imagen_productos" class="form-control form-control-sm" />
                                        <input type="text" class="target-table" hidden>
                                        <div class="col-12 p-0">
                                            <p style="margin-bottom: 0;">
                                                Elegir color
                                            </p>
                                            <select id="color_imagen" name="color_imagen"
                                                class="form-control form-control-sm" style="margin-bottom: 20px;">
                                                <option disabled selected>Seleccione..</option>
                                                @foreach ($color as $co)
                                                    <option value="{{ $co->id_color }}">{{ $co->nombre_color }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6" style="padding-top: 24px;">
                                        <div id="divFileUpload" style="margin-bottom: 20px;">
                                            <input id="file" name="file" type="file" accept="image/*" />
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <p style="margin-bottom: 0;">
                                            Imagen principal
                                        </p>
                                        <select name="principal_imagen_productos" class="form-control form-control-sm">
                                            <option value="0">No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                    <div class="col-6">
                                        <p style="margin-bottom: 0;">
                                            Orden para mostrar en E-commerce
                                        </p>
                                        <select name="seccion_imagen_productos" class="form-control form-control-sm">
                                            <option value="1">Active</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="col-3">
                                <p class="m-0 bg-secondary text-white"
                                    style="border: 1px solid #e5e5e5; padding-left: 10px;">Imagen Previa</p>
                                <div id="file-preview-zone"
                                    style="width: 100%; height: 266px; border: 1px solid #e5e5e5;">

                                </div>
                            </div>
                        </div>


                        <div class="col-12 mt-3 p-0">
                            <div class="col-12" style="padding: 0;">
                                <span>Visualización de imágenes</span>
                            </div>
                            <div class="col-12 p-0 mt-1" id="modificarImagenesModal">
                                {{-- <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width: 100px;">Imagenes</th>
                                            <th>Principal</th>
                                        </tr>
                                    </thead>
                                    <tbody id="modificarImagenesModal">

                                    </tbody>
                                </table> --}}
                            </div>
                            <div class="col-12" id="visualizacion_imagenes" style="padding: 0;">

                            </div>
                        </div>
                    </div>

                    <div class="modal-footer" style="overflow: hidden;">
                        <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Agregar</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <style>
        .sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
            width: 100%;
        }

        .sortable li {
            margin: 0 3px 3px 3px;
            padding: 10px;
            padding-left: 1.5em;
        }

        .default {
            background: #fff;
            padding: 10px !important;
            border-bottom: 1px solid #e5e5e5;
            color: #333333;
            list-style: none;
        }

        .highlight {
            height: 170px;
            font-weight: bold;
            font-size: 45px;
            background-color: #333333;
        }
    </style>


    {{-- <script src="//code.jquery.com/jquery-1.11.0.min.js"></script> --}}
    <script src="{{ asset('js/jquery.js') }}"></script>

    <script>
        const talles = {!! json_encode($talles) !!}

        function abrirModalImagen(id, codigo, target) {
            $('#modal_codigo_producto_imagen').html(codigo);
            $('#pk_productos_imagen_productos').val(id);
            $('.target-table').val(target);
            var imagen = "";
            var tabla = '';
            $.ajax({
                type: "ajax",
                url: "{{ route('productos.buscarImagen') }}",
                type: 'GET',
                data: {
                    id: id
                },

                success: function(data) {

                    $('#modificarImagenesModal').empty().html(data);
                }
            });
        }

        function borrarImagenModal(id_imagen) {
            var imagen = "";
            var id = $('#pk_productos_imagen_productos').val();
            var codigo = $('#modal_codigo_producto_imagen').text();
            var target = 'namopua-table';
            $.ajax({
                type: "ajax",
                url: "{{ route('productos.borrarImagen') }}",
                type: 'GET',
                data: {
                    id_imagen: id_imagen
                },
                success: function(data) {
                    abrirModalImagen(id, codigo, target);
                }
            });
        }

        function buscarProductos() {
            var codigo = $('#buscador_productos').val();

            $.ajax({
                url: "{{ route('productos.tablaProductos') }}",
                data: {
                    codigo: codigo,
                    table: 'olimpia-table'
                },

                success: function(data) {

                    $('#olimpia-table').html(data);
                }
            });
            $.ajax({
                url: "{{ route('productos.tablaProductos') }}",
                data: {
                    codigo: codigo,
                    table: 'namopua-table'
                },

                success: function(data) {

                    $('#namopua-table').html(data);
                }
            });
        }

        $(document).ready(function() {

            $(document).on('change', "#colores_modal_colores_tallas", function() {
                let optionSelected = $("#colores_modal_colores_tallas option:selected");
                var tamanos = optionSelected.data('tamanos');
                var color = optionSelected.attr('value')
                console.log(color)
                console.log(tamanos)
                if (tamanos != null || color != null) {
                    if (tamanos != "") {
                        renderTallas(JSON.parse(tamanos.replace(/'/g, '"')), color, $(
                            "#id_producto_talla_color").val())
                    } else {
                        $("#lista_tallas_colores").html("")
                    }
                }
            })
            $(document).on('click', '#namopua-table .paginate_productos .pagination a', function(event) {
                event.preventDefault();
                //alert('ok');
                var page = $(this).attr('href').split('page=')[1];
                paginateTable(page, "namopua-table")
            });
            $(document).on('click', '#olimpia-table .paginate_productos .pagination a', function(event) {
                event.preventDefault();
                //alert('ok');
                var page = $(this).attr('href').split('page=')[1];
                paginateTable(page, "olimpia-table")
            });
            //funcion para paginar tabla dinamicamente
            function paginateTable(page, target) {
                var codigo = $('#buscador_productos').val();
                $.ajax({
                    url: "{{ route('productos.tablaProductos') }}?page=" + page,
                    data: {
                        codigo: codigo,
                        table: target
                    },
                    success: function(data) {
                        $("#" + target).html(data);
                    }
                });
            }

            $('#formulario').submit(function(e) {
                e.preventDefault();
                var formData = new FormData($('#formulario')[0]);
                var imagen = "";
                var id = $('#pk_productos_imagen_productos').val();
                var codigo = $('#modal_codigo_producto_imagen').text();
                var target = 'namopua-table';
                $.ajax({
                    url: "{{ route('productos.imagenes') }}",
                    type: 'POST',
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(data) {
                        abrirModalImagen(id, codigo, target);

                        $('#file-preview-zone').empty();
                        $('#formulario').trigger("reset");
                    }

                });

            });

        });

        $(document).on('click', '.delete-talle', function() {
            let id_producto = $(this).data('producto')
            let id_color = $(this).data('color')
            let talle = $(this).data('talle')
            $.ajax({
                url: '{{ route('productos.deleteTalles') }}',
                data: {
                    _token: "{{ csrf_token() }}",
                    id_producto: id_producto,
                    id_color: id_color,
                    talle: talle
                },
                success: function(resp) {
                    console.log(resp)
                    $("#colores_modal_colores_tallas option:selected").data('tamanos', JSON.stringify(
                        resp).replace(/'/g, '"'));
                    renderTallas(resp, id_color, id_producto)
                }
            })
        })

        function abrirModalTalleColor(id, cod, target) {
            $("#id_producto_talla_color").val(id)
            $(".codigo_productos").html(cod)
            $("#color-talla-modal").modal('toggle')
            $.ajax({
                url: "{{ route('productos.getColoresTallas') }}",
                data: {
                    id_producto: id,
                    _token: '{{ csrf_token() }}'
                },
                success: function(resp) {
                    if (resp.length > 0) {
                        if (resp[0].tamanos != null) {
                            console.log(resp[0])
                            renderTallas(JSON.parse(resp[0].tamanos), resp[0].id_color_productos, id)
                        }
                        var colores = "";
                        resp.forEach(color => {
                            colores += `<option value="${color.id_color}" data-tamanos="${(color.tamanos != null ? color.tamanos.replace(/"/g, "'") : "")}">${color.id_color } -
                                            ${color.nombre_color}</option>`
                        });
                        $("#colores_modal_colores_tallas option").hide()
                        $("#colores_modal_colores_tallas").html(colores)
                    } else {
                        $("#colores_modal_colores_tallas").val('')
                        $("#colores_modal_colores_tallas option").hide()
                        $("#colores_modal_colores_tallas").change()
                        $("#lista_tallas_colores").html('')
                    }
                }
            })
        }

        function renderTallas(tamanos, color, producto) {
            var listaTamanos = "";
            $("#lista_tallas_colores").html("")
            setTimeout(() => {
                tamanos.forEach(function(tamano, index) {
                    let indexTalla = talles.findIndex(talle => talle.id_talles == tamano);
                    if (indexTalla != -1) {
                        listaTamanos +=
                            `<div class="delete-talle" data-color="${color}" data-producto="${producto}" data-talle="${talles[indexTalla].id_talles}">${talles[indexTalla].nombre_talles}</div>`
                    }
                })
                $("#lista_tallas_colores").html(listaTamanos)
            }, 100);
        }

        function todos_ninos() {
            if ($('.todos_ninos').is(':checked')) {
                $('.talles_ninos').prop("checked", true);
            } else {
                $('.talles_ninos').prop("checked", false);
            }
        }

        function todos_adultos() {
            if ($('.todos_adultos').is(':checked')) {
                $('.talles_adultos').prop("checked", true);
            } else {
                $('.talles_adultos').prop("checked", false);
            }
        }

        function todos_especiales() {
            if ($('.todos_especiales').is(':checked')) {
                $('.talles_especiales').prop("checked", true);
            } else {
                $('.talles_especiales').prop("checked", false);
            }
        }

        function agregarColorTalla() {
            var nino = document.querySelectorAll('input[name=talles_ninos]:checked');
            var adulto = document.querySelectorAll('input[name=talles_adultos]:checked');
            var especiales = document.querySelectorAll('input[name=talles_especiales]:checked');
            var tallas_nino = [];
            var tallas_adulto = [];
            var tallas_espacial = [];
            nino.forEach(function(elem) {
                tallas_nino.push(elem.value);
            });
            adulto.forEach(function(elem1) {
                tallas_adulto.push(elem1.value);
            });
            especiales.forEach(function(elem2) {
                tallas_espacial.push(elem2.value);
            });
            var id_producto = $('#id_producto_talla_color').val();
            var codigo = $('#buscador_productos').val();
            var id_color = $("#colores_modal_colores_tallas").val()
            $.ajax({
                type: "ajax",
                url: "{{ route('productos.agregarTalla') }}",
                type: 'POST',
                data: {
                    id_producto: id_producto,
                    tallas_nino: tallas_nino,
                    tallas_adulto: tallas_adulto,
                    tallas_espacial: tallas_espacial,
                    id_color: id_color,
                    _token: '{{ csrf_token() }}'
                },

                success: function(resp) {
                    console.log(resp)
                    $("#colores_modal_colores_tallas option[value=" + id_color + "]").data('tamanos', JSON
                        .stringify(resp.tallas).replace(/"/g, "'"))
                    $("#colores_modal_colores_tallas").change()
                    renderTallas(resp.tallas, id_color, id_producto)
                    // $.ajax({
                    //     url: "{{ route('productos.tablaProductos') }}",
                    //     data: {
                    //         codigo: codigo,
                    //         table: $(".target-table").val()
                    //     },

                    //     success: function(data) {

                    //         $('#' + $(".target-table").val()).html(data);
                    //     }
                    // });

                }
            })
        }

        function agregarTalla() {
            var nino = document.querySelectorAll('input[name=talles_ninos]:checked');
            var adulto = document.querySelectorAll('input[name=talles_adultos]:checked');
            var especiales = document.querySelectorAll('input[name=talles_especiales]:checked');
            var tallas_nino = [];
            var tallas_adulto = [];
            var tallas_espacial = [];
            nino.forEach(function(elem) {
                tallas_nino.push(elem.value);
            });
            adulto.forEach(function(elem1) {
                tallas_adulto.push(elem1.value);
            });
            especiales.forEach(function(elem2) {
                tallas_espacial.push(elem2.value);
            });
            var id_producto = $('#pk_productos_talles_productos').val();
            var codigo = $('#buscador_productos').val();
            $.ajax({
                type: "ajax",
                url: "{{ route('productos.agregarTalla') }}",
                type: 'POST',
                data: {
                    id_producto: id_producto,
                    tallas_nino: tallas_nino,
                    tallas_adulto: tallas_adulto,
                    tallas_espacial: tallas_espacial,
                    _token: '{{ csrf_token() }}'
                },

                success: function() {

                    $.ajax({
                        url: "{{ route('productos.tablaProductos') }}",
                        data: {
                            codigo: codigo,
                            table: $(".target-table").val()
                        },

                        success: function(data) {

                            $('#' + $(".target-table").val()).html(data);
                        }
                    });

                }
            })

        }


        function abrirModalColor(id, cod, target) {
            $('#titulo_color').html(cod);
            $('#pk_productos_color_productos').val(id);
            $('.target-table').val(target);
        }

        function todos_color() {
            if ($('.todos_color').is(':checked')) {
                $('.color_select').prop("checked", true);
            } else {
                $('.color_select').prop("checked", false);
            }
        }

        function agregarColor() {
            var color = document.querySelectorAll('input[name=color_select]:checked');
            var total_color = [];
            color.forEach(function(elem) {
                total_color.push(elem.value);
            });
            var id_producto = $('#pk_productos_color_productos').val();
            var codigo = $('#buscador_productos').val();
            $.ajax({
                type: "ajax",
                url: "{{ route('productos.agregarColor') }}",
                type: 'POST',
                data: {
                    id_producto: id_producto,
                    total_color: total_color,
                    _token: '{{ csrf_token() }}'
                },

                success: function() {
                    $.ajax({
                        url: "{{ route('productos.tablaProductos') }}",
                        data: {
                            codigo: codigo,
                            table: $(".target-table").val()
                        },

                        success: function(data) {

                            $("#" + $(".target-table").val()).html(data);
                        }
                    });

                }
            })
        }


        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var filePreview = document.createElement('img');
                    filePreview.id = 'file-preview';
                    //e.target.result contents the base64 data from the image uploaded
                    filePreview.src = e.target.result;
                    var previewZone = document.getElementById('file-preview-zone');
                    previewZone.appendChild(filePreview);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        var fileUpload = document.getElementById('file');
        fileUpload.onchange = function(e) {
            readFile(e.srcElement);
        }

        function agregar_productos() {
            var codigo = document.getElementById("codigo_productos").value;
            var nombre = document.getElementById("nombre_productos").value;
            var tipo = document.getElementById("tipo_producto").value;
            var pk_categorias = document.getElementById("pk_categorias_productos").value;
            var pk_subcategorias = document.getElementById("pk_subcategorias_productos").value;
            var pk_marcas = document.getElementById("pk_marcas_productos").value;
            var clasicos = document.getElementById("clasico_productos").value;
            var lanzamiento = document.getElementById("lanzamientos_productos").value;
            var descuentos = document.getElementById("descuentos_productos").value;
            var precio_descuentos = document.getElementById("precio_descuento_productos").value;
            var mayorista = document.getElementById("mayorista").value;
            var minorista = document.getElementById("minorista").value;
            var descripcion = $("#descripcion_producto").val();
            var premium = $('#premium_productos').val();

            var id_stock = "";
            $.ajax({
                type: "ajax",
                url: "{{ route('productos.store') }}",
                type: 'POST',
                data: {
                    codigo: codigo,
                    nombre: nombre,
                    tipo: tipo,
                    pk_categorias: pk_categorias,
                    pk_subcategorias: pk_subcategorias,
                    pk_marcas: pk_marcas,
                    clasicos: clasicos,
                    lanzamiento: lanzamiento,
                    descuentos: descuentos,
                    precio_descuentos: precio_descuentos,
                    mayorista: mayorista,
                    minorista: minorista,
                    descripcion: descripcion,
                    premium: premium,
                    _token: '{{ csrf_token() }}'
                },

                success: function(resp) {
                    if (resp.codigo != null) {
                        alert('el código ya existe')
                    } else {
                        location.reload();
                    }

                }
            })
        };

        function modificarProductos(id, cod, nombre, tipo, cate, subcate, marca, lanza, desc, precio_desc, mayorista,
            minorista, descripcion, premium, clasicos) {

            if (lanza == 1) {
                var lanza_mod = 1;
            } else {
                var lanza_mod = 0;
            }
            if (desc == 1) {
                var desc_mod = 1;
            } else {
                var desc_mod = 0;
            }
            if (premium == 1) {
                var premium_mod = 1;
            } else {
                var premium_mod = 0;
            }

            if (clasicos == '1') {
                var clasicos_mod = 1;
            } else {
                var clasicos_mod = 0;
            }

            console.log(clasicos);

            $('#modal_nombre_productos').html(cod);
            $('#modal_id_productos').val(id);
            $('#modal_nombre_productos_input').val(nombre);
            $('#modal_codigo_productos').val(cod);
            $('#modal_tipo_productos').val(tipo);
            $('#modal_id_categorias').val(cate);
            $('#modal_id_subcategorias').val(subcate);
            $('#modal_id_marcas').val(marca);
            $('#modal_clasicos').val(clasicos_mod);
            $('#modal_lanzamientos').val(lanza_mod);
            $('#modal_descuentos').val(desc_mod);
            $('#modal_precio_descuentos').val(precio_desc);
            $('#modal_precio_mayorista').val(mayorista);
            $('#modal_precio_minoristas').val(minorista);
            $('#modal_descripcion').val(descripcion);
            $('#modal_premium').val(premium_mod);


        };

        function modificar_productos() {
            var id = $('#modal_id_productos').val();
            var nombre = $('#modal_nombre_productos_input').val();
            var codigo = $('#modal_codigo_productos').val();
            var tipo = $('#modal_tipo_productos').val();
            var cate = $('#modal_id_categorias').val();
            var subcate = $('#modal_id_subcategorias').val();
            var marca = $('#modal_id_marcas').val();
            var clasicos = $('#modal_clasicos').val();
            var lanza = $('#modal_lanzamientos').val();
            var desc = $('#modal_descuentos').val();
            var precio_desc = $('#modal_precio_descuentos').val();
            var mayorista = $('#modal_precio_mayorista').val();
            var minorista = $('#modal_precio_minoristas').val();
            var descripcion = $('#modal_descripcion').val();
            var premium = $('#modal_premium').val();

            $.ajax({
                type: "ajax",
                url: "{{ route('productos.modificar') }}",
                type: 'POST',
                data: {
                    id: id,
                    codigo: codigo,
                    nombre: nombre,
                    tipo: tipo,
                    cate: cate,
                    subcate: subcate,
                    marca: marca,
                    clasicos: clasicos,
                    lanza: lanza,
                    desc: desc,
                    precio_desc: precio_desc,
                    mayorista: mayorista,
                    minorista: minorista,
                    descripcion: descripcion,
                    premium: premium,
                    _token: '{{ csrf_token() }}'
                },

                success: function() {

                    location.reload();

                }
            })

        };

        function modificarExistencia(id, codigo, nombre, active) {
            $('#id_existente_producto').val(id);
            $('#codigo_mod_existencia').html(codigo);
            $('#nombre_mod_existencia').html(nombre);
            $('#select_existente').val(active);
        };

        function modificarProductoExistente() {
            var id = $('#id_existente_producto').val();
            var active = $('#select_existente').val();

            $.ajax({
                type: "ajax",
                url: "{{ route('productos.modificarExistente') }}",
                type: 'POST',
                data: {
                    id: id,
                    active: active,
                    _token: '{{ csrf_token() }}'
                },

                success: function() {

                    location.reload();

                }
            })

        }
    </script>
@endsection
