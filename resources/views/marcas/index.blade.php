@extends('layouts.app')

@section('content')

    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Marca</li>
            </ul>
        </div>
    </div>

    <section>
        <div class="col-sm-12" style="padding: 10px;">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-body" style="padding: 0;">
                                <div class="form-inline">
                                    @csrf
                                    <div class="form-group">
                                        <label for="inlineFormInput" class="sr-only">Agregar marca</label>
                                        <input id="nombre" type="text" placeholder="Ej. Brabus" class="mr-3 form-control">
                                    </div>
                                    <div class="form-group">
                                        <button onclick="agregar_marcas()" class="mr-3 btn btn-primary">Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Activos
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_marcas" id="activas">
                                        @foreach ($marcasActivas as $m)
                                            <tr data-id="{{ $m->id_marcas }}">
                                                <th scope="row">{{ $m->id_marcas }}</th>
                                                <td contenteditable="true" class="column_name"
                                                    data-column_name="nombre_marcas" data-id="{{ $m->id_marcas }}">
                                                    {{ $m->nombre_marcas }}</td>
                                                <td>
                                                    <i class="fa fa-arrow-right cambiar_estado p-1"
                                                        data-id="{{ $m->id_marcas }}" data-nuevo-estado="false"
                                                        data-column-name="active_marcas"></i>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Inactivos
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_marcas" id="inactivas">
                                        @foreach ($marcasInactivas as $m)
                                            <tr data-id="{{ $m->id_marcas }}">
                                                <th scope="row">{{ $m->id_marcas }}</th>
                                                <td contenteditable="true" class="column_name"
                                                    data-column_name="nombre_marcas" data-id="{{ $m->id_marcas }}">
                                                    {{ $m->nombre_marcas }}</td>
                                                <td>
                                                    <i class="fa fa-arrow-left cambiar_estado p-1"
                                                        data-id="{{ $m->id_marcas }}" data-nuevo-estado="true"
                                                        data-column-name="active_marcas"></i>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        const url_tabla = "/marcas/"

        function agregar_marcas() {
            var nombre = document.getElementById("nombre").value;
            var id_stock = "";
            console.log(nombre);
            $.ajax({
                type: "ajax",
                url: "{{ route('marcas.store') }}",
                type: 'POST',
                data: {
                    nombre: nombre,
                    _token: '{{ csrf_token() }}'
                },

                success: function() {

                    // var categoria = $(this).val();
                    $.get('/tabla_marcas', function(data) {
                        //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
                        console.log(data);
                        for (var i = 0; i < data.length; i++)
                            id_stock += '<tr data-id="'+data[i].id_marcas+'">' +
                            '<td>' + data[i].id_marcas + '</td>' +
                            '<td contenteditable="true" class="column_name_marcas" data-column_name="nombre_marcas" data-id="' +
data[i].id_marcas + '">' + data[i].nombre_marcas + '</td>' +
                            '<td><i class="fa fa-arrow-right cambiar_estado p-1" data-id="'+data[i].id_marcas+'" data-nuevo-estado="false" data-column-name="active_marcas"></i></td>'
                            '</tr>',

                            $("#activas").empty();
                        $('#activas').append(id_stock);
                    })


                }
            })
        };

        //nueva fila para las tablas de estado
        function nuevaFilaEstado(data) {
            let arrow = ""
            let nuevoEstado
            let fila = ""
            if (data.active_marcas) {
                arrow = "right"
                nuevoEstado = "false"
            } else {
                arrow = "left"
                nuevoEstado = "true"
            }
            fila = `
            
            <tr data-id="${data.id_marcas}">
                <th scope="row">${data.id_marcas}</th>
                <td contenteditable="true" class="column_name"
                    data-column_name="nombre_marcas" data-id="${data.id_marcas}">
                    ${data.nombre_marcas}</td>
                <td>
                    <i class="fa fa-arrow-${arrow} cambiar_estado p-1"
                        data-id="${data.id_marcas}" data-nuevo-estado="${nuevoEstado}"
                        data-column-name="active_marcas"></i>
                </td>
            </tr>
            `;
            //se agrega la nueva fila a la tabla activos o inactivos
            if (data.active_marcas) {
                $("#activas").append(fila)
            } else {
                $("#inactivas").append(fila)
            }
        }
    </script>

@endsection
