@php
$fundacion = new DateTime('2001-01-01');
$actual = new DateTime();

$anos_trayectoria = $fundacion->diff($actual)->y;
@endphp
<li class="nav-item dropdown" style="list-style: none; float: left; font-family: 'Roboto', sans-serif;">

    <a style="padding: 0;" id="carrito-icon" rel="nofollow" data-target="#" href="#" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false" class="nav-link carrito-icon dropdown-toggle">
        <img src="{{ asset('img/icon/bag-w.png') }}" style="width: 19px;" />
    </a>

    <div class="carrito-numero"
        style="border-radius: 50px; background: #fff; color: #E5B3BE; position: absolute; top: 8px; right: -20px; padding: 0 6px;">
        @if (session('cart'))
            <span>
                <?php
                $cant = 0;
                ?>
                @foreach (session('cart') as $position => $y)
                    <?php
                    $cant += $y['cantidad'];
                    ?>
                @endforeach
                @if ($cant > 0)
                    {{ $cant }}
                @endif

            </span>

        @else
            <span>0</span>
        @endif
    </div>


    <ul aria-labelledby="carrito-icon" class="dropdown-menu carrito-icon"
        style="overflow: hidden; background: #fff; padding: 0; border-radius: 15px;">

        <div class="col-sm-12"
            style="padding: 15px 15px 15px 15px; border-bottom: 1px solid #e5e5e5; margin-bottom: 10px; position: absolute; top: 0; left: 0; z-index: 30000;">
            @if (session('cart'))
                <p style="color: #101010; margin-bottom: 0;">

                    <?php
                    $cant = 0;
                    ?>
                    @foreach (session('cart') as $position => $y)
                        <?php
                        $cant += $y['cantidad'];
                        ?>
                    @endforeach
                    @if ($cant > 0)
                        Tenés
                        <span id="item-letter" style="color: #1677BB; font-weight: normal">
                            {{ $cant }} prenda(s)
                        </span> en tu carrito

                    @endif
                </p>
            @else
                <p style="color: #101010; margin-bottom: 0; text-align: center;">
                    <span id="item-letter" style="color: #1677BB; font-weight: normal; font-size: 15px;">
                        Tenés 0 prenda(s)
                    </span> en tu carrito
                </p>
            @endif
        </div>
        @if (session('cart'))
            <div class="col-sm-12" style="margin-top: 50px; height: 400px; overflow-y: scroll; padding: 0;">
                <?php $sum = 0; ?>
                @foreach (session('cart') as $position => $y)

                    <li>
                        <div class="col-sm-12"
                            style="padding: 0; border-bottom: 1px solid #e5e5e5; overflow: hidden;">
                            <div style="width:25%; float: left; padding: 10px 0px 10px 20px;">
                                <img src="../img/productos/{{ $y['imagen_producto'] }}" style="width: 100%;" />
                            </div>
                            <div style="width: 55%; float: left; padding: 10px 10px; text-align: left;">
                                <p style="margin-bottom: 0px; color: #101010; display: none;">{{ $y['id_token'] }}</p>
                                <p style="margin-bottom: 0px; color: #101010;">{{ $y['nombre_productos'] }}</p>
                                <p style="margin-bottom: 0px; color: #101010;">Codigo: {{ $y['codigo_productos'] }}</p>
                                <p style="margin-bottom: 0px; color: #101010;">Colores:
                                    {{ implode(', ', $y['colores']) }}
                                </p>
                                <p style="margin-bottom: 0px; color: #101010;">Cantidad: {{ $y['cantidad'] }}</p>
                                @if (isset($y['tallas'][0]))
                                    @if ($y['tallas'][0] != null)
                                        <p style="margin-bottom: 0px; color: #101010;">
                                            Tallas: {{ implode(', ', $y['tallas']) }}
                                        </p>
                                    @endif
                                @endif
                                <p style="margin-bottom: 0px; color: #101010;">Sub total: Gs.
                                    <span
                                        style="font-size: 20px;">{{ number_format($y['total'], 0, ',', '.') }}</span>
                                </p>

                                <p style="margin-bottom: 0px; color: #101010;">

                                </p>
                            </div>
                            <div style="width: 20%; float: left; padding: 10px 20px 0 0; text-align: right;">
                                <a class="trash-carrito" href="#" onclick="eliminarCarrito('{{ $y['id_token'] }}')"
                                    style="padding: 2px 7px; font-size: 20px;">
                                    <img src="{{ asset('img/icon/bin-w.svg') }}" style="width: 20px;" />
                                </a>
                            </div>
                        </div>
                    </li>
                    <?php $sum += $y['total']; ?>
                @endforeach
            </div>
        @else
            <div class="col-sm-12"
                style="margin-top: 80px; margin-bottom: 20px; padding: 0; width: 100%; text-align: center; overflow: hidden;">
                <li style="width: 100%; text-align: center;">
                    <p style="color: #101010;">No tienes productos en el carrito</p>
                    <a id="ir_al_shop" href="/shop"
                        style="color: #1677BB; font-size: 15px; font-family: 'Roboto', sans-serif; border-radius: 30px;">Ir
                        a la tienda</a>
                </li>
            </div>
        @endif
        @if (session('cart'))
            <div style="padding: 0 20px 15px 20px; border-top: 1px solid #e5e5e5;">
                <li style="width: 100%;">
                    <div style="padding: 20px; width: 100%;">

                        <table style="width: 100%; font-size: 20px; color: #fff;">
                            <tbody>
                                <tr>
                                    <td style="text-align: left;">
                                        TOTAL
                                    </td>
                                    <td style="text-align: right;">

                                        Gs. <?php
                                            if (!empty($sum)) {
                                                echo number_format($sum, 0, ',', '.');
                                            } else {
                                                echo 0;
                                            }
                                            ?>

                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </li>
                <a href="/carrito" class="btn btn-primary"
                    style="width: 100%; margin-bottom: 10px; border-radius: 30px;">Ir al Carrito</a>
                <!-- <button class="btn btn-secondary" style="width: 100%;">Comprar</button> -->
            </div>
        @else

        @endif
    </ul>

</li>
