@php
$fundacion = new DateTime('1980-01-01');
$actual = new DateTime();

$anos_trayectoria = $fundacion->diff($actual)->y;

function get_categorias_por_tipo($tipos = [])
{
    $ids = DB::table('productos')
        ->where('tipo_producto', '!=', 'Olimpia')
        ->when($tipos, function ($query, $tipos) {
            $query->Where('tipo_producto', $tipos[0]);
            if (count($tipos) > 1) {
                for ($i = 1; $i < count($tipos); $i++) {
                    $query->orWhere('tipo_producto', $tipos[$i]);
                }
            }
            return $query;
        })
        ->where('activo_productos', true)
        ->distinct()
        ->get(['pk_subcategorias_productos'])
        ->toArray();
    $ids = array_column($ids, 'pk_subcategorias_productos');
    $subcategorias = Cache::remember($tipos[0], 120, function () use ($ids) {
        return DB::table('subcategorias')
            ->whereIn('id_subcategorias', $ids)
            ->where('active_subcategorias', true)
            ->where('pk_categorias_subcategorias', '!=', 4)
            ->get();
    });
    return $subcategorias;
}

$subcategoriasDamas = get_categorias_por_tipo(['Dama']);
$subcategoriasCaballero = get_categorias_por_tipo(['Hombre']);
$subcategoriasEspeciales = get_categorias_por_tipo(['EspecialesHombre', 'EspecialMujer']);
$subcategoriasNino = get_categorias_por_tipo(['Niño', 'Niña']);

@endphp

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ñamopu'a Paraguay</title>
    <meta name="description"
        content="Somos una empresa paraguaya con más de 42 años de trayectoria familiar en confecciones de prendas de jeans de máxima calidad y a buen precio, hechas con mano de obra 100% nacional">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="Ñamopu'a Paraguay, Ñamopua Paraguay, Industria de textil en Paraguay, industria de confecciones de jeans en Paraguay, textil Paraguay, textil en Paraguay, Confecciones en Paraguay, Confecciones de jeans en Paraguay,
    jeans en Paraguay, jeans, Confecciones, Industria de textil, empresa en Paraguay, confecciones de prendas de jeans en Paraguay, confecciones de prendas de jeans,
    fabrica de jeans en paraguay, jens en paraguay, jeans, fabrica de jeans, fabrica, prducción de jeans en Paraguay, producción de jeans, mercado de mercosur, industria paraguaya, importación de telas, compra de insumos en Paraguay,
    lavandería industrial en Paraguay, lavandería industrial, colocación de botones para jeans en Paraguay, colocación de botones para jeans, logistica de envio en Paraguay, logistica de envio al mercosur,
    sistema de producción modular en Paraguay, sistema OEM en Paraguay, Sistema de producción modular, sistema OEM, ñamopua Paraguay, ñamopu'a Paraguay, ñamopu'a Paraguay S.A., importación de telas en Paraguay, importación de telas, David han Paraguay, David Han">
    <meta name="description"
        content="Somos una empresa paraguaya con {{ $anos_trayectoria }} años de trayectoria en confecciones de prendas de jeans de máxima calidad y a buen precio, hechas con mano de obra 100% nacional">

    <meta name="robots" content="all,follow">
    <!-- Facebook Open Graph -->
    <meta property="og:title" content="Ñamopu'ā Paraguay" />
    <meta property="og:url" content="https://www.namopuapy.com/" />
    <meta property="og:description"
        content="Somos una empresa paraguaya con {{ $anos_trayectoria }} años de trayectoria en confecciones de prendas de jeans de máxima calidad y a buen precio, hechas con mano de obra 100% nacional" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="es_ES" />
    <meta property="og:locale:alternate" content="pt_BR" />
    <meta property="og:locale:alternate" content="en_US" />
    <meta property="og:image"
        content="https://www.namopuapy.com/img/logo/logo-namopua-paraguay-confecciones-de-jeans-principal.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:site_name" content="Ñamopu'ā Paraguay" />
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" media="print"
        onload="this.media='all'">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{ asset('css/fontastic.css') }}">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="{{ asset('css/grasp_mobile_progress_circle-1.0.0.min.css') }}">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('css/style.default.css') }}" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}">

    <!-- Responsive -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

    <link rel="stylesheet" href="{{ asset('swiper/swiper-bundle.min.css') }}" />

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <!-- Tweaks for older IEs-->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <link href="https://fonts.googleapis.com/css2?family=Julius+Sans+One&display=swap" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;400;600;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">

    {{-- <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> --}}
    <link rel="apple-touch-icon" href="{{ asset('public/img/apple-touch-icon.png') }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('public/img/apple-touch-icon-57x57.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('public/img/apple-touch-icon-72x72.png') }}" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('public/img/apple-touch-icon-76x76.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('public/img/apple-touch-icon-114x114.png') }}" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('public/img/apple-touch-icon-120x120.png') }}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('public/img/apple-touch-icon-144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('public/img/apple-touch-icon-152x152.png') }}" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/img/apple-touch-icon-180x180.png') }}" />

    <!-- Custom stylesheet - for your changes-->

</head>

<body style="background-color: #e44c65;">
    
    @include('preloader')

    <style>
        .col-sm-20 {
            width: 20%;
        }

        #header-new-mobile {
            height: 65px;
            position: fixed !important;
            top: 0;
            z-index: 30000;
            width: 100%;
        }

        .active_nav {
            background: #fff !important;
        }
    </style>

    <header id="header-new">
        <div class="contenedor_general" style=" position: relative;">

            <nav class="navbar navbar-expand-lg navbar-menu">
                <a class="navbar-brand" href="/">
                    <img src="{{ asset('img/logo/logo-namopua-paraguay-confecciones-de-jeans-principal-w.png') }}" alt="" style="width: 30px;">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto" style="margin: 0 auto;">
                        <li style="margin-right: 60px;"><a href="/">Home</a></li>
                        <li class="nav-item dropdown" style="margin-right: 60px; padding-top: 0;">
                            <a id="shop-menu" style="padding-top: 4px;" rel="nofollow" data-target="#" href="#"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                class="nav-link user-icon dropdown-toggle">
                                Tienda
                            </a>
                            <ul aria-labelledby="shop-menu" class="dropdown-menu shop-menu"
                                style="left: -100px !important; padding: 0; border-radius: 20px;">
                      
                                <div class="card-body" style="font-size: 15px; padding-bottom: 0;">
                                    <div class="col-sm-12" style="padding: 0; overflow: hidden;">
                                        
                                        <div class="col-sm-<?= count($subcategoriasEspeciales) > 0 ? 3 : 3 ?>"
                                            style="margin-bottom: 10px; padding: 10px; float: left;">
                                            <div class="col-sm-12" style="padding: 0; margin-bottom: 10px;">
                                                <p style="margin-bottom: 0;"><b>DAMAS</b></p>
                                            </div>
                                            @foreach ($subcategoriasDamas as $sub)
                                                <div class="col-sm-12" style="padding: 0; margin-bottom: 3px;">
                                                    <a class="text-dark" href="{{ url('BuscarShopGeneroMain/' . 'Dama' . '/' . $sub->id_subcategorias) }}">{{ $sub->nombre_subcategorias }}</a>
                                                </div>
                                            @endforeach
                      
                                        </div>
                                        <div class="col-sm-<?= count($subcategoriasEspeciales) > 0 ? 3 : 3 ?>"
                                            style="margin-bottom: 10px; padding: 10px; float: left;">
                                            <div class="col-sm-12" style="padding: 0; margin-bottom: 10px;">
                                                <p style="margin-bottom: 0;"><b>CABALLEROS</b></p>
                                            </div>
                                            {{-- @foreach (\App\subcategorias::con_productos('Caballero') as $sub) --}}
                                            @foreach ($subcategoriasCaballero as $sub)
                                                {{-- @if ($sub->tipo_producto == 'Hombre') --}}
                                                <div class="col-sm-12" style="padding: 0; margin-bottom: 3px;">
                                                    <a class="text-dark" href="{{ url('BuscarShopGeneroMain/' . 'Hombre' . '/' . $sub->id_subcategorias) }}">{{ $sub->nombre_subcategorias }}</a>
                                                </div>
                                                {{-- @endif --}}
                                            @endforeach
                                        </div>
                                        @if (count($subcategoriasEspeciales) > 0)
                                            <div class="col-sm-3"
                                                style="margin-bottom: 10px; padding: 10px; float: left;">
                                                <div class="col-sm-12" style="padding: 0; margin-bottom: 10px;">
                                                    <p style="margin-bottom: 0;"><b>TALLAS ESPECIALES</b></p>
                                                </div>
                                                {{-- @foreach (\App\subcategorias::con_productos('Caballero') as $sub) --}}
                                                @foreach ($subcategoriasEspeciales as $sub)
                                                    {{-- @if ($sub->tipo_producto == 'Hombre') --}}
                                                    <div class="col-sm-12" style="padding: 0; margin-bottom: 3px;">
                                                        <a class="text-dark" href="{{ url('BuscarShopGeneroMain/' . 'TallasEspeciales' . '/' . $sub->id_subcategorias) }}">{{ $sub->nombre_subcategorias }}</a>
                                                    </div>
                                                    {{-- @endif --}}
                                                @endforeach
                                            </div>
                                        @endif

                                        @if (count($subcategoriasEspeciales) > 0)
                                            <div class="col-sm-3"
                                                style="margin-bottom: 10px; padding: 10px; float: left;">
                                                <div class="col-sm-12" style="padding: 0; margin-bottom: 10px;">
                                                    <p style="margin-bottom: 0;"><b>NIÑO</b></p>
                                                </div>
                                                {{-- @foreach (\App\subcategorias::con_productos('Caballero') as $sub) --}}
                                                @foreach ($subcategoriasNino as $sub)
                                                    {{-- @if ($sub->tipo_producto == 'Hombre') --}}
                                                    <div class="col-sm-12" style="padding: 0; margin-bottom: 3px;">
                                                        <a class="text-dark" href="{{ url('BuscarShopGeneroMain/' . 'Niño' . '/' . $sub->id_subcategorias) }}">{{ $sub->nombre_subcategorias }}</a>
                                                    </div>
                                                    {{-- @endif --}}
                                                @endforeach
                                            </div>
                                        @endif

                                        <div class="col-sm-12" style="padding: 10px 0 20px 0; overflow: hidden;">
                                            <a class="btn btn-primary" href="/shop" style="border-radius: 50px; width: 100%;">
                                                Ver todos los productos
                                            </a>
                                        </div>
                                    </div>
                                </div>
                      
                            </ul>
                        </li>
                        
                        <li><a href="/contactos">Contactos</a></li>
                      
                        
                        
                    </ul>

                    <li class="nav-item dropdown" style="list-style: none; float: left; padding: 0; margin-right: 12px;">
                        <a id="search-icon" rel="nofollow" data-target="#" href="#" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false" class="nav-link user-icon dropdown-toggle">
                            <img src="{{ asset('img/icon/search-w.svg') }}" style="width: 20px; margin-right: 10px;" />
                        </a>
                        <ul aria-labelledby="search-icon" class="dropdown-menu search-icon"
                            style="left: 0px !important; background: #fff; padding: 15px 0; border-radius: 10px;">
                            <li style="width: 100%; padding-top: 0;">
                                <div class="col-sm-12">
                                    <!-- buscar produtos desde input buscador -->
                                    <div style="width: 40px; float: left;">
                                        
                                    </div>
                                    <div class="col-12" style="position: relative;">
                                        <input type="text" class="form-control input_search" id="input_search" placeholder="Buscar Produtos"  style="padding-left: 30px; color: #101010;"/>
                                        <i class="fa fa-search" style="position: absolute; top: 12px; left: 15px; color: #101010;"></i>
                                    </div>
                                    <div id="produtoLista">
                  
                                    </div>
                  
                                </div>
                            </li>
                        </ul>
                    </li>

                    @if (Auth::guest())
                        <li class="nav-item dropdown"
                            style="list-style: none; float: left; padding: 0; margin-right: 25px;">
                            <a id="user-icon" rel="nofollow" data-target="#" href="#" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false" class="nav-link user-icon dropdown-toggle">
                                <img src="{{ asset('img/icon/user-w.png') }}" style="width: 17px;" />
                            </a>
                            <ul aria-labelledby="user-icon" class="dropdown-menu user-icon"
                                style="left: 0px !important; background: #fff; border-radius: 15px;">
                                <li>
                                    <div class="card-body" style="font-size: 15px; text-align: left; padding: 10px;">
                                        <p class="text-dark"><b>INICIAR SESIÓN</b></p>
                                        <form method="POST" action="{{ route('login') }}">
                                            @csrf
                    
                                            <div class="form-group row">
                                                <label for="email" class="col-md-12 col-form-label text-dark"
                                                    style="text-align: left; padding-bottom: 0; font-size: 13px;">{{ __('Correo Electrónico') }}</label>
                    
                                                <div class="col-md-12">
                                                    <input id="email" type="email"
                                                        class="form-control form-control-sm text-dark @error('email') is-invalid @enderror"
                                                        name="email" value="{{ old('email') }}" required
                                                        autocomplete="email" autofocus style="color: #101010 !important;">
                    
                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                    
                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label text-md-right text-dark"
                                                    style="padding-bottom: 0; font-size: 13px;">{{ __('Contraseña') }}</label>
                    
                                                <div class="col-md-12">
                                                    <input id="password" type="password"
                                                        class="form-control form-control-sm text-dark @error('password') is-invalid @enderror"
                                                        name="password" required autocomplete="current-password" style="color: #101010 !important;">
                    
                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                    
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="remember"
                                                            id="remember" {{ old('remember') ? 'checked' : '' }}>
                    
                                                        <label class="form-check-label text-dark" for="remember">
                                                            {{ __('Recuerdame') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="form-group row mb-0">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-primary"
                                                        style="margin-bottom: 15px; border-radius: 15px;">
                                                        {{ __('Iniciar Sesión') }}
                                                    </button>
                    
                                                    @if (Route::has('password.request'))
                                                        <a class="" href=" {{ route('password.request') }}"
                                                            style="color: #1677BB; font-size: 13px; margin-top: 0px;">
                                                            {{ __('Olvidaste tu contraseña?') }}
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </form>
                    
                                        <div class="form-group row mb-0 mt-4">
                                            <div class="col-md-12">
                                                <p class="text-dark m-0" style="font-size: 13px;">Aún no estas registrado?</p>
                                                <a href="/register" style="color: #1677BB;">Click aquí para
                                                    registrarse</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li class="nav-item dropdown"
                            style="list-style: none; float: left; padding: 0; margin-right: 25px;">
                            <a id="user-icon-login" rel="nofollow" data-target="#" href="#" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false" class="nav-link user-icon dropdown-toggle">
                                <img src="{{ asset('img/icon/user-w.png') }}" style="width: 17px;" /> 
                                <span>Hola, {{ Auth::user()->name }}</span>
                            </a>
                            <ul aria-labelledby="user-icon-login" class="dropdown-menu user-icon-login"
                                style="left: 70px !important; border-radius: 15px;">
                                <li style="padding: 0 15px 10px 15px; border-bottom: 1px solid #e5e5e5; width: 100%;">
                                    <a href="#" style="color: #101010;">
                                        <i class="fa fa-user" style="margin-right: 10px;"></i> Hola, {{ Auth::user()->name }}
                                    </a>
                                </li>
                                @if (Auth::user()->perfil_users == 'usuario')
                                    <li style="padding: 20px 10px 10px 10px; border-bottom: 1px solid #fff; width: 100%;">
                                        <a href="/home" style="color: #101010;">
                                            Dashboard
                                        </a>
                                    </li>
                                @endif
                    
                                <li style="padding: 15px 15px 10px 15px; border-bottom: 1px solid #fff; width: 100%;">
                                    <a href="{{ route('guest.micuenta') }}" style="color: #101010;">
                                        Mi Cuenta
                                    </a>
                                </li>
                                <li style="padding: 15px 15px 10px 15px; border-bottom: 1px solid #fff; width: 100%;">
                                    <a href="{{ route('guest.historial') }}" style="color: #101010;">
                                        Mis Compras
                                    </a>
                                </li>
                    
                                <li style="width: 100%;">
                                    <a style="color: #101010" href="#"
                                        onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                        class="nav-link logout">
                                        Cerrar Sesión
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                    
                            </ul>
                        </li>

                        
                    @endif

                    @include('layouts.menu-carrito')
                </div>
            </nav>

        </div>
    </header>

    <header id="header-new-mobile">

        
        <div class="col-sm-12" style="padding: 17px;">
            <a href="/"><img src="{{ asset('img/logo/Icono-namopua-paraguay.svg') }}"
                    style="width: 30px;" /></a>
        </div>
        <div class="menu-mobile" style="position: absolute; top: 17px; right: 20px; z-index: 30000;">
            <img src="{{ asset('img/icon/menu.svg') }}" style="width: 22px;" />
        </div>
        <div class="close-drop" style="position: absolute; top: 17px; right: 20px; z-index: 30000;">
            <img src="{{ asset('img/icon/close-w.svg') }}" style="width: 20px;" />
        </div>

    </header>

    <div class="menu-drop-mobile" style="padding-top: 65px;">
        <div class="col-sm-12" style="padding-top: 30px; border-bottom: 1px solid #353535; border-top: 1px solid #353535; overflow: hidden; margin-bottom: 20px;">
            <div class="header-mobile-login">
                @if (Auth::guest())
                    <a href="/login">
                        <img src="{{ asset('img/icon/user-w.png') }}" style="width: 17px;" />
                        <p style="margin-top: 10px;">Iniciar Sesión</p>
                    </a>
                @else
                    <img src="{{ asset('img/icon/user-w.png') }}" style="width: 17px;" />
                    <p style="margin-top: 10px;">{{ Auth::user()->name }}</p>
                @endif

            </div>
            <div class="header-mobile-login">
                @if (Auth::guest())
                    <a href="/register">
                        <img src="{{ asset('img/icon/register-w.svg') }}" style="width: 22px;" />
                        <p style="margin-top: 10px;">Registrar</p>
                    </a>
                @else
                    <a href="{{ route('guest.historial') }}">
                        <img src="{{ asset('img/icon/bag-w.png') }}" style="width: 22px;" />
                        <p style="margin-top: 7px;">Mis compras</p>
                    </a>
                @endif
            </div>
            <div class="header-mobile-login">
                <a href="https://api.whatsapp.com/send?phone=595981143380&text=Hola!">
                    <img src="{{ asset('img/icon/contact-w.svg') }}" style="width: 22px;" />
                    <p style="margin-top: 10px;">
                        Contacto
                    </p>
                </a>
            </div>
            <div class="header-mobile-login">
                <a href="/carrito">
                    <img src="{{ asset('img/icon/bag-w.png') }}" style="width: 19px;" />
                    <p style="margin-top: 10px;">Carrito</p>
                </a>
            </div>
        </div>
        <div class="col-sm-12" style="border-bottom: 1px solid #353535;">
            <ul>
                <li><a href="/"><img src="{{ asset('img/icon/home-run-w.svg') }}" style="width: 17px; margin-right: 12px;" /> Home</a></li>
                <li>
                    <a href="/shop">
                        <img src="{{ asset('img/icon/pant-w.svg') }}" style="width: 17px; margin-right: 12px;" />
                        Tienda
                    </a>
                </li>
            </ul>
        </div>

        <div class="col-sm-12" style="border-bottom: 1px solid #353535; padding-top: 20px;">
            <ul>
                <li>
                    <a href="{{ route('shop.tiendaOlimpia') }}">
                        <img src="{{ asset('img/logo-olimpia-w.svg') }}" style="width: 50px; margin-right: 12px;">
                        Productos de Olimpia
                    </a>
                </li>
            </ul>
        </div>

        <div class="col-sm-12 datos-menu-mobile" style="padding: 15px 15px 0px 15px;">
            <ul>
                <li><b>Atención al cliente:</b></li>
                <li>+595 21 307 034</li>
                <li>+595 981 143 380</li>

                <li style="margin-top: 10px;"><b>Correo Electrónico:</b> </li>
                <li>info@namopuapy.com</li>

                <li style="margin-top: 10px;"><b>Dirección:</b></li>
                <li>Pizarro 912 c/Coronel Garcia de Zuniga - Lambaré, Paraguay</li>
            </ul>
        </div>

        <div class="col-12" style="border-top: 1px solid #353535; padding-top: 20px;">
            @if (Auth::guest())

            @else
                <a style="color: #fff; border-radius: 30px; width: 100%;" href="#"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                    class="btn btn-primary">
                    Cerrar Sesión
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endif
        </div>
    </div>

    <div class="space">

    </div>


    <div class="col-sm-12" style="padding: 0; overflow: hidden; background: #f7f7f7;">

        @yield('content')


    </div>

    <div id="menu-bottom-responsive">
        <div class="col-12 menu-bottom-contenedor">
            @yield('bottom-menu-section')
            <div class="container" id="container-menu-responsive">
                <div class="menu-bottom">
                    <a href="/carrito" style="color: #fff;">
                        <img src="{{ asset('img/icon/carrito-w.svg') }}" style="width: 20px;" />
                        <span id="menu-bottom-number">
                            @if (session('cart'))
                                <span>
                                    <?php $cant = 0; ?>
                                    @foreach (session('cart') as $position => $y)
                                        <?php $cant += $y['cantidad']; ?>
                                    @endforeach
                                    @if ($cant > 0)
                                        {{ $cant }}
                                    @endif
                                </span>
                            @else
                                <span>0</span>
                            @endif
                        </span>
                        <p style="margin-bottom: 0; color: #fff; font-size: 9px;">Carrito</p>
                    </a>
                </div>
                <div class="menu-bottom">
                    @if (Auth::guest())
                        <a href="/shop" style="color: #fff;">
                            <img src="{{ asset('img/icon/bag-w.svg') }}" style="width: 20px;" />
                            <p style="margin-bottom: 0; font-size: 9px;">Tienda</p>
                        </a>
                    @else
                        <a href="/historial" style="color: #fff;">
                            <img src="{{ asset('img/icon/bag-w.svg') }}" style="width: 20px;" />
                            <p style="margin-bottom: 0; font-size: 9px;">Mis compras</p>
                        </a>
                    @endif
                </div>
                <div class="menu-bottom">
                    @if (Auth::guest())
                        <a href="/login" style="color: #fff;">
                            <img src="{{ asset('img/icon/person-w.svg') }}" style="width: 20px;" />
                            <p style="margin-bottom: 0; font-size: 9px;">Iniciar Sesión</p>
                        </a>
                    @else
                        <img src="{{ asset('img/icon/person-w.svg') }}" style="width: 20px;" />
                        <p style="margin-bottom: 0; color: #fff; font-size: 9px;">{{ Auth::user()->name }}</p>
                    @endif
                </div>
                <div class="menu-bottom">
                    <a href="https://api.whatsapp.com/send?phone=595981143380&text=Hola%20tengo%20una%20consulta%20"
                        target="_blank">
                        <img src="{{ asset('img/icon/whatsapp-w.svg') }}" style="width: 20px;" />
                        <p style="margin-bottom: 0; color: #fff; font-size: 9px;">Whatsapp</p>
                    </a>
                </div>

                <div class="menu-bottom" id="resposive_scroll">
                    <i class="fa fa-chevron-up" style="font-size: 20px; color: #fff;"></i>
                    <p style="margin-bottom: 0; color: #fff; font-size: 9px;">Inicio</p>
                </div>
            </div>
        </div>
        
    </div>

    <footer id="footer">
        <div class="col-sm-12">
            <div class="contenedor_general">
                <div class="col-12 mb-4" style="overflow: hidden;">
                    <ul>
                        <li style="color: #fff; margin-bottom: 15px;">Shopping</li>
                        @foreach ($categorias as $cat)
                            @if (strtolower($cat->nombre_categorias) != 'olimpia')
                                <li style="float: left; margin-right: 20px; margin-bottom: 5px;">
                                    <a  href="{{ route('shop.subcategorias', $cat->id_subcategorias) }}">{{ $cat->nombre_subcategorias }}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>

                    <div class="col-12 pt-3" style="border-bottom: 1px solid #353535; overflow: hidden;">

                    </div>
                </div>
                <div class="footer-response">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-3">
                                <ul>
                                    <li style="color: #fff; margin-bottom: 15px;">Compras Internacionales</li>
                                    <li><a href="javascript:void(0)">#1 en venta con los mejores precios a nivel internacional</a></li>
                                    <li><a href="javascript:void(0)">Tienda online (exclusiva mavoristas)</a></li>
                                    <li><a href="javascript:void(0)">Pedidos internacionales por envío de contenedor</a></li>
                                </ul>
                            </div>
        
                            <div class="col-md-3">
                                <ul>
                                    <li style="color: #fff; margin-bottom: 15px;">Tienda</li>
                                    <li><a href="/micuenta">Mi cuenta</a></li>
                                    <li><a href="/carrito">Carrito</a></li>
                                    <li><a href="/register">Registrarse</a></li>
                                    <li><a href="/login">Iniciar Sesión</a></li>
                                    <li><a href="/password/reset">Olvide mi cuenta</a></li>
                                    <li><a href="{{ route('guest.terminosCondiciones') }}">Términos y condiciones</a></li>
                                </ul>
                            </div>
        
                            <div class="col-md-3">
                                <ul>
                                    <li style="color: #fff; margin-bottom: 15px;">Link</li>
                                    <li><a href="/">Home</a></li>
                                    <li><a href="/shop">Tienda</a></li>
                                    <!-- <li><a href="/nosotros">Nosotros</a></li>
                                        <li><a href="/trabaje_con_nosotros">Trabaje con Nosotros</a></li> -->
                                    <li><a href="/contactos">Contactos</a></li>
                                </ul>
                            </div>
                            
                            <div class="col-md-3">
                                <ul>
                                    <li style="color: #fff; margin-bottom: 15px;">Nuestra Industria</li>
                                    <li><a href="#"><i class="fa fa-phone"></i> +595 21 307 034</a></li>
                                    <li><a href="https://api.whatsapp.com/send?phone=595981143380&text=Hola!" target="_blank"><i class="fa fa-whatsapp"></i> +595 981 143 380 (Paraguay)</a></li>
                                    <li><a href="https://api.whatsapp.com/send?phone=595981384268&text=Hola!" target="_blank"><i class="fa fa-whatsapp"></i> +595 981 384 268 (Internacional)</a></li>
                                    <li><a href="mailto:internacional@namopuapy.com.py"><i class="fa fa-envelope"></i> internacional@namopuapy.com.py</a></li>
                                    <li><a href="https://www.google.com/maps/place/%C3%91amopu'a+Paraguay+S.A./@-25.3343054,-57.6380596,17z/data=!3m1!4b1!4m5!3m4!1s0x945da91b9e6bcafd:0xda006ba311fb142e!8m2!3d-25.3343103!4d-57.6358656?hl=es"
                                            target="blank"><i class="fa fa-map-marker"></i> Pizarro 912 c/ Cnel. Garcia de Zuñiga</a></li>
                                    <li style="margin-left: 10px;">Asunción - Paraguay</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-12 col-sm-12 mb-4">
                        <div class="col-12 pt-2 mb-4" style="border-bottom: 1px solid #353535; overflow: hidden;">

                        </div>
                        <a href="{{ route('shop.tiendaOlimpia') }}">
                            <img src="{{ asset('img/logo-olimpia-w.svg') }}" style="width: 50px; margin-right: 10px;"> Tienda bajo licencia oficial
                        </a>

                        <div class="col-12 pt-4" style="border-bottom: 1px solid #353535; overflow: hidden;">

                        </div>
                    </div>
                </div>

                <div class="col-md-12 box-footer" style="float: left; width: 100%;">
                    <ul>
                        <li style="color: #fff; margin-bottom: 15px;">Articulo de interés</li>
                        <li style="float: left; margin-right: 20px;"><a href="https://www.davidhan.com.py/"
                                title="Página Oficial de David Han Paraguay" target="_blank">David Han Paraguay</a></li>
                        <li style="float: left; margin-right: 20px;"><a
                                href="https://www.mre.gov.py/index.php/noticias-de-embajadas-y-consulados/inversor-coreano-dono-al-ministerio-de-salud-500-unidades-de-test-para-deteccion-del-coronavirus"
                                title="https://www.mre.gov.py/" target="_blank">Contribuciones de Salud</a></li>
                        <li style="float: left; margin-right: 20px;"><a href="https://www.uip.org.py/"
                                title="https://www.uip.org.py/" target="_blank">UIP</a></li>
                        <li style="float: left; margin-right: 20px;"><a href="https://www.uip.org.py/tag/uip-joven/"
                                title="https://www.uip.org.py/tag/uip-joven/" target="_blank">UIP Joven</a></li>
                        <li style="float: left; margin-right: 20px;"><a href="https://www.ajeparaguay.com/miembros"
                                title="https://www.ajeparaguay.com/miembros" target="_blank">AJE Paraguay</a></li>
                        <li style="float: left; margin-right: 20px;"><a
                                href="https://www.hoy.com.py/nacionales/salud-recibio-la-donacion-de-500-unidades-de-test-para-deteccion-de-covid-19"
                                title="Test para detección de COVID-19" target="_blank">Test para detección de
                                Covid-19</a></li>
                        <li style="float: left; margin-right: 20px;"><a href="https://clubolimpia.com.py/"
                                title="Club Olimpia Oficial" target="_blank">Club Olimpia</a></li>
                        <li style="float: left; margin-right: 20px;"><a
                                href="https://py.usembassy.gov/es/author/usembassyparaguay/"
                                title="https://py.usembassy.gov/es/author/usembassyparaguay/" target="_blank">Emb.
                                USA</a></li>
                        <li style="float: left; margin-right: 20px;"><a
                                href="https://www.facebook.com/AjeParaguay/posts/agradecemos-y-felicitamos-la-iniciativa-de-nuestro-socio-david-han-por-tan-loabl/3849986171709009/"
                                title="AJE Paraguay Facebook" target="_blank">Noticias AJE Paraguay</a></li>
                        <li style="float: left; margin-right: 20px;"><a href="https://pure-clinica.com/"
                                title="David Han - Pure-clinica Paraguay" target="_blank">Pure Clinica</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-sm-12" style="overflow: hidden;">
            <div class="contenedor_general">
                <div class="col-sm-12 copyright">

                    <div class="col-12 pt-2 mb-3" style="border-bottom: 1px solid #353535; overflow: hidden;">

                    </div>


                    <div class="col-sm-8 box-copyright" style="float: left; padding: 0; margin-top: 6px;">
                        Copyright © <?php echo date('Y'); ?> Ñamopu'a Paraguay S.A.
                    </div>
                    <div class="col-sm-4 box-copyright-redes" style="float: left; padding: 0; text-align: right;">
                        <a href="https://www.facebook.com/namopuaparaguaysa" target="_blank"
                            title="Facebook Ñamopu'a Paraguay S.A.">
                            <i class="fa fa-facebook-square" style="margin-right: 15px; font-size: 30px;"></i>
                        </a>
                        <a href="https://instagram.com/namopuaparaguay?igshid=1o3tora5vknk1" target="_blank"
                            title="Instagram Ñamopu'a Paraguay S.A.">
                            <i class="fa fa-instagram" style="font-size: 30px;"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </footer>

    <style>
        #footer {
            /* background: #000; */
            background: #101010;
            color: #a1a1a1;
            color: white;
            font-size: 13px;
            overflow: hidden;
            padding: 50px 0 40px 0;
        }

        #footer ul {
            margin-left: -40px;
        }

        #footer ul li {
            list-style: none;
            margin-bottom: 3px;
        }

        #footer a {
            color: #a1a1a1;
        }

        #footer a:hover {
            color: #fff;
            text-decoration: none;
        }

        .footer-response {
            float: left;
            width: 100%;
        }

        .copyright {
            float: left;
            padding-top: 10px;
        }

        @media screen and (max-width: 1150px) {
            #footer {
                padding: 50px 0 140px 0;
            }
        }

        @media screen and (max-width: 767px) {
            .footer-response {
                width: 100%;
            }

            .box-footer {
                width: 50%;
                margin-bottom: 20px;
            }

            #footer ul {
                margin-left: -55px;
            }

            .copyright {
                padding: 15px 0 0 0;
            }
        }

        @media screen and (max-width: 575px) {
            .box-copyright {
                width: 70%;
            }

            .box-copyright-redes {
                width: 30%;
            }

        }

    </style>

    <div id="scrollUp">
        <i class="fa fa-long-arrow-up scrollToUp"></i>
    </div>

    <div class="whatsapp" onclick="abrirWhatsapp()">
        <i class="fa fa-whatsapp"></i>
    </div>

    <div class="whatsapp_box">
        <div class="col-12" style="background: #25d366; padding: 8px 15px; color: #fff;">
            <i class="fa fa-whatsapp"></i> Atención al Cliente <i class="fa fa-times cerrarWhatsapp" onclick="abrirWhatsapp()" style="float: right; font-size: 21px; cursor: pointer"></i>
        </div>

        <div class="col-12 p-2">
            <div class="col-12" style="padding: 10px; margin-bottom: 10px; border: 1px solid #f1f1f1; border-radius: 10px; background: #f1f1f1;">
                <a href="https://api.whatsapp.com/send?phone=595981143380&text=Hola%20tengo%20una%20consulta%20" target="_blank" style="width: 100%; text-decoration: none;">
                    <table style="width: 100%;">
                        <tbody>
                            <tr>
                                <td>
                                    {{-- <i class="fa fa-whatsapp" style="font-size: 30px; color: #24D366;"></i> --}}
                                    <span style="font-size: 40px;">🇵🇾</span>
                                </td>
                                <td>
                                    <p class="m-0 text-dark">Linea para Paraguay</p>
                                    <p class="m-0 text-dark" style="font-size: 12px;">+595 981 143 380</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </a>
            </div>
    
            <div class="col-12" style="padding: 10px; border: 1px solid #f1f1f1; border-radius: 10px; background: #f1f1f1;">
                <a href="https://api.whatsapp.com/send?phone=595981384268&text=Hola%20tengo%20una%20consulta%20" target="_blank" style="width: 100%; text-decoration: none;">
                    <table style="width: 100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <span style="font-size: 40px;">🌎</span>
                                </td>
                                <td>
                                    <p class="m-0 text-dark">Linea para Internacional</p>
                                    <p class="m-0 text-dark" style="font-size: 12px;">+595 981 384 268</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </a>
            </div>
        </div>
        
    </div>

    <style>
        .whatsapp_box {
            width: 300px;
            background: #fff;
            position: fixed;
            bottom: 20px;
            right: 80px;
            border-radius: 10px;
            box-shadow: 0 0 15px -5px #b1b1b1;
            overflow: hidden;
            display: none;
        }

        .whatsapp {
            position: fixed;
            bottom: 20px;
            right: 20px;
            background: #25d366;
            border-radius: 50%;
            padding: 10px 12px;
            z-index: 30001;
            width: 50px;
            height: 50px;
        }

        .whatsapp i {
            color: #fff; 
            font-size: 30px;
        }

        #scrollUp {
            position: fixed;
            bottom: 80px;
            right: 20px;
            width: 50px;
            height: 50px;
            border-radius: 100px;
            background: #505050;
            color: #fff;
            z-index: 30001;
            padding: 13px 21px;
        }

        #scrollUp:hover {
            background: #202020;
        }

        .scrollToUp {
            font-size: 20px;
        }

        @media screen and (max-width: 1150px) {
            #scrollUp {
                bottom: 20px;
                right: 20px;
                padding: 13px 21px;
                z-index: 3;
            }

            .scrollToUp {
                font-size: 20px;
            }

            .whatsapp {
                display: none;
            }

        }
    </style>

    <!-- JavaScript files-->

    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.cookie/jquery.cookie.js') }}"> </script>
    {{-- <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script> --}}
    <script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}">
    </script>
    {{-- <script src="{{ asset('js/charts-home.js') }}"></script> --}}
    <script src="{{ asset('js/fontawesome.min.js') }}"></script>
    <!-- Main File-->
    <script src="{{ asset('js/front.js') }}"></script>
    <!-- Lazy Load -->
    <script type="text/javascript" src="{{ asset('js/jquery.lazy.min.js') }}"></script>

    <script src="{{ asset('swiper/swiper-bundle.min.js') }}"></script>


    {{-- <script src="{{ asset('js/jquery.fancybox.js') }}"></script> --}}

    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>



    <script>
        function abrirWhatsapp() {
            $('.whatsapp_box').toggle();
        }


        function eliminarCarrito(key) {
            $.ajax({
                type: "ajax",
                url: "{{ route('carrito.removerItemCarrito') }}",
                type: 'GET',
                data: {
                    key: key
                },
                success: function(data) {
                    console.log(data);
                    location.reload();
                }
            });
        }
        $(".menu-mobile").on("click", function() {
            $('.menu-drop-mobile').css({
                "margin-top": "0px"
            });
            $('.menu-mobile').css({
                "display": "none"
            });
            $('.close-drop').css({
                "display": "block"
            });
        });

        $(".close-drop").on("click", function() {
            $('.menu-drop-mobile').css({
                "margin-top": "-1000px"
            });
            $('.menu-mobile').css({
                "display": "block"
            });
            $('.close-drop').css({
                "display": "none"
            });
        });

        $(document).ready(function() {
            $('#scrollUp').hide();

            $(window).scroll(function() {
                if ($(this).scrollTop() > 100) {
                    $('#scrollUp').fadeIn('1000');
                    $("#header-new-mobile").addClass("active_nav");
                } else {
                    $('#scrollUp').fadeOut('1000');
                    $("#header-new-mobile").removeClass("active_nav");
                }
            });

            $('#scrollUp').click(function() {
                $('body, html').animate({
                    scrollTop: 0
                }, 500);
            });

            $('#resposive_scroll').click(function() {
                $('body, html').animate({
                    scrollTop: 0
                }, 500);
            });
            
            //$('#menu-bottom-responsive').hide();

            // $(window).scroll(function(){
            //     if($(this).width() > 991) {
            //         $('#menu-bottom-responsive').css({"display": "none"});
            //     } else {
            //         if($(this).scrollTop() > 100 ){
            //             $('#menu-bottom-responsive').fadeIn('1000');
            //         } else {
            //             $('#menu-bottom-responsive').fadeOut('1000');
            //         }
            //     }
            // });
            //
            // $('#menu-bottom-responsive').click(function() {
            //     $('body, html').animate({
            //         scrollTop: 0
            //     }, 500);
            // });

            $(".preview a").on("click", function() {
                $(".selected").removeClass("selected");
                $(this).addClass("selected");
                var picture = $(this).data();

                event.preventDefault(); //prevents page from reloading every time you click a thumbnail

                $(".full img").fadeOut(100, function() {
                    $(".full img").attr("src", picture.full);
                    $(".full").attr("href", picture.full);
                    $(".full").attr("title", picture.title);

                }).fadeIn();

            }); // end on click

            // $(".full").fancybox({
            //     helpers : {
            //         title: {
            //             type: 'inside'
            //         }
            //     },
            //     closeBtn : true,
            // });

            $('#input_search').keyup(function() {
                var query = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{ route('guest.autocompletarInput') }}",
                    type: 'POST',
                    data: {
                        query: query,
                        _token: _token
                    },
                    success: function(data) {
                        console.log(data);
                        $('#produtoLista').css({
                            "display": "block"
                        });
                        $("#produtoLista").empty();
                        $('#produtoLista').html(data);
                    }
                });

            });


            $('img.shelf-img').each(function(i, item) {
                var img_height = $(item).height();
                var div_height = $(item).parent().height();
                if (img_height < div_height) {
                    //INCREASE HEIGHT OF IMAGE TO MATCH CONTAINER
                    $(item).css({
                        'width': 'auto',
                        'height': div_height
                    });
                    //GET THE NEW WIDTH AFTER RESIZE
                    var img_width = $(item).width();
                    //GET THE PARENT WIDTH
                    var div_width = $(item).parent().width();
                    //GET THE NEW HORIZONTAL MARGIN
                    var newMargin = (div_width - img_width) / 2 + 'px';
                    //SET THE NEW HORIZONTAL MARGIN (EXCESS IMAGE WIDTH IS CROPPED)
                    $(item).css({
                        'margin-left': newMargin
                    });
                } else {
                    //CENTER IT VERTICALLY (EXCESS IMAGE HEIGHT IS CROPPED)
                    var newMargin = (div_height - img_height) / 2 + 'px';
                    $(item).css({
                        'margin-top': newMargin
                    });
                }
            });

        });

        function number_format(number, decimals, dec_point, thousands_sep) {
            // Strip all characters but numerical ones.
            number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? '.' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? ',' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        }

        //SLIDER PARA PRODUCTOS
        var swiper = new Swiper('.swiper', {
          // Default parameters
          slidesPerView: 1,
          preloadImages: true,
          // Responsive breakpoints
          breakpoints: {
            // when window width is >= 320px
            320: {
              slidesPerView: 2,
              spaceBetween: 10
            },
            // when window width is >= 480px
            480: {
              slidesPerView: 2,
              spaceBetween: 10
            },
            510: {
              slidesPerView: 2,
              spaceBetween: 15
            },
            // when window width is >= 640px
            991: {
              slidesPerView: 3,
              spaceBetween: 20
            }, 
            1199: {
              slidesPerView: 5,
              spaceBetween: 20
            }, 
          }, 
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
        })

        //SLIDER PARA PRODUCTOS
        var swiper = new Swiper('.swiper_dama', {
          // Default parameters
          slidesPerView: 1,
          // Responsive breakpoints
          breakpoints: {
            // when window width is >= 320px
            320: {
              slidesPerView: 2,
              spaceBetween: 30
            },
            // when window width is >= 480px
            480: {
              slidesPerView: 2,
              spaceBetween: 15
            },
            // when window width is >= 640px
            991: {
              slidesPerView: 3,
              spaceBetween: 15
            }, 
            1199: {
              slidesPerView: 3,
              spaceBetween: 20
            }, 
          }, 
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
        })

        //SLIDER PARA PRODUCTOS
        var swiper = new Swiper('.swiper_dama_2', {
          // Default parameters
          slidesPerView: 1,
          // Responsive breakpoints
          breakpoints: {
            // when window width is >= 320px
            320: {
              slidesPerView: 2,
              spaceBetween: 30
            },
            // when window width is >= 480px
            480: {
              slidesPerView: 2,
              spaceBetween: 15
            },
            // when window width is >= 640px
            991: {
              slidesPerView: 3,
              spaceBetween: 15
            }, 
            1199: {
              slidesPerView: 3,
              spaceBetween: 20
            }, 
          }, 
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
        })


        var swiper = new Swiper('.swiper_dama_unit', {
          // Default parameters
          slidesPerView: 1,
          // Responsive breakpoints
          breakpoints: {
            // when window width is >= 320px
            320: {
              slidesPerView: 1,
              spaceBetween: 30
            },
            // when window width is >= 480px
            480: {
              slidesPerView: 1,
              spaceBetween: 30
            },
            // when window width is >= 640px
            991: {
              slidesPerView: 1,
              spaceBetween: 30
            }, 
            1199: {
              slidesPerView: 1,
              spaceBetween: 20
            }, 
          }, 
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
        })

    </script>

    <script type="text/javascript">
        $(function() {
            $('.lazy').lazy({
                effect: "fadeIn",
                effectTime: 500,
                threshold: 0
            });
        });
    </script>



</body>

</html>
