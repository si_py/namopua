<ul style="margin-bottom: 0;">
  <li style="margin-right: 245px; text-align: left; padding: 0;">
      <a href="/">
          <img src="{{ asset('img/logo/logo-namopua-paraguay-confecciones-de-jeans--otoño.png') }}"
              style="width: 30px;" />
      </a>
  </li>
  <li style="margin-right: 60px;"><a href="/">Home</a></li>
  <li class="nav-item dropdown" style="margin-right: 60px; padding-top: 0;">
      <a id="shop-menu" style="padding-top: 4px;" rel="nofollow" data-target="#" href="#"
          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
          class="nav-link user-icon dropdown-toggle">
          Tienda
      </a>
      <ul aria-labelledby="shop-menu" class="dropdown-menu shop-menu"
          style="left: -100px !important; padding: 0;">

          <div class="card-body" style="font-size: 15px; padding-bottom: 0;">
              <div class="col-sm-12" style="padding: 0; overflow: hidden;">
                  <div class="col-sm-12" style="padding: 10px;">
                      <a href="/shop" style="color: #202020;">
                          Ver todos los productos
                      </a>
                  </div>
                  <div class="col-sm-<?= count($subcategoriasEspeciales) > 0 ? 2 : 3 ?>"
                      style="margin-bottom: 30px; padding: 10px; float: left;">
                      <div class="col-sm-12" style="padding: 0; margin-bottom: 20px;">
                          <p style="margin-bottom: 0;"><b>Damas</b></p>
                      </div>

                      {{-- @foreach (\App\subcategorias::con_productos('Dama') as $sub) --}}
                      {{-- @foreach (\App\subcategorias::subProdutos() as $sub) --}}
                      @foreach ($subcategoriasDamas as $sub)
                          {{-- @if ($sub->tipo_producto == 'Dama') --}}
                          <div class="col-sm-12" style="padding: 0; margin-bottom: 3px;">
                              <a
                                  href="{{ url('BuscarShopGeneroMain/' . 'Dama' . '/' . $sub->id_subcategorias) }}">{{ $sub->nombre_subcategorias }}</a>
                          </div>
                          {{-- @endif --}}
                      @endforeach

                  </div>
                  <div class="col-sm-<?= count($subcategoriasEspeciales) > 0 ? 2 : 3 ?>"
                      style="margin-bottom: 30px; padding: 10px; float: left;">
                      <div class="col-sm-12" style="padding: 0; margin-bottom: 20px;">
                          <p style="margin-bottom: 0;"><b>Caballeros</b></p>
                      </div>
                      {{-- @foreach (\App\subcategorias::con_productos('Caballero') as $sub) --}}
                      @foreach ($subcategoriasCaballero as $sub)
                          {{-- @if ($sub->tipo_producto == 'Hombre') --}}
                          <div class="col-sm-12" style="padding: 0; margin-bottom: 3px;">
                              <a
                                  href="{{ url('BuscarShopGeneroMain/' . 'Hombre' . '/' . $sub->id_subcategorias) }}">{{ $sub->nombre_subcategorias }}</a>
                          </div>
                          {{-- @endif --}}
                      @endforeach
                  </div>
                  @if (count($subcategoriasEspeciales) > 0)
                      <div class="col-sm-3"
                          style="margin-bottom: 30px; padding: 10px; float: left;">
                          <div class="col-sm-12" style="padding: 0; margin-bottom: 20px;">
                              <p style="margin-bottom: 0;"><b>Tallas Especiales</b></p>
                          </div>
                          {{-- @foreach (\App\subcategorias::con_productos('Caballero') as $sub) --}}
                          @foreach ($subcategoriasEspeciales as $sub)
                              {{-- @if ($sub->tipo_producto == 'Hombre') --}}
                              <div class="col-sm-12" style="padding: 0; margin-bottom: 3px;">
                                  <a
                                      href="{{ url('BuscarShopGeneroMain/' . 'TallasEspeciales' . '/' . $sub->id_subcategorias) }}">{{ $sub->nombre_subcategorias }}</a>
                              </div>
                              {{-- @endif --}}
                          @endforeach
                      </div>
                  @endif

                  <div class="col-sm-5" style="margin-bottom: 0; padding: 10px; float: left;">
                      <div class="col-sm-12" style="padding: 0; margin-bottom: 20px;">
                          <a href="{{ route('shop.tiendaOlimpia') }}">
                              <img src="{{ asset('img/olimpia-banner-menu-principal.jpg') }}"
                                  style="width: 100%; height: auto">
                          </a>
                      </div>

                      <div class="col-sm-12" style="padding: 0; margin-bottom: 10px;">
                          <a href="/">
                              <img src="{{ asset('img/tapabocas-covid-banner-menu-principal.jpg') }}"
                                  style="width: 100%; height: auto">
                          </a>
                      </div>

                  </div>
              </div>
          </div>

      </ul>
  </li>
  
  <li style="margin-right: 420px;"><a href="/contactos">Contactos</a></li>

  <li class="nav-item dropdown" style="list-style: none; float: left; padding: 0; margin-right: 12px;">
      <a id="search-icon" rel="nofollow" data-target="#" href="#" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false" class="nav-link user-icon dropdown-toggle">
          <img src="{{ asset('img/icon/search-w.svg') }}" style="width: 20px; margin-right: 10px;" />
      </a>
      <ul aria-labelledby="search-icon" class="dropdown-menu search-icon"
          style="left: -100px !important; background: #404040;">
          <li style="width: 100%; padding-top: 0;">
              <div class="col-sm-12">
                  <!-- buscar produtos desde input buscador -->
                  <div style="width: 40px; float: left;">
                      <img src="{{ asset('img/icon/search-w.svg') }}"
                          style="width: 20px; margin-top: 10px;" />
                  </div>
                  <div style="width: 328px; float: left;">
                      <input type="text" class="form-control input_search" id="input_search"
                          placeholder="Buscar Produtos" />
                  </div>
                  <div id="produtoLista">

                  </div>

              </div>
          </li>
      </ul>
  </li>
  @if (Auth::guest())
      <li class="nav-item dropdown"
          style="list-style: none; float: left; padding: 0; margin-right: 25px;">
          <a id="user-icon" rel="nofollow" data-target="#" href="#" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false" class="nav-link user-icon dropdown-toggle">
              <img src="{{ asset('img/icon/user-w.png') }}" style="width: 17px;" />
          </a>
          <ul aria-labelledby="user-icon" class="dropdown-menu user-icon"
              style="left: -100px !important; background: #404040;">
              <li>
                  <div class="card-body" style="font-size: 15px; text-align: left;">
                      <p>
                          <b>Iniciar Sesión</b>
                      </p>
                      <form method="POST" action="{{ route('login') }}">
                          @csrf

                          <div class="form-group row">
                              <label for="email" class="col-md-12 col-form-label"
                                  style="text-align: left; padding-bottom: 0;">{{ __('Correo Electrónico') }}</label>

                              <div class="col-md-12">
                                  <input id="email" type="email"
                                      class="form-control @error('email') is-invalid @enderror"
                                      name="email" value="{{ old('email') }}" required
                                      autocomplete="email" autofocus>

                                  @error('email')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="password" class="col-md-4 col-form-label text-md-right"
                                  style="padding-bottom: 0;">{{ __('Contraseña') }}</label>

                              <div class="col-md-12">
                                  <input id="password" type="password"
                                      class="form-control @error('password') is-invalid @enderror"
                                      name="password" required autocomplete="current-password">

                                  @error('password')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                              </div>
                          </div>

                          <div class="form-group row">
                              <div class="col-md-12">
                                  <div class="form-check">
                                      <input class="form-check-input" type="checkbox" name="remember"
                                          id="remember" {{ old('remember') ? 'checked' : '' }}>

                                      <label class="form-check-label" for="remember">
                                          {{ __('Recuerdame') }}
                                      </label>
                                  </div>
                              </div>
                          </div>

                          <div class="form-group row mb-0">
                              <div class="col-md-12">
                                  <button type="submit" class="btn btn-primary"
                                      style="margin-bottom: 25px;">
                                      {{ __('Iniciar Sesión') }}
                                  </button>

                                  @if (Route::has('password.request'))
                                      <a class="" href=" {{ route('password.request') }}"
                                          style="color: #1677BB; font-size: 13px; margin-top: 10px;">
                                          {{ __('Olvidaste tu contraseña?') }}
                                      </a>
                                  @endif
                              </div>
                          </div>
                      </form>

                      <div class="form-group row mb-0">
                          <div class="col-md-12">
                              <p>Aún no estas registrado?
                                  <a href="/register" style="color: #1677BB;">Click aquí para
                                      registrarse</a>
                              </p>
                          </div>
                      </div>
                  </div>
              </li>
          </ul>
      </li>
  @else

      <li class="nav-item dropdown"
          style="list-style: none; float: left; padding: 0; margin-right: 25px;">
          <a id="user-icon-login" rel="nofollow" data-target="#" href="#" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false" class="nav-link user-icon dropdown-toggle">
              <img src="{{ asset('img/icon/user-w.png') }}" style="width: 17px;" />
          </a>
          <ul aria-labelledby="user-icon-login" class="dropdown-menu user-icon-login"
              style="left: -100px !important;">
              <li style="padding: 15px; border-bottom: 1px solid #353535; width: 100%;">
                  <a href="#" style="color: #fff;">
                      <img src="{{ asset('img/icon/user-w.png') }}"
                          style="width: 11px; margin-right: 10px;" /> Hola, {{ Auth::user()->name }}
                  </a>
              </li>
              @if (Auth::user()->perfil_users == 'usuario')
                  <li style="padding: 15px; border-bottom: 1px solid #353535; width: 100%;">
                      <a href="/home" style="color: #fff;">
                          Dashboard
                      </a>
                  </li>
              @endif

              {{-- <li style="padding: 15px; border-bottom: 1px solid #353535; width: 100%;"> --}}
              {{-- <a href="{{ route('guest.micuenta') }}"> --}}
              {{-- Mi cuenta --}}
              {{-- </a> --}}
              {{-- </li> --}}

              <li style="padding: 15px; border-bottom: 1px solid #353535; width: 100%;">
                  <a href="{{ route('guest.micuenta') }}">
                      Mi Cuenta
                  </a>
              </li>
              <li style="padding: 15px; border-bottom: 1px solid #353535; width: 100%;">
                  <a href="{{ route('guest.historial') }}">
                      Mis Compras
                  </a>
              </li>

              <li style="width: 100%;">
                  <a style="color: #fff" href="#"
                      onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                      class="nav-link logout">
                      Cerrar Sesión
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST"
                      style="display: none;">
                      {{ csrf_field() }}
                  </form>
              </li>

          </ul>
      </li>
  @endif

  @include('layouts.menu-carrito')
</ul>