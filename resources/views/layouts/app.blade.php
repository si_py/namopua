@php
$fundacion = new DateTime('2001-01-01');
$actual = new DateTime();

$anos_trayectoria = $fundacion->diff($actual)->y;
@endphp
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin | Ñamopu'a Py</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{ asset('css/fontastic.css') }}">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="{{ asset('css/grasp_mobile_progress_circle-1.0.0.min.css') }}">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('css/style.default.css') }}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <!-- Tweaks for older IEs-->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

    <style>
        .cambiar_estado:hover {
            cursor: pointer;
        }

    </style>
</head>

<body>
    <!-- Side Navbar -->
    <nav class="side-navbar shrink" style="border-right: 4px solid #34B35A;">
        <div class="side-navbar-wrapper">
            <!-- Sidebar Header    -->
            <div class="sidenav-header d-flex align-items-center justify-content-center">
                <!-- User Info-->
                <div class="sidenav-header-inner text-center">
                    <img src="{{ asset('img/logo/logo-namopua-paraguay-confecciones-de-jeans-principal-w.png') }}"
                        alt="person" class="img-fluid rounded-circle" style="width: 40px; height: 40px;">

                </div>
                <!-- Small Brand information, appears on minimized sidebar-->
                <div class="sidenav-header-logo">
                    <!-- <a href="/" class="brand-small text-center"> <strong>Npy</strong></a> -->
                    <img src="{{ asset('img/logo/logo-namopua-paraguay-confecciones-de-jeans-principal-w.png') }}"
                        alt="person" class="brand-small text-center" style="width: 40px; height: 40px;">
                </div>
            </div>
            <!-- Sidebar Navigation Menus-->
            <div class="main-menu">
                <h5 class="sidenav-heading">Menu</h5>
                <ul id="side-main-menu" class="side-menu list-unstyled">
                    <li><a href="/home"> <i class="icon-home"></i>Home</a></li>
                    <li>
                        <a href="#setup" aria-expanded="false" data-toggle="collapse">
                            <i class="icon-interface-windows"></i>Setup
                        </a>
                        <ul id="setup" class="collapse list-unstyled ">
                            <li><a href="/categorias"> <i class="icon-form"></i>Categorias</a></li>
                            <li><a href="/subcategorias"> <i class="fa fa-bar-chart"></i>Sub Categorias</a></li>
                            <li><a href="/marcas"> <i class="icon-pencil-case"></i>Marcas</a></li>
                            <li><a href="/color"> <i class="icon-picture"></i>Color</a></li>
                            <li><a href="/talles"> <i class="icon-list"></i>Tallas</a></li>
                            <li><a href="/divisas"> <i class="fa fa-dollar-sign"></i> Divisa</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#config-pagina" aria-expanded="false" data-toggle="collapse">
                            <i class="icon icon-page"></i>Configurar Paginas
                        </a>
                        <ul id="config-pagina" class="collapse list-unstyled ">
                            <li><a href="/secciones"> <i class="icon icon-list"></i>Secciones</a></li>
                            <li><a href="/sliders"> <i class="icon icon-list"></i>Sliders</a></li>
                            <li><a href="/contenido"> <i class="icon icon-screen"></i>Contenido</a></li>
                        </ul>
                    </li>
                    <li><a href="/productos"> <i class="icon-grid"></i>Productos</a></li>
                    <li>
                        <a href="#user" aria-expanded="false" data-toggle="collapse">
                            <i class="icon icon-page"></i>Usuarios
                        </a>
                        <ul id="user" class="collapse list-unstyled ">
                            <li><a href="/usuario"> <i class="icon icon-list"></i>Adm. Usuarios</a></li>
                            <li><a href="#"> <i class="icon icon-list"></i>Adm. Clientes</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('curriculum.verCurriculum') }}"> <i
                                class="icon-grid"></i>Curriculum</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="page active">
        <!-- navbar-->
        <header class="header">
            <nav class="navbar">
                <div class="container-fluid" style="padding: 0 15px;">
                    <div class="navbar-holder d-flex align-items-center justify-content-between">
                        <div class="navbar-header">
                            <a id="toggle-btn" href="#" class="menu-btn">
                                <i class="icon-bars"> </i>
                            </a>
                            <div class="brand-text d-none d-md-inline-block">
                                <span style="color: #fff;">Ñamopu'a Py </span>
                                <a href="/" class="btn btn-primary btn-sm"
                                    style="border-radius: 30px; margin-left: 20px;">
                                    Visitar la pagina
                                </a>
                            </div>
                        </div>
                        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">

                            <!-- Languages dropdown    -->
                            <li class="nav-item dropdown">
                                <a id="languages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false"
                                    class="nav-link language dropdown-toggle">
                                    <i class="icon-user" style="font-size: 16px; color: #34B35A;"></i>
                                    <span class="d-none d-sm-inline-block"
                                        style="color: #34B35A;">{{ auth()->user()->name }}</span>
                                </a>
                                <ul aria-labelledby="languages" class="dropdown-menu">
                                    <li>
                                        <a style="color: #808080" href="#"
                                            onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                            class="nav-link logout">
                                            Cerrar Sesión
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                </ul>
                            </li>
                            <!-- Log out-->
                            <li class="nav-item" title="Cerrar Sesion">


                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>


        <div class="col-sm-12" style="padding: 0; overflow: hidden;">

            @yield('content')

        </div>


        <footer class="main-footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <p><a href="https://namopuapy.com" target="_blank" class="external">Ñamopu'a Paraguay S.A.
                                &copy; <?php echo date('Y'); ?></a></p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <p><a href="https://soluciones-inteligentes.net" target="_blank"
                                class="external">Soluciones
                                Inteligentes</a></p>
                        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions and it helps me to run Bootstrapious. Thank you for understanding :)-->
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- JavaScript files-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.cookie/jquery.cookie.js') }}"> </script>
    <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('js/charts-home.js') }}"></script>
    <script src="{{ asset('js/fontawesome.min.js') }}"></script>
    <!-- Main File-->
    <script src="js/front.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script>

       


        function eliminarCarrito(key) {
            $.ajax({
                type: "ajax",
                url: "{{ route('carrito.removerItemCarrito') }}",
                type: 'GET',
                data: {
                    key: key
                },
                success: function(data) {
                    console.log(data);
                    location.reload();
                }
            });
        }

        $(document).ready(function() {
            $('.summernote').summernote({
                height: 400,
                dialogsInBody: true,
                callbacks: {

                },
            });
        });

        // update categorias
        $(document).on('blur', '.column_name', function() {
            var id_stock = "";
            var column_name = $(this).data("column_name");
            var column_value = $(this).text();
            var id = $(this).data("id");
            console.log(column_name);
            console.log(column_value);
            console.log(id);
            if (column_value != '') {
                $.ajax({
                    url: "categorias/" + id,
                    method: "POST",
                    data: {
                        column_name: column_name,
                        column_value: column_value,
                        id: id,
                        _token: '{{ csrf_token() }}',
                        _method: 'PATCH'
                    },
                    success: function(data) {
                        // var categoria = $(this).val();
                        // $.get('/tabla_categorias', function(data) {
                        //     //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
                        //     console.log(data);
                        //     for (var i = 0; i < data.length; i++)
                        //         id_stock += '<tr>' +
                        //         '<td>' + data[i].id_categorias + '</td>' +
                        //         '<td contenteditable="true" class="column_name" data-column_name="nombre_categorias" data-id="' +
                        //         data[i].id_categorias + '">' + data[i].nombre_categorias +
                        //         '</td>' +
                        //         '</tr>',

                        //         $(".table-categorias").empty();
                        //     $('.table-categorias').append(id_stock);
                        // })
                    }
                })
            }
        });

        // update marcas
        $(document).on('blur', '.column_name', function() {
            var id_stock = "";
            var column_name = $(this).data("column_name");
            var column_value = $(this).text();
            var id = $(this).data("id");
            console.log(column_name);
            console.log(column_value);
            console.log(id);
            if (column_value != '') {
                $.ajax({
                    url: "marcas/" + id,
                    method: "POST",
                    data: {
                        column_name: column_name,
                        column_value: column_value,
                        id: id,
                        _token: '{{ csrf_token() }}',
                        _method: 'PATCH'
                    },
                    success: function(data) {
                        // var categoria = $(this).val();
                        // $.get('/tabla_marcas', function(data) {
                        //     //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
                        //     console.log(data);
                        //     for (var i = 0; i < data.length; i++)
                        //         id_stock += '<tr>' +
                        //         '<td>' + data[i].id_marcas + '</td>' +
                        //         '<td contenteditable="true" class="column_name" data-column_name="nombre_marcas" data-id="' +
                        //         data[i].id_marcas + '">' + data[i].nombre_marcas + '</td>' +
                        //         '</tr>',

                        //         $(".table_marcas").empty();
                        //     $('.table_marcas').append(id_stock);
                        // })
                    }
                })
            }
        });

        // update color
        $(document).on('blur', '.column_name', function() {
            var id_stock = "";
            var column_name = $(this).data("column_name");
            var column_value = $(this).text();
            var id = $(this).data("id");
            console.log(column_name);
            console.log(column_value);
            console.log(id);
            if (column_value != '') {
                $.ajax({
                    url: "color/" + id,
                    method: "POST",
                    data: {
                        column_name: column_name,
                        column_value: column_value,
                        id: id,
                        _token: '{{ csrf_token() }}',
                        _method: 'PATCH'
                    },
                    success: function(data) {
                        // var categoria = $(this).val();
                        
                        // var categoria = $(this).val();
                        $.get('/tabla_color', function(data) {
                            //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
                            console.log(data);
                            let activos = data.activos
                            for (var i = 0; i < activos.length; i++) {
                                activos += '<tr>' +
                                    '<td>' + activos[i].id_color + '</td>' +
                                    '<td contenteditable="true" class="column_name" data-column_name="nombre_color" data-id="' +
                                    activos[i].id_color + '">' + activos[i].nombre_color + '</td>' +
                                    '<td contenteditable="true" class="column_name" data-column_name="prefijo_color" data-id="' +
                                    activos[i].id_color + '" style="color:' + activos[i].prefijo_color +
                                    '">' +
                                    activos[i].prefijo_color + '</td>' +
                                    '</tr>'
                            }
                            let inactivos = data.inactivos
                            for (var i = 0; i < inactivos.length; i++) {
                                activos += '<tr>' +
                                    '<td>' + inactivos[i].id_color + '</td>' +
                                    '<td contenteditable="true" class="column_name" data-column_name="nombre_color" data-id="' +
                                    inactivos[i].id_color + '">' + inactivos[i].nombre_color + '</td>' +
                                    '<td contenteditable="true" class="column_name" data-column_name="prefijo_color" data-id="' +
                                    inactivos[i].id_color + '" style="color:' + inactivos[i].prefijo_color +
                                    '">' +
                                    inactivos[i].prefijo_color + '</td>' +
                                    '</tr>'
                            }
                            $(".table_color#activos").empty();
                            $('.table_color#activos').append(activos);
                            $(".table_color#inactivos").empty();
                            $('.table_color#inactivos').append(inactivos);
                        })
                    }
                })
            }
        });

        // update taller
        $(document).on('blur', '.column_name', function() {
            var id_stock = "";
            var column_name = $(this).data("column_name");
            var column_value = $(this).text();
            var id = $(this).data("id");
            var estado = $(this).data("estado");
            console.log(estado)
            console.log(column_name);
            console.log(column_value);
            console.log(id);
            if (column_value != '') {
                $.ajax({
                    url: "talles/" + id,
                    method: "POST",
                    data: {
                        column_name: column_name,
                        column_value: column_value,
                        id: id,
                        _token: '{{ csrf_token() }}',
                        _method: 'PATCH'
                    },
                    success: function(data) {
                        // var categoria = $(this).val();
                        $.get('/tabla_talles', function(data) {
                            //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
                            console.log(data);
                            for (var i = 0; i < data.length; i++) {
                                id_stock += '<tr>' +
                                    '<td>' + data[i].id_talles + '</td>' +
                                    '<td contenteditable="true" class="column_name" data-column_name="nombre_talles" data-id="' +
                                    data[i].id_talles + '">' + data[i].nombre_talles + '</td>' +
                                    '</tr>'
                            }

                            if (estado == "true") {
                                $(".table_talles#activos").empty();
                                $('.table_talles#activos').append(id_stock);
                            } else {
                                $(".table_talles#inactivos").empty();
                                $('.table_talles#inactivos').append(id_stock);
                            }
                        })
                    }
                })
            }
        });

        // update taller
        $(document).on('blur', '.column_name', function() {
            var id_stock = "";
            var column_name = $(this).data("column_name");
            var column_value = $(this).text();
            var id = $(this).data("id");
            console.log(column_name);
            console.log(column_value);
            console.log(id);
            if (column_value != '') {
                $.ajax({
                    url: "secciones/" + id,
                    method: "POST",
                    data: {
                        column_name: column_name,
                        column_value: column_value,
                        id: id,
                        _token: '{{ csrf_token() }}',
                        _method: 'PATCH'
                    },
                    success: function(data) {
                        // var categoria = $(this).val();
                        $.get('/tabla_secciones', function(data) {
                            //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
                            console.log(data);
                            for (var i = 0; i < data.length; i++)
                                id_stock += '<tr>' +
                                '<td>' + data[i].id_secciones + '</td>' +
                                '<td contenteditable="true" class="column_name" data-column_name="nombre_secciones" data-id="' +
                                data[i].id_secciones + '">' + data[i].nombre_secciones +
                                '</td>' +
                                '<td contenteditable="true" class="column_name" data-column_name="referencias_secciones" data-id="' +
                                data[i].id_secciones + '">' + data[i].referencias_secciones +
                                '</td>' +
                                '<td contenteditable="true" class="column_name" data-column_name="activo_secciones" data-id="' +
                                data[i].id_secciones + '">' + data[i].activo_secciones +
                                '</td>' +
                                '</tr>',

                                $(".table_secciones").empty();
                            $('.table_secciones').append(id_stock);
                        })
                    }
                })
            }
        });


        function number_format(number, decimals, dec_point, thousands_sep) {
            // Strip all characters but numerical ones.
            number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        }

        // cambiar estado de x registro
        $(document).ready(function() {
            $(document).on('click', '.cambiar_estado', function(e) {
                var column_name = $(e.currentTarget).data("column-name");
                var nuevo_estado = $(e.currentTarget).data('nuevo-estado');
                var id = $(e.currentTarget).data("id");
                $.ajax({
                    type: "ajax",
                    url: url_tabla + id,
                    method: "POST",
                    data: {
                        column_name: column_name,
                        nuevo_estado: nuevo_estado,
                        id: id,
                        _token: '{{ csrf_token() }}',
                        _method: 'PATCH'
                    },
                    success: function(data) {
                        if (data.success) {
                            $("tr[data-id=" + id + "]").remove()
                            nuevaFilaEstado(data.data)
                        } else {
                            console.log("error")
                        }
                    }
                })
            })
        })
    </script>



</body>

</html>
