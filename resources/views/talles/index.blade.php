@extends('layouts.app')

@section('content')

    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Tallas</li>
            </ul>
        </div>
    </div>

    <section>
        <div class="col-sm-12" style="padding: 10px;">
            <div class="row">
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body" style="padding: 0;">
                            <div class="form-inline">
                                @csrf
                                <div class="form-group">
                                    <label for="inlineFormInput" class="sr-only">Agregar tallas</label>
                                    <input id="nombre" type="text" placeholder="Ej. 42" class="mr-3 form-control">
                                </div>
                                <div class="form-group">
                                    <button onclick="agregar_talles()" class="mr-3 btn btn-primary">Agregar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                Activos
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_talles" id="activos">
                                        @foreach ($tallesActivos as $t)
                                            <tr>
                                                <th scope="row">{{ $t->id_talles }}</th>
                                                <td contenteditable="true" class="column_name" data-estado="true"
                                                    data-column_name="nombre_talles" data-id="{{ $t->id_talles }}">
                                                    {{ $t->nombre_talles }}</td>
                                                <td><i class="fa fa-arrow-right cambiar_estado"
                                                        data-id="{{ $t->id_talles }}" data-nuevo-estado="false"
                                                        data-column-name="estado_talles"></i></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                Inactivos
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_talles" id="inactivos">
                                        @foreach ($tallesInactivos as $t)
                                            <tr data-id="{{ $t->id_talles }}">
                                                <th scope="row">{{ $t->id_talles }}</th>
                                                <td contenteditable="true" class="column_name" data-estado="false"
                                                    data-column_name="nombre_talles" data-id="{{ $t->id_talles }}">
                                                    {{ $t->nombre_talles }}</td>
                                                <td><i class="fa fa-arrow-left cambiar_estado" data-id="{{ $t->id_talles }}"
                                                        data-nuevo-estado="true" data-column-name="estado_talles"></i></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        const url_tabla = '/talles/'

        function agregar_talles() {
            var nombre = document.getElementById("nombre").value;
            var id_stock = "";
            console.log(nombre);
            $.ajax({
                type: "ajax",
                url: "{{ route('talles.store') }}",
                type: 'POST',
                data: {
                    nombre: nombre,
                    _token: '{{ csrf_token() }}'
                },

                success: function(resp) {
                    if (resp.errors != null) {
                        alert('Ya existe la talla: ' + nombre)
                    } else {
                        // var categoria = $(this).val();
                        $.get('/tabla_talles', function(data) {
                            //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
                            console.log(data);
                            for (var i = 0; i < data.length; i++) {
                                id_stock += '<tr data-id="' + data[i].id_talles + '">' +
                                    '<td>' + data[i].id_talles + '</td>' +
                                    '<td contenteditable="true" class="column_name" data-column_name="nombre_marcas" data-id="' +
                                    data[i].id_talles + '">' + data[i].nombre_talles + '</td>' +
                                    '<td><i class="fa fa-arrow-right cambiar_estado" data-id="' + data[
                                        i].id_talles +
                                    '" data-nuevo-estado="false" data-column-name="estado_talles"></i></td>' +
                                    '</tr>'
                            }
                            $(".table_talles").empty();
                            $('.table_talles#activos').append(id_stock);
                        })
                    }
                }
            })
        };

        function nuevaFilaEstado(data) {
            let estado = data.estado_talles
            $("tr[data-id=" + data.id_talles + "]").remove()
            let fila = `
            <tr data-id="${data.id_talles}">
                <th scope="row">${data.id_talles}</th>
                <td contenteditable="true" class="column_name" data-column_name="nombre_talles" data-id="${data.id_talles}">
                  ${data.nombre_talles}
                </td>
                <td><i class="fa fa-arrow-${estado ? 'right' : 'left'} cambiar_estado" data-id="${data.id_talles}" data-nuevo-estado="${estado ? 'false' : 'true'}" data-column-name="estado_talles"></i></td>
            </tr>
          `
            if (estado) {
                $(".table_talles#activos").append(fila)
            } else {
                $(".table_talles#inactivos").append(fila)
            }
        }
    </script>

@endsection
