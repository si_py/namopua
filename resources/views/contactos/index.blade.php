@extends('layouts.front')

@section('content')

<div class="container-lg">
  <div class="col-sm-12 title-shop">
    <h1 style="font-weight: 900; font-family: 'Montserrat', sans-serif;">Contactos</h1>
    <p style="font-weight: 400;">envíanos tus datos y nuestro equipo se contactará con usted</p>

  </div>
  <style>
  .title-shop {
    text-align: center;
    padding: 80px 0 80px 0;
  }
  .title-shop h1 {
    font-size: 100px;
    font-weight: 900;
  }
  .title-shop p{
    font-size: 17px;
    font-weight: 100;
    color: #909090;
    margin-top: 0px;
    margin-bottom: 0;
    font-family: 'Montserrat', sans-serif;
  } 
  .texto_input, .texto_textarea {
    border-radius: 30px !important;
  }
  #input-search:focus, #email:focus, #password:focus {
    color: #808080 !important;
    border-color: #707070;
    background: transparent;
    outline: none !important;
    outline-width: 0 !important;
    box-shadow: none;
    -moz-box-shadow: none;
    -webkit-box-shadow: none;
  }
  @media screen and (max-width: 767px) {
    .title-shop h1 {
      font-size: 70px;
      font-weight: 900;
    }
    .title-shop p{ 
      padding-left: 25px;
      padding-right: 25px;
    }
  }



    section {
      padding-top: 80px; padding-bottom: 80px;
    }
    h1, label, span, h2, a, p, textarea { 
      font-family: 'Montserrat', sans-serif; 
    }
    textarea { color: #808080 !important; padding: 20px !important;}
    .btn-primary {
      background-color: #1677BB;
      border: 1px solid #1677BB;
    }
    .btn-primary:hover {
      background-color: #175b89;
      border: 1px solid #175b89;
    }

    a{
      color: #1677BB;
    }
    a:hover {
      text-decoration: none;
      color: #175b89;
    }
    .texto_input {
        border-radius: 30px !important;
        padding: 25px !important;
        font-family: 'Montserrat', sans-serif;
        margin-bottom: 0  !important;
        padding-left: 70px !important;
    }
    .texto_input::placeholder, textarea::placeholder {
        font-size: 17px !important;
        font-family: 'Montserrat', sans-serif !important;
        font-weight: 400 !important;
    }
    
    .btn-outline-primary {
        border-radius: 30px;
        padding: 5px 20px 5px 20px;
        border: 2px solid #1677BB;
        color: #1677BB;
        font-weight: 600;
    }
    .btn-outline-primary:hover {
        background: #1677BB;
        border: 2px solid #1677BB;
    }
    .derecha {
        float: left; 
        overflow: hidden; 
        padding-top: 0px;
        color: #808080;
    }
    .izquierda {

    }
    .izquierda h1{
        font-weight: 600; 
        font-size: 60px; 
        margin-bottom: 0;
        color: #000;
    }
    .title {
        margin-bottom: 30px; 
        margin-top: 0px; 
        overflow: hidden; 
        text-align: right;
    }
    .btn {
        text-align: right;
    }
    .footer-izquierda {
        width: 80%;
        float: left;
    }
    .footer-derecha {
        width: 20%;
        float: left;
        text-align: right;
    }
    .card-body {
        overflow: hidden; 
        padding: 50px 20px;
    }
    .contenedor { 
      padding-top: 0px; 
      margin-bottom: 100px;
    }

    .form-control:focus {
        box-shadow: none;
    }

    @media screen and (max-width: 991px){
      .derecha {
          float: left; 
          overflow: hidden; 
          padding-top: 25px;
      }
      .izquierda {text-align: center;}
      .izquierda h1{ font-size: 40px; margin-bottom: 10px;}
      .title {text-align: center;}
      .btn {text-align: center;}
      .footer-izquierda { width: 40%; }
      .footer-derecha { width: 60%; }
      .card-body {
          overflow: hidden; padding: 50px 0px;
      }
      .contenedor { padding-top: 0px; }
    }

    @media screen and (max-width: 767px){
      .derecha span {
        font-size: 15px;
      }
    }
  </style>
</div>


<div class="container" style="overflow: hidden;">
  <div class="row justify-content-center">
    <div class="col-md-12 contenedor">
      <div class="col-12" style="border-radius: 30px; border: 1px solid #cecece;">
        <div class="card-body" style="overflow: hidden;">

          <div class="col-lg-6 izquierda" style="float: left;">
            <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0;">
              <input id="name" type="text" class="form-control texto_input" name="name" placeholder="Empresa" autofocus>
              <img src="img/icon/suitcase-alt.svg" style="width: 30px; position: absolute; top: 12px; left: 20px;">
            </div>
            <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0;">
              <input id="name" type="text" class="form-control texto_input" name="name" placeholder="Nombre y Apellido" autofocus>
              <img src="img/icon/user-1.svg" style="width: 30px; position: absolute; top: 12px; left: 20px;">
            </div>
            <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0;">
              <input id="name" type="text" class="form-control texto_input" name="name" placeholder="Correo Electrónico" autofocus>
              <img src="img/icon/envelope-alt.svg" style="width: 30px; position: absolute; top: 12px; left: 20px;">
            </div>
            <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0;">
              <input id="name" type="text" class="form-control texto_input" name="name" placeholder="Teléfono" autofocus>
              <img src="img/icon/phone.svg" style="width: 30px; position: absolute; top: 12px; left: 20px;">
            </div>
            <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0;">
              <textarea class="form-control texto_textarea" style="height: 345px;" placeholder="Mensaje "></textarea>
            </div>
            <div class="col-md-12" style="position: relative; margin-bottom: 15px; padding: 0; text-align: right;">
              <button class="btn btn-primary" style="padding: 5px 20px; border-radius: 30px;">Enviar</button>
            </div>
          </div>

          <div class="col-lg-6 derecha" style="float: left;">
            <span style="color: #808080; margin-bottom: 15px;"><img src="img/icon/phone-808080.svg" style="width: 17px; margin-right: 10px;"> Atención al cliente: +595 21 307 034</span><br>
            <span style="color: #808080; margin-bottom: 15px;"><img src="img/icon/whatsapp-808080.svg" style="width: 17px; margin-right: 10px;"> Whatsapp: +595 981 143 380 (Paraguay)</span><br>
            <span style="color: #808080; margin-bottom: 15px;"><img src="img/icon/whatsapp-808080.svg" style="width: 17px; margin-right: 10px;"> Whatsapp: +595 981 384 268 (Internacional)</span><br>
            <span style="color: #808080; margin-bottom: 15px;"><img src="img/icon/envelope-alt-808080.svg" style="width: 20px; margin-right: 7px;"> Correo Electrónico: info@namopuapy.com</span><br>
            <span style="color: #808080; margin-bottom: 45px;"><img src="img/icon/location-808080.svg" style="width: 15px; margin-right: 12px;"> Dirección: Pizarro 912 c/Coronel Garcia de Zuniga  <span style="padding-left: 32px;">Lambaré, Paraguay</span></span>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d57699.60758353966!2d-57.66760240422785!3d-25.330213928458534!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x945da91b9e6bcafd%3A0xda006ba311fb142e!2s%C3%91amopu&#39;a%20Paraguay%20S.A.!5e0!3m2!1ses!2spy!4v1576267128511!5m2!1ses!2spy" width="100%" height="450px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>


@endsection