@extends('layouts.app')

@section('content')

<!-- Breadcrumb-->
<div class="breadcrumb-holder">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Home</a></li>
      <li class="breadcrumb-item active">Usuarios</li>
    </ul>
  </div>
</div>

<section>
  <div class="col-sm-12" style="padding: 10px;">
    <div class="row">
      <div class="col-lg-4">
        <div class="card">
          <div class="card-header">
            <div class="card-body" style="padding: 0;">
              <form action="{{route('usuario.store')}}" method="POST" role="form">
                @csrf
                <div class="form-group">
                  <label for="inlineFormInput">Nombre y apellido</label>
                  <input name="nombre" type="text" placeholder="Ej. Juan Perez" class="mr-3 form-control" style="width: 100%;">
                </div>

                <div class="form-group">
                  <label for="inlineFormInput">Razon Social</label>
                  <input name="razon" type="text" placeholder="Ej. Brabus S.A." class="mr-3 form-control" style="width: 100%;">
                </div>

                <div class="form-group">
                  <label for="inlineFormInput">Ruc / C.I.no.</label>
                  <input name="ruc" type="text" placeholder="Ej. 3456456-0" class="mr-3 form-control" style="width: 100%;">
                </div>

                <div class="form-group">
                  <label for="inlineFormInput">Dirección</label>
                  <input name="direccion" type="text" placeholder="Ej. Pizzaro 912" class="mr-3 form-control" style="width: 100%;">
                </div>

                <div class="form-group">
                  <label for="inlineFormInput">Teléfono</label>
                  <input name="telefono" type="text" placeholder="Ej. +595 981 456 456" class="mr-3 form-control" style="width: 100%;">
                </div>

                <div class="form-group">
                  <label for="inlineFormInput">E-mail</label>
                  <input name="email" type="email" placeholder="Ej. juan@namopua.com" class="mr-3 form-control" style="width: 100%;">
                </div>

                <div class="form-group">
                  <label for="inlineFormInput">Contraseña</label>
                  <input name="password" type="password" class="mr-3 form-control" style="width: 100%;">
                </div>

                <div class="form-group">
                  <label for="inlineFormInput">Confirma contraseña</label>
                  <input name="password-confirm" type="password" class="mr-3 form-control" style="width: 100%;">
                </div>

                <div class="form-group">
                  <button type="submit" class="mr-3 btn btn-primary">Agregar</button>
                </div>
              </form>
            </div>
          </div>

        </div>
      </div>

      <div class="col-lg-8">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Razon Social</th>
                    <th>Ruc</th>
                    <th>Dirección</th>
                    <th>Teléfono</th>
                    <th>Perfil</th>
                    <th>Estado</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody class="table_usuario">
                  @foreach($user as $u)
                  @if($u->perfil_users == "usuario")
                  <tr>
                    <td>{{$u->id}}</td>
                    <td>{{$u->name}}</td>
                    <td>{{$u->email}}</td>
                    <td>{{$u->razon_users}}</td>
                    <td>{{$u->ruc_users}}</td>
                    <td>{{$u->direccion_users}}</td>
                    <td>{{$u->telefono_users}}</td>
                    <td>{{$u->perfil_users}}</td>
                    <td>{{$u->estado_users}}</td>
                    <td><button onclick="modificar_usuario('{{ $u->id }}', 
                                                           '{{ $u->name }}',
                                                           '{{ $u->email }}',
                                                           '{{ $u->razon_users }}', 
                                                           '{{ $u->ruc_users }}',
                                                           '{{ $u->direccion_users }}', 
                                                           '{{ $u->telefono_users }}', 
                                                           '{{ $u->password }}')" data-toggle="modal" data-target="#modal_editar_usuario" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></td>
                  </tr>
                  @endif
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="modal_editar_usuario">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="text-align: left;">
        <span>Codigo: <span id="codigo_usuario_modal"></span> Usuario: <span id="nombre_usuario_modal_titulo"></span></span>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <div class="col-12">
            <label>Nombre</label>
            <input type="hidden" class="form-control" id="id_usuario_modal">
            <input type="text" id="nombre_usuario_modal" class="form-control">
          </div>
          <div class="col-12">
            <label>Correo Electrónico</label>
            <input type="text" id="email_usuario_modal" class="form-control">
          </div>
          <div class="col-12">
            <label>Razón Social</label>
            <input type="text" id="razon_usuario_modal" class="form-control">
          </div>
          <div class="col-12">
            <label>RUC</label>
            <input type="text" id="ruc_usuario_modal" class="form-control">
          </div>
          <div class="col-12">
            <label>Dirección</label>
            <input type="text" id="direccion_usuario_modal" class="form-control">
          </div>
          <div class="col-12">
            <label>Teléfono</label>
            <input type="text" id="telefono_usuario_modal" class="form-control">
          </div>
          <div class="col-12">
            <label>Contraseña</label>
            <input type="password" id="pass_usuario_modal" class="form-control">
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="btn_modificar_usuario()">Modificar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function modificar_usuario(id, nombre, email, razon, ruc, direccion, telefono, pass) {
    $('#id_usuario_modal').val(id);
    $('#nombre_usuario_modal').val(nombre);
    $('#email_usuario_modal').val(email);
    $('#razon_usuario_modal').val(razon);
    $('#ruc_usuario_modal').val(ruc);
    $('#direccion_usuario_modal').val(direccion);
    $('#telefono_usuario_modal').val(telefono);
    $('#pass_usuario_modal').val(pass);
    $('#nombre_usuario_modal_titulo').html(nombre);
    $('#codigo_usuario_modal').html(id);
  }

  function btn_modificar_usuario() {
    var id = $('#id_usuario_modal').val();
    var nombre = $('#nombre_usuario_modal').val();
    var email = $('#email_usuario_modal').val();
    var razon = $('#razon_usuario_modal').val();
    var ruc = $('#ruc_usuario_modal').val();
    var direccion = $('#direccion_usuario_modal').val();
    var telefono = $('#telefono_usuario_modal').val();
    var pass = $('#pass_usuario_modal').val();
    $.ajax({
      type:"ajax",
      url: "{{route('usuario.modificar_usuarioUsuario')}}",
      type: 'POST',
      data: { id: id, 
              nombre: nombre, 
              email: email, 
              razon: razon, 
              ruc: ruc,
              direccion: direccion, 
              telefono: telefono, 
              pass: pass,
              _token:'{{csrf_token()}}'},

      success: function(){
        location.reload();
      }
    });
  }
// function agregar_marcas() {
//   var nombre = document.getElementById("nombre").value;
//   var id_stock="";
//   console.log(nombre);
//   $.ajax({
//     type:"ajax",
//     url: "{{route('marcas.store')}}",
//     type: 'POST',
//     data: {nombre: nombre, _token:'{{csrf_token()}}'},
//
//     success: function(){
//
//       // var categoria = $(this).val();
//     $.get('/tabla_marcas', function(data){
// //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
//       console.log(data);
//       for (var i=0; i<data.length;i++)
//       id_stock+= '<tr>'+
//                  '<td>'+data[i].id_marcas+'</td>'+
//                  '<td contenteditable="true" class="column_name_marcas" data-column_name="nombre_marcas" data-id="'+data[i].id_marcas+'">'+data[i].nombre_marcas+'</td>'+
//                  '</tr>',
//
//                  $(".table_marcas").empty();
//                  $('.table_marcas').append(id_stock);
//          })
//
//
//     }
//   })
// };

</script>

@endsection
