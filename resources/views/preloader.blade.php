<div class="preloader">
  <!-- <video id="video" autoplay preload>
    <source src="../video/preloader2.mp4" type="video/mp4">
  </video> -->
  <img src="{{ asset('img/logo/logo-namopua-paraguay-confecciones-de-jeans-principal-w.png') }}">
</div>

<style>
  .preloader {
    width: 100%;
    height: 100vh;
    background: #e44c65;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 40000;
    display: flex;
    justify-content: center;
    align-content: center;
    flex-direction: column;
    text-align: center;
  }
  .preloader img {
    margin: 0 auto;
    width: 80px;
    margin-top: -60px;
    }
</style>

<script type="text/javascript">
  $(document).ready(function() {
      setTimeout(function() {
          $(".preloader").fadeOut(500);
          //$("#video").fadeOut(500);
      },1000);
  });
</script>
