<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Importacion extends Model
{
    protected $table = 'internacional';

    protected $primaryKey = 'id';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'id',
      'empresa',
      'nombre',
      'correo', 
      'web', 
      'telefono', 
      'pais',
      'mensaje',
      'pdf'
    ];
  
    public static function filterAndPaginatePatients($name)
    {
      $rowsPerPage = 5;
  
      return  Importacion::name($name)
          ->orderBy('id')->get();
    }
  
    public function scopeName($query, $name)
    {
      if($name){
          return $query->where('id_color', 'ilike', '%'.$name.'%');
      }
    }
}
