<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Sliders;
use App\Secciones;
use App\Contenido;
use App\categorias;
use App\subcategorias;
use App\ImagenProductos;
use App\ColorImagen;
use App\Pedido;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;

class HomeController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
    if (!\Session::has('cart')) \Session::put('cart', array());
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */


  public function index(Request $request)
  {

    Log::info('HomeController::index', ["request" => $request->all()]);
    $secciones = Secciones::All();
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All();
    $slider = Sliders::All();
    $imagenProductos = ImagenProductos::getAll2();
    $categorias = categorias::where('active_categorias', true)->get();
    $colorImagen = ColorImagen::colorImagen();
    $cont = Contenido::getAll();
    $user = Auth::user();
    //$session = \Session::get('cart');

    $pedido = DB::table('pedido')
      //->join('detalle_pedido', 'detalle_pedido.id_venta_detalle_pedido', 'pedido.id_pedido')
      ->join('users', 'users.id', 'pedido.user_pedido')
      ->orderby('id_pedido', 'DESC')
      ->paginate(10);

    $pendiente = DB::table('pedido')
      ->join('users', 'users.id', 'pedido.user_pedido')
      ->where('estado_pedido', '=', 'Pendiente')
      ->get();

    $buscar = DB::table('pedido')
      ->join('users', 'users.id', 'pedido.user_pedido')
      ->where('estado_pedido', '=', 'Pasar a Buscar')
      ->get();

    $enviando = DB::table('pedido')
      ->join('users', 'users.id', 'pedido.user_pedido')
      ->where('estado_pedido', '=', 'Enviando')
      ->get();

    $concretado = DB::table('pedido')
      ->join('users', 'users.id', 'pedido.user_pedido')
      ->where('estado_pedido', '=', 'Concretado')
      ->get();

    if ($user->perfil_users == 'usuario') {
      return view('admin.index', compact('pedido', 'pendiente', 'buscar', 'enviando', 'concretado'));
    } elseif ($user->perfil_users == 'cliente') {
      //dd('hola');
      //return view('guest.index', compact('cont', 'colorImagen', 'categorias', 'imagenProductos', 'secciones', 'contenido', 'sliderUnd', 'slider'));
      return back();
    }
  }

  public function enviarDetalle(Request $request)
  {
    return DB::table('detalle_pedido')
      ->join('pedido', 'pedido.id_pedido', 'detalle_pedido.id_venta_detalle_pedido')
      ->where('id_venta_detalle_pedido', '=', $request->id)
      ->orderBy('cod_producto_detalle_pedido', 'ASC')
      ->get();
  }

  function descargarPDF(Request $request)
  {
    $pedido = DB::table('pedido')
      ->join('users', 'users.id', 'pedido.user_pedido')
      ->join('direccion_envio', 'direccion_envio.id_direccion_envio', 'pedido.id_direccion_envio_pedido')
      ->join('datos_factura', 'datos_factura.id_datos_factura', 'pedido.id_datos_factura_envio_pedido')
      ->select([
        'users.name',
        'datos_factura.telefono_datos_factura as telefono',
        'datos_factura.ruc_datos_factura as ruc',
        'direccion_envio.direccion_envio as direccion',
        'direccion_envio.ciudad_direccion_envio as ciudad',
        'pedido.fecha_pedido as fecha',
        'pedido.hora_pedido as hora',
        'pedido.total_pedido as total',
      ])
      ->where('id_pedido', $request->id)
      ->first();
    $mpdf = new Mpdf();
    $now = Date('Y-m-d-h-i');
    $mpdf->WriteHTML('
    <table style="width: 100%; font-family: Roboto, sans-serif; font-size: 20px">
      <tbody>
        <tr>
          <td colspan="3">
            Cliente: ' . $pedido->name . '
          </td>
          <td>
          </td>
          <td colspan="3">
            RUC: ' . $pedido->ruc . '
          </td>
        </tr>
        <tr>
          <td colspan="3">
            Dirección: ' . $pedido->direccion . ' - ' . $pedido->ciudad . '
          </td>
          <td>
          </td>
          <td colspan="3">
            Teléfono: ' . $pedido->telefono . '
          </td>
        </tr>
        <tr>
            <td colspan="7">
              Fecha y hora: ' . $pedido->fecha . ' - ' . $pedido->hora . '
            </td>
        </tr>
        <tr>
          <td colspan="7" style="padding: 20px 0;">
          </td>
        </tr>
        <tr>
            <th style="width: 15%;">Cod.</th>
            <th style="width: 15%;">Cantidad</th>
            <th style="width: 40%;">Descripción</th>
            <th style="width: 15%;">Talla</th>
            <th style="width: 25%;">Color</th>
            <th style="width: 15%;">Precio unitario</th>
            <th style="width: 15%;">Total</th>
        </tr>
        ' . $request->table . '
        <tr>
          <td colspan="7" style="padding: 20px 0;">
          </td>
        </tr>
        <tr>
          <th style="text-align: left; font-size: 20px">
            Total:
          </th>
          <th colspan="6" style="text-align: left; font-size: 20px">Gs. ' . number_format($pedido->total, 0, ',', '.') . '</th>
        </tr>
      </tbody>
    </table>');

    $mpdf->Output(public_path('storage/detalles/') . "detalle-$request->id-$now.pdf");
    // $mpdf->Output("detalle-$request->id-$now.pdf", Destination::INLINE);
    return response()->json([
      'file' => asset("storage/detalles/detalle-$request->id-$now.pdf"),
      'file_name' => "detalle-$request->id-$now.pdf"
    ]);
  }

  public function enviarComprobante(Request $request)
  {
    return DB::table('documentos')
      ->join('pedido', 'pedido.id_pedido', 'documentos.id_pedido_documentos')
      ->where('id_pedido_documentos', '=', $request->id)
      ->get();
  }

  public function modificarProceso(Request $request)
  {
    $id = $request->id_pedido;

    $proceso = pedido::whereid_pedido($id)->firstOrFail();
    $proceso->estado_pedido = $request->proceso;
    $proceso->save();
  }
}
