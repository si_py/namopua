<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Sliders;
use App\Secciones;
use App\Contenido;
use App\Productos;
use App\categorias;
use App\ColorImagen;
use App\subcategorias;
use App\ImagenProductos;
use App\Documentos;
use App\Imagen;
use Image;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class GuestController extends Controller
{
  // public function __construct()
  // {
  //   if(!\Session::has('guest')) \Session::put('guest', array());
  // }


  public function index(Request $request)
  {

    // $imagenes = Imagen::with('colores')->take(1)->get()->toArray();
    // $colores = Productos::with('colores')->take(1)->get()->toArray();
    // $ninos = Productos::with(['imagenes', 'colores'])
    // ->take(6)
    // ->get()->toArray();
    // dd($ninos, $imagenes, $colores);


    // $session = \Session::get('guest');
    //dd($session);
    Log::info('GuestController::index', ["request" => $request->all()]);
    // $secciones = Secciones::All();
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All();
    //$slider = Sliders::orderBy('id_sliders', 'desc')->get();
    // $productos = Productos::All();
    // $imagenProductos = ImagenProductos::getAll2();
    $categorias_id = DB::table('productos')
      ->where('tipo_producto', '!=', 'Olimpia')
      ->where('activo_productos', true)
      ->distinct()
      ->get(['pk_subcategorias_productos'])
      ->toArray();
    $categorias_id = array_column($categorias_id, 'pk_subcategorias_productos');
    $categorias = DB::table('subcategorias')
      ->whereIn('id_subcategorias', $categorias_id)
      ->where('active_subcategorias', true)
      ->where('pk_categorias_subcategorias', '!=', 4)
      ->join('categorias', 'categorias.id_categorias', 'subcategorias.pk_categorias_subcategorias')
      ->get();
    $colorImagen = ColorImagen::colorImagen();
    //$cont = Contenido::getAll();

    $slider = DB::table('sliders')
      ->join('secciones', 'secciones.id_secciones', 'sliders.pk_secciones_sliders')
      ->where('nombre_secciones', '=', 'Slider')
      ->orderBy('orden', 'ASC')
      ->get();

    $cont = DB::table('contenido')
      ->join('secciones', 'id_secciones', 'contenido.pk_secciones_contenido')
      ->where('nombre_secciones', '=', 'historia')
      ->get();

    $color = ColorImagen::all();

    // $masVendidos = DB::table('color_imagen')
    //   ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    //   ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
    //   ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
    //   ->where('activo_productos', '=', true)->take(20)
    //   ->orderBy('id_productos', 'DESC')
    //   ->get();
    // $masVendidos = ColorImagen::imagenesProductos([], null, 20)->get();
    // dd($masVendidos);
    //dd($masVendidos);

    //    foreach ($masVendidos as $v){
    //      echo "<pre>";
    //      echo $v->path_imagen;
    //      echo "</pre>";
    //    }
    //
    //    die();

    // $ofertas = DB::table('color_imagen')
    //   ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    //   ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
    //   ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
    //   ->where('productos.descuentos_productos', '=', 1)
    //   ->where('color_imagen.principal_color_imagen', '=', true)
    //   ->where('seccion_color_imagen', '=', 1)
    //   ->where('activo_productos', '=', true)
    //   ->orderBy('id_productos', 'DESC')
    //   ->take(10)
    //   ->get();
    $ofertas = ColorImagen::imagenesProductos([
      [
        'key' => 'principal_color_imagen',
        'value' => true
      ],
      [
        'key' => 'seccion_color_imagen',
        'value' => 1
      ],
      [
        'key' => 'descuentos_productos',
        'value' => 1
      ]
    ], null, 10)
      ->get();

    $hombres = $this->productos_por_tipo('Hombre');
    $damas = $this->productos_por_tipo('Dama');

    $nina = DB::table('color_imagen')
      ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
      ->where('activo_productos', true)
      ->where('principal_color_imagen', true)
      ->where('tipo_producto', 'Niña')
      ->take(2)
      ->orderBy('id_productos', 'DESC')
      ->orderBy('color_imagen.id_color_imagen', 'ASC')
      ->get();

    $ninos = DB::table('color_imagen')
      ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
      ->where('activo_productos', true)
      ->where('principal_color_imagen', true)
      ->where('tipo_producto', 'Niño')
      ->take(6)
      ->orderBy('id_productos', 'DESC')
      ->orderBy('color_imagen.id_color_imagen', 'ASC')
      ->get();

    $especialesHombre = $this->productos_por_tipo('EspecialesHombre', 2)->toArray();
    $especialesMujer = $this->productos_por_tipo('EspecialMujer', 2)->toArray();

    $especiales = array_merge($especialesHombre, $especialesMujer);

    $olimpia = $this->productos_por_tipo('Olimpia');

    $empleo = DB::table('empleos')->orderBy('nombre', 'ASC')->get();
    return view('guest.index', compact('empleo', 'olimpia', 'especiales', 'ninos', 'nina', 'damas', 'hombres', 'color', 'ofertas', 'cont', 'colorImagen', 'categorias', 'contenido', 'sliderUnd', 'slider'));
  }

  function productos_por_tipo($tipo, $take = 8)
  {
    return ColorImagen::imagenesProductos([
      [
        'key' => 'principal_color_imagen',
        'value' => true
      ],
      [
        'key' => 'seccion_color_imagen',
        'value' => 1
      ],
      [
        'key' => 'tipo_producto',
        'value' => $tipo
      ]
    ], null, $take)
      ->get([
        'codigo_productos',
        'descuentos_productos',
        'mayorista',
        'precio_descuento_productos',
        'nombre_productos',
        'path_imagen',
        'id_productos',
        'premium',
        'lanzamientos_productos'
      ]);
  }


  public function get_categorias()
  {
    $categorias_id = DB::table('productos')
      ->where('tipo_producto', '!=', 'Olimpia')
      ->where('activo_productos', true)
      ->distinct()
      ->get(['pk_subcategorias_productos'])
      ->toArray();
    $categorias_id = array_column($categorias_id, 'pk_subcategorias_productos');
    $categorias = DB::table('subcategorias')
      ->whereIn('id_subcategorias', $categorias_id)
      ->where('active_subcategorias', true)
      ->where('pk_categorias_subcategorias', '!=', 4)
      ->join('categorias', 'categorias.id_categorias', 'subcategorias.pk_categorias_subcategorias')
      ->get();
    return $categorias;
  }

  public function buscarEmpleo(Request $request)
  {
    $empleo_requisito = DB::table('empleos')
      ->where('id', '=', $request->empleo)
      ->first();

    $descripcion = $empleo_requisito->descripcion;

    return $descripcion;
  }

  public function agregarEmpleoGuest(Request $request)
  {
    //dd($request->toArray());
    //Log::info(_FUNCTION_);
    $response['code'] = 500;
    $response['message'] = 'Hubo un error inesperado en guardar de HomeController';
    //dd($request->toArray());
    try {
      $datos = array(
        'nombre' => $request->nombre_empleo,
        'documento' => $request->ci_empleo,
        'telefono' => $request->telefono_empleo,
        'direccion' => $request->direccion_empleo,
        'ciudad' => $request->ciudad_empleo,
        'barrio' => $request->barrio_empleo,
        'experiencia' => $request->experiencia_empleo,
        'descripcion' => $request->descripcion_empleo,
        'pk_empleos' => $request->vigentes_empleo,
        'created_at' => now(),
      );
      $insertId = DB::table('curriculum')->insertGetId($datos);
      if ($insertId > 0) {
        try {

          $pdf = $request->file('cv');
          $cv = $pdf->getClientOriginalName();
          $cv = $request->nombre . '-' . date('Y-m-d_h_i_s') . '-' . $cv;
          if ($pdf->move(public_path('uploads/curriculum'), $cv)) {
            $update = DB::table('curriculum')
              ->where('id', '=', $insertId)
              ->update(['pdf_path' => $cv]);
            if ($update > 0) {

              $response['code'] = 200;
              $response['message'] = 'OK';
            }
          } else {
            $response['code'] = 500;
            $response['message'] = 'No se pudo guardar el CV';
          }
        } catch (\Throwable $th) {
          // Log::info('Error en el segundo TRY de la función guardar de HomeController: '.$th->getMessage());
          $response['code'] = 500;
          $response['message'] = 'Error en el segundo TRY de la función guardar de GuestController: ' . $th->getMessage();
          return $response;
        }
      }
    } catch (\Throwable $th) {
      // Log::info('Error en el primer TRY de la función guardar de HomeController: '.$th->getMessage());
      $response['code'] = 500;
      $response['message'] = 'Error en el primer TRY de la función guardar de GuestController';
    }
    return $response;
  }

  public function historial(Request $request)
  {
    $categorias = $this->get_categorias();

    $user = DB::table('pedido')
      ->join('users', 'users.id', 'pedido.user_pedido')
      ->join('datos_factura', 'datos_factura.id_datos_factura', 'pedido.id_datos_factura_envio_pedido')
      ->join('direccion_envio', 'id_direccion_envio', 'pedido.id_direccion_envio_pedido')
      ->leftjoin('documentos', 'documentos.id_pedido_documentos', 'pedido.id_pedido')
      ->where('id', '=', Auth::user()->id)
      ->orderBy('id_pedido', 'DESC')->paginate(3);

    return view('guest.historial', compact('categorias', 'user'));
  }

  public function buscarDetallePedido(Request $request)
  {
    return DB::table('detalle_pedido')
      ->join('pedido', 'pedido.id_pedido', 'detalle_pedido.id_venta_detalle_pedido')
      ->where('id_pedido', '=', $request->id)
      ->get();
  }


  public function agregarComprobacion(Request $request)
  {
    $image_resize = Image::make($request->file);
    $image_resize->resize(400, null, function ($constraint) {
      $constraint->aspectRatio();
      $constraint->upsize();
    });
    $image_resize->orientate();
    $nombre_archivo = time() . "." . $request->file->extension();
    $image_resize->save(public_path('img/comprobantes/' . $nombre_archivo));

    $documentos = new Documentos;
    $documentos->path_documentos = $nombre_archivo;
    $documentos->id_pedido_documentos = $request->id_pedido_documento;
    $documentos->save();

    return redirect()->route('guest.historial');
  }

  public function micuenta(Request $request)
  {
    if (Auth::user()) {
      $categorias = $this->get_categorias();
      return view('guest.micuenta', compact('categorias'));
    } else {
      return redirect()->back();
    }
  }

  public function editar_datos(Request $request)
  {
    $user = Auth::user();
    $response['success'] = false;
    $response['data'] = [];
    if ($user) {
      $correct_pass = Hash::check($request->pass_confirm, $user->password);
      if ($correct_pass) {
        DB::table('users')
          ->where('id', $user->id)
          ->update([
            'name' => $request->name ?? $user->name,
            'razon_users' => $request->razon_users ?? $user->razon_users,
            'ruc_users' => $request->ruc_users ?? $user->ruc_users,
            'email' => $request->email ?? $user->email,
            'telefono_users' => $request->telefono_users ?? $user->telefono_users,
            'direccion_users' => $request->direccion_users ?? $user->direccion_users,
          ]);
        $user = DB::table('users')->where('id', $user->id)->first([
          'name',
          'razon_users',
          'ruc_users',
          'email',
          'telefono_users',
          'direccion_users'
        ]);
        $response['success'] = true;
        $response['data'] = $user;
        return response()->json($response, 200);
      } else {
        return response()->json($response, 200);
      }
    } else {
      return response()->json($response, 200);
    }
  }

  public function autocompletarInput(Request $request)
  {
    $query = $request['query'];

    if ($request["query"]) {
      //$data = Productos::where('nombre_productos','LIKE','%'.$query.'%')->get();
      // $data = DB::table('productos')
      // ->where('nombre_productos', 'LIKE', '%'.$query.'%')
      // ->get();

      $data = DB::table('color_imagen')
        ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
        ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
        ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
        ->leftjoin('subcategorias', 'subcategorias.id_subcategorias', 'productos.pk_subcategorias_productos')
        ->where('active_subcategorias', true)
        ->where('nombre_productos', 'ILIKE', '%' . $query . '%')
        ->get();

      $output = '<ul class="dropdown-menu" style=" display:block; position: relative; width: 100%; padding: 10px; margin: 0px; background: transparent; border: none;">';

      foreach ($data as $p) {
        if ($p->principal_color_imagen == "true") {
          if ($p->seccion_color_imagen == 1) {

            if ($p->descuentos_productos == true) {
              $etiqueta = 'Oferta';
            } else {
              if ($p->premium == true) {
                $etiqueta = 'Premium';
              } else {
                $etiqueta = "";
              }
            }

            if (is_null($p->precio_descuento_productos)) {
              $precio = number_format($p->mayorista);
            } else {
              $precio = number_format($p->precio_descuento_productos);
            }


            $output .= '<a class="resultado_auto" href="/shop/' . $p->id_productos . '" style="width: 100%;">' .
              '<li style="font-size: 13px;">' .
              '<div class="col-2" style="float: left; padding: 0;">' .
              '<img src="img/productos/' . $p->path_imagen . '" style="width: 100%;">' .
              '</div>' .
              '<div class="col-10" style="float: left; text-align: left;">' .
              '<p style="margin-bottom: 0;">' . $etiqueta . '</p>' .
              '<p style="margin-bottom: 0;">' . $p->nombre_productos . '</p>' .
              '<p style="margin-bottom: 0;"> Codigo: ' . $p->codigo_productos . '</p>' .
              '<p style="margin-bottom: 0;"> Gs. ' . $precio . '</p>' .
              '</div>' .
              '</li>' .
              '</a>';
          }
        }
      }
      $output .= '</ul>';
      return $output;
    }
  }

  public function terminosCondiciones(Request $request)
  {
    $secciones = Secciones::All();
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All();
    $slider = Sliders::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $categorias = $this->get_categorias();
    $colorImagen = ColorImagen::colorImagen();
    $cont = Contenido::getAll();
    return view('paginas.terminos_y_condiciones', compact('cont', 'colorImagen', 'categorias', 'imagenProductos', 'productos', 'secciones', 'contenido', 'sliderUnd', 'slider'));
  }

  public function nosotros(Request $request)
  {
    $secciones = Secciones::All();
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All();
    $slider = Sliders::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $categorias = $this->get_categorias();
    $colorImagen = ColorImagen::colorImagen();
    $cont = Contenido::getAll();
    return view('paginas.nosotros', compact('cont', 'colorImagen', 'categorias', 'imagenProductos', 'productos', 'secciones', 'contenido', 'sliderUnd', 'slider'));
  }

  public function trabajeConNosotros(Request $request)
  {
    $secciones = Secciones::All();
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All();
    $slider = Sliders::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $categorias = $this->get_categorias();
    $colorImagen = ColorImagen::colorImagen();
    $cont = Contenido::getAll();
    return view('paginas.trabaje_con_nosotros', compact('cont', 'colorImagen', 'categorias', 'imagenProductos', 'productos', 'secciones', 'contenido', 'sliderUnd', 'slider'));
  }
}
