<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Sliders;
use App\Secciones;
use App\Contenido;
use App\Divisas;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
class ContenidoController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index(Request $request)
  {
    $secciones = Secciones::All();
    $contenido = Contenido::filterAndPaginatePatients($request->id_contenido);
    $id = '';
    if ($request->name) { $id = $request->id_contenido; };

    

    return view('contenido.index', compact('secciones', 'contenido'));
  }

  public function create()
  {
    $secciones = Secciones::All();
      return view('contenido.create', compact('secciones'));
  }

  public function store(Request $request)
  {
    Log::info("ContenidoController:store", ["request" => $request->all()]);

      $image = $request->file('file');
      $avatarName = $image->getClientOriginalName();
      $image->move(public_path('img'),$avatarName);

      $contenido = new Contenido();
      $contenido->pk_secciones_contenido = $request->pk_secciones_contenido;
      $contenido->titulo_contenido = $request->titulo_contenido;
      $contenido->texto_contenido = $request->texto_contenido;
      $contenido->path_contenido = $avatarName;
      $contenido->created_contenido = auth()->user()->id;
      $contenido->save();

      DB::commit();

      return redirect()->route('contenido.index');

  }

  public function modificarContenido(Request $request) 
  {
    $contenido = DB::table('contenido')
    ->where('id_contenido', '=', $request->id)
    ->first();

    return compact('contenido');
  }


  public function modificarContenidoModal(Request $request) 
  {
    $contenido = Contenido::whereid_contenido($request->id)->firstOrFail();
    $contenido->titulo_contenido = $request->titulo;
    $contenido->pk_secciones_contenido = $request->seccion;
    $contenido->texto_contenido = $request->texto;
    $contenido->save();

  }

  public function verDivisas(Request $request) 
  {
    $divisa = DB::table('divisas')->get();

    return view('divisas.verDivisas', compact('divisa'));
  }

  public function agregarDivisa(Request $request) 
  {
    $divisa = new Divisas();
    $divisa->nombre = $request->nombre; 
    $divisa->prefijo = $request->prefijo;
    $divisa->divisa = $request->divisa;
    $divisa->save();
  }

  public function modificarrDivisa(Request $request) 
  {
    $divisa = Divisas::whereid($request->id)->firstOrFail();
    $divisa->nombre = $request->nombre; 
    $divisa->prefijo = $request->prefijo;
    $divisa->divisa = $request->divisa;
    $divisa->save();
  }


}
