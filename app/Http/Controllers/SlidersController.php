<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Sliders;
use App\Secciones;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class SlidersController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(Request $request)
  {
    $sliders = DB::table('sliders')->orderBy('orden', 'ASC')->get();
    $id = '';
    if ($request->name) {
      $id = $request->id_sliders;
    };
    $secciones = Secciones::All();

    return view('sliders.index', compact('sliders', 'id', 'secciones'));
  }

  public function show()
  {
  }

  public function create()
  {
    $secciones = Secciones::All();
    return view('sliders.create', compact('secciones'));
  }

  public function images(Request $request)
  {
    Log::info("SlidersController:images", ["request" => $request->all()]);

    $image = $request->file('file');
    $avatarName = $image->getClientOriginalName();
    $image->move(public_path('img'), $avatarName);

    $imageM = $request->file('file-mobile');
    $avatarM = $imageM->getClientOriginalName();
    $imageM->move(public_path('img'), $avatarM);

    $imageUpload = new Sliders();
    $imageUpload->path_sliders = $avatarName;
    $imageUpload->path_mobile_sliders = $avatarM;
    $imageUpload->pk_secciones_sliders = $request->pk_secciones_sliders;
    $imageUpload->titulo_sliders = $request->titulo_sliders;
    $imageUpload->namin_titulo_sliders = $request->titulo_main_sliders;
    $imageUpload->descripcion_sliders = $request->descripcion_sliders;
    $imageUpload->orden = $request->orden;
    $imageUpload->created_sliders = auth()->user()->id;
    $imageUpload->save();

    DB::commit();

    return redirect()->route('sliders.index');
  }

  public function eliminarSliders($id)
  {

    $sliders = Sliders::find($id);
    $sliders->delete();
  }

  public function store()
  {
  }
  public function edit(Request $request, $id)
  {
    $slider = Sliders::find($id);
    $deskPath = $slider->path_sliders;
    $mobPath = $slider->path_mobile_sliders;
    $response['success'] = true;
    $response['data'] = [];
    if (!empty($request->file('file'))) {
      $image = $request->file('file');
      $deskPath = $image->getClientOriginalName();
      $image->move(public_path('img'), $deskPath);
    }

    if (!empty($request->file('file-mobile'))) {
      $imageM = $request->file('file-mobile');
      $mobPath = $imageM->getClientOriginalName();
      $imageM->move(public_path('img'), $mobPath);
    }

    $update = DB::table('sliders')
      ->where('id_sliders', $id)
      ->update([
        'path_sliders' => $deskPath,
        'path_mobile_sliders' => $mobPath,
        'pk_secciones_sliders' => $request->pk_secciones_sliders,
        'titulo_sliders' => $request->titulo_sliders,
        'namin_titulo_sliders' => $request->titulo_main_sliders,
        'descripcion_sliders' => $request->descripcion_sliders,
        'orden' => $request->orden,
        'created_sliders' => Auth::user()->id
      ]);
    if ($update) {
      $response['data'] = DB::table('sliders')
        ->where('id_sliders', $id)
        ->join('secciones', 'secciones.id_secciones', 'sliders.pk_secciones_sliders')
        ->first();
    } else {
      $response['success'] = false;
    }
    return response()->json($response);
  }

  // public function update(Request $request)
  // {
  //   Log::info("TallesController::update", ["request" => $request->all()]);
  //
  //    if($request->ajax()){
  //          $response["success"] = true;
  //          $response["message"] = "Actualizacion Exitoso";
  //
  //          $data = [$request->column_name => $request->column_value];
  //          $anthropometry = Talles::find($request->id);
  //          if(!$anthropometry){
  //              $response["success"] = false;
  //              $response["message"] = 'Hubo un Error';
  //             // echo '<div class="alert alert-danger">Hubo un Error</div>';
  //              return $response;
  //          }
  //          $anthropometry->update($data);
  //          return $response;
  //      }
  // }
}
