<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\categorias;
use App\subcategorias;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        // $session = \Session::get('guest');
        //dd($session);
        Log::info('LoginController::index', ["request" => $request->all()]);
        // $secciones = Secciones::All();
        // $contenido = Contenido::All();
        // $sliderUnd = Sliders::All();
        // $slider = Sliders::All();
        // $productos = Productos::All();
        // $imagenProductos = ImagenProductos::getAll2();
        $categorias = subcategorias::where('active_subcategorias', true)->get();
        // $colorImagen = ColorImagen::colorImagen();
        // $cont = Contenido::getAll();
        $user = Auth::user();


        //return view('login.index', compact('cont', 'colorImagen', 'categorias', 'imagenProductos', 'productos', 'secciones', 'contenido', 'sliderUnd', 'slider'));
        return view('iniciar_sesion.index', compact('categorias','user'));

        //return redirect()->route('guest.index');
    }

    
}
