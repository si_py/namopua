<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\categorias;
use App\subcategorias;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class CarritoController extends Controller
{
    public function __construct()
    {
        if(!\Session::has('cart')) \Session::put('cart', array());
    }

    public function index(Request $rquest)
    {
        
        $categorias = subcategorias::where('active_subcategorias', true)->get();
        return view('carrito.index', compact('categorias'));
    }
}
