<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Talles;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TallesController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(Request $request)
  {
    $tallesActivos = DB::table('talles')->where('estado_talles', true)->get();
    $tallesInactivos = DB::table('talles')->where('estado_talles', false)->get();
    $id = '';
    if ($request->name) {
      $id = $request->id_talles;
    };

    return view('talles.index', compact('tallesActivos', 'tallesInactivos', 'id'));
  }

  public function show()
  {
  }

  public function store(Request $request)
  {
    $validation = Validator::make($request->all(), [
      "nombre" => "required|unique:talles,nombre_talles"
    ]);
    $error_messages = $validation->errors()->messages();
    // dd($error_messages);
    if (!empty($error_messages)) {
      return response()->json(['errors' => true, 'data' => null], 200);
    }
    $insert = DB::table('talles')
      ->insert([
        'nombre_talles' => $request->nombre,
        'created_talles' => auth()->user()->id
      ]);
    return response()->json(['erros' => null, 'data' => $insert]);
  }

  public function TallesBuscar()
  {
    return DB::table('talles')->orderBy('id_talles', 'ASC')
      ->get();
  }

  public function update(Request $request)
  {
    Log::info("TallesController::update", ["request" => $request->all()]);
    if (isset($request->nuevo_estado)) {
      DB::table('talles')->where('id_talles', $request->id)->update([
        'estado_talles' => $request->nuevo_estado
      ]);
      $response['success'] = true;
      $response['data'] = DB::table('talles')
        ->where('id_talles', $request->id)->first();
      return response()->json($response);
    }
    if ($request->ajax()) {
      $response["success"] = true;
      $response["message"] = "Actualizacion Exitoso";

      $data = [$request->column_name => $request->column_value];
      $anthropometry = Talles::find($request->id);
      if (!$anthropometry) {
        $response["success"] = false;
        $response["message"] = 'Hubo un Error';
        // echo '<div class="alert alert-danger">Hubo un Error</div>';
        return $response;
      }
      $anthropometry->update($data);
      return $response;
    }
  }
}
