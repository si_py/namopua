<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Secciones;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class SeccionesController extends Controller
{
  /**
 * Create a new controller instance.
 *
 * @return void
 */
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index(Request $request)
  {
     $secciones = Secciones::filterAndPaginatePatients($request->id_secciones);
     $id = '';
     if ($request->name) { $id = $request->id_secciones; };
     $seccion = Secciones::All();

     return view('secciones.index', compact('secciones', 'id', 'seccion'));

  }

  public function show()
  {

  }

  public function store(Request $request)
  {

    $insert = DB::table('secciones')
              ->insert(['nombre_secciones' => $request->nombre,
                        'referencias_secciones' => $request->referencia,
                        'activo_secciones' => 1,
                        'created_secciones' => auth()->user()->id]);
          return response()->json ($insert);
  }

  public function SeccionesBuscar(){
      return DB::table('secciones')->orderBy('id_secciones', 'ASC')
      ->get();
  }

  public function update(Request $request)
  {
    Log::info("SeccionesController::update", ["request" => $request->all()]);

     if($request->ajax()){
           $response["success"] = true;
           $response["message"] = "Actualizacion Exitoso";

           $data = [$request->column_name => $request->column_value];
           $anthropometry = Secciones::find($request->id);
           if(!$anthropometry){
               $response["success"] = false;
               $response["message"] = 'Hubo un Error';
              // echo '<div class="alert alert-danger">Hubo un Error</div>';
               return $response;
           }
           $anthropometry->update($data);
           return $response;
       }
  }
}
