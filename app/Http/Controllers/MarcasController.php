<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Marcas;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class MarcasController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(Request $request)
  {
    $marcasActivas = DB::table('marcas')->where('active_marcas', true)->get();
    $marcasInactivas = DB::table('marcas')->where('active_marcas', false)->get();
    $id = '';
    if ($request->name) {
      $id = $request->id_marcas;
    };

    return view('marcas.index', compact('marcasActivas', 'marcasInactivas', 'id'));
  }

  public function show()
  {
  }

  public function store(Request $request)
  {

    DB::table('marcas')
      ->insert([
        'nombre_marcas' => $request->nombre,
        'active_marcas' => true,
        'created_marcas' => auth()->user()->id
      ]);
    $response = DB::table('marcas')->where('active_marcas', true)->get();
    return response()->json($response);
  }

  public function MarcasBuscar()
  {
    return DB::table('marcas')->where('active_marcas', true)->orderBy('id_marcas', 'ASC')
      ->get();
  }

  public function update(Request $request)
  {
    Log::info("MarcasController::update", ["request" => $request->all()]);

    if ($request->ajax()) {
      $response["success"] = true;
      $response["message"] = "Actualizacion Exitoso";

      if (isset($request->nuevo_estado)) {
        DB::table('marcas')->where('id_marcas', $request->id)->update(['active_marcas' => $request->nuevo_estado]);
        $response['success'] = true;
        $response['data'] = DB::table('marcas')
          ->where('id_marcas', $request->id)->first();
        return response()->json($response);
      } else {
        $data = [$request->column_name => $request->column_value];
        $anthropometry = Marcas::find($request->id);
        if (!$anthropometry) {
          $response["success"] = false;
          $response["message"] = 'Hubo un Error';
          // echo '<div class="alert alert-danger">Hubo un Error</div>';
          return $response;
        }
        $anthropometry->update($data);
        return $response;
      }
    }
  }
}
