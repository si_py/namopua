<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Color;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ColorController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(Request $request)
  {
    $coloresActivos = DB::table('color')->where('estado_color', true)->get();
    $coloresInactivos = DB::table('color')->where('estado_color', false)->get();

    return view('color.index', compact('coloresActivos', 'coloresInactivos'));
  }

  public function show()
  {
  }

  public function store(Request $request)
  {
    $validation = Validator::make($request->all(), [
      "nombre_color" => "required|unique:color,nombre_color",
      "prefijo_color" => "required|unique:color,prefijo_color",
    ]);
    $error_messages = $validation->errors()->messages();
    // dd($error_messages);
    if (!empty($error_messages)) {
      return response()->json(['errors' => true, 'data' => null], 200);
    }

    $insert = DB::table('color')
      ->insert([
        'nombre_color' => $request->nombre_color,
        'prefijo_color' => $request->prefijo_color,
        'created_color' => auth()->user()->id
      ]);
    return response()->json(['erros' => null, 'data' => $insert]);
  }

  public function ColorBuscar()
  {
    $activos = DB::table('color')->orderBy('id_color', 'ASC')->get();
    $inactivos = DB::table('color')->orderBy('id_color', 'ASC')->get();
    return response()->json(['activos' => $activos, 'inactivos' => $inactivos], 200);
  }

  public function update(Request $request)
  {
    Log::info("ColorController::update", ["request" => $request->all()]);

    if (isset($request->nuevo_estado)) {
      DB::table('color')->where('id_color', $request->id)->update([
        'estado_color' => $request->nuevo_estado
      ]);
      $response['success'] = true;
      $response['data'] = DB::table('color')
        ->where('id_color', $request->id)->first();
      return response()->json($response);
    }
    if ($request->ajax()) {
      $response["success"] = true;
      $response["message"] = "Actualizacion Exitoso";

      $data = [$request->column_name => $request->column_value];
      $anthropometry = Color::find($request->id);
      if (!$anthropometry) {
        $response["success"] = false;
        $response["message"] = 'Hubo un Error';
        // echo '<div class="alert alert-danger">Hubo un Error</div>';
        return $response;
      }
      $anthropometry->update($data);
      return $response;
    }
  }
}
