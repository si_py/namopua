<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\categorias;
use App\subcategorias;
use App\DireccionEnvio;
use App\DatosFactura;
use App\Pedido;
use App\DetallePedido;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class CajaController extends Controller
{
    public function __construct()
    {
        if(!\Session::has('cart')) \Session::put('cart', array());
    }

    public function index(Request $rquest)
    {
        $user = auth()->user()->id;
        $direccion = DB::table('direccion_envio')
        ->join('users', 'users.id', 'direccion_envio.user_direccion_envio')
        ->where('user_direccion_envio', '=', $user)
        ->get();
        $categorias = subcategorias::where('active_subcategorias', true)->get();
        $factura = DB::table('datos_factura')
        ->where('user_datos_factura', '=', $user)
        ->get();
        return view('caja.index', compact('categorias', 'direccion', 'factura'));
    }

    public function direccionEnvio(Request $request) 
    {
        $direccion = new DireccionEnvio();
        $direccion->ciudad_direccion_envio = $request->city;
        $direccion->direccion_envio = $request->direccion;
        $direccion->coordenadas_direccion_envio = $request->coords;
        $direccion->referencia_direccion_envio = $request->referencia;
        $direccion->user_direccion_envio = $request->id;
        $direccion->created_direccion_envio = $request->id;
        $direccion->save();
    }

    public function buscarDireccionEnvioLast(Request $request) 
    {
        $dir = DireccionEnvio::All('id_direccion_envio')->last();
        return $dir;
    }

    public function buscarDireccionEnvio(Request $request) 
    {
        return DB::table('direccion_envio')
        ->join('users', 'users.id', 'direccion_envio.user_direccion_envio')
        ->where('user_direccion_envio', '=', $request->id)
        ->get();
    }

    public function agregarFactura(Request $request)
    {
        $factura = new DatosFactura();
        $factura->user_datos_factura = auth()->user()->id;
        $factura->razon_datos_factura = $request->razon;
        $factura->ruc_datos_factura = $request->ruc;
        $factura->telefono_datos_factura = $request->tel;
        $factura->direccion_datos_factura = $request->dir;
        $factura->save();
    }

    public function buscarFacturaLast(Request $request) 
    {
        $fact = DatosFactura::All('id_datos_factura')->last();
        return $fact;
    }

    public function buscarFactura(Request $request) 
    {
        return DB::table('datos_factura')
        ->where('user_datos_factura', '=', $request->id_user)
        ->get();
    }

    public function realizarPedido(Request $request)
    {


        //dd($request->hora);
        if($request->id_factura == null or $request->id_factura == "") {
            
           
            
            $facturaExistente = DB::table('datos_factura')
            ->where('ruc_datos_factura', '=', $request->ruc)
            ->get();

            if(count($facturaExistente) > 0) {
                
                $pedido = new Pedido;
                $pedido->fecha_pedido = $request->fecha;
                $pedido->hora_pedido = $request->hora;
                $pedido->id_direccion_envio_pedido = $request->id_datos_envio;
                $pedido->total_pedido = $request->total_factura;
                $pedido->shipping_pedido = $request->shipping;
                $pedido->user_pedido = $request->id_user;
                $pedido->id_datos_factura_envio_pedido = $facturaExistente[0]->id_datos_factura;
                $pedido->save();

            }else {
                $factura = new DatosFactura();
                $factura->user_datos_factura = auth()->user()->id;
                $factura->razon_datos_factura = $request->razon;
                $factura->ruc_datos_factura = $request->ruc;
                $factura->telefono_datos_factura = $request->telefono;
                $factura->direccion_datos_factura = $request->direccion;
                $factura->save();

                $fa = DatosFactura::All();

                $pedido = new Pedido;
                $pedido->fecha_pedido = $request->fecha;
                $pedido->hora_pedido = $request->hora;
                $pedido->id_direccion_envio_pedido = $request->id_datos_envio;
                $pedido->total_pedido = $request->total_factura;
                $pedido->shipping_pedido = $request->shipping;
                $pedido->user_pedido = $request->id_user;
                $pedido->id_datos_factura_envio_pedido = ($fa->last()->id_datos_factura);
                $pedido->save();
            
            }


                
               // if($f->ruc_datos_factura) {
               //     dd('existe');
                    // $pedido = new Pedido;
                    // $pedido->fecha_pedido = $request->fecha;
                    // $pedido->id_direccion_envio_pedido = $request->id_datos_envio;
                    // $pedido->total_pedido = $request->total_factura;
                    // $pedido->shipping_pedido = $request->shipping;
                    // $pedido->user_pedido = $request->id_user;
                    // $pedido->id_datos_factura_envio_pedido = $facturaExistente[0]->id_datos_factura;
                    // $pedido->save();

               // } else {
               //     dd('no');
                    // $factura = new DatosFactura();
                    // $factura->user_datos_factura = auth()->user()->id;
                    // $factura->razon_datos_factura = $request->razon;
                    // $factura->ruc_datos_factura = $request->ruc;
                    // $factura->telefono_datos_factura = $request->telefono;
                    // $factura->direccion_datos_factura = $request->direccion;
                    // $factura->save();

                    // $fa = DatosFactura::All();

                    // $pedido = new Pedido;
                    // $pedido->fecha_pedido = $request->fecha;
                    // $pedido->id_direccion_envio_pedido = $request->id_datos_envio;
                    // $pedido->total_pedido = $request->total_factura;
                    // $pedido->shipping_pedido = $request->shipping;
                    // $pedido->user_pedido = $request->id_user;
                    // $pedido->id_datos_factura_envio_pedido = ($fa->last()->id_datos_factura);
                    // $pedido->save();
                //}
            


            
        } else {
            $pedido = new Pedido;
            $pedido->fecha_pedido = $request->fecha;
            $pedido->hora_pedido = $request->hora;
            $pedido->id_direccion_envio_pedido = $request->id_datos_envio;
            $pedido->total_pedido = $request->total_factura;
            $pedido->shipping_pedido = $request->shipping;
            $pedido->user_pedido = $request->id_user;
            $pedido->id_datos_factura_envio_pedido = $request->id_factura;
            $pedido->save();
            
        }
        
        $venta = Pedido::All();


        $session = \Session::get('cart');
        //dd($session);
        foreach($session as $key=>$se) {
            if($se['precio_descuento'] == null) {
                $precio = $se['mayoristas'];
            } else {
                $precio = $se['precio_descuento'];
            }
            
            $detalle = new DetallePedido;
            $detalle->cod_producto_detalle_pedido = $se['codigo_productos'];
            $detalle->cantidad_detalle_pedido = $se['cantidad'];
            $detalle->precio_detalle_pedido = $precio;
            $detalle->nombre_producto_detalle_pedido = $se['nombre_productos'];
            $detalle->id_venta_detalle_pedido = ($venta->last()->id_pedido);
            $detalle->detalle_pedido = json_encode($se['detalles']);
            $detalle->save();
        }
    }

    public function pedidoFinalizado(Request $request) 
    {
        $idToDelete = 0;

        $products = session()->pull('cart', []); // Second argument is a default value
   
        foreach($products as $pro) {
            if($key = array_search($idToDelete, array_column($products, 'id_token')) !== false) {
                //dd($products['id_token']);
                unset($products[$key]);
            }
        }


        $categorias = subcategorias::where('active_subcategorias', true)->get();
        
        return view('caja.pedidoFinalizado', compact('categorias'));
        
    }


}
