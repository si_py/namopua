<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\User;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{
  /**
 * Create a new controller instance.
 *
 * @return void
 */
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index(Request $request)
  {
     $user = User::All();
     $id = '';
     if ($request->name) { $id = $request->id; };

     return view('usuario.index', compact('user', 'id'));

  }

  public function show()
  {

  }

  public function store(Request $request)
  {

    Log::info("UsuarioController:store", ["request" => $request->all()]);

    $user = new User();
    $user->name = $request->nombre;
    $user->email = $request->email;
    $user->password = bcrypt($request->password);
    $user->razon_users = $request->razon;
    $user->ruc_users = $request->ruc;
    $user->direccion_users = $request->direccion;
    $user->telefono_users = $request->telefono;
    $user->perfil_users = "usuario";
    $user->estado_users = "activo";

    $user->save();

    DB::commit();

    return redirect()->route('usuario.index');
  }

  public function modificar_usuarioUsuario(Request $request) 
  {
    $u = User::whereid($request->id)->firstOrFail();
    $u->name = $request->nombre;
    $u->email = $request->email;
    $u->razon_users = $request->razon;
    $u->ruc_users = $request->ruc;
    $u->direccion_users = $request->direccion;
    $u->telefono_users = $request->telefono;
    $u->password = bcrypt($request->pass);
    $u->save();
  }

  
}
