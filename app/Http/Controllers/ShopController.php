<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Sliders;
use App\Secciones;
use App\Contenido;
use App\categorias;
use App\subcategorias;
use App\ImagenProductos;
use App\Productos;
use App\Marcas;
use App\ColorImagen;
use App\TallesProductos;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{
  //  /**
  // * Create a new controller instance.
  // *
  // * @return void
  // */
  //  public function __construct()
  //  {
  //      $this->middleware('auth');
  //  }

  public function __construct()
  {
    if (!\Session::has('cart')) \Session::put('cart', array());
  }

  public function index(Request $request)
  {
    $session = \Session::get('cart');
    $secciones = Secciones::All();
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All()->take(1);
    $slider = Sliders::All();
    $productos = DB::table('productos')
      ->orderBy('id_productos', 'DESC')
      ->get();
    $imagenProductos = ImagenProductos::getAll2();
    $cat = categorias::where('active_categorias', true)->orderBy('nombre_categorias', 'ASC')->get();
    //$categorias = Subcategorias::orderBy('nombre_subcategorias', 'ASC')->get();
    $marca = Marcas::orderBy('nombre_marcas')->where('active_marcas', true)->get();
    $imagenesProductos = $this->getChunk($request, false);
    // dd($imagenesProductos);
    $colorImagen = ColorImagen::colorImagen();

    // $categorias = DB::table('subcategorias')
    //   ->join('categorias', 'categorias.id_categorias', 'subcategorias.pk_categorias_subcategorias')
    //   ->where('active_categorias', true)
    //   ->where('active_subcategorias', true)
    //   ->get();
    $product_ids = DB::table('productos')
      ->get('pk_subcategorias_productos')
      ->toArray();
    $product_ids = array_column($product_ids, 'pk_subcategorias_productos');

    $categorias = $this->get_categorias();


    $descuentosCount = DB::table('productos')
      ->where('descuentos_productos', true)->get()->count();
    $lanzamientosCount = DB::table('productos')
      ->where('lanzamientos_productos', true)->get()->count();
    $premiumCount = DB::table('productos')
      ->where('premium', true)->get()->count();
    return view('shop.index', compact('session', 'imagenesProductos', 'descuentosCount', 'lanzamientosCount', 'premiumCount', 'colorImagen', 'marca', 'cat', 'categorias', 'imagenProductos', 'productos', 'secciones', 'contenido', 'sliderUnd', 'slider'));
  }
  public function getChunk(Request $request, $return_view = true, $tipo = null)
  {
    $products = ColorImagen::imagenesProductos()
      ->when($request, function ($query, $request) {
        if ($request->tipo != null) {
          return $query->where('tipo_producto', $request->tipo);
        } else {
          return $query
            ->where('tipo_producto', '!=', 'Olimpia');
        }
      })
      ->where('principal_color_imagen', true)
      ->where('seccion_color_imagen', 1)
      ->paginate(12);
    if ($return_view) {
      $colorImagen = ColorImagen::colorImagen();
      $response = "";
      foreach ($products as $p) {
        $response .= view('shop.product-card', compact('p', 'colorImagen'));
      }
      return response($response, 200);
    } else {
      return $products;
    }
  }

  public function show(Request $request, $id)
  {
    $session = \Session::get('cart');
    $productos = Productos::findOrFail($id);
    $cat = categorias::where('active_categorias', true)->get();
    $productosColor = Productos::productosColor();
    $productosImagen = Productos::productosImagen();
    $params = [
      ['key' => 'pk_productos_color_imagen', 'value' => $id]
    ];
    if ($request->color) {
      $params[] = ['key' => 'pk_color_color_imagen', 'value' => $request->color];
    } else {
      $params[] = ['key' => 'seccion_color_imagen', 'value' => 1];
    }

    $tipo = $productos->tipo_producto;

    //dd($params);

    $imagenesProductos = ColorImagen::imagenesProductos($params)->get();

    //dd($imagenesProductos);

    $colorImagen = DB::table('color_imagen')
      ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      ->where('pk_productos_color_imagen', $id)
      // ->when($request->color, function($query, $color){
      //   if($color){
      //     return $query->where('pk_color_color_imagen', $color);
      //   }
      // })
      ->where('principal_color_imagen', true)
      ->where('color_imagen.activo', '=', true)
      ->orderBy('color_imagen.seccion_color_imagen', 'ASC')
      // ->orderBy('id_color_imagen', 'ASC')
      ->get();
    $categorias = $this->get_categorias();
    $imagen_principal = DB::table('color_imagen')
      ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      ->where('pk_productos_color_imagen', $id)
      ->where('principal_color_imagen', true)
      ->when($request->color, function ($query, $color) {
        if ($color) {
          return $query->where('pk_color_color_imagen', $color);
        }
        return $query->where('seccion_color_imagen', 1);
      })
      ->orderBy('color_imagen.seccion_color_imagen', 'ASC')
      // ->orderBy('id_color_imagen', 'ASC')
      ->first();
    // dd($imagen_principal);
    // $talles = DB::table('talles')->get(['id_talles', 'nombre_talles']);
    // dd($imagenesProductos);
    $ids = array_column($colorImagen->toArray(), 'pk_color_color_imagen');
    $ids = array_unique($ids);
    $orders = array_map(function ($item) {
      return "pk_color_productos = {$item} desc";
    }, $ids);
    $rawOrder = implode(', ', $orders);
    $tallesProductoTable = DB::table('color_productos')
      ->where('pk_productos_color_productos', $id)
      ->whereIn('pk_color_productos', $ids)
      ->orderByRaw($rawOrder)
      ->get(['tamanos', 'pk_color_productos'])->toArray();
    // $tallesProductoTable = array_column($tallesProductoTable, 'tamanos');
    $talles = [];
    $tallesProducto = [];
    foreach ($tallesProductoTable as $talle) {
      $tallesProducto[] = $talle;
      $talles[] = json_decode($talle->tamanos);
    }

    $final_talles = [];
    foreach ($talles as $talle) {
      if ($talle) {
        $final_talles = array_merge($final_talles, $talle);
      }
    }
    $talles =  array_values(array_unique($final_talles));
    $subcate = DB::table('subcategorias')
      ->select('id_subcategorias', 'nombre_subcategorias')
      ->where('id_subcategorias', "=", $productos->pk_subcategorias_productos)
      ->where('active_subcategorias', true)
      ->first();
    $talles = DB::table('talles')
      ->whereIn('id_talles', $talles)
      ->where('estado_talles', true)
      ->get(['id_talles', 'nombre_talles']);

    // $masVendidos = DB::table('color_imagen')
    //   ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    //   ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
    //   ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
    //   ->join('subcategorias', 'subcategorias.id_subcategorias', 'productos.pk_subcategorias_productos')
    //   ->where('active_subcategorias', true)
    //   ->where('activo_productos', '=', true)
    //   ->where('id_productos', '!=', $id)
    //   ->where('principal_color_imagen', true)
    //   ->where('seccion_color_imagen', 1)
    //   ->where('pk_subcategorias_productos', $productos->pk_subcategorias_productos)
    //   ->orderBy('id_productos', 'DESC')
    //   ->take(15)->get();

    // $coloresRelacionados = DB::table('color_imagen')
    // ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    // ->get();

    $damas = $this->productos_por_tipo($tipo, 20, [], []);

    //dd($coloresRelacionados);
    $divisa = DB::table('divisas')->first();

    // dd($masVendidos, $masVendidos->total());
    //dd($session);
    return view('shop.productos', compact('divisa', 'tallesProducto','damas', 'talles', 'categorias', 'imagen_principal', 'imagenesProductos', 'subcate', 'session', 'talles', 'cat', 'colorImagen', 'productosImagen', 'productosColor', 'productos'));
  }

  public function BuscarImagenes($color, $productos)
  {
    $response['images'] = array_reverse(DB::table('color_imagen')
      ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
      ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      ->where('pk_color_color_imagen', 'LIKE', $color)
      ->where('pk_productos_color_imagen', 'LIKE', $productos)
      ->orderBy('principal_color_imagen', 'ASC')
      ->orderBy('id_color_imagen', 'DESC')
      ->get()->toArray());
    $tamanos = DB::table('color_productos')->where('pk_productos_color_productos', $productos)->where('pk_color_productos', $color)->first(['tamanos'])->tamanos ?? [];
    $tallas = DB::table('talles')->get(['id_talles', 'nombre_talles']);
    $response['tallas'] = [];
    if ($tamanos != null) {
      foreach ($tallas as $talla) {
        if (preg_match("/\b$talla->id_talles\b/i", $tamanos)) {
          $response['tallas'][] = $talla;
        }
      }
    }
    return response()->json($response, 200);
  }

  public function sesion()
  {
    return \Session::get('cart');
  }

  public function add(Request $request, $id)
  {
    $producto = Productos::where('id_productos', $id)->get();

    foreach ($producto as $p) {
      $id_pro = $p;
      //dd($p->id_productos);
      $carrito = $request->session()->get('cart');
      $cart = Session::push('cart', $id_pro);
    }
    $valor_almacenado = session('cart');

    return redirect('shop');
  }

  public function agregarCarrito(Request $request)
  {
    $id_pro = $request->id_productos;
    // $id_color = $request->id_color_imagen;
    $precio = $request->precio;
    // $talla = DB::table('talles')->where('id_talles', $request->talla)->first(['nombre_talles'])->nombre_talles;
    $cantidad = $request->cantidad;
    $tipo = $request->tipo;
    $producto = Productos::where('id_productos', $id_pro)->get();
    $id_colores = [];
    $colores = [];
    $tallas = array_column($request->detalles, 'talla');
    $detalles = [];
    foreach ($request->detalles as $detalle) {
      $id_colores[] = $detalle['color'];
      $colores[] = $detalle['nombre_color'];
      $detalles[] = $detalle;
    }
    $colores = array_unique($colores);
    $id_colores = array_values(array_unique($id_colores));
    $tallas = array_values(array_unique($tallas));
    $producto = DB::table('color_imagen')
      ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
      ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      ->where('pk_productos_color_imagen', $id_pro)
      // ->where('id_color_imagen', $id_colores[0])
      ->first();
    // foreach ($producto as $key => $p) {

    if ($producto->precio_descuento_productos == null) {
      $total = $producto->mayorista * $cantidad;
    } else {
      $total = $producto->precio_descuento_productos * $cantidad;
    }
    $array = ([
      "id_token" => random_int(10000, 99999),
      "id_productos" => $producto->id_productos,
      "nombre_productos" => $producto->nombre_productos,
      "codigo_productos" => $producto->codigo_productos,
      "imagen_producto" => $producto->path_imagen,
      "colores" => $colores,
      "cantidad" => $cantidad,
      "tallas" => $tallas,
      "precio_descuento" => $producto->precio_descuento_productos,
      "mayoristas" => $producto->mayorista,
      "total" => $total,
      "detalles" => $detalles,
      "tipo" => $tipo,
    ]);

    //agregamos el item al carrito
    Session::push('cart', $array);
    // }

    $valor_almacenado = session('cart');

    return $valor_almacenado;
  }


  function removerItemCarrito(Request $request)
  {
    $idToDelete = $request->key;

    $products = session()->pull('cart', []); // Second argument is a default value

    $key = array_search($idToDelete, array_column($products, 'id_token'));

    if ($key !== false) {
      unset($products[$key]);
    }

    session()->put('cart', $products);

    $valor_almacenado = session('cart');
    return $valor_almacenado;
  }

  public function categorias(Request $request, $id)
  {
    $busquedaProductos = DB::table('productos')->get();

    $category = categorias::findOrFail($id);
    $session = \Session::get('cart');
    $secciones = Secciones::All();
    // $contenido = Contenido::All();
    // $sliderUnd = Sliders::All()->take(1);
    // $slider = Sliders::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $cat = categorias::where('active_categorias', true)->get();

    $categorias = $this->get_categorias();
    $marca = Marcas::where('active_marcas', true)->get();

    $imagenesProductos = ColorImagen::imagenesProductos([['key' => 'pk_categorias_productos', 'value' => $id]])->get();

    $colorImagen = ColorImagen::colorImagen();
    // $colorImagen = DB::table('color_imagen')
    //   ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    //   ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
    //   ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
    //   ->leftjoin('categorias', 'categorias.id_categorias', 'productos.pk_categorias_productos')
    //   ->where('pk_categorias_productos', '=', $id)
    //   ->where('active_categorias', true)
    //   ->where('activo_productos', '=', true)
    //   ->orderBy('id_productos', 'DESC')
    //   ->get();

    $descuentosCount = DB::table('productos')
      ->where('descuentos_productos', true)->get()->count();
    $lanzamientosCount = DB::table('productos')
      ->where('lanzamientos_productos', true)->get()->count();
    $premiumCount = DB::table('productos')
      ->where('premium', true)->get()->count();
    return view('shop.buscarProductosCategorias', compact('category', 'imagenesProductos', 'descuentosCount', 'lanzamientosCount', 'premiumCount', 'cat', 'categorias', 'marca', 'imagenProductos', 'colorImagen', 'productos', 'session'));
  }

  public function get_categorias()
  {
    $categorias_id = DB::table('productos')
      ->where('tipo_producto', '!=', 'Olimpia')
      ->where('activo_productos', true)
      ->distinct()
      ->get(['pk_subcategorias_productos'])
      ->toArray();
    $categorias_id = array_column($categorias_id, 'pk_subcategorias_productos');
    $categorias = DB::table('subcategorias')
      ->whereIn('id_subcategorias', $categorias_id)
      ->where('active_subcategorias', true)
      ->where('pk_categorias_subcategorias', '!=', 4)
      ->join('categorias', 'categorias.id_categorias', 'subcategorias.pk_categorias_subcategorias')
      ->get();
    return $categorias;
  }

  public function subcategorias(Request $request, $id)
  {
    $busquedaProductos = DB::table('productos')->get();

    $subcat = Subcategorias::findOrFail($id);
    $session = \Session::get('cart');
    $secciones = Secciones::All();
    // $contenido = Contenido::All();
    // $sliderUnd = Sliders::All()->take(1);
    // $slider = Sliders::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $cat = categorias::where('active_categorias', true)->get();
    $categorias = $this->get_categorias();

    $marca = Marcas::where('active_marcas', true)->get();
    $imagenesProductos = ColorImagen::imagenesProductos([['key' => 'pk_subcategorias_productos', 'value' => $id]])->get();
    // dd($imagenesProductos);
    // dd($imagenesProductos);
    $colorImagen = ColorImagen::colorImagen();
    // $colorImagen = DB::table('color_imagen')
    //   ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    //   ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
    //   ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
    //   ->leftjoin('subcategorias', 'subcategorias.id_subcategorias', 'productos.pk_subcategorias_productos')
    //   ->where('active_subcategorias', true)
    //   ->where('pk_subcategorias_productos', '=', $id)
    //   ->where('activo_productos', '=', true)
    //   ->orderBy('id_productos', 'DESC')
    //   ->get();
    //dd($colorImagen);

    $descuentosCount = DB::table('productos')
      ->where('descuentos_productos', true)->get()->count();
    $lanzamientosCount = DB::table('productos')
      ->where('lanzamientos_productos', true)->get()->count();
    $premiumCount = DB::table('productos')
      ->where('premium', true)->get()->count();
    return view('shop.buscarProductosSubCategorias', compact('subcat', 'imagenesProductos', 'descuentosCount', 'lanzamientosCount', 'premiumCount', 'cat', 'categorias', 'marca', 'imagenProductos', 'colorImagen', 'productos', 'session'));
  }

  public function marcas(Request $request, $id)
  {
    $busquedaProductos = DB::table('productos')->get();

    $marc = Marcas::findOrFail($id);
    $session = \Session::get('cart');
    $secciones = Secciones::All();
    // $contenido = Contenido::All();
    // $sliderUnd = Sliders::All()->take(1);
    // $slider = Sliders::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $cat = categorias::where('active_categorias', true)->get();
    $categorias = $this->get_categorias();

    $marca = Marcas::where('active_marcas', true)->get();

    $imagenesProductos = ColorImagen::imagenesProductos([['key' => 'pk_marcas_productos', 'value' => $id]])->get();

    $colorImagen = ColorImagen::colorImagen();
    // $colorImagen = DB::table('color_imagen')
    //   ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    //   ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
    //   ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
    //   ->leftjoin('marcas', 'marcas.id_marcas', 'productos.pk_marcas_productos')
    //   ->where('pk_marcas_productos', '=', $id)
    //   ->where('active_marcas', true)
    //   ->where('activo_productos', '=', true)
    //   ->orderBy('id_productos', 'DESC')
    //   ->get();
    //dd($colorImagen);

    $descuentosCount = DB::table('productos')
      ->where('descuentos_productos', true)->get()->count();
    $lanzamientosCount = DB::table('productos')
      ->where('lanzamientos_productos', true)->get()->count();
    $premiumCount = DB::table('productos')
      ->where('premium', true)->get()->count();
    return view('shop.buscarMarcas', compact('marc', 'descuentosCount', 'imagenesProductos', 'lanzamientosCount', 'premiumCount', 'cat', 'categorias', 'marca', 'imagenProductos', 'colorImagen', 'productos', 'session'));
  }

  public function premium(Request $request)
  {
    $busquedaProductos = DB::table('productos')->get();

    $session = \Session::get('cart');
    $secciones = Secciones::All();
    // $contenido = Contenido::All();
    // $sliderUnd = Sliders::All()->take(1);
    // $slider = Sliders::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $cat = categorias::where('active_categorias', true)->get();
    $categorias = $this->get_categorias();

    $marca = Marcas::where('active_marcas', true)->get();
    $imagenesProductos = ColorImagen::imagenesProductos([['key' => 'premium', 'value' => true]])->get();


    $resultado = 'Premium';

    $colorImagen = ColorImagen::colorImagen();
    // $colorImagen = DB::table('color_imagen')
    //   ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    //   ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
    //   ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
    //   ->where('premium', '=', true)
    //   ->where('activo_productos', '=', true)
    //   ->orderBy('id_productos', 'DESC')
    //   ->get();
    //dd($colorImagen);

    $descuentosCount = DB::table('productos')
      ->where('descuentos_productos', true)->get()->count();
    $lanzamientosCount = DB::table('productos')
      ->where('lanzamientos_productos', true)->get()->count();
    $premiumCount = DB::table('productos')
      ->where('premium', true)->get()->count();
    return view('shop.buscarPremium', compact('cat', 'descuentosCount', 'imagenesProductos', 'lanzamientosCount', 'premiumCount', 'categorias', 'marca', 'imagenProductos', 'colorImagen', 'productos', 'session', 'resultado'));
  }
  public function lanzamientos(Request $request)
  {
    $busquedaProductos = DB::table('productos')->get();

    $session = \Session::get('cart');
    $secciones = Secciones::All();
    // $contenido = Contenido::All();
    // $sliderUnd = Sliders::All()->take(1);
    // $slider = Sliders::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $cat = categorias::where('active_categorias', true)->get();
    $categorias = $this->get_categorias();

    $marca = Marcas::where('active_marcas', true)->get();

    $resultado = 'Lanzamientos';
    $imagenesProductos = ColorImagen::imagenesProductos([['key' => 'lanzamientos_productos', 'value' => true]])->get();


    $colorImagen = ColorImagen::colorImagen();
    // $colorImagen = DB::table('color_imagen')
    //   ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    //   ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
    //   ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
    //   ->where('lanzamientos_productos', '=', true)
    //   ->where('activo_productos', '=', true)
    //   ->orderBy('id_productos', 'DESC')
    //   ->get();
    //dd($colorImagen);

    $descuentosCount = DB::table('productos')
      ->where('descuentos_productos', true)->get()->count();
    $lanzamientosCount = DB::table('productos')
      ->where('lanzamientos_productos', true)->get()->count();
    $premiumCount = DB::table('productos')
      ->where('premium', true)->get()->count();
    return view('shop.buscarLanzamientos', compact('cat', 'descuentosCount', 'imagenesProductos', 'lanzamientosCount', 'premiumCount', 'categorias', 'marca', 'imagenProductos', 'colorImagen', 'productos', 'session', 'resultado'));
  }

  public function ofertas(Request $request)
  {
    $busquedaProductos = DB::table('productos')->get();

    $session = \Session::get('cart');
    $secciones = Secciones::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $cat = categorias::where('active_categorias', true)->get();
    $categorias = $this->get_categorias();

    $marca = Marcas::where('active_marcas', true)->get();

    $resultado = 'Ofertas';
    $imagenesProductos = ColorImagen::imagenesProductos([['key' => 'descuentos_productos', 'value' => true]])->get();


    $colorImagen = ColorImagen::colorImagen();
    // $colorImagen = DB::table('color_imagen')
    //   ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    //   ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
    //   ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
    //   ->where('descuentos_productos', '=', true)
    //   ->where('activo_productos', '=', true)
    //   ->orderBy('id_productos', 'DESC')
    //   ->get();
    //dd($colorImagen);

    $descuentosCount = DB::table('productos')
      ->where('descuentos_productos', true)->get()->count();
    $lanzamientosCount = DB::table('productos')
      ->where('lanzamientos_productos', true)->get()->count();
    $premiumCount = DB::table('productos')
      ->where('premium', true)->get()->count();
    return view('shop.buscarOfertas', compact('cat', 'descuentosCount', 'lanzamientosCount', 'imagenesProductos', 'premiumCount', 'categorias', 'marca', 'imagenProductos', 'colorImagen', 'productos', 'session', 'resultado'));
  }

  public function genero(Request $request)
  {
    //dd($request->toArray());

    if (count($request->toArray()) > 0) {
      $busquedaProductos = DB::table('productos')->get();

      $session = \Session::get('cart');
      $secciones = Secciones::All();
      $productos = Productos::All();
      $imagenProductos = ImagenProductos::getAll2();
      $cat = categorias::where('active_categorias', true)->get();
      $categorias = $this->get_categorias();

      $marca = Marcas::where('active_marcas', true)->get();

      // $imagenesProductos = ColorImagen::imagenesProductos();
      $resultado = $request->texto;
      // if($resultado == "tallesEspeciales"){
      //   $colorImagen = DB::table('color_imagen')
      //     ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      //     ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      //     ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
      //     ->where('tipo_producto', '=', "EspecialesHombre")
      //     ->orWhere('tipo_producto', '=', "EspecialMujer")
      //     ->where('activo_productos', '=', true)
      //     ->orderBy('id_productos', 'DESC')
      //     ->get();
      // }else{
      //   $colorImagen = DB::table('color_imagen')
      //     ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      //     ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      //     ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
      //     ->where('tipo_producto', '=', $request->texto)
      //     ->where('activo_productos', '=', true)
      //     ->orderBy('id_productos', 'DESC')
      //     ->get();
      // }
      if ($resultado == 'tallesEspeciales') {
        $imagenesProductos = DB::table('color_imagen')
          ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
          ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
          ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
          ->where('tipo_producto', '=', 'EspecialesHombre')
          ->where('activo_productos', '=', true)
          ->orderBy('id_productos', 'DESC')
          ->get()->toArray();
        $imagenesProductos = array_merge($imagenesProductos, DB::table('color_imagen')
          ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
          ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
          ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
          ->where('tipo_producto', '=', 'EspecialMujer')
          ->where('activo_productos', '=', true)
          ->orderBy('id_productos', 'DESC')
          ->get()->toArray());
      } else {
        $imagenesProductos = DB::table('color_imagen')
          ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
          ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
          ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
          ->when($resultado, function ($query, $resultado) {
            if ($resultado == "Ninos") {
              return $query->where('tipo_producto', '=', 'Niño')->orWhere('tipo_producto', '=', 'Niña');
            }
            if ($resultado == "tallesEspeciales") {
              return $query->where('tipo_producto', '=', 'EspecialesHombre')->orWhere('tipo_producto', '=', 'EspecialMujer');
            }
            return $query->where('tipo_producto', '=', $resultado);
          })
          ->where('activo_productos', '=', true)
          ->orderBy('id_productos', 'DESC')
          ->get();
      }
      $colorImagen = ColorImagen::colorImagen();
      $descuentosCount = DB::table('productos')
        ->where('descuentos_productos', true)->get()->count();
      $lanzamientosCount = DB::table('productos')
        ->where('lanzamientos_productos', true)->get()->count();
      $premiumCount = DB::table('productos')
        ->where('premium', true)->get()->count();
      return view('shop.buscarGenero', compact('cat', 'descuentosCount', 'lanzamientosCount', 'imagenesProductos', 'premiumCount', 'categorias', 'marca', 'imagenProductos', 'colorImagen', 'productos', 'session', 'resultado'));
    } else {
      return redirect('shop');
    }
  }

  public function BuscarShopGenero($genero)
  {
    $busquedaProductos = DB::table('productos')->get();

    $session = \Session::get('cart');
    $secciones = Secciones::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $cat = categorias::where('active_categorias', true)->get();
    $categorias = $this->get_categorias();

    $marca = Marcas::where('active_marcas', true)->get();

    $resultado = $genero;
    $imagenesProductos = ColorImagen::imagenesProductos([['key' => 'tipo_producto', 'value' => $genero]])->get();


    $colorImagen = ColorImagen::colorImagen();
    // $colorImagen = DB::table('color_imagen')
    //   ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    //   ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
    //   ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
    //   ->where('tipo_producto', '=', $genero)
    //   ->where('activo_productos', '=', true)
    //   ->orderBy('id_productos', 'DESC')
    //   ->get();

    $descuentosCount = DB::table('productos')
      ->where('descuentos_productos', true)->get()->count();
    $lanzamientosCount = DB::table('productos')
      ->where('lanzamientos_productos', true)->get()->count();
    $premiumCount = DB::table('productos')
      ->where('premium', true)->get()->count();
    return view('shop.BuscarShopGenero', compact('cat', 'descuentosCount', 'imagenesProductos', 'lanzamientosCount', 'premiumCount', 'categorias', 'marca', 'imagenProductos', 'colorImagen', 'productos', 'session', 'resultado'));
  }

  public function BuscarShopGeneroMain($genero, $id)
  {
    $busquedaProductos = DB::table('productos')->get();

    $session = \Session::get('cart');
    $secciones = Secciones::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $cat = categorias::where('active_categorias', true)->get();
    $categorias = $this->get_categorias();

    $marca = Marcas::where('active_marcas', true)->get();

    $resultado = $genero;
    $id_subcat = DB::table('subcategorias')
      ->where('id_subcategorias', '=', $id)
      ->where('active_subcategorias', true)
      ->get();
    if ($genero == 'TallasEspeciales') {
      $especialesHombre = ColorImagen::imagenesProductos(
        [
          ['key' => 'pk_subcategorias_productos', 'value' => $id],
          ['key' => 'tipo_producto', 'value' => 'EspecialesHombre'],
        ],
      )
        ->get()->toArray();
      $especialMujer = ColorImagen::imagenesProductos(
        [
          ['key' => 'pk_subcategorias_productos', 'value' => $id],
          ['key' => 'tipo_producto', 'value' => 'EspecialMujer'],
        ],
      )
        ->get()->toArray();
      $imagenesProductos = array_merge($especialesHombre, $especialMujer);
    } else {
      $imagenesProductos = ColorImagen::imagenesProductos([['key' => 'pk_subcategorias_productos', 'value' => $id], ['key' => 'tipo_producto', 'value' => $genero]])->get();
    }

    $colorImagen = ColorImagen::colorImagen();
    // $colorImagen = DB::table('color_imagen')
    //   ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    //   ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
    //   ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
    //   ->leftjoin('subcategorias', 'subcategorias.id_subcategorias', 'productos.pk_subcategorias_productos')
    //   ->where('tipo_producto', '=', $genero)
    //   ->where('id_subcategorias', '=', $id)
    //   ->where('active_subcategorias', true)
    //   ->where('activo_productos', '=', true)
    //   ->orderBy('id_productos', 'DESC')
    //   ->get();

    $descuentosCount = DB::table('productos')
      ->where('descuentos_productos', true)->get()->count();
    $lanzamientosCount = DB::table('productos')
      ->where('lanzamientos_productos', true)->get()->count();
    $premiumCount = DB::table('productos')
      ->where('premium', true)->get()->count();
    return view('shop.buscarGeneroMain', compact('id_subcat', 'descuentosCount', 'imagenesProductos', 'lanzamientosCount', 'premiumCount', 'cat', 'categorias', 'marca', 'imagenProductos', 'colorImagen', 'productos', 'session', 'resultado'));
  }
  public function tiendaOlimpia(Request $request)
  {
    $session = \Session::get('cart');
    $secciones = Secciones::All();
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All()->take(1);

    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();

    $slider = DB::table('sliders')
      ->join('secciones', 'secciones.id_secciones', 'sliders.pk_secciones_sliders')
      ->where('nombre_secciones', '=', 'Olimpia')
      ->get();

    $cat = categorias::orderBy('nombre_categorias', 'ASC')->get();
    //$categorias = Subcategorias::orderBy('nombre_subcategorias', 'ASC')->get();
    $marca = Marcas::where('active_marcas', true)->orderBy('nombre_marcas')->get();
    $colorImagen = ColorImagen::colorImagen();
    // $imagenesProductos = ColorImagen::imagenesProductos()->get();
    $request->tipo = 'Olimpia';
    $imagenesProductos = $this->getChunk($request, false, 'Olimpia');
    $categorias = $this->get_categorias();


    return view('shop.shopOlimpia', compact('session', 'colorImagen', 'marca', 'cat', 'imagenesProductos', 'categorias', 'imagenProductos', 'productos', 'secciones', 'contenido', 'sliderUnd', 'slider'));
  }


  function productos_por_tipo($tipo, $take = 8, $wheres = [], $other_types = [])
  {
    $productos = Productos::with(['colores', 'imagen_principal'])
      ->select([
        'id_productos',
        'nombre_productos',
        'codigo_productos',
        'tipo_producto',
        'lanzamientos_productos',
        'minorista',
        'mayorista',
        'premium',
        'descuentos_productos',
        'precio_descuento_productos',
      ])
      ->where('activo_productos', true)
      ->when($tipo, function ($query, $tipo) {
        if ($tipo != null) {
          $query->where('tipo_producto', $tipo);
        }
      })
      ->whereExists(function ($query) {
        return $query->select(DB::raw(1))
          ->from('color_imagen')
          ->whereColumn('color_imagen.pk_productos_color_imagen', 'productos.id_productos');
      })
      ->when($take, function ($query, $take) {
        if ($take != null) {
          $query->take($take);
        }
      })
      ->orderBy('id_productos', 'DESC');

    foreach ($other_types as $value) {
      $productos->orWhere('tipo_producto', $value);
    }
    foreach ($wheres as $key => $value) {
      $productos->where($key, $value);
    }

    return $productos->get()->toArray();

  }
}




