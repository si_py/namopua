<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Sliders;
use App\Secciones;
use App\Contenido;
use App\categorias;
use App\ImagenProductos;
use App\ColorImagen;
use App\subcategorias;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class ContactosController extends Controller
{
  public function index(Request $request)
  {
    $secciones = Secciones::All();
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All();
    $slider = Sliders::All();
    $imagenProductos = ImagenProductos::getAll2();
    $categorias = subcategorias::where('active_subcategorias', true)->get();
    $colorImagen = ColorImagen::colorImagen();
    $cont = Contenido::getAll();
    $user = Auth::user();
    
    return view('contactos.index', compact('cont', 'colorImagen', 'categorias', 'imagenProductos', 'secciones', 'contenido', 'sliderUnd', 'slider'));

  }
}
