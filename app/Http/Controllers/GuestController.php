<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Sliders;
use App\Secciones;
use App\Contenido;
use App\Productos;
use App\categorias;
use App\ColorImagen;
use App\subcategorias;
use App\ImagenProductos;
use App\Documentos;
use App\Imagen;
use App\Importacion;
use Image;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class GuestController extends Controller
{

  public function index(Request $request)
  {

    Log::info('GuestController::index', ["request" => $request->all()]);
    
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All();
   
    $categorias_id = DB::table('productos')
      ->where('tipo_producto', '!=', 'Olimpia')
      ->where('activo_productos', true)
      ->distinct()
      ->get(['pk_subcategorias_productos'])
      ->toArray();
    $categorias_id = array_column($categorias_id, 'pk_subcategorias_productos');
    $categorias = DB::table('subcategorias')
      ->whereIn('id_subcategorias', $categorias_id)
      ->where('active_subcategorias', true)
      ->where('pk_categorias_subcategorias', '!=', 4)
      ->join('categorias', 'categorias.id_categorias', 'subcategorias.pk_categorias_subcategorias')
      ->get();
    //$cont = Contenido::getAll();

    $slider = DB::table('sliders')
      ->join('secciones', 'secciones.id_secciones', 'sliders.pk_secciones_sliders')
      ->where('nombre_secciones', '=', 'Slider')
      ->orderBy('orden', 'ASC')
      ->get();

    $carrusel =  DB::table('sliders')
    ->join('secciones', 'secciones.id_secciones', 'sliders.pk_secciones_sliders')
    ->where('nombre_secciones', '=', 'Slider inicio')
    ->orderBy('orden', 'ASC')
    ->get();

    $cont = DB::table('contenido')
      ->join('secciones', 'id_secciones', 'contenido.pk_secciones_contenido')
      ->where('nombre_secciones', '=', 'historia')
      ->orderBy('id_contenido', 'ASC')
      ->get();

    $ofertas = $this->productos_por_tipo(null, 20, ['descuentos_productos' => 1]);

    $hombres = $this->productos_por_tipo('Hombre', 20, [], ['Unisex']);
    $damas = $this->productos_por_tipo('Dama', 20, [], ['Unisex']);

    $damas2 = $this->productos_por_tipo_random('Dama', 20, [], ['Unisex']);

    $damasUnit = $this->productos_por_tipo('Dama', 1, [], ['Unisex']);

    $nina = $this->productos_por_tipo('Niña', 20);
    $ninos = $this->productos_por_tipo('Niño', 20);

    $especialesHombre = $this->productos_por_tipo('EspecialesHombre', 20);
    $especialesMujer = $this->productos_por_tipo('EspecialMujer', 20);

    $especiales = array_merge($especialesHombre, $especialesMujer);

    $olimpia = $this->productos_por_tipo('Olimpia');

    $olimpia2 = $this->productos_por_tipo_random('Olimpia');

    // $color = DB::table('color_productos')
    // ->join('color', 'color.id_color', 'pk_color_productos')
    // ->get();

    $color = DB::table('color_imagen')
    ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    ->select('pk_color_color_imagen', 'nombre_color', 'prefijo_color', 'activo', 'id_color', 'pk_productos_color_imagen')
    ->where('activo', '=', true)
    ->groupBy('pk_color_color_imagen', 'nombre_color', 'prefijo_color', 'activo', 'id_color', 'pk_productos_color_imagen')
    ->get();

    //dd($color);

    $subcategorias = $this->get_categorias();

    $clasicos = $this->productos_clasicos('clasicos', 20);

    $divisa = DB::table('divisas')->first();

    //dd($clasicos);

    $empleo = DB::table('empleos')->orderBy('nombre', 'ASC')->get();
    return view('guest.index', 
           compact('divisa', 
                   'carrusel',
                   'empleo', 
                   'olimpia', 
                   'olimpia2', 
                   'especiales', 
                   'ninos', 
                   'nina', 
                   'damas', 
                   'damas2', 
                   'hombres', 
                   'ofertas', 
                   'cont', 
                   'categorias', 
                   'contenido', 
                   'sliderUnd', 
                   'slider', 
                   'color', 
                   'damasUnit', 
                   'subcategorias', 
                   'clasicos'));
  }
  
  function productos_por_tipo($tipo, $take = 8, $wheres = [], $other_types = [])
  {
    $productos = Productos::with(['colores', 'imagen_principal'])
      ->select([
        'id_productos',
        'nombre_productos',
        'codigo_productos',
        'tipo_producto',
        'lanzamientos_productos',
        'minorista',
        'mayorista',
        'premium',
        'descuentos_productos',
        'precio_descuento_productos',
      ])
      ->where('activo_productos', true)
      ->when($tipo, function ($query, $tipo) {
        if ($tipo != null) {
          $query->where('tipo_producto', $tipo);
        }
      })
      ->whereExists(function ($query) {
        return $query->select(DB::raw(1))
          ->from('color_imagen')
          ->whereColumn('color_imagen.pk_productos_color_imagen', 'productos.id_productos');
      })
      ->when($take, function ($query, $take) {
        if ($take != null) {
          $query->take($take);
        }
      })
      ->orderBy('id_productos', 'DESC');

    foreach ($other_types as $value) {
      $productos->orWhere('tipo_producto', $value);
    }
    foreach ($wheres as $key => $value) {
      $productos->where($key, $value);
    }

    return $productos->get()->toArray();

  }

  function productos_por_tipo_random($tipo, $take = 8, $wheres = [], $other_types = [])
  {
    $productos = Productos::with(['colores', 'imagen_principal'])
      ->select([
        'id_productos',
        'nombre_productos',
        'codigo_productos',
        'tipo_producto',
        'lanzamientos_productos',
        'minorista',
        'mayorista',
        'premium',
        'descuentos_productos',
        'precio_descuento_productos',
      ])
      ->where('activo_productos', true)
      ->when($tipo, function ($query, $tipo) {
        if ($tipo != null) {
          $query->where('tipo_producto', $tipo);
        }
      })
      ->whereExists(function ($query) {
        return $query->select(DB::raw(1))
          ->from('color_imagen')
          ->whereColumn('color_imagen.pk_productos_color_imagen', 'productos.id_productos');
      })
      ->when($take, function ($query, $take) {
        if ($take != null) {
          $query->take($take);
        }
      })
      ->orderBy('id_productos', 'ASC');
      // ->inRandomOrder();
    foreach ($other_types as $value) {
      $productos->orWhere('tipo_producto', $value);
    }
    foreach ($wheres as $key => $value) {
      $productos->where($key, $value);
    }

    return $productos->get()->toArray();

  }

  function productos_clasicos($tipo, $take = 8, $wheres = [], $other_types = [])
  {
    $productos = Productos::with(['colores', 'imagen_principal'])
      ->select([
        'id_productos',
        'nombre_productos',
        'codigo_productos',
        'tipo_producto',
        'lanzamientos_productos',
        'minorista',
        'mayorista',
        'premium',
        'descuentos_productos',
        'precio_descuento_productos',
        'clasicos'
      ])
      ->where('activo_productos', true)
      ->when($tipo, function ($query, $tipo) {
        if ($tipo != null) {
          $query->where('clasicos', true);
        }
      })
      ->whereExists(function ($query) {
        return $query->select(DB::raw(1))
          ->from('color_imagen')
          ->whereColumn('color_imagen.pk_productos_color_imagen', 'productos.id_productos');
      })
      ->when($take, function ($query, $take) {
        if ($take != null) {
          $query->take($take);
        }
      })
      ->orderBy('id_productos', 'DESC');

    foreach ($other_types as $value) {
      $productos->orWhere('tipo_producto', $value);
    }
    foreach ($wheres as $key => $value) {
      $productos->where($key, $value);
    }

    return $productos->get()->toArray();


    // ->orderBy('color_imagen.id_color_imagen', 'ASC')
    // return ColorImagen::imagenesProductos([
    //   [
    //     'key' => 'principal_color_imagen',
    //     'value' => true
    //   ],
    //   [
    //     'key' => 'seccion_color_imagen',
    //     'value' => 1
    //   ],
    //   [
    //     'key' => 'tipo_producto',
    //     'value' => $tipo
    //   ]
    // ], null, $take)
    //   ->get([
    //     'codigo_productos',
    //     'descuentos_productos',
    //     'mayorista',
    //     'precio_descuento_productos',
    //     'nombre_productos',
    //     'path_imagen',
    //     'id_productos',
    //     'premium',
    //     'lanzamientos_productos'
    //   ]);
  }


  public function get_categorias()
  {
    $categorias_id = DB::table('productos')
      ->where('tipo_producto', '!=', 'Olimpia')
      ->where('activo_productos', true)
      ->distinct()
      ->get(['pk_subcategorias_productos'])
      ->toArray();
    $categorias_id = array_column($categorias_id, 'pk_subcategorias_productos');
    $categorias = DB::table('subcategorias')
      ->whereIn('id_subcategorias', $categorias_id)
      ->where('active_subcategorias', true)
      ->where('pk_categorias_subcategorias', '!=', 4)
      ->join('categorias', 'categorias.id_categorias', 'subcategorias.pk_categorias_subcategorias')
      ->get();
    return $categorias;
  }

  public function buscarEmpleo(Request $request)
  {
    $empleo_requisito = DB::table('empleos')
      ->where('id', '=', $request->empleo)
      ->first();

    $descripcion = $empleo_requisito->descripcion;

    return $descripcion;
  }

  public function agregarEmpleoGuest(Request $request)
  {
    //dd($request->toArray());
    //Log::info(_FUNCTION_);
    $response['code'] = 500;
    $response['message'] = 'Hubo un error inesperado en guardar de HomeController';
    //dd($request->toArray());
    try {
      $datos = array(
        'nombre' => $request->nombre_empleo,
        'documento' => $request->ci_empleo,
        'telefono' => $request->telefono_empleo,
        'direccion' => $request->direccion_empleo,
        'ciudad' => $request->ciudad_empleo,
        'barrio' => $request->barrio_empleo,
        'experiencia' => $request->experiencia_empleo,
        'descripcion' => $request->descripcion_empleo,
        'pk_empleos' => $request->vigentes_empleo,
        'created_at' => now(),
      );
      $insertId = DB::table('curriculum')->insertGetId($datos);
      if ($insertId > 0) {
        try {

          $pdf = $request->file('cv');
          $cv = $pdf->getClientOriginalName();
          $cv = $request->nombre . '-' . date('Y-m-d_h_i_s') . '-' . $cv;
          if ($pdf->move(public_path('uploads/curriculum'), $cv)) {
            $update = DB::table('curriculum')
              ->where('id', '=', $insertId)
              ->update(['pdf_path' => $cv]);
            if ($update > 0) {

              $response['code'] = 200;
              $response['message'] = 'OK';
            }
          } else {
            $response['code'] = 500;
            $response['message'] = 'No se pudo guardar el CV';
          }
        } catch (\Throwable $th) {
          // Log::info('Error en el segundo TRY de la función guardar de HomeController: '.$th->getMessage());
          $response['code'] = 500;
          $response['message'] = 'Error en el segundo TRY de la función guardar de GuestController: ' . $th->getMessage();
          return $response;
        }
      }
    } catch (\Throwable $th) {
      // Log::info('Error en el primer TRY de la función guardar de HomeController: '.$th->getMessage());
      $response['code'] = 500;
      $response['message'] = 'Error en el primer TRY de la función guardar de GuestController';
    }
    return $response;
  }

  public function historial(Request $request)
  {
    $categorias = $this->get_categorias();

    $user = DB::table('pedido')
      ->join('users', 'users.id', 'pedido.user_pedido')
      ->join('datos_factura', 'datos_factura.id_datos_factura', 'pedido.id_datos_factura_envio_pedido')
      ->join('direccion_envio', 'id_direccion_envio', 'pedido.id_direccion_envio_pedido')
      ->leftjoin('documentos', 'documentos.id_pedido_documentos', 'pedido.id_pedido')
      ->where('id', '=', Auth::user()->id)
      ->orderBy('id_pedido', 'DESC')->paginate(3);

    return view('guest.historial', compact('categorias', 'user'));
  }

  public function buscarDetallePedido(Request $request)
  {
    return DB::table('detalle_pedido')
      ->join('pedido', 'pedido.id_pedido', 'detalle_pedido.id_venta_detalle_pedido')
      ->where('id_pedido', '=', $request->id)
      ->get();
  }


  public function agregarComprobacion(Request $request)
  {
    $image_resize = Image::make($request->file);
    $image_resize->resize(400, null, function ($constraint) {
      $constraint->aspectRatio();
      $constraint->upsize();
    });
    $image_resize->orientate();
    $nombre_archivo = time() . "." . $request->file->extension();
    $image_resize->save(public_path('img/comprobantes/' . $nombre_archivo));

    $documentos = new Documentos;
    $documentos->path_documentos = $nombre_archivo;
    $documentos->id_pedido_documentos = $request->id_pedido_documento;
    $documentos->save();

    return redirect()->route('guest.historial');
  }

  public function micuenta(Request $request)
  {
    if (Auth::user()) {
      $categorias = $this->get_categorias();
      return view('guest.micuenta', compact('categorias'));
    } else {
      return redirect()->back();
    }
  }

  public function editar_datos(Request $request)
  {
    $user = Auth::user();
    $response['success'] = false;
    $response['data'] = [];
    if ($user) {
      $correct_pass = Hash::check($request->pass_confirm, $user->password);
      if ($correct_pass) {
        DB::table('users')
          ->where('id', $user->id)
          ->update([
            'name' => $request->name ?? $user->name,
            'razon_users' => $request->razon_users ?? $user->razon_users,
            'ruc_users' => $request->ruc_users ?? $user->ruc_users,
            'email' => $request->email ?? $user->email,
            'telefono_users' => $request->telefono_users ?? $user->telefono_users,
            'direccion_users' => $request->direccion_users ?? $user->direccion_users,
          ]);
        $user = DB::table('users')->where('id', $user->id)->first([
          'name',
          'razon_users',
          'ruc_users',
          'email',
          'telefono_users',
          'direccion_users'
        ]);
        $response['success'] = true;
        $response['data'] = $user;
        return response()->json($response, 200);
      } else {
        return response()->json($response, 200);
      }
    } else {
      return response()->json($response, 200);
    }
  }

  public function autocompletarInput(Request $request)
  {
    $query = $request['query'];

    if ($request["query"]) {
      //$data = Productos::where('nombre_productos','LIKE','%'.$query.'%')->get();
      // $data = DB::table('productos')
      // ->where('nombre_productos', 'LIKE', '%'.$query.'%')
      // ->get();

      $data = DB::table('color_imagen')
        ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
        ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
        ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
        ->leftjoin('subcategorias', 'subcategorias.id_subcategorias', 'productos.pk_subcategorias_productos')
        ->where('active_subcategorias', true)
        ->where('nombre_productos', 'ILIKE', '%' . $query . '%')
        ->get();

      $output = '<ul class="dropdown-menu" style=" display:block; position: relative; width: 100%; padding: 10px; margin: 0px; background: transparent; border: none;">';

      foreach ($data as $p) {
        if ($p->principal_color_imagen == "true") {
          if ($p->seccion_color_imagen == 1) {

            if ($p->descuentos_productos == true) {
              $etiqueta = 'Oferta';
            } else {
              if ($p->premium == true) {
                $etiqueta = 'Premium';
              } else {
                $etiqueta = "";
              }
            }

            if (is_null($p->precio_descuento_productos)) {
              $precio = number_format($p->mayorista);
            } else {
              $precio = number_format($p->precio_descuento_productos);
            }


            $output .= '<a class="resultado_auto" href="/shop/' . $p->id_productos . '" style="width: 100%;">' .
              '<li style="font-size: 13px; border-bottom: 1px solid #e5e5e5;">' .
                '<div class="col-2" style="float: left; padding: 0;">' .
                '<img src="/img/productos/' . $p->path_imagen . '" style="width: 100%; border-radius: 5px; border: 1px solid #e5e5e5;">' .
                '</div>' .
                '<div class="col-10" style="float: left; text-align: left;">' .
                  '<p style="margin-bottom: 0; color: #101010;">' . $etiqueta . '</p>' .
                  '<p style="margin-bottom: 0; color: #101010;">' . $p->nombre_productos . '</p>' .
                  '<p style="margin-bottom: 0; color: #101010;"> Codigo: ' . $p->codigo_productos . '</p>' .
                  '<p style="margin-bottom: 0; color: #101010;"> Gs. ' . $precio . '</p>' .
                '</div>' .
              '</li>' .
              '</a>';
          }
        }
      }
      $output .= '</ul>';
      return $output;
    }
  }

  public function terminosCondiciones(Request $request)
  {
    $secciones = Secciones::All();
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All();
    $slider = Sliders::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $categorias = $this->get_categorias();
    $colorImagen = ColorImagen::colorImagen();
    $cont = Contenido::getAll();
    return view('paginas.terminos_y_condiciones', compact('cont', 'colorImagen', 'categorias', 'imagenProductos', 'productos', 'secciones', 'contenido', 'sliderUnd', 'slider'));
  }

  public function nosotros(Request $request)
  {
    $secciones = Secciones::All();
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All();
    $slider = Sliders::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $categorias = $this->get_categorias();
    $colorImagen = ColorImagen::colorImagen();
    $cont = Contenido::getAll();
    return view('paginas.nosotros', compact('cont', 'colorImagen', 'categorias', 'imagenProductos', 'productos', 'secciones', 'contenido', 'sliderUnd', 'slider'));
  }

  public function trabajeConNosotros(Request $request)
  {
    $secciones = Secciones::All();
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All();
    $slider = Sliders::All();
    $productos = Productos::All();
    $imagenProductos = ImagenProductos::getAll2();
    $categorias = $this->get_categorias();
    $colorImagen = ColorImagen::colorImagen();
    $cont = Contenido::getAll();
    return view('paginas.trabaje_con_nosotros', compact('cont', 'colorImagen', 'categorias', 'imagenProductos', 'productos', 'secciones', 'contenido', 'sliderUnd', 'slider'));
  }

  public function vistaExportacion(Request $request) 
  {
    $contenido = Contenido::All();
    $sliderUnd = Sliders::All();
   
    $categorias_id = DB::table('productos')
      ->where('tipo_producto', '!=', 'Olimpia')
      ->where('activo_productos', true)
      ->distinct()
      ->get(['pk_subcategorias_productos'])
      ->toArray();
    $categorias_id = array_column($categorias_id, 'pk_subcategorias_productos');
    $categorias = DB::table('subcategorias')
      ->whereIn('id_subcategorias', $categorias_id)
      ->where('active_subcategorias', true)
      ->where('pk_categorias_subcategorias', '!=', 4)
      ->join('categorias', 'categorias.id_categorias', 'subcategorias.pk_categorias_subcategorias')
      ->get();

    return view('guest.vistaExportacion', compact('contenido', 'sliderUnd', 'categorias_id', 'categorias_id', 'categorias'));
  }

  public function agregarExportacion(Request $request) 
  {

    $directory_separator = DIRECTORY_SEPARATOR;

    $archivos = $request->file('pdf');

    //dd($archivos);

    $arch = $archivos->getClientOriginalName();
    $arch = $arch . '-' . date('Y-m-d_h_i_s');
    $archivos->move(public_path('uploads/pdf' . $directory_separator), $arch);

    $import = new Importacion();
    $import->empresa = $request->empresa;
    $import->nombre = $request->name;
    $import->correo = $request->correo; 
    $import->web = $request->web; 
    $import->telefono = $request->telefono; 
    $import->pais = $request->pais; 
    $import->mensaje = $request->mensaje;
    $import->pdf = $arch;
    $import->save();

  }
}
