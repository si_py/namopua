<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\categorias;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class CategoriasController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(Request $request)
  {
    $category = categorias::filterAndPaginatePatients($request->id_categorias);
    $categoriasActivas = DB::table('categorias')->where('active_categorias', true)->get();
    $categoriasInactivas = DB::table('categorias')->where('active_categorias', false)->get();
    $id = '';
    if ($request->name) {
      $id = $request->id_categorias;
    };

    return view('categorias.index', compact('category', 'categoriasActivas', 'categoriasInactivas', 'id'));
  }

  public function show()
  {
  }

  public function store(Request $request)
  {

    $insert = DB::table('categorias')
      ->insert([
        'nombre_categorias' => $request->nombre,
        'created_categorias' => auth()->user()->id
      ]);
    return response()->json($insert);
  }

  public function categoriaBuscar()
  {
    return DB::table('categorias')
      ->where('active_categorias', true)
      ->orderBy('id_categorias', 'ASC')
      ->get();
  }

  // public function edit($id)
  //     {
  //         $category = Category::findOrFail($id);
  //         return view('category.edit', compact('category'));
  //     }
  //
  public function update(Request $request)
  {
    Log::info("CategoriasController::update", ["request" => $request->all()]);

    if ($request->ajax()) {

      if (isset($request->nuevo_estado)) {
        DB::table('categorias')->where('id_categorias', $request->id)->update(['active_categorias' => $request->nuevo_estado]);
        $response['success'] = true;
        $response['data'] = DB::table('categorias')
          ->where('id_categorias', $request->id)->first();
        return response()->json($response);
      } else {
        $response["success"] = true;
        $response["message"] = "Actualizacion Exitoso";

        $data = [$request->column_name => $request->column_value];
        $anthropometry = categorias::find($request->id);
        if (!$anthropometry) {
          $response["success"] = false;
          $response["message"] = 'Hubo un Error';
          // echo '<div class="alert alert-danger">Hubo un Error</div>';
          return $response;
        }
        $anthropometry->update($data);
        return $response;
      }
    }
  }
}
