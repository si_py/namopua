<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\categorias;
use App\subcategorias;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class SubCategoriasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $subcategory = subcategorias::filterAndPaginatePatients($request->id_subcategorias);
        $id = '';
        if ($request->name) {
            $id = $request->id_subcategorias;
        };
        $categoria = categorias::where('active_categorias', true)->get();
        $subActive = DB::table('subcategorias')->where('active_subcategorias', true)
        ->join('categorias', 'categorias.id_categorias', 'subcategorias.pk_categorias_subcategorias')
        ->get();
        $subInactive = DB::table('subcategorias')->where('active_subcategorias', false)
        ->join('categorias', 'categorias.id_categorias', 'subcategorias.pk_categorias_subcategorias')
        ->get();

        return view('subcategorias.index', compact('subActive', 'subInactive', 'subcategory', 'id', 'categoria'));
    }

    public function show()
    {
    }

    public function store(Request $request)
    {

        $insert = DB::table('subcategorias')
            ->insert([
                'nombre_subcategorias' => $request->nombre,
                'pk_categorias_subcategorias' => $request->categoria,
                'active_subcategorias' => 1,
                'created_subcategorias' => auth()->user()->id
            ]);
        return response()->json($insert);
    }

    public function subcategoriaBuscar()
    {
        return DB::table('subcategorias')
            ->join('categorias', 'categorias.id_categorias', 'subcategorias.pk_categorias_subcategorias')
            ->orderBy('id_subcategorias', 'ASC')
            ->get();
    }

    public function edit($id)
    {
        $category = subcategorias::findOrFail($id);
        $cat = categorias::where('active_categorias', true)->get();
        return view('subcategorias.edit', compact('category', 'cat'));
    }

    public function update(Request $request, $slug)
    {
        if (isset($request->nuevo_estado)) {
            DB::table('subcategorias')->where('id_subcategorias', $slug)->update(['active_subcategorias' => $request->nuevo_estado]);
            $response['success'] = true;
            $response['data'] = DB::table('subcategorias')
            ->join('categorias', 'categorias.id_categorias', 'subcategorias.pk_categorias_subcategorias')
            ->where('id_subcategorias', $slug)->first();
            return response()->json($response);
        } else {
            $categoryEdit = subcategorias::whereid_subcategorias($slug)->firstOrFail();
            $categoryEdit->nombre_subcategorias = $request->nombre_subcategorias;
            $categoryEdit->pk_categorias_subcategorias = $request->pk_categorias_subcategorias;
            $categoryEdit->save();
            return redirect()->route('subcategorias.index');
        }
    }
}
