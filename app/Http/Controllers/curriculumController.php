<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Empleos;

class curriculumController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function verCurriculum(Request $request)
  {
    $empleo = DB::table('empleos')->orderBy('nombre', 'ASC')->get();
    $curri = DB::table('curriculum')
      ->join('empleos', 'empleos.id', 'curriculum.pk_empleos')
      ->select(
        'curriculum.id',
        'curriculum.nombre',
        'empleos.nombre as nombre_empleo',
        'curriculum.pdf_path',
        'curriculum.created_at',
        'curriculum.telefono',
        'curriculum.documento',
        'curriculum.direccion',
        'curriculum.ciudad',
        'curriculum.barrio',
        'curriculum.experiencia',
        'curriculum.descripcion'
      )
      ->orderBy('created_at', 'ASC')->paginate(10);

    return view('curriculum.vistaCurriculum', compact('empleo', 'curri'));
  }


  public function agregarEmpleo(Request $request)
  {
    $empleo = new Empleos();
    $empleo->nombre = $request->nombre;
    $empleo->descripcion = $request->descripcion;
    $empleo->activo = true;
    $empleo->save();

    $table = DB::table('empleos')->orderBy('nombre', 'ASC')->get();

    return $table;
  }

  public function editarEmpleo(Request $request)
  {
    $empleo = Empleos::whereid($request->id)->firstOrFail();
    $empleo->nombre = $request->nombre;
    $empleo->descripcion = $request->descripcion;
    $empleo->activo = $request->activo;
    $empleo->save();

    $table = DB::table('empleos')->orderBy('nombre', 'ASC')->get();

    return $table;
  }

  public function eliminarCurriculum(Request $request)
  {
    try {
      DB::table('curriculum')
        ->where('id', $request->id)
        ->delete();
        return true;
      } catch (\Throwable $th) {
      return false;
    }
  }
}
