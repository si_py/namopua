<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Sliders;
use App\Secciones;
use App\Contenido;
use App\Productos;
use App\categorias;
use App\subcategorias;
use App\Marcas;
use App\Imagen;
use App\ImagenProductos;
use App\Color;
use App\ColorProductos;
use App\ColorImagen;
use App\Talles;
use App\TallesProductos;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductosController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
    $this->id_categoria_olimpia = 4;
  }

  public function index(Request $request)
  {
    $secciones = Secciones::All();
    $categoria = categorias::where('active_categorias', true)->get();
    $subcategoria = subcategorias::where('active_subcategorias', true)->get();
    $marcas = Marcas::where('active_marcas', true)->get();
    //$productoFull = Productos::getAll();
    $namopuaproductoFull = DB::table('productos')
      ->join('categorias', 'categorias.id_categorias', 'productos.pk_categorias_productos')
      ->join('subcategorias', 'subcategorias.id_subcategorias', 'productos.pk_subcategorias_productos')
      ->join('marcas', 'marcas.id_marcas', 'productos.pk_marcas_productos')
      ->orderBy('id_productos', 'DESC')
      ->where('pk_categorias_productos', "!=", $this->id_categoria_olimpia)
      ->paginate(10);
    $olimpiaproductoFull = DB::table('productos')
      ->join('categorias', 'categorias.id_categorias', 'productos.pk_categorias_productos')
      ->join('subcategorias', 'subcategorias.id_subcategorias', 'productos.pk_subcategorias_productos')
      ->join('marcas', 'marcas.id_marcas', 'productos.pk_marcas_productos')
      ->orderBy('id_productos', 'DESC')
      ->where('pk_categorias_productos', $this->id_categoria_olimpia)
      ->paginate(10);
    $imagen = Imagen::All();
    $imagenProductos = ColorImagen::colorImagen();
    $imagenProductos2 = ColorImagen::colorImagen2();
    $color = DB::table('color')->where('estado_color', true)->get();
    $colorProductos = ColorProductos::getAll();
    $talles = DB::table('talles')->where('estado_talles', true)->get(['id_talles', 'nombre_talles']);
    // $tallesProductos = TallesProductos::getAll();
    $productos = Productos::filterAndPaginatePatients($request->id_productos);
    $id = '';
    if ($request->name) {
      $id = $request->id_contenido;
    };
    return view('productos.index', compact('imagenProductos2', 'talles', 'colorProductos', 'color', 'imagenProductos', 'imagen', 'secciones', 'productos', 'categoria', 'subcategoria', 'marcas', 'namopuaproductoFull', 'olimpiaproductoFull'));
  }

  public function show()
  {
  }

  public function store(Request $request)
  {
    $validation = Validator::make($request->all(), [
      "codigo" => "required|unique:productos,codigo_productos"
    ]);
    $error_messages = $validation->errors()->messages();
    if (!empty($error_messages)) {
      return response()->json($error_messages, 200);
    }
    DB::table('productos')
      ->insert([
        'codigo_productos' => $request->codigo,
        'nombre_productos' => $request->nombre,
        'tipo_producto' => $request->tipo,
        'pk_categorias_productos' => $request->pk_categorias,
        'pk_subcategorias_productos' => $request->pk_subcategorias,
        'pk_marcas_productos' => $request->pk_marcas,
        'lanzamientos_productos' => $request->lanzamiento,
        'descuentos_productos' => $request->descuentos,
        'precio_descuento_productos' => $request->precio_descuentos,
        'mayorista' => $request->mayorista,
        'minorista' => $request->minorista,
        'activo_productos' => 1,
        'created_productos' => auth()->user()->id,
        'descripcion_productos' => $request->descripcion,
        'premium' => $request->premium, 
        'clasicos' => $request->clasicos
      ]);
    return response()->json($error_messages);
  }

  public function subirImagen(Request $request)
  {
    //dd($request->toarray());
    Log::info("ProductosController:subirImagen", ["request" => $request->all()]);

    if ($request->ajax()) {

      $colorI = DB::table('color_imagen')
        ->select('orden')
        ->where('pk_productos_color_imagen', '=', $request->pk_productos_imagen_productos)
        ->where('pk_color_color_imagen', '=', $request->color_imagen)
        ->orderBy('orden', 'DESC')
        ->first();


      $image = $request->file('file');
      $name = $image->getClientOriginalName();

      //dd($name);

      $name = date('Y-m-d_h_i_s') . '-' . $name;
      if ($image->move(public_path('img/productos/'), $name)) {
        $upload = new Imagen();
        $upload->path_imagen = $name;
        $upload->created_imagen =  auth()->user()->id;
        $upload->save();

        
        $imagen = Imagen::All();

        $imagenProducto = new ImagenProductos();
        $imagenProducto->pk_imagen_productos = ($upload->id_imagen);
        $imagenProducto->pk_productos_imagen_productos = $request->pk_productos_imagen_productos;
        $imagenProducto->principal_imagen_productos = $request->principal_imagen_productos;
        $imagenProducto->created_imagen_productos = auth()->user()->id;
        $imagenProducto->save();

        

        if($colorI) {
          $orden = $colorI->orden + 1;
        } else {
          $orden = 1;
        }

        $colorImagen = new ColorImagen();
        $colorImagen->pk_imagen_color_imagen = ($upload->id_imagen);
        $colorImagen->pk_color_color_imagen = $request->color_imagen;
        $colorImagen->pk_productos_color_imagen = $request->pk_productos_imagen_productos;
        $colorImagen->principal_color_imagen = $request->principal_imagen_productos;
        $colorImagen->seccion_color_imagen = $request->seccion_imagen_productos;
        $colorImagen->created_color_imagen = auth()->user()->id;
        $colorImagen->orden = $orden;
        $colorImagen->save();
      } else {
        $rta['cod'] = 500;
        $rta['msg'] = 'Hubo un error al intentar guardar las imagenes.';
      }
    }
  }

  public function destroy(Request $request, $id)
  {
    $imagen = ColorImagen::findOrFail($id);
    if ($imagen->delete()) {

      return redirect()->route('productos.index');
    }
    return 'Algo ha salido mal';
  }

  public function deleteColor(Request $request, $id)
  {
    try {
      //remueve el color del producto
      $imagen = ColorProductos::findOrFail($id);
      if ($imagen) {
        //remueve las imagenes relacionadas al color
        $color = $imagen->pk_color_productos;
        if ($imagen->delete()) {
          $color_imagen = ColorImagen::where('pk_color_color_imagen', $color)->firstOrFail();
          $id_imagen = $color_imagen->pk_imagen_color_imagen;
          if ($color_imagen->delete()) {
            $imagen_productos = ImagenProductos::where('pk_imagen_productos', $id_imagen)->firstOrFail();
            $imagen_productos->delete();

            $imagen = Imagen::where('id_imagen', $id_imagen)->firstOrFail();
            $imagen->delete();

            return redirect()->route('productos.index');
          }
        }
      }
    } catch (\Throwable $th) {
      return 'Algo ha salido mal';
    }
  }

  // public function deleteTalles(Request $request, $id)
  // {
  //   $imagen = TallesProductos::findOrFail($id);
  //   if ($imagen->delete()) {

  //     return redirect()->route('productos.index');
  //   }
  //   return 'Algo ha salido mal';
  // }
  public function deleteTalles(Request $request)
  {
    $color_producto = DB::table('color_productos')
      ->where('pk_productos_color_productos', $request->id_producto)
      ->where('pk_color_productos', $request->id_color)
      ->first();
    $talles = json_decode($color_producto->tamanos);
    //busca la talla en el array
    $index_talla = array_search($request->talle, $talles);
    //borra el item en x index
    array_splice($talles, $index_talla, 1);
    //actualiza la columna
    DB::table('color_productos')
      ->where('pk_productos_color_productos', $request->id_producto)
      ->where('pk_color_productos', $request->id_color)
      ->update([
        'tamanos' => json_encode($talles)
      ]);
    return response()->json($talles, 200);
  }

  public function subirColor(Request $request)
  {
    Log::info("ProductosController:subirColor", ["request" => $request->all()]);

    $colorProducto = new ColorProductos();
    $colorProducto->pk_color_productos = $request->pk_color_productos;
    $colorProducto->pk_productos_color_productos = $request->pk_productos_color_productos;
    $colorProducto->created_color_productos = auth()->user()->id;
    $colorProducto->save();
    DB::commit();

    return redirect()->route('productos.index');
  }

  public function subirTalles(Request $request)
  {
    Log::info("ProductosController:subirTalles", ["request" => $request->all()]);

    $tallesProducto = new TallesProductos();
    $tallesProducto->pk_talles_productos = $request->pk_talles_productos;
    $tallesProducto->pk_productos_talles_productos = $request->pk_productos_talles_productos;
    $tallesProducto->created_talles_productos = auth()->user()->id;
    $tallesProducto->save();
    DB::commit();

    return redirect()->route('productos.index');
  }

  public function modificar(Request $request)
  {
    //dd($request->toarray());
    $prod = Productos::whereid_productos($request->id)->firstOrFail();
    $prod->nombre_productos = $request->nombre;
    $prod->codigo_productos = $request->codigo;
    $prod->tipo_producto = $request->tipo;
    $prod->pk_categorias_productos = $request->cate;
    $prod->pk_subcategorias_productos = $request->subcate;
    $prod->pk_marcas_productos = $request->marca;
    $prod->lanzamientos_productos = $request->lanza;
    $prod->descuentos_productos = $request->desc;
    $prod->precio_descuento_productos = $request->precio_desc;
    $prod->mayorista = $request->mayorista;
    $prod->minorista = $request->minorista;
    $prod->descripcion_productos = $request->descripcion;
    $prod->premium = $request->premium;
    $prod->clasicos = $request->clasicos;
    $prod->save();
  }

  public function modificarExistente(Request $request)
  {
    $prod = Productos::whereid_productos($request->id)->firstOrFail();
    $prod->activo_productos = $request->active;
    $prod->save();
  }

  public function agregarTallaBK(Request $request)
  {

    if ($request->tallas_nino == null) {
    } else {
      foreach ($request->tallas_nino as $key => $value) {
        $talla = new TallesProductos();
        $talla->pk_talles_productos = $request->tallas_nino[$key];
        $talla->pk_productos_talles_productos = $request->id_producto;
        $talla->created_talles_productos = auth()->user()->id;
        $talla->save();
      }
    }

    if ($request->tallas_adulto == null) {
    } else {
      foreach ($request->tallas_adulto as $key => $value) {
        $talla = new TallesProductos();
        $talla->pk_talles_productos = $request->tallas_adulto[$key];
        $talla->pk_productos_talles_productos = $request->id_producto;
        $talla->created_talles_productos = auth()->user()->id;
        $talla->save();
      }
    }

    if ($request->tallas_espacial == null) {
    } else {
      foreach ($request->tallas_espacial as $key => $value) {
        $talla = new TallesProductos();
        $talla->pk_talles_productos = $request->tallas_espacial[$key];
        $talla->pk_productos_talles_productos = $request->id_producto;
        $talla->created_talles_productos = auth()->user()->id;
        $talla->save();
      }
    }
  }
  public function getTallasColores(Request $request)
  {
    $coloresTallas = DB::table('color_productos')
      ->where('pk_productos_color_productos', $request->id_producto)
      ->join('color', 'color.id_color', 'color_productos.pk_color_productos')
      ->get();
    return response()->json($coloresTallas, 200);
  }
  public function agregarTalla(Request $request)
  {
    $tallas['color'] = $request->id_color;
    $tallas['tallas'] = [];
    $nuevas_tallas = [];
    if ($request->tallas_nino != null) {
      foreach ($request->tallas_nino as $key => $value) {
        $nuevas_tallas[] = $value;
      }
    }

    if ($request->tallas_adulto != null) {
      foreach ($request->tallas_adulto as $key => $value) {
        $nuevas_tallas[] = $value;
      }
    }

    if ($request->tallas_espacial != null) {
      foreach ($request->tallas_espacial as $key => $value) {
        $nuevas_tallas[] = $value;
      }
    }
    $tallas_anteriores =
      DB::table('color_productos')
      ->where('pk_color_productos', $request->id_color)
      ->where('pk_productos_color_productos', $request->id_producto)
      ->first(['tamanos'])->tamanos ?? "[]";
    $tallas_anteriores = json_decode($tallas_anteriores);
    $tallas_anteriores = array_merge($tallas_anteriores, $nuevas_tallas);
    $tallas_anteriores = array_values(array_unique($tallas_anteriores));
    // dd($tallas_anteriores);

    $tallas['tallas'] = $tallas_anteriores;
    DB::table('color_productos')
      ->where('pk_color_productos', $request->id_color)
      ->where('pk_productos_color_productos', $request->id_producto)
      ->update([
        'tamanos' => json_encode($tallas_anteriores)
      ]);
    return response()->json($tallas, 200);
  }

  public function agregarColor(Request $request)
  {
    if ($request->total_color == null) {
    } else {
      foreach ($request->total_color as $key => $value) {
        $color = new ColorProductos();
        $color->pk_color_productos = $request->total_color[$key];
        $color->pk_productos_color_productos = $request->id_producto;
        $color->created_color_productos = auth()->user()->id;
        $color->save();
      }
    }
  }

  public function tablaProductos(Request $request)
  {
    if ($request->codigo == null) {
      $productoFull = DB::table('productos')
        ->join('categorias', 'categorias.id_categorias', 'productos.pk_categorias_productos')
        ->join('subcategorias', 'subcategorias.id_subcategorias', 'productos.pk_subcategorias_productos')
        ->join('marcas', 'marcas.id_marcas', 'productos.pk_marcas_productos')
        ->orderBy('id_productos', 'DESC')
        ->when($request, function ($query, $request) {
          if ($request->table == "namopua-table") {
            $query->where("pk_categorias_productos", "!=", $this->id_categoria_olimpia);
          } else {
            $query->where("pk_categorias_productos", $this->id_categoria_olimpia);
          }
        })
        ->paginate(10);
    } else {
      $productoFull = DB::table('productos')
        ->join('categorias', 'categorias.id_categorias', 'productos.pk_categorias_productos')
        ->join('subcategorias', 'subcategorias.id_subcategorias', 'productos.pk_subcategorias_productos')
        ->join('marcas', 'marcas.id_marcas', 'productos.pk_marcas_productos')
        ->where('codigo_productos', 'ILIKE', '%' . $request->codigo . '%')
        ->orderBy('id_productos', 'DESC')
        ->when($request, function ($query, $request) {
          if ($request->table == "namopua-table") {
            $query->where("pk_categorias_productos", "!=", $this->id_categoria_olimpia);
          } else {
            $query->where("pk_categorias_productos", $this->id_categoria_olimpia);
          }
        })
        ->paginate(10);
    }
    //dd($productoFull);
    $colorProductos = ColorProductos::getAll();
    // $tallesProductos = TallesProductos::getAll();
    $imagenProductos2 = ColorImagen::colorImagen2();
    // dd($productoFull);
    $table = $request->table;

    return view('productos.tablaProductos', compact('productoFull', 'table', 'colorProductos', 'imagenProductos2'));
  }

  public function buscarImagen(Request $request)
  {
    $tipoColor = DB::table('color_imagen')
    ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
    ->select('pk_color_color_imagen', 'nombre_color', 'prefijo_color', 'activo', 'id_color', 'seccion_color_imagen')
    ->where('pk_productos_color_imagen', '=', $request->id)
    ->groupBy('pk_color_color_imagen', 'nombre_color', 'prefijo_color', 'activo', 'id_color', 'seccion_color_imagen')
    ->orderBy('color_imagen.seccion_color_imagen', 'ASC')
    ->get();

    //dd($tipoColor);

    $color = DB::table('color_imagen')
      ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
      ->where('pk_productos_color_imagen', '=', $request->id)
      ->orderBy('color_imagen.orden', 'ASC')
      ->orderBy('id_color_imagen', 'ASC')
      ->get();
    //dd($color);

    return view('productos.tablaImagenes', compact('tipoColor', 'color'));
  }

  public function borrarImagen(Request $request)
  {
    //dd($request->id);
    $imagen = ColorImagen::findOrFail($request->id_imagen);
    $imagen->delete();
  }

  public function ordenarImagenProducto(Request $request) 
  {
    $id = $request->order;

    foreach($id as $index=>$i) {
      if($index == 0) {
        $colorImagen = ColorImagen::whereid_color_imagen($i)->firstOrFail();
        $colorImagen->orden = ($index+1);
        $colorImagen->principal_color_imagen = true;
        $colorImagen->save();
      }else {
        $colorImagen = ColorImagen::whereid_color_imagen($i)->firstOrFail();
        $colorImagen->orden = ($index+1);
        $colorImagen->principal_color_imagen = false;
        $colorImagen->save();
      }
      
    }
  }

  public function habilitarColor(Request $request) 
  {

    $color = DB::table('color_imagen')
      ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
      ->where('pk_productos_color_imagen', '=', $request->id)
      ->where('pk_color_color_imagen', '=', $request->id_color)
      ->orderBy('color_imagen.orden', 'ASC')
      ->orderBy('id_color_imagen', 'ASC')
      ->get();

    //dd($color);

    if($request->habilitar == 'inhabilitar') {
      
      foreach($color as $c) {

        $colorImagen = ColorImagen::whereid_color_imagen($c->id_color_imagen)->firstOrFail();
        $colorImagen->activo = false;
        $colorImagen->save();

      }

    } else {
      //dd('habilitar');

      foreach($color as $c) {

        $colorImagen = ColorImagen::whereid_color_imagen($c->id_color_imagen)->firstOrFail();
        $colorImagen->activo = true;
        $colorImagen->save();

      }
    }
  }
}
