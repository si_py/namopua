<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return route('home');
        }
    }

    public function handle($request, Closure $next)
    {
        if(Auth::user()){
            if (Auth::user()->perfil_users == "usuario") {
                return $next($request);
            } else {
                if ($request->getRequestUri() == '/logout') {
                    return $next($request);
                }
            }
        }
        return redirect()->back();
    }
}
