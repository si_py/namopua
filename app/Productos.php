<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Productos extends Model
{
  protected $table = 'productos';

  protected $primaryKey = 'id_productos';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'id_productos',
    'nombre_productos',
    'codigo_productos',
    'pk_categorias_productos',
    'pk_subcategorias_productos',
    'pk_marcas_productos',
    'lanzamientos_productos',
    'descuentos_productos',
    'precio_lista_productos',
    'mayorista', 
    'minorista',
    'created_productos', 
    'activos_productos',
    'descripcion_productos',
    'premium',
    'clasicos'
  ];

  public $principal_imagen;

  public function __construct($principal_imagen = false)
  {
    $this->principal_imagen = $principal_imagen;
    return $this;
  }

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  Productos::name($name)
      ->orderBy('id_productos')
      //            ->paginate($rowsPerPage);
      ->get();
  }

  public function scopeName($query, $name)
  {
    if ($name) {
      return $query->where('id_productos', 'ilike', '%' . $name . '%');
    }
  }

  public static function getAll()
  {
    return DB::table('productos')
      ->join('categorias', 'categorias.id_categorias', 'productos.pk_categorias_productos')
      ->join('subcategorias', 'subcategorias.id_subcategorias', 'productos.pk_subcategorias_productos')
      ->join('marcas', 'marcas.id_marcas', 'productos.pk_marcas_productos')
      ->orderBy('id_productos', 'DESC')
      ->get();
  }

  public static function productosColor()
  {
    return DB::table('color_productos')
      ->join('productos', 'productos.id_productos', 'color_productos.pk_color_productos')
      ->join('color', 'color.id_color', 'color_productos.pk_productos_color_productos')
      ->orderBy('id_productos', 'DESC')
      ->get();
  }

  public static function productosImagen()
  {
    return DB::table('imagen_productos')
      ->join('productos', 'productos.id_productos', 'imagen_productos.pk_productos_imagen_productos')
      ->join('imagen', 'imagen.id_imagen', 'imagen_productos.pk_imagen_productos')
      ->orderBy('id_productos', 'DESC')
      ->get();
  }

  /* public function colores()
  {
    return $this->hasManyThrough(Color::class, ColorProductos::class, 'pk_productos_color_productos', 'id_color', 'id_productos', 'pk_color_productos')
      // ->join('color_productos', 'color_productos.pk_productos_color_productos', 'color_imagen.pk_productos_color_imagen')
      ->select([
        'prefijo_color',
      ])
      ->whereExists(function ($query) {
        return $query->select(DB::raw(1))
          ->from('color_imagen')
          ->whereColumn('color_imagen.pk_productos_color_imagen', 'color_productos.pk_productos_color_productos');
      });
  } */
  public function colores()
  {
    return $this->hasManyThrough(Color::class, ColorImagen::class, 'pk_productos_color_imagen', 'id_color', 'id_productos', 'pk_color_color_imagen')
      // ->join('color_productos', 'color_productos.pk_productos_color_productos', 'color_imagen.pk_productos_color_imagen')
      ->select([
        'id_color',
        'prefijo_color',
        DB::raw('MAX(seccion_color_imagen) as seccion_color_imagen'),
      ])
      ->where('color_imagen.activo', '=', true)
      ->groupBy('prefijo_color', 'id_color', 'pk_productos_color_imagen')
      ->orderBy('seccion_color_imagen', 'ASC');
  }
  public function imagenes()
  {
    // dd($this->principal_imagen);
    return $this->hasManyThrough(Imagen::class, ColorImagen::class, 'pk_productos_color_imagen', 'id_imagen', 'id_productos', 'pk_imagen_color_imagen')
      ->select([
        'pk_imagen_color_imagen',
        'pk_productos_color_imagen',
        'id_imagen',
        'path_imagen'
      ])
      ->orderBy('color_imagen.seccion_color_imagen');
  }

  public function imagen_principal()
  {
    return $this->hasManyThrough(Color::class, ColorImagen::class, 'pk_productos_color_imagen', 'id_color', 'id_productos', 'pk_color_color_imagen')
      ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      ->select([
        'path_imagen',
        'prefijo_color',
      ])
      ->where('principal_color_imagen', true)
      ->orderBy('seccion_color_imagen');
  }
}
