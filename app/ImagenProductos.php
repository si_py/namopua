<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class ImagenProductos extends Model
{
  protected $table = 'imagen_productos';

  protected $primaryKey = 'id_imagen_productos';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'id_imagen_productos',
    'pk_imagen_productos',
    'pk_productos_imagen_productos',
    'created_imagen_productos'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  ImagenProductos::name($name)
      ->orderBy('id_imagen_productos')
      //            ->paginate($rowsPerPage);
      ->get();
  }

  public function scopeName($query, $name)
  {
    if ($name) {
      return $query->where('id_imagen_productos', 'ilike', '%' . $name . '%');
    }
  }

  public static function getAll()
  {
    return DB::table('imagen_productos')
      ->join('imagen', 'imagen.id_imagen', 'imagen_productos.pk_imagen_productos')
      ->get();
  }

  public static function getAll2()
  {
    return DB::table('imagen_productos')
      ->join('imagen', 'imagen.id_imagen', 'imagen_productos.pk_imagen_productos')
      ->join('productos', 'productos.id_productos', 'imagen_productos.pk_productos_imagen_productos')
      ->orderBy('id_productos', 'DESC')
      ->get();
  }
}
