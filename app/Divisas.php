<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisas extends Model
{
    protected $table = 'divisas';

    protected $primaryKey = 'id';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'id',
      'nombre',
      'prefijo',
      'divisa'
    ];
  
    public static function filterAndPaginatePatients($name)
    {
      $rowsPerPage = 5;
  
      return  Divisas::name($name)
          ->orderBy('id')->get();
    }
  
    public function scopeName($query, $name)
    {
      if($name){
          return $query->where('id_color', 'ilike', '%'.$name.'%');
      }
    }
  
}
