<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Color extends Model
{
  protected $table = 'color';

  protected $primaryKey = 'id_color';
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'id_color',
    'nombre_color',
    'created_color',
    'prefijo_color'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  Color::name($name)
        ->orderBy('id_color')
  //            ->
  
        ->get();
  }

  public function scopeName($query, $name)
  {
    if($name){
        return $query->where('id_color', 'ilike', '%'.$name.'%');
    }
  }

  public static function getAll()
  {
  return DB::table('color')
  ->join('color_productos', 'color_productos.pk_productos_color_productos', 'color.id_color')
  ->get();
  }
}
