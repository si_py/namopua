<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class TallesProductos extends Model
{
  protected $table = 'talles_productos';

  protected $primaryKey = 'id_talles_productos';
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'id_talles_productos',
    'pk_talles_productos',
    'pk_productos_ctallesproductos',
    'created_talles_productos'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  TallesProductos::name($name)
        ->orderBy('id_talles_productos')
  //            ->paginate($rowsPerPage);
        ->get();
  }

  public function scopeName($query, $name)
  {
    if($name){
        return $query->where('id_talles_productos', 'ilike', '%'.$name.'%');
    }
  }

  public static function getAll()
  {
    return DB::table('talles_productos')
    ->join('talles', 'talles.id_talles', 'talles_productos.pk_talles_productos')
    ->get();
  }
}
