<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Contenido extends Model
{
  protected $table = 'contenido';

  protected $primaryKey = 'id_contenido';
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'id_contenido',
    'pk_secciones_contenido',
    'titulo_contenido',
    'texto_contenido',
    'created_contenido'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  Contenido::name($name)
        ->orderBy('id_contenido')
  //            ->paginate($rowsPerPage);
        ->get();
  }

  public function scopeName($query, $name)
  {
    if($name){
        return $query->where('id_contenido', 'ilike', '%'.$name.'%');
    }
  }

  public static function getAll()
  {
  return DB::table('contenido')->orderBy('id_contenido', 'ASC' )->get();
  }
}
