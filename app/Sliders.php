<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Sliders extends Model
{
  protected $table = 'sliders';

  protected $primaryKey = 'id_sliders';
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'id_sliders',
    'path_sliders',
    'pk_secciones_sliders',
    'path_mobile_sliders',
    'titulo_sliders',
    'namin_titulo_sliders',
    'descripcion_sliders',
    'created_sliders'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  Sliders::name($name)
        ->orderBy('id_sliders')
  //            ->paginate($rowsPerPage);
        ->get();
  }

  public function scopeName($query, $name)
  {
    if($name){
        return $query->where('id_sliders', 'ilike', '%'.$name.'%');
    }
  }

  public static function getAll()
  {
  return DB::table('sliders')->get();
  }
}
