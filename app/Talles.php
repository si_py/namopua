<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Talles extends Model
{
  protected $table = 'talles';

  protected $primaryKey = 'id_talles';
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'id_talles',
    'nombre_talles',
    'created_talles'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  Talles::name($name)
        ->orderBy('id_talles')
  //            ->paginate($rowsPerPage);
        ->get();
  }

  public function scopeName($query, $name)
  {
    if($name){
        return $query->where('nombre_talles', 'ilike', '%'.$name.'%');
    }
  }

  public static function getAll()
  {
  return DB::table('talles')->get();
  }
}
