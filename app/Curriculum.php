<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Curriculum extends Model
{
    protected $table = 'curriculum';

    protected $primaryKey = 'id';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'id',
      'pdf_path',
      'pk_empleos',
      'nombre', 
      'documento', 
      'imagen_documento',
      'direccion',
      'ciudad',
      'barrio',
      'telefono',
      'experiencia',
      'descripcion'
    ];

    public static function filterAndPaginatePatients($name)
    {
      $rowsPerPage = 5;

      return  Curriculum::name($name)
          ->orderBy('id')
    //            ->paginate($rowsPerPage);
          ->get();
    }

    public function scopeName($query, $name)
    {
      if($name){
          return $query->where('nombre', 'ilike', '%'.$name.'%');
      }
    }
}
