<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Pedido extends Model
{
    protected $table = 'pedido';

    protected $primaryKey = 'id_pedido';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'id_pedido',
      'fecha_pedido',
      'id_direccion_envio_pedido',
      'razon_pedido',
      'ruc_pedido',
      'telefono_pedido',
      'direccion_pedido',
      'total_pedido',
      'estado_pedido'
    ];
  
    public static function filterAndPaginatePatients($name)
    {
      $rowsPerPage = 5;
  
      return  Pedido::name($name)
          ->orderBy('id_pedido')
          ->get();
    }
  
    public function scopeName($query, $name)
    {
      if($name){
          return $query->where('id_direcid_pedidocion_envio', 'ilike', '%'.$name.'%');
      }
    }
}
