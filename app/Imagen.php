<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Imagen extends Model
{
  protected $table = 'imagen';

  protected $primaryKey = 'id_imagen';
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'id_imagen',
    'path_imagen',
    'created_imagen'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  Imagen::name($name)
        ->orderBy('id_imagen')
  //            ->paginate($rowsPerPage);
        ->get();
  }

  public function scopeName($query, $name)
  {
    if($name){
        return $query->where('id_imagen', 'ilike', '%'.$name.'%');
    }
  }

  public static function getAll()
  {
  return DB::table('imagen')->get();
  }

  public function colores(){
    return $this->hasManyThrough(Color::class, ColorImagen::class, 'pk_color_color_imagen', 'id_color', 'id_imagen');
  }
}
