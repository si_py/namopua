<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class DetallePedido extends Model
{
    protected $table = 'detalle_pedido';

    protected $primaryKey = 'id_detalle_pedido';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'id_detalle_pedido',
      'cod_produto_detalle_pedido',
      'color_detalle_pedido',
      'talla_detalle_pedido',
      'cantidad_detalle_pedido',
      'precio_detalle_pedido',
      'nombre_producto_detalle_producto', 
      'id_venta_detalle_pedido'
    ];
  
    public static function filterAndPaginatePatients($name)
    {
      $rowsPerPage = 5;
  
      return  DetallePedido::name($name)
          ->orderBy('id_detalle_pedido')
    //            ->
    
          ->get();
    }
  
    public function scopeName($query, $name)
    {
      if($name){
          return $query->where('id_detalle_pedido', 'ilike', '%'.$name.'%');
      }
    }
}
