<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Marcas extends Model
{
  protected $table = 'marcas';

  protected $primaryKey = 'id_marcas';
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'id_marcas',
    'nombre_marcas',
    'created_marcas'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  Marcas::name($name)
        ->orderBy('id_marcas')
  //            ->paginate($rowsPerPage);
        ->get();
  }

  public function scopeName($query, $name)
  {
    if($name){
        return $query->where('nombre_marcas', 'ilike', '%'.$name.'%');
    }
  }

  public static function getAll()
  {
  return DB::table('marcas')->get();
  }
}
