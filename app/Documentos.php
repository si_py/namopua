<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Documentos extends Model
{
    protected $table = 'documentos';

    protected $primaryKey = 'id_documentos';
    /**
     * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'id_documentos',
        'path_documentos',
        'id_pedido_documentos'
    ];

    public static function filterAndPaginatePatients($name)
    {
        $rowsPerPage = 5;

        return  Documentos::name($name)
            ->orderBy('id_documentos')
            ->paginate($rowsPerPage);
            //->get();
    }

    public function scopeName($query, $name)
    {
        if($name){
            return $query->where('id_documentos', 'ilike', '%'.$name.'%');
        }
    }
}
