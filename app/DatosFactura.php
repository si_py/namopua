<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class DatosFactura extends Model
{
    protected $table = 'datos_factura';

    protected $primaryKey = 'id_datos_factura';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'id_datos_factura',
      'user_datos_factura',
      'razon_datos_factura',
      'ruc_datos_factura',
      'email_datos_factura',
      'telefono_datos_factura',
      'direccion_datos_factura'
    ];
  
    public static function filterAndPaginatePatients($name)
    {
      $rowsPerPage = 5;
  
      return  DatosFactura::name($name)
          ->orderBy('id_datos_factura')
    //            ->
    
          ->get();
    }
  
    public function scopeName($query, $name)
    {
      if($name){
          return $query->where('id_datos_factura', 'ilike', '%'.$name.'%');
      }
    }
}
