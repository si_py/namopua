<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DatabaseRestore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:restore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restore a database backup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //directorio de backups /storage/app/backup/
        $path = storage_path("/app/backup/");

        //escanea el directorio de backups y trae el ultimo archivo de acuerdo al nombre
        $files = scandir($path, SCANDIR_SORT_DESCENDING);

        //el primer archivo en el array es el mas reciente
        $filename = $files[0];

        //credenciales para la conexion a la base de datos
        $usr = env('DB_USERNAME');
        $pass = env('DB_PASSWORD');
        $host = env('DB_HOST');
        $port = env('DB_PORT', 5432);
        $db = env('DB_DATABASE');

        /* //comprueba que la base de datos exista
        $command = 'psql --dbname=postgresql://' . $usr . ':' . $pass . '@' . $host . ':' . $port . '/' . $db;

        //parametros obligatorios de exec() -> no se usan
        $returnVar = NULL;
        $output  = NULL;
        exec($command, $output, $returnVar);
        if ($returnVar != 0) {
            echo "no existe la base de datos, creandola...\n";
            //crea la base de datos si no existe
            $command = `psql -U $usr -c "CREATE DATABASE $db"`;

            //parametros obligatorios de exec() -> no se usan
            $returnVar = NULL;
            $output  = NULL;
            exec($command, $output, $returnVar);
            echo "Se ha creado correctamente\n";
        } */

        $this->info('Restaurando datos, puede llevar unos minutos');

        // restaura el archivo desde $path con el nombre de archivo $filename
        $command = 'psql postgresql://' . $usr . ':' . $pass . '@' . $host . ':' . $port . '/' . $db . ' < ' . $path . $filename;

        //parametros obligatorios de exec() -> no se usan
        $returnVar = NULL;
        $output  = NULL;
        exec($command, $output, $returnVar);

        if ($returnVar == 0) {
            echo "Se ha restaurado la base de datos correctamente\n";
        } else {
            echo "Ha ocurrido un error\n";
        }
        return 0;
    }
}
