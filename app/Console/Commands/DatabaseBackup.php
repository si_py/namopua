<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use SebastianBergmann\Environment\Console;
use Symfony\Component\Process\Process;

class DatabaseBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup de la base de datos';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //crea la carpeta /backup si no existe
        // if (!file_exists(storage_path() . '/app/backup/')) {
        //     mkdir(storage_path() . '/app/backup/', 0777, true);
        // }

        if (!file_exists(storage_path() . '/app/backup/')) {
            $this->error('No existe la carpeta "backup", creando...');
            mkdir(storage_path() . '/app/backup/', 0777, true);
            $this->info('Se ha creado la carpeta con éxito');
        }
        //nombre del archivo
        // ejemplo backup-2021-06-22.sql
        $filename = "backup-" . Carbon::now()->format('d-m-Y') . "-" . Carbon::now()->format('H-i-s') . ".sql";

        // storage_path() devuelve la carpeta "storage" en root
        // se concatena entonces como: /storage/app/backup
        $path = storage_path() . "/app/backup/";

        //credenciales para la conexion a la base de datos
        $usr = env('DB_USERNAME');
        $pass = env('DB_PASSWORD');
        $host = env('DB_HOST');
        $port = env('DB_PORT', 5432);
        $db = env('DB_DATABASE');

        //Preparamos el comando
        //ejemplo de comando:
        //pg_dump --dbname=postgresql://postgres:Postgres@localhost:5432/fashiongo > /storage/app/backup/filename.sql
        $command = 'pg_dump '.$usr.'://' . $usr . ':' . $pass . '@' . $host . ':' . $port . '/' . $db . ' > ' . $path . $filename;;

        //codigo retornado al terminar el comando (default = 0)
        $returnVar = NULL;
        //devuelve los comandos ejecutados en un array
        $output  = NULL;


        if ($command != "") {
            $this->info('Creando backup, por favor espere, puede tomar unos minutos');
            //ejecuta el comando
            exec($command, $output, $returnVar);
        }

        //eliminar el archivo más antiguo antes de env(cantidad_backup) dias si el backup se realizó correctamente
        if ($returnVar == 0) {
            $this->info("Se ha creado exitosamente el archivo: $filename");
            return 0;
        } else {
            $this->error('Ha ocurrido un error, codigo de salida: ' . $returnVar);
            return $returnVar;
        }
    }
}
