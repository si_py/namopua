<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use SebastianBergmann\Environment\Console;

class ClearImgFolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:folder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Limpiar la carpeta de imagenes, comparando con la base de datos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //trae las imagenes cargadas en la base de datos
        $imagenes = DB::table('imagen')->get('path_imagen')->toArray();
        $imagenes = array_column($imagenes, 'path_imagen');

        //escanea la carpeta y trae todas las imagenes cargadas
        $path = public_path('img/productos/');
        $files = scandir($path);
        $files = array_splice($files, 2);


        //primer conteo de imagenes antes de eliminar
        $this->comment('Cantidad de archivos antes: ' . count($files));

        //recorre cada imagen y comprueba que está en la base de datos, si no lo está, lo elimina de la carpeta
        foreach ($files as $file) {
            if (!in_array($file, $imagenes)) {
                File::delete($path . $file);
            }
        }
        //vuelve a escanear la carpeta
        $files = scandir($path);
        $files = array_splice($files, 2);

        //arroja el conteo de imagenes después de eliminar los que no estén en la base de datos
        $this->comment('Cantidad de archivos despues: ' . count($files));
        return 0;
    }
}
