<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class ColorImagen extends Model
{
  protected $table = 'color_imagen';

  protected $primaryKey = 'id_color_imagen';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'id_color_imagen',
    'pk_color_color_imagen',
    'pk_imagen_color_imagen',
    'pk_productos_color_imagen',
    'created_color_imagen', 
    'activo'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  ColorImagen::name($name)
      ->orderBy('id_color_imagen')
      ->paginate($rowsPerPage);
    //->get();
  }

  public function scopeName($query, $name)
  {
    if ($name) {
      return $query->where('id_color_imagen', 'ilike', '%' . $name . '%');
    }
  }

  public static function imagenesProductos($params = [], $offset = null, $take = null)
  {
    $colorImagen = DB::table('color_imagen')
      ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
      ->where('activo_productos', true)
      ->when($params, function ($query, $params) {
        if (count($params) > 0) {
          foreach ($params as $where) {
            $query->where($where['key'], $where['value']);
          }
        }
        return $query;
      })
      ->when($offset, function ($query, $offset) {
        if ($offset != null) {
          $query->offset($offset);
        }
      })
      ->when($take, function ($query, $take) {
        if ($take != null) {
          $query->take($take);
        }
      })
      ->orderBy('id_productos', 'DESC')
      ->orderBy('color_imagen.id_color_imagen', 'ASC');
    return $colorImagen;
  }

  // public function colores(){
  //   return $this->belongsTo(Color::class, 'id_color', 'pk_color_color_imagen');
  // }

  public static function colorImagen()
  {
    $rowsPerPage = 5;
    return DB::table('color_imagen')
      ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      ->where('color_imagen.activo', '=', true)
      ->orderBy('color_imagen.seccion_color_imagen', 'ASC')
      ->orderBy('color_imagen.id_color_imagen', 'ASC')
      // ->orderBy('id_color_imagen', 'DESC')
      ->get();
  }

  public static function colorImagen2()
  {
    $rowsPerPage = 5;
    return DB::table('color_imagen')
      ->join('color', 'color.id_color', 'color_imagen.pk_color_color_imagen')
      ->join('imagen', 'imagen.id_imagen', 'color_imagen.pk_imagen_color_imagen')
      ->join('productos', 'productos.id_productos', 'color_imagen.pk_productos_color_imagen')
      ->orderBy('id_productos', 'DESC')
      ->get();
  }

  public function colores(){
    return $this->belongsTo(Color::class, 'pk_color_color_imagen', 'id_color');
  }
}
