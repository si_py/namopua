<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class DireccionEnvio extends Model
{
    protected $table = 'direccion_envio';

  protected $primaryKey = 'id_direccion_envio';
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'id_direccion_envio',
    'direccion_envio',
    'ciudad_direccion_envio',
    'referencia_direccion_envio',
    'coordenadas_direccion_envio',
    'user_direccion_envio',
    'created_direccion_envio'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  DireccionEnvio::name($name)
        ->orderBy('id_direccion_envio')
  //            ->
  
        ->get();
  }

  public function scopeName($query, $name)
  {
    if($name){
        return $query->where('id_direccion_envio', 'ilike', '%'.$name.'%');
    }
  }

}
