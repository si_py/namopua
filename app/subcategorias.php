<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class subcategorias extends Model
{
  protected $table = 'subcategorias';

  protected $primaryKey = 'id_subcategorias';
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'id_subcategorias',
    'nombre_subcategorias',
    'pk_categorias_subcategorias',
    'active_subcategorias',
    'created_subcategorias'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  subcategorias::name($name)
        ->orderBy('id_subcategorias')
        ->where('active_subcategorias', true)
  //            ->paginate($rowsPerPage);
        ->get();
  }

  public function scopeName($query, $name)
  {
    if($name){
        return $query->where('nombre_subcategorias', 'ilike', '%'.$name.'%');
    }
  }

  public static function getAll()
  {
    return DB::table('subcategorias')
    ->join('categorias', 'categorias.id_categorias', 'subcategorias.pk_categorias_subcategorias')->get();
  }

  // public static function con_productosBk($tipo){
  //   $subcategorias = DB::table('subcategorias')->join('productos', 'productos.pk_subcategorias_productos', 'subcategorias.id_subcategorias')->where('tipo_producto', $tipo)->get()->toArray();
  //   $subcategorias = array_unique(array_column($subcategorias, 'nombre_subcategorias'));
  //   $subcategoriasid = array_unique(array_column($subcategorias, 'id_subcategorias'));
  //   foreach ($subcategoriasid as $subcate) {
  //   }
  //   return $subcategorias;
  // }
  public function con_productos(){
    return $this->hasMany(Productos::class, 'pk_subcategorias_productos', 'id_subcategorias');
  }

  public static function subProdutos()
  {
    return DB::table('subcategorias')
    ->select('nombre_subcategorias', 'tipo_producto', 'id_subcategorias')
    ->distinct()
    ->where('active_subcategorias', true)
    ->join('productos', 'productos.pk_subcategorias_productos', 'subcategorias.id_subcategorias')
    ->orderBy('nombre_subcategorias', 'ASC')
    ->get();
  }
}
