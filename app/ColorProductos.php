<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class ColorProductos extends Model
{
  protected $table = 'color_productos';

  protected $primaryKey = 'id_color_productos';
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'id_color_productos',
    'pk_color_productos',
    'pk_productos_color_productos',
    'created_color_productos'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  ColorProductos::name($name)
        ->orderBy('id_color_productos')
  //            ->paginate($rowsPerPage);
        ->get();
  }

  public function scopeName($query, $name)
  {
    if($name){
        return $query->where('id_color_productos', 'ilike', '%'.$name.'%');
    }
  }

  public static function getAll()
  {
    return DB::table('color_productos')
    ->join('color', 'color.id_color', 'color_productos.pk_color_productos')
    ->get();
  }
}
