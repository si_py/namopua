<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class categorias extends Model
{
    protected $table = 'categorias';

    protected $primaryKey = 'id_categorias';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'id_categorias',
      'nombre_categorias',
      'created_categorias'
    ];

    public static function filterAndPaginatePatients($name)
    {
      $rowsPerPage = 5;

      return  categorias::name($name)
          ->orderBy('id_categorias')
    //            ->paginate($rowsPerPage);
          ->get();
    }

    public function scopeName($query, $name)
    {
      if($name){
          return $query->where('nombre_categorias', 'ilike', '%'.$name.'%');
      }
    }

    public static function getAll()
    {
    return DB::table('categorias')->get();
    }
}
