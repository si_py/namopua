<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Secciones extends Model
{
  protected $table = 'secciones';

  protected $primaryKey = 'id_secciones';
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'id_secciones',
    'nombre_secciones',
    'referencias_secciones',
    'activo_secciones',
    'created_secciones'
  ];

  public static function filterAndPaginatePatients($name)
  {
    $rowsPerPage = 5;

    return  Secciones::name($name)
        ->orderBy('id_secciones')
  //            ->paginate($rowsPerPage);
        ->get();
  }

  public function scopeName($query, $name)
  {
    if($name){
        return $query->where('nombre_secciones', 'ilike', '%'.$name.'%');
    }
  }

  public static function getAll()
  {
  return DB::table('secciones')->get();
  }
}
