<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Empleos extends Model
{
    protected $table = 'empleos';

    protected $primaryKey = 'id';
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'id',
      'nombre'
    ];

    public static function filterAndPaginatePatients($name)
    {
      $rowsPerPage = 5;

      return  Empleos::name($name)
          ->orderBy('id')
    //            ->paginate($rowsPerPage);
          ->get();
    }

    public function scopeName($query, $name)
    {
      if($name){
          return $query->where('nombre', 'ilike', '%'.$name.'%');
      }
    }
}
