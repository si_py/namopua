<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::bind('productos', function($slug){
//   return App\Productos::where('id_productos', $slug)->first();
// });

// Route::get('/', function () {
//     return view('guest.index');
// });

Route::get('/', 'GuestController@index')->name('guest');

Route::resource('/login', 'LoginController');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('/home', 'HomeController@index')->name('home');
//front
Route::resource('/guest', 'GuestController');
//vista de historial de clientes
Route::get('/historial', 'GuestController@historial')->name('guest.historial');
//vista de historial de clientes
Route::get('/micuenta', 'GuestController@micuenta')->name('guest.micuenta');
//vista de historial de clientes
Route::get('/buscarDetallePedido', 'GuestController@buscarDetallePedido')->name('guest.buscarDetallePedido');
//enviar comprobante en historia
Route::post('/agregarComprobacion', 'GuestController@agregarComprobacion')->name('guest.agregarComprobacion');
//autocompletar buscador en header
Route::post('autocompletarInput', 'GuestController@autocompletarInput')->name('guest.autocompletarInput');
Auth::routes();

Route::resource('/shop', 'ShopController');
Route::get('/get-chunk-products', 'ShopController@getChunk')->name('shop.get.chunk');
Route::get('/shop/{id}', 'ShopController@show')->name('shop.producto');

Route::get('/shop/categorias/{id}', 'ShopController@categorias')->name('shop.categorias');

Route::get('/shop/subcategorias/{id}', 'ShopController@subcategorias')->name('shop.subcategorias');

Route::get('/shop/marcas/{id}', 'ShopController@marcas')->name('shop.marcas');

Route::get('/premium', 'ShopController@premium')->name('shop.premium');

Route::get('/lanzamientos', 'ShopController@lanzamientos')->name('shop.lanzamientos');

Route::get('/ofertas', 'ShopController@ofertas')->name('shop.ofertas');

Route::get('/busqueda', 'ShopController@genero')->name('shop.genero');
// Actualizar caja de imagenes en shop productos unitarios
Route::get('BuscarShopGenero/{genero}', 'ShopController@BuscarShopGenero')->name('shop.BuscarShopGenero');
// Actualizar caja de imagenes en shop productos para genero y id_subcategorias
Route::get('BuscarShopGeneroMain/{genero}/{id}', 'ShopController@BuscarShopGeneroMain')->name('shop.BuscarShopGeneroMain');
// Actualizar caja de imagenes en shop productos unitarios
Route::get('busqueda_imagenes/{color}/{productos}', 'ShopController@BuscarImagenes');


//Shop Olimpia
Route::get('/TiendaOlimpia', 'ShopController@tiendaOlimpia')->name('shop.tiendaOlimpia');


//Carrito
Route::get('/cart/show', 'ShopController@sesion');
//agregar item al carrito
Route::get('cart/add/{id}','ShopController@add');
//buscar producto para front.php
Route::get('busquedaProductos','GuestController@buscarProducto')->name('guest.buscarProducto');
//agregar carrito por ajax desde vista de productos
Route::get('agregarCarrito', 'ShopController@agregarCarrito')->name('carrito.agregarCarrito');
//remover carrito por ajax desde vista de productos
Route::get('removerItemCarrito', 'ShopController@removerItemCarrito')->name('carrito.removerItemCarrito');
//vista de contactos
Route::resource('/contactos', 'ContactosController');

Route::resource('/carrito', 'CarritoController');

Route::resource('/caja', 'CajaController');
//realizar pedido
Route::post('/agregarFactura', 'CajaController@agregarFactura')->name('caja.agregarFactura');
//buscar ultima factura
Route::get('/buscarFacturaLast', 'CajaController@buscarFacturaLast')->name('caja.buscarFacturaLast');
//buscar factura para desplegar en vista
Route::get('/buscarFactura', 'CajaController@buscarFactura')->name('caja.buscarFactura');
//agregar direccion de envio
Route::post('/direccionEnvio', 'CajaController@direccionEnvio')->name('caja.direccionEnvio');
//buscar ultima factura
Route::get('/buscarDireccionEnvioLast', 'CajaController@buscarDireccionEnvioLast')->name('caja.buscarDireccionEnvioLast');
//buscar direccion de envio para vista de caja
Route::get('/buscarDireccionEnvio', 'CajaController@buscarDireccionEnvio')->name('caja.buscarDireccionEnvio');
//realizar pedido
Route::post('/realizarPedido', 'CajaController@realizarPedido')->name('caja.realizarPedido');
//realizar pedido
Route::get('/pedidoFinalizado', 'CajaController@pedidoFinalizado')->name('caja.pedidoFinalizado');
//vista terminos y condiciones
Route::get('/terminos_y_condiciones', 'GuestController@terminosCondiciones')->name('guest.terminosCondiciones');
//vista nosotros
Route::get('/nosotros', 'GuestController@nosotros')->name('guest.nosotros');
//vista trabaje con nosotros
Route::get('/trabaje_con_nosotros', 'GuestController@trabajeConNosotros')->name('guest.trabajeConNosotros');
//Busqueda de requisitos de empleo 
Route::get('/buscarEmpleo', 'GuestController@buscarEmpleo')->name('guest.buscarEmpleo');

Route::post('/agregarEmpleoGuest', 'GuestController@agregarEmpleoGuest')->name('guest.agregarEmpleoGuest');

Route::get('exportacion', 'GuestController@vistaExportacion')->name('guest.vistaExportacion');

Route::post('agregarExportacion', 'GuestController@agregarExportacion')->name('guest.agregarExportacion');


Route::group(['middleware' => ['auth']], function () {

  Route::post('/editar-datos', 'GuestController@editar_datos')->name('guest.editar.datos');

  //buscar detalle de pedido
  Route::get('/enviarDetalle', 'HomeController@enviarDetalle')->name('admin.enviarDetalle');
  //buscar comprobante de pedido
  Route::get('/enviarComprobante', 'HomeController@enviarComprobante')->name('admin.enviarComprobante');
  //descargar pdf de detalle
  Route::post('/descargar-pdf', 'HomeController@descargarPDF')->name('admin.descargarDetallesPDF');
  //modificar proceso
  Route::get('/modificarProceso', 'HomeController@modificarProceso')->name('admin.modificarProceso');

  Route::post('logout', 'Auth\LoginController@logout')->name('logout');
  //front
  Route::resource('/usuario', 'UsuarioController');
  //categorias
  Route::resource('/categorias', 'CategoriasController');
  // Actualizar tabla despues de insercion de categorias
  Route::get('tabla_categorias', 'CategoriasController@categoriaBuscar');
  //subcategorias
  Route::resource('/subcategorias', 'SubCategoriasController');
  // Actualizar tabla despues de insercion de subcategorias
  Route::get('tabla_subcategorias', 'SubCategoriasController@subcategoriaBuscar');
  //Marcas
  Route::resource('/marcas', 'MarcasController');
  //Productos
  Route::resource('/productos', 'ProductosController');
  // Actualizar tabla despues de insercion de subcategorias
  Route::get('tabla_marcas', 'MarcasController@MarcasBuscar');

  Route::resource('/color', 'ColorController');
  // Actualizar tabla despues de insercion de subcategorias
  Route::get('tabla_color', 'ColorController@ColorBuscar');

  Route::post('/agregarColor', 'ProductosController@agregarColor')->name('productos.agregarColor');

  Route::resource('/talles', 'TallesController');
  // Actualizar tabla despues de insercion de subcategorias
  Route::get('tabla_talles', 'TallesController@TallesBuscar');

  Route::resource('/secciones', 'SeccionesController');
  // Actualizar tabla despues de insercion de subcategorias
  Route::get('tabla_secciones', 'SeccionesController@SeccionesBuscar');

  Route::resource('/sliders', 'SlidersController');
  //Cargar imagen de producto
  Route::post('image','SlidersController@images')->name('sliders.images');
  //modificar slider
  Route::post('/editar-slider/{id}', 'SlidersController@edit')->name('sliders.edit');
  Route::put('/eliminarSliders/{id}', 'SlidersController@eliminarSliders')->name('sliders.eliminarSliders');

  Route::resource('/contenido', 'ContenidoController');

  Route::get('modificarContenido', 'ContenidoController@modificarContenido')->name('contenido.modificarContenido');

  Route::post('modificarContenidoModal', 'ContenidoController@modificarContenidoModal')->name('contenido.modificarContenidoModal');

  Route::resource('/productos', 'ProductosController');

  Route::post('/productos-imagen', 'ProductosController@subirImagen')->name('productos.imagenes');
  //Borrar imagen
  Route::get('delete/{id}', 'ProductosController@destroy')->name('productos.delete');
  //Borrar color
  Route::get('deleteColor/{id}', 'ProductosController@deleteColor')->name('productos.deleteColor');
  //Borrar talles
  Route::get('/deleteTalles', 'ProductosController@deleteTalles')->name('productos.deleteTalles');


  Route::post('/productos-color', 'ProductosController@subirColor')->name('productos.color');

  Route::post('/productos-talles', 'ProductosController@subirTalles')->name('productos.talles');

  Route::get('/obtener-colores-tallas', 'ProductosController@getTallasColores')->name('productos.getColoresTallas');
  Route::post('/agregarTalla', 'ProductosController@agregarTalla')->name('productos.agregarTalla');

  Route::post('/modificar', 'ProductosController@modificar')->name('productos.modificar');

  Route::post('/modificarExistente', 'ProductosController@modificarExistente')->name('productos.modificarExistente');

  Route::get('pagination_productos', 'ProductosController@tablaProductos')->name('productos.tablaProductos');
  
  Route::get('buscarImagenModal', 'ProductosController@buscarImagen')->name('productos.buscarImagen');

  Route::get('borrarImagenModal', 'ProductosController@borrarImagen')->name('productos.borrarImagen');


  // Ordenar imagenes de productos
  Route::post('ordenarImagenProducto', 'ProductosController@ordenarImagenProducto')->name('productos.ordenarImagenProducto');
  // Habilitar color
  Route::post('habilitarColor', 'ProductosController@habilitarColor')->name('productos.habilitarColor');

  //Modificar Usuario
  Route::post('modificar_usuarioUsuario', 'UsuarioController@modificar_usuarioUsuario')->name('usuario.modificar_usuarioUsuario');

  Route::get('verCurriculum', 'curriculumController@verCurriculum')->name('curriculum.verCurriculum');
  Route::get('eliminarCurriculum', 'curriculumController@eliminarCurriculum')->name('curriculum.eliminar');

  Route::post('agregarEmpleo', 'curriculumController@agregarEmpleo')->name('curriculum.agregarEmpleo');

  Route::post('editarEmpleo', 'curriculumController@editarEmpleo')->name('curriculum.editarEmpleo');

  Route::get('divisas', 'ContenidoController@verDivisas')->name('divisa.verDivisas');

  Route::post('agregarDivisa', 'ContenidoController@agregarDivisa')->name('divisa.agregarDivisa');

  Route::post('modificarrDivisa', 'ContenidoController@modificarrDivisa')->name('divisa.modificarrDivisa');

});
