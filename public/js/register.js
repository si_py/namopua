$(document).ready(function () {
    $("#telefono").on("focus", function (e) {
        $("#error_msg").hide();
    });
    $("#register_form").on("submit", function (e) {
        if (phonenumber($("#telefono").val()) == false) {
            e.preventDefault();
            $("#error_msg").show();
            $("#error_msg strong").text("ingrese un número de teléfono válido");
        }
    });
    function phonenumber(inputtxt) {
        var phoneno3 = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/;
        var phoneno4 = /^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/;
        if (inputtxt.match(phoneno3) || inputtxt.match(phoneno4)) {
            return true;
        } else {
            console.log("Ingrese un número de teléfono válido");
            return false;
        }
    }
});
